/**
 * @fileOverview ./bin/reportComplexityAatSuite.js
 * @description A basic implementation of Plato (https://github.com/es-analysis/plato) to
 * facilitate generation of a complexity analysis report on the TelegrafMoneyApp's
 * Automated Acceptance Test Suite codebase.
 */

var plato = require('plato')
var log = console.log

var codebaseSrc = [
  './features/steps/**',
  './features/support/**
]

var outputDir = './report/complexity/aat'
var config = {
  title: 'TelegrafMoneyAppAatSuite'
}

var postGenerate = function (report) {
  log('Generated complexity report for Automated Acceptance Test Suite!')
}

plato.inspect(codebaseSrc, outputDir, config, postGenerate)

// EOF
