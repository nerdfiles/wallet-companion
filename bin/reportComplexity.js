/**
 * @fileOverview ./bin/reportComplexity.js
 * @description A basic implementation of Plato (https://github.com/es-analysis/plato) to
 * facilitate generation of a complexity analysis report on the TelegrafMoneyApp project
 * front end codebase.
 */

var plato = require('plato')

var log = console.log

var codebaseSrc = [
  './app/scripts/**'
]

var outputDir = '/Users/nerdfiles/Projects/Artifacts/wallet-companion-api/app/public/reports/client-complexity'

var config = {
  title: 'Wallet Companion'
}

var postGenerate = function (report) {
  log('Generated complexity report!')
}

plato.inspect(codebaseSrc, outputDir, config, postGenerate)

// EOF
