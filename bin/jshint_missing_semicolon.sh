#!/bin/bash
# @description
# Find all the semicolon errors after running through an attempted build with 
# gulp. We're essentially grepping over known errors.
# @author nerdfiles
function jshint__missing_semicolon() {
  export CWD=$(pwd);
  # @override Some string that we've inheritied from the reference architecture
  # of the codebase at hand.
  for i in `jshint $CWD | grep "Missing semicolon" | cut -d' ' -f1`;
  do
    for j in `echo $i | sed -e 's/\(.*\)\:/\1/g'`;
    do
      for k in `jshint $j | grep "Missing semicolon" | cut -d' ' -f3`;
      do
        export c=$(echo $k | sed -e 's/\,//g');
        echo "<<<<<<<<<<<<<<$c :: $j";
        sed -i -e $(echo $c,$c)"s/\(\s*\)$/\1;/" $j;
        echo ">>>>>>>>>>>>>>$c :: $j";
      done
    done
  done
}
alias jshint.missing_semicolon=jshint__missing_semicolon;

function jshint__string_must_use_singlequote() {
  export CWD=$(pwd);
  # @override Some string that we've inheritied from the reference architecture
  # of the codebase at hand.
  for i in `jshint $CWD | grep "Strings must use singlequote" | cut -d' ' -f1`;
  do
    for j in `echo $i | sed -e 's/\(.*\)\:/\1/g'`;
    do
      for k in `jshint $j | grep "Strings must use singlequote" | cut -d' ' -f3`;
      do
        export c=$(echo $k | sed -e 's/\,//g');
        echo "<<<<<<<<<<<<<<$c :: $j";
        sed -i -e $(echo $c,$c)"s/\"/'/g" $j;
        echo ">>>>>>>>>>>>>>$c :: $j";
      done
    done
  done
}
