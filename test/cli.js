// ./src/cli.js
/**
 * @description
 */

"use strict";

var __parser__ = function (f) {

  return f.toString().
    replace(/^[^\/]+\/\*!?/, '').
    replace(/\*\/[^\/]+$/, '');
};

var cli = __parser__(function () {/*!
Usage:
  app size
  app fin
  app -h | --help
  app --version
*/});


module.exports = cli;


