/* global browser: false */
'use strict';
// ./test/aat/config.offers.js
/**
 * @fileOverview ./test/aat/config.offers.js
 * @description Test suite for Properties UI (development)
 * @see https://wallet-companion.herokuapp.com/api/v1/offers
 */
var q = require('q');
var FirefoxProfile = require('firefox-profile');

function getFirefoxProfile() {

  var deferred = q.defer();

  var profile = new FirefoxProfile();

  profile.setPreference("geo.prompt.testing", true);
  profile.setPreference("geo.prompt.testing.allow", true);
  profile.setPreference("geo.enabled", true);
  profile.setPreference("geo.wifi.uri", 'data:application/json,{"location": {"lat": 37, "lng": -115}, "accuracy": 100.0}');
  profile.encoded(function(encodedProfile) {
    var multiCapabilities = [{
      browserName: 'firefox',
      firefox_profile : encodedProfile
    }];
    deferred.resolve(multiCapabilities);
  });

  return deferred.promise;
};

const path = require('path');
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'https://localhost:9000/!#/app/login',
  // getMultiCapabilities: getFirefoxProfile,
  capabilities: {
    browserName: 'chrome',
    prefs: {
      'profile.managed_default_content_settings.geolocation': 1
    }
  },
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  specs: [
    '../../features/Offer-Listing-Add-Coin.feature',
    '../../features/Offer-Listing-Coin-Availability.feature',
    '../../features/Offer-Listing-Delete-Coin.feature',
    '../../features/Offer-Listing-Description-Complete.feature',
    '../../features/Offer-Listing-Description-Help.feature',
    '../../features/Offer-Listing-Description.feature',
    '../../features/Offer-Listing-Edit.feature',
    '../../features/Offer-Listing-Location-Assets.feature',
    '../../features/Offer-Listing-Location-Map.feature',
    '../../features/Offer-Listing-Location-Photos-Confirm.feature',
    '../../features/Offer-Listing-Location-Photos.feature',
    '../../features/Offer-Listing-Location.feature',
    '../../features/Offer-Listing-Manage-Coin-Inventory.feature',
    '../../features/Offer-Listing-Preview.feature',
    '../../features/Offer-Listing-Coin-Type.feature',
    '../../features/Offer-Listing-Welcome.feature'
  ],
  cucumberOpts: {
    require  : [
      '../../features/support/*.js',
      '../../features/steps/*.js'
    ],
    tags     : [],
    strict   : true,
    format   : 'json:test/acceptance-test--properties--results.json',
    dryRun   : false,
    compiler : []
  },
  onPrepare: function () {
    browser.manage().window().setSize(320, 568);
  }
};

