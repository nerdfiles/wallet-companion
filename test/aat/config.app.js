/* global browser: false */
// ./test/aat/config.js

const path = require('path');
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'https://localhost:9000/!#/app/login',
  capabilities: {
    browserName: 'chrome',
    prefs: {
      'profile.managed_default_content_settings.geolocation': 1
    }
  },
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  specs: [
    './app.feature'
  ],
  cucumberOpts: {
    require  : [
      '../../features/support/*.js',
      '../../features/steps/*.js'
    ],
    tags     : [],
    strict   : true,
    format   : 'json:test/app-acceptance-test--smoke-test--results.json',
    dryRun   : false,
    compiler : []
  },
  onPrepare: function () {
    browser.manage().window().setSize(320, 480);
  }
};

