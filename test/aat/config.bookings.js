/* global browser: false */
// ./test/aat/config.js
/**
 * @fileOverview ./test/aat/config.bookings.js
 * @description Test suite for Reservations UI (development)
 * "Cook Bookings" are public interactions with a cryptocurrency or appcoin.
 * The ontological predecessors were "Reservations", but all in similar
 * fashion Casinos and Hotels implement the same bookkeeping methods
 * which can be correlated with new categorical formations of capital
 * momenta of capial which can be correlated to translationally
 * invariant "action" spaces with translationally invariant unit vector
 * measured by mechanical probabilistic statistical significance and
 * recorded using Ijiri semantics as {X-impulse, Y-force} microeconomic
 * microdata inputs.
 * @see https://wallet-companion.herokuapp.com/api/v1/reservations
 */
var q = require('q');
var FirefoxProfile = require('firefox-profile');

/**
 * @name getFirefoxProfile
 * @returns {undefined}
 */
function getFirefoxProfile() {

  var deferred = q.defer();
  var profile = new FirefoxProfile();
  profile.setPreference('geo.prompt.testing', true);
  profile.setPreference('geo.prompt.testing.allow', true);
  profile.setPreference('geo.enabled', true);
  profile.setPreference('geo.wifi.uri', 'data:application/json,{"location": {"lat": 37, "lng": -115}, "accuracy": 100.0}');

  profile.encoded(function(encodedProfile) {
    var multiCapabilities = [{
      browserName: 'firefox',
      firefox_profile : encodedProfile
    }];
    deferred.resolve(multiCapabilities);
  });

  return deferred.promise;
}

const path = require('path');

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'https://localhost:9000/!#/app/login',
  // getMultiCapabilities: getFirefoxProfile,
  capabilities: {
    browserName: 'chrome',
    prefs: {
      'profile.managed_default_content_settings.geolocation': 1
    }
  },
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  specs: [
    '../../features/Buyer--Bookings-Listing.feature'
  ],
  cucumberOpts: {
    require  : [
      '../../features/support/*.js',
      '../../features/steps/*.js'
    ],
    tags     : [],
    strict   : true,
    format   : 'json:test/acceptance-test--bookings--results.json',
    dryRun   : false,
    compiler : []
  },
  onPrepare: function () {
    browser.manage().window().setSize(375, 667);
  }
};

