/* global browser: false */
// ./test/aat/config.auth.js
/**
 * @fileOverview ./test/aat/config.user.js
 * @description Test suite for User UI (development)
 * @see https://wallet-companion.herokuapp.com/api/v1/users
 */
var q = require('q');
var FirefoxProfile = require('firefox-profile');

function getFirefoxProfile() {
  var deferred = q.defer();

  var profile = new FirefoxProfile();
  profile.setPreference("geo.prompt.testing", true);
  profile.setPreference("geo.prompt.testing.allow", true);
  profile.setPreference("geo.enabled", true);
  profile.setPreference("geo.wifi.uri", 'data:application/json,{"location": {"lat": 37, "lng": -115}, "accuracy": 100.0}');
  profile.encoded(function(encodedProfile) {
    var multiCapabilities = [{
      browserName: 'firefox',
      firefox_profile : encodedProfile
    }];
    deferred.resolve(multiCapabilities);
  });

  return deferred.promise;
}

const path = require('path');
exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'https://localhost:9000/!#/app/login',
  // getMultiCapabilities: getFirefoxProfile,
  capabilities: {
    browserName: 'chrome',
    prefs: {
      'profile.managed_default_content_settings.geolocation': 1
    }
  },
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  specs: [
    '../../features/Log-In.feature',
    '../../features/Login-Page.feature'
  ],
  cucumberOpts: {
    require  : [
      '../../features/support/*.js',
      '../../features/steps/*.js'
    ],
    tags     : [],
    strict   : true,
    format   : 'json:test/acceptance-test--login--results.json',
    dryRun   : false,
    compiler : []
  },
  onPrepare: function () {
    browser.manage().window().setSize(375, 667);
  }
};

