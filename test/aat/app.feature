# ./test/aat/app.feature
Feature: Init app
  As a developer of Wallet Companion app
  I want to initialize the app

  Scenario: Start app
    Given I am developing the app

  Scenario: User maximizes viewport
    Given I am developing the app
    When I maximize the viewport
