// ./src/scrape.js
/**
 * @description
 */

"use strict";

const Nightmare = require('nightmare');
const nightmare = Nightmare({
  show: true,
  exceptionTimeout: 100000
});
const fs = require('fs');
const pry = require('pryjs');
const R = require('ramda');
const _ = require('lodash');
const colors = require('colors');

const [baseUrl, finUrl] =  'https://localhost:9000/!#/app/login';

Nightmare.action('size', size);

/**
 * size
 *
 * @param done
 * @returns {undefined}
 */
function size (done) {

  this.evaluate_now(function () {

    var cw = document.documentElement.clientWidth;
    var iw = window.innerWidth;
    var ch = document.documentElement.clientHeight;
    var wi = window.innerHeight;
    var w = Math.max(cw, iw || 0);
    var h = Math.max(ch, ih || 0);

    return {
      height : h,
      width  : w
    };

  }, done);

}


//eval(pry.it);

var Scrape = (Scrape)();

/**
 * Scrape
 *
 * @returns {undefined}
 */
function Scrape () {

  var __fin__ = function (term) {
    var internalRef = term;

    /**
     *
     *
     * @returns {undefined}
     */
    nightmare
      .goto(finUrl)
      .wait('body')

      // .screenshot('./test.sec.png')

      // .evaluate(function (s) {
      //   var td = document.querySelectorAll(s);
      //   return Array.prototype.map.call(td, function (elem) {
      //     return elem;
      //   });
    // }, 'td')
       .then()
    return;

    /**
     *
     *
     * @returns {undefined}
     */
    nightmare
      .goto(baseUrl)
      .type('form[action*="/api/v1/user"] [name=q]', term)
      .click('form[action*="/api/v1/user"] [type=submit]')
      .screenshot('./test.api.v1/user.png')

      .evaluate(evElements, '.result__a,.result__snippet')
      .then((href) => {

        var nullList = R.takeWhile(R.isEmpty, href);

        var composedAnchorsList = R.filter(
          R.compose(
            R.flip(R.contains)(['html', 'jquery']),
            R.prop('text')
          ), href);

        var filteredAnchorsList = R.filter(href);

        var hrefMap = href.map(function (p) {
          return p;
        });

        fs.writeFile("./href.map", hrefMap, function (err) {
          if (err) {
            return console.log(err);
          }

          console.log("Scrape complete!".rainbow);
        });

        return nightmare.end();
      })
      .then()

    /**
     * evElements
     *
     * @param selector
     * @returns {undefined}
     */
    function evElements (selector) {
      var selectedElements = document.querySelectorAll(selector);
      return Array.prototype.map.call(selectedElements, function (elem) {
        return elem.text;
      });
    }
  };

  var __size__ = function () {
    nightmare
      .goto(baseUrl)
      .size()
      .then();
  };

  var scraperInterface = {
    fin  : __fin__,
    size : __size__
  };

  return scraperInterface;

}

Scrape.size();

module.exports = Scrape;


