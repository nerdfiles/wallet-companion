// ./test/units/config.js
module.exports = function(config){
  config.set({
  basePath : '../..',
  files : [
    'www/vendor-*.js',
    'bower_components/angular-mocks/angular-mocks.js',
    'app/scripts/**/*.js',
    'app/units/*.js'
  ],

  exclude : [],
  autoWatch : true,
  frameworks: ['jasmine'],
  browsers : ['Chrome'],
  plugins : [
    'karma-junit-reporter',
    'karma-chrome-launcher',
    'karma-firefox-launcher',
    'karma-jasmine'
  ],
  junitReporter : {
    outputFile: 'test/unit/report.xml',
    suite: 'unit'
  }

})}
