# Testing

Karma and Protractor can be leveraged to run TDD and BDD 
respectively. These testing paradigms can be put to use in terms of:

1. Automated Acceptance Testing (BDD)
2. Regression Testing (BDD)
3. Integration Testing (BDD)
4. Unit Testing (TDD)
5. End-to-End Testing (BDD)

Protractor's goal is to test everything that Karma does not. Both can lend 
information to Code Coverage reporting, as they are both automation tools 
for grouping, organizing, and running tests written in either Jasmine or
Cucumber. The tools themselves can be mixed and re-purposed along the lines
of 1-5, so 1-5 must be determined beforehand: _Do we need this kind of test?_

While Protractor is the go-to tool for E2E testing, Cucumber introduces a bit 
of overheard, if considered as a solution to providing E2E tests proper. More 
strictly, Cucumber supports 1-3, not 5; however, it can be utilized for 5, if
the overhead incured is palatable, such that all tests follow the Gherkin
Syntax. Generally, AAT speaks to the gap between QA and Front End Dev, where 
before E2E testing was primary strategy for communication between these teams. 
From this perspective, the Gherkin Syntax is a business-domain language that 
can benefit from the automation capabilities of protractor while being 
high-level enough to peg tested features to an issue tracking or task tracking
system, which might contain 'stories' and 'tasks' to be aligned with `.features` 
files and `scenarios`, respectively.

## Chai

See http://chaijs.com/api/bdd/

## Nock

See http://slides.com/siegfriedehret/nock-and-cucumber/#/5

## Jasmine

```
describe('jasmine expect function', function() {
  it('should match input with expected output', function() {
    expect(true).toBeTruthy();
    expect(false).not.toBeTruthy();
    expect(false).toBeFalsy();
    expect(true).not.toBeFalsy();
    expect({}).toBeDefined();
    expect(undefined).not.toBeDefined();
    expect(null).toBeNull();
    expect(undefined).not.toBeNull();
    expect({}).not.toBeNull();
    expect('test2').toEqual('test2');
    expect('test1').not.toEqual('test2');
    expect('test1').toNotEqual('test3');
    expect([1, 2, 3]).toEqual([1, 2, 3]);
    expect(1).toEqual(1);
    expect({ dog: 1 }).toEqual({ god: 1 });
    expect(1.223).toBeCloseTo(1.22);
    expect(1.233).not.toBeCloseTo(1.22);
    expect(1.23326).toBeCloseTo(1.23324, 3);
    expect([1, 2, 3]).toContain(2);
    expect([1, 2, 3]).not.toContain(4);
    expect('reggie').toMatch(/reggie/i);
    expect('phone: 123-456-6789').toMatch(/\d{3}-\d{3}-\d{4}/);
    expect(2).toBeGreaterThan(1);
    expect(2).toBeLessThan(3);
    expect(object.someMethod).toThrow(new Error("Unexpected error!"));
  });
});

beforeEach(function() {
  this.addMatchers({
    toBeGET: function() {
      var actual = this.actual.method;
      return actual === 'GET';
    },
    toHaveUrl: function(expected) {
      var actual = this.actual.url;
      this.message = function() {
        return "Expected request to have url " + expected + " but was " + actual
      };
      return actual === expected;
    }
  });
});

spyOn(obj, 'method')
expect(obj.method).toHaveBeenCalled()
expect(obj.method).toHaveBeenCalledWith('test1', 'test2')
obj.method.callCount
obj.method.mostRecentCall.args
obj.method.reset()
var dummy = jasmine.createSpy('dummy')
$('button#mybutton').click(dummy)
spyOn(obj, 'method').and.callThrough()
spyOn(obj, 'method').and.return('Pow!')
spyOn(obj, 'method').and.callFake(function() {
  return "data";
});

spyOn(configurations, 'getObjectId').and.callFake(function () {
  switch (arguments[0]) {
    case "object1":
      return 1;
    case "object2":
      return 2;
    default:
      return -1;
   }
});

obj.method.argsForCall
var myFunc = jasmine.createSpyObj('sender', ['send']);
myFunc.send.andCallFake(function() {return ['some', 'mock', 'data'];});

myFunc.send.andCallFake(function(arg1,arg2) {
  return [arg1*2, arg2*3];
});

expect(this.sender.send).toHaveBeenCalledWith("message", any(Function), any(Function));
```

## Protractor

### Navigation

```
browser.get('/#!/app/login'); 
browser.navigate().back();
browser.navigate().forward();
browser.sleep(10000); 
browser.waitForAngular(); 
browser.getLocationAbsUrl() 
browser.wait(element(by.id('some-element-id')).isPresent());
element(by.id('create')).click(); 
```

### Discovery

```
element(by.id('create')).isPresent(); 
element(by.id('create')).isEnabled();
element(by.id('create')).isDisplayed();
element(by.id('user_name'));
element(by.css('#myItem'));
element(by.model('user.firstname')); 
element(by.binding('user.fullname')); 
element(by.textarea('user.extraDetails'));
element(by.buttonText('Save'));
element(by.partialButtonText('Save'));
element(by.linkText('Save'));
element(by.partialLinkText('Save'));
element(by.css('[ng-click="cancel()"]')); 
```

### Nesting

```
var user = element(by.cssContainingText('.user', 'Bob'));
var allOptions = element.all(by.options('listing for listing in list')); 
var list = element.all(by.css('.some--list li'));
var listUsers = element.all(by.repeater('user in data.users'));
var listXpath= element.all(by.xpath('//div'));
expect(list.count()).toBe(3);
expect(list.get(0).getText()).toBe('First');
expect(list.get(1).getText()).toBe('Second');
expect(list.first().getText()).toBe('First');
expect(list.last().getText()).toBe('Last');
``` 

### Forms

```
element(by.id('username')). sendKeys("test@test.com");
sendKeys(protractor.Key.ENTER);
sendKeys(protractor.Key.TAB);
element(by.id('user_name')).clear();
```

### Style

```
element(by.id('item1')).getLocation().then(function(location) {
  var x = location.x;
  var y = location.y;
});
element(by.id('item1')).getSize().then(function(size) {
 var width = size.width;
 var height = size.height;
});
```

## Cucumber

Cucumber supports for callbacks and promises. Cucumber bridges `.feature` files 
and _step definitions_. Gherkin expressions can be matched by strings, regular 
expressions, or strings containing special variables for interpolation; e.g.
`{stringInDoubleQuotes}`.

### Step Definition using Promises

```
module.exports = function() {
  this.Given(/^I go to Dashboard homepage$/, function() {
    browser.get('http://example.com/#/');
    return browser.waitForAngular();
  });

  this.Then(/^I click on the "([^"]*)"$/, function(text) {
    return element(by.css('[href="#/results"]')).click();
  });

  this.Then(/^the results page is displayed$/, () => {
    return browser.get('http://example.com/#/results');
  });
  this.When(/^I click on the "([^"]*)" tab$/, function(text) {
    return element(by.css('[href="#/statistics"]')).click();
  });

  this.Then(/^the statistics page is displayed$/, () => {
    return browser.get('http://example.com/#/statistics');
  });
}
```

### Step Definition using Callbacks

```
module.exports = function() {
  this.Given(/^I go to Dashboard homepage$/, function(done) {
    browser.get('http://example.com/#/');
    browser.waitForAngular().then(done);
  });

  this.Then(/^I click on the "([^"]*)"$/, function(text, done) {
    element(by.css('[href="#/results"]')).click().then(done);
  });

  this.Then(/^the results page is displayed$/, (done) => {
    browser.get('http://example.come/#/results').then(done);
  });

  // from the Gherkin file, where a bit of text is wrapped in quotation marks
  this.When("I click on the {stringInDoubleQuotes} tab", function(string, done) {
    element(by.css('[href="#/content"]')).click().then(done);
  });

  this.Then(/^the content page is displayed$/, (done) => {
    browser.get('http://example.com/#/content').then(done);
  });
}
```
