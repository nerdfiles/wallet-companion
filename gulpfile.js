
var appName = 'WalletCompanion';
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var del = require('del');
var beep = require('beepbeep');
var http = require('http');
var https = require('https');
var fs = require('fs');
var express = require('express');
var ssl = require('express-ssl');
var path = require('path');
var open = require('open');
var stylish = require('jshint-stylish');
var connectLr = require('connect-livereload');
var streamqueue = require('streamqueue');
var runSequence = require('run-sequence');
var merge = require('merge-stream');
var ripple = require('ripple-emulator');
var wiredep = require('wiredep');
var exec = require('child_process').exec;
var replace = require('gulp-replace');
var gutil = require('gulp-util');
var env = require('gulp-env');
var dest = require('gulp-dest');
var traceur = require('gulp-traceur-compiler');
var bump = require('gulp-bump');
var KarmaServer = require('karma').Server;
var fun = require('regex-fun');
var _ = require('lodash');
// @usage eval(pry.it);
var pry = require('pryjs');

require('dotenv').config();

/**
 * @description Parse arguments.
 */
var args = require('yargs')
    .alias('e', 'emulate')
    .alias('b', 'build')
    .alias('r', 'run')
    // remove all debug messages (console.logs, alerts etc) from release build
    .alias('release', 'strip-debug')
    .default('build', false)
    // .default('port', 8443)
    .default('port', 9001)
    .default('strip-debug', false)
    .argv;

var build = !!(args.build || args.emulate || args.run);
var emulate = args.emulate;
var run = args.run;
var port = args.port;
var stripDebug = !!args.stripDebug;
var targetDir = path.resolve(build ? 'www' : '.tmp');

// if we just use emualate or run without specifying platform, we assume iOS
// in this case the value returned from yargs would just be true
if (emulate === true) {
    emulate = 'ios';
}
if (run === true) {
    run = 'ios';
}

// global error handler
var errorHandler = function(error) {
  if (build) {
    throw error;
  } else {
    beep(2, 170);
    plugins.util.log(error);
  }
};

gulp.task('testUnit', testUnit);

gulp.task('appCheck', appCheck)
gulp.task('localEnvCheck', localEnvCheck);
gulp.task('clean', cleanTask);

// precompile .scss and concat with ionic.css
gulp.task('styles', stylesTask);

// build templatecache, copy scripts.
// if build: concat, minsafe, uglify and versionize
gulp.task('scripts', scriptsTask);
gulp.task('keythereum', keythereumTask);
gulp.task('bitcoreLib', bitcoreLibTask);
gulp.task('bitcoreMessage', bitcoreMessageTask);
gulp.task('bitcoreMnemonic', bitcoreMnemonicTask);
gulp.task('cryptojs', cryptojsTask);
gulp.task('cuid', cuidTask);
gulp.task('bignumber', bignumberTask);
gulp.task('web3', web3Task);
gulp.task('ethereumjs', ethereumjsTask);

// copy fonts
gulp.task('fonts', fontsTask);

// generate iconfont
gulp.task('iconfont', iconfontTask);

// copy images
gulp.task('images', imagesTask);

// lint js sources based on .jshintrc ruleset
gulp.task('jsHint', jsHintTask);

// concatenate and minify vendor sources
gulp.task('vendor', vendorTask);

// inject the files in index.html
gulp.task('index', [
  'jsHint',
  'scripts',
  'cuid',
  'cryptojs',
  'bignumber',
  'web3',
  'ethereumjs',
  'bitcoreLib',
  'bitcoreMessage',
  'bitcoreMnemonic',
  'keythereum'
], indexTask);

// start local express server
gulp.task('server_insecure', serveTask);
//gulp.task('server_secure', serveSecureTask);

gulp.task('serve', ['server_insecure']);
//gulp.task('serve', ['server_secure']);

// ripple emulator
gulp.task('ripple', ['scripts', 'styles', 'watchers'], rippleTask);

// start watchers
gulp.task('watchers', watchersTask);

// no-op = empty function
gulp.task('noop', noopTask);

gulp.task('build-release', ['bump-version-major']);
gulp.task('build-rc', ['bump-version-rc']);
gulp.task('build-dev', ['bump-version-minor']);
gulp.task('build-patch', ['bump-version-patch']);

// our main sequence, with some conditional jobs depending on params
gulp.task('default', defaultTask);
gulp.task('develop', defaultTask);

// set debug window width + height
gulp.task('debug_window_size', debug_window_sizeTask);

// run custom debug Chrome window
gulp.task('debug_window', ['debug_window_size'], debug_windowTask);

gulp.task('ngdocs', [], ngdocs);
gulp.task('connect_report', connectReport);
gulp.task('connect_ngdocs', connectNgdocs);

gulp.task('bump-version-major', bumpVersionMajor);
gulp.task('bump-version-minor', bumpVersionMinor);
gulp.task('bump-version-patch', bumpVersionPatch);
gulp.task('bump-version-rc', bumpVersionRc);

// ionic emulate wrapper
gulp.task('ionic:emulate', ionicEmulate);

// ionic run wrapper
gulp.task('ionic:run', ionicRun);

// ionic resources wrapper
gulp.task('icon', icon);

gulp.task('splash', splash);

gulp.task('resources', resources);

// select emulator device
gulp.task('select', select);


/**
 * @name testUnit
 */
function testUnit(done) {
  new KarmaServer({
    configFile : __dirname + '/test/unit/config.js',
    singleRun  : false
  }, done).start();
}

/**
 * @name appCheck
 * @returns {*} undefined
 */
function appCheck(callback) {
  exec('ps ax | grep -e "selenium\\S\\+\\ -port\\ 4444" | cut -d\' \' -f1', function(err, stdout, stderr) {
    var PID = stdout;
    if (PID !== '') {
      exec('protractor ./test/aat/config.app.js', function(err, output, stderr) {
        process.stdout.write(output)
        callback(err)
      })
    } else {
      console.log('Selenium is not running on http://localhost:4444... skipping tests...')
    }
  })
}

/**
 * @name localEnvCheck
 * @description
 * @returns {undefined}
 */
function localEnvCheck(done) {
  var LOCAL, BUILD, OUTCOME;
  if ( process.env['BUILD_TARGET_ENV'] === 'production' ) {
    OUTCOME = 'https://wallet-companion.herokuapp.com/api/v1';
  } else if (process.env['BUILD_TARGET_ENV'] === 'local') {
    OUTCOME = 'https://localhost:3000/api/v1';
  } else {
    OUTCOME = 'https://localhost:3000/api/v1';
  }
  console.log('Using ' + process.env);
  return gulp.src(['config/constants.js'])
    .pipe(replace(/__HOST__/g, OUTCOME))
    .pipe(dest('config/:name.js', { name: 'constants' }))
    .pipe(gulp.dest('app/scripts/config'))
    .on('error', errorHandler);
}

/**
 * @name cleanTask
 * @description clean target dir
 * @param done
 * @returns {undefined}
 */
function cleanTask(done) {
  return del([targetDir], done);
}

/**
 * @name stylesTask
 * @description
 */
function stylesTask() {

  var options = build ? { style: 'compressed' } : { style: 'expanded' };

  var sassStream = gulp.src('app/styles/interface.scss')
    .pipe(plugins.sass(options))
    .on('error', function(err) {
      console.log('err: ', err);
      beep();
    });

  // build ionic css dynamically to support custom themes
  var ionicStream = gulp.src('app/styles/ionic-styles.scss')
    .pipe(plugins.cached('ionic-styles'))
    .pipe(plugins.sass(options))
    // cache and remember ionic .scss in order to cut down re-compile time
    .pipe(plugins.remember('ionic-styles'))
    .on('error', function(err) {
        console.log('err: ', err);
        beep();
      });

  return streamqueue({ objectMode: true }, ionicStream, sassStream)
    .pipe(plugins.autoprefixer('last 1 Chrome version', 'last 3 iOS versions', 'last 3 Android versions'))
    .pipe(plugins.concat('interface.css'))
    .pipe(plugins.if(build, plugins.stripCssComments()))
    .pipe(plugins.if(build && !emulate, plugins.rev()))
    .pipe(gulp.dest(path.join(targetDir, 'styles')))
    .on('error', errorHandler);
}

function keythereumTask() {
  var dest = path.join(targetDir, 'keys');

  var scriptStream = gulp
    .src([
      'keythereum.js'
    ], {
      cwd: 'bower_components/keythereum/dist'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('keythereum.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function bignumberTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'bignumber.js'
    ], {
      cwd: 'bower_components/bignumber.js'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('bignumber.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function web3Task() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'web3.js'
    ], {
      cwd: 'bower_components/web3/dist'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('web3.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function ethereumjsTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'ethereumjs-tx/ethereumjs-tx-1.3.3.js'
    ], {
      cwd: 'bower_components/browser-builds/dist'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('ethereumjs-tx-1.3.3.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}


/**
 * bitcoreLibTask
 *
 * @returns {undefined}
 */
function bitcoreLibTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'bitcore-lib.js'
    ], {
      cwd: 'scripts'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('bitcore-lib.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function bitcoreMessageTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'bitcore-message.js'
    ], {
      cwd: 'scripts'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('bitcore-message.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function bitcoreMnemonicTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'bitcore-mnemonic.js'
    ], {
      cwd: 'scripts'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('bitcore-mnemonic.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}


function cryptojsTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'crypto-js/build/rollups/aes.js'
    ], {
      cwd: 'bower_components'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('aes.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

function cuidTask() {
  var dest = path.join(targetDir, 'lib');

  var scriptStream = gulp
    .src([
      'cuid/dist/browser-cuid.js'
    ], {
      cwd: 'bower_components'
    })
    .pipe(plugins.if(!build, plugins.changed(dest)));

  return streamqueue({ objectMode: true }, scriptStream)
    .pipe(plugins.if(build, plugins.concat('browser-cuid.js')))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}


/**
 * @name scriptsTask
 * @description
 */
function scriptsTask() {
  var dest = path.join(targetDir, 'scripts');

  var minifyConfig = {
    collapseWhitespace: true,
    collapseBooleanAttributes: true,
    removeAttributeQuotes: true,
    removeComments: true
  };

  // prepare angular template cache from html templates
  // (remember to change appName var to desired module name)
  var templateStream = gulp
    .src('**/*.html', { cwd: 'app/templates'})
    .pipe(plugins.angularTemplatecache('templates.js', {
      root: 'templates/',
      module: appName,
      htmlmin: build && minifyConfig
    }));

  var scriptStream = gulp
    .src([
      'templates.js',
      'app.js',
      '**/*.js'
    ], {
      cwd: 'app/scripts'
    })
    .pipe(traceur())
    .pipe(plugins.if(!build, plugins.changed(dest)));

  // const sourcemaps = require('gulp-sourcemaps');
  // const traceur = require('gulp-traceur');
  // const concat = require('gulp-concat');

  // gulp.task('default', () =>
  //   gulp.src('src/*.js')
  //     .pipe(sourcemaps.init())
  //     .pipe(traceur())
  //     .pipe(concat('all.js'))
  //     .pipe(sourcemaps.write('.'))
  //     .pipe(gulp.dest('dist'))
  // );

  return streamqueue({ objectMode: true }, scriptStream, templateStream)
    .pipe(plugins.if(build, plugins.ngAnnotate()))
    .pipe(plugins.if(stripDebug, plugins.stripDebug()))
    .pipe(plugins.if(build, plugins.concat('app.js')))
    //.pipe(plugins.if(build, plugins.uglify()))
    .pipe(plugins.if(build && !emulate, plugins.rev()))
    .pipe(gulp.dest(dest))

    .on('error', errorHandler);
}

/**
 * @name fontsTask
 * @description
 */
function fontsTask() {
  return gulp
    .src(['app/fonts/*.*', 'bower_components/ionic/release/fonts/*.*', 'bower_components/font-awesome/fonts/*.*'])

    .pipe(gulp.dest(path.join(targetDir, 'fonts')))

    .on('error', errorHandler);
}

/**
 * @name iconfontTask
 * @description
 */
function iconfontTask(){
  return gulp.src('app/icons/*.svg', {
        buffer: false
    })
    .pipe(plugins.iconfontCss({
      fontName: 'ownIconFont',
      path: 'app/icons/own-icons-template.css',
      targetPath: '../styles/own-icons.css',
      fontPath: '../fonts/'
    }))
    .pipe(plugins.iconfont({
        fontName: 'ownIconFont'
    }))
    .pipe(gulp.dest(path.join(targetDir, 'fonts')))
    .on('error', errorHandler);
}

/**
 * @name imagesTask
 * @description
 */
function imagesTask() {
  return gulp.src('app/images/**/*.*')
    .pipe(gulp.dest(path.join(targetDir, 'images')))

    .on('error', errorHandler);
}

/**
 * @name jsHintTask
 * @description
 */
function jsHintTask(done) {
  return gulp
    .src('app/scripts/**/*.js')
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter(stylish))
    // .on('data', file)
    .on('error', errorHandler);
    done();

  /**
   * @name file
   * @param {object:Buffer} data File buffer for erroneous file.
   * @returns {*} undefined
   */
  function file(data) {
    _.filter(data, function(f) {
      var errorLabel = 'Missing semicolon';
      var str = f && f[0];
      if (str && _.isFunction(str.replace)) {
        var condition1 = str.replace(/\([^0-9]*\)\([0-9]*\)\(.*$\)/, /($2)/);
        console.log(condition1);
      }
      // var condition2 = str.replace(/\(\s*\)/, ';');
      // return (f.message.indexOf(errorLabel) === -1);
      return true;
    })
  }
}

/**
 * @name vendorTask
 * @description
 */
function vendorTask() {
  var vendorFiles = wiredep().js;

  return gulp.src(vendorFiles)
    .pipe(plugins.concat('vendor.js'))
    // .pipe(plugins.if(build, plugins.uglify()))
    .pipe(plugins.if(build, plugins.rev()))

    .pipe(gulp.dest(targetDir))

    .on('error', errorHandler);
}

/**
 * @name indexTask
 * @description
 */
function indexTask() {

  // build has a '-versionnumber' suffix
  var cssNaming = 'styles/interface*';

  // injects 'src' into index.html at position 'tag'
  var _inject = function(src, tag) {
    return plugins.inject(src, {
      starttag: '<!-- inject:' + tag + ':{{ext}} -->',
      read: false,
      addRootSlash: false
    });
  };

  // get all our javascript sources
  // in development mode, it's better to add each file seperately.
  // it makes debugging easier.
  var _getAllScriptSources = function() {
    var scriptStream = gulp.src(['scripts/app.js', 'scripts/**/*.js'], { cwd: targetDir });
    return streamqueue({ objectMode: true }, scriptStream);
  };

  return gulp.src('app/index.html')
    // inject css
    .pipe(_inject(gulp.src(cssNaming, { cwd: targetDir }), 'app-styles'))

    // inject vendor.js
    .pipe(_inject(gulp.src('vendor*.js', { cwd: targetDir }), 'vendor'))

    // inject bitcore
    .pipe(_inject(gulp.src('lib/bitcore-lib.js', { cwd: targetDir }), 'bitcore-lib'))
    .pipe(_inject(gulp.src('lib/bitcore-message.js', { cwd: targetDir }), 'bitcore-message'))
    .pipe(_inject(gulp.src('lib/bitcore-mnemonic.js', { cwd: targetDir }), 'bitcore-mnemonic'))

    // inject browser-builds--ethereum
    .pipe(_inject(gulp.src('lib/ethereumjs-tx-1.3.3.js', { cwd: targetDir }), 'ethereumjs-tx'))

    // inject bignumber
    .pipe(_inject(gulp.src('lib/bignumber.js', { cwd: targetDir }), 'bignumber'))

    // inject web3
    .pipe(_inject(gulp.src('lib/web3.js', { cwd: targetDir }), 'web3'))

    // inject keythereum
    .pipe(_inject(gulp.src('keys/keythereum.js', { cwd: targetDir }), 'keythereum'))

    // inject cuid
    .pipe(_inject(gulp.src('lib/browser-cuid.js', { cwd: targetDir }), 'cuid'))

    // inject cryptojs
    .pipe(_inject(gulp.src('lib/aes.js', { cwd: targetDir }), 'cryptojs'))

    // inject app.js (build) or all js files individually (dev)
    .pipe(plugins.if(build,
      _inject(gulp.src('scripts/app*.js', { cwd: targetDir }), 'app'),
      _inject(_getAllScriptSources(), 'app')
    ))

    .pipe(gulp.dest(targetDir))
    .on('error', errorHandler);
}

/**
 * @name serveTask
 * @description
 */
function serveTask() {
  var app = express()

  app
    .use(!build ? connectLr() : function(){})
    .use(express.static(targetDir));
    // opens a regular browser window - replace if debug customizations are causing problems
    //open('http://localhost:' + port + '/');

  http
    .createServer(app)
    .listen(port)
}

/**
 * @name serveSecureTask
 * @description
 */
function serveSecureTask() {
  var pkey = fs.readFileSync(path.resolve(__dirname, 'keys', 'server.key'), 'utf8');
  var cert = fs.readFileSync(path.resolve(__dirname, 'keys', 'server.crt'), 'utf8');
  var app = express();

  app
    //.use(ssl())
    .use(!build ? connectLr() : function(){})
    .use(express.static(targetDir));

  https
    .createServer({
      key  : pkey,
      cert : cert
    }, app)
    .listen(port);
}

/**
 * @name ionicEmulate
 * @description
 */
function ionicEmulate() {
  return plugins.shell.task([
    'ionic emulate ' + emulate + ' --livereload --consolelogs'
  ])
}

/**
 * @name ionicRun
 * @description
 */
function ionicRun() {
  return plugins.shell.task([
    'ionic run ' + run
  ]);
}

/**
 * @name icon
 * @description
 */
function icon() {
  return plugins.shell.task([
    'ionic resources --icon'
  ]);
}

/**
 * @name splash
 * @description
 */
function splash() {
  return plugins.shell.task([
    'ionic resources --splash'
  ]);
}

/**
 * @name resources
 * @description
 */
function resources() {
  return plugins.shell.task([
    'ionic resources'
  ]);
}

/**
 * @name select
 * @description
 */
function select() {

  return plugins.shell.task([
    './helpers/emulateios',
    './helpers/emulateandroid'
  ]);
}

/**
 * @name rippleTask
 * @description
 */
function rippleTask() {

  var options = {
    keepAlive: false,
    open: true,
    port: 4400
  };

  // Start the ripple server
  ripple.emulate.start(options);

  open('http://localhost:' + options.port + '?enableripple=true');
}

/**
 * @name watchersTask
 * @description
 */
function watchersTask() {
  plugins.livereload.listen();
  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/fonts/**', ['fonts']);
  gulp.watch('app/icons/**', ['iconfont']);
  gulp.watch('app/images/**', ['images']);
  // gulp.watch('app/scripts/**/*.js', ['index', 'appCheck']);
  // gulp.watch('app/scripts/**/*.js', ['index', 'testUnit']);
  gulp.watch('app/scripts/**/*.js', ['index']);
  gulp.watch('./bower.json', ['vendor']);
  gulp.watch('app/templates/**/*.html', ['index']);
  gulp.watch('app/index.html', ['index']);
  gulp.watch(targetDir + '/**')
    .on('change', plugins.livereload.changed)
    .on('error', errorHandler);
}

/**
 * @name noopTask
 * @description
 */
function noopTask() {
  return function noop() {}
}

/**
 * @name versionBumpErrorHandler
 * @description
 */
function versionBumpErrorHandler() {
  console.log('WARNING: Failed to bump version!');
}

/**
 * @name bumpVersionMajor
 * @description
 */
function bumpVersionMajor() {
  return gulp.src(['./package.json', './config.xml'])
    .pipe(bump({ type: 'major' }).on('error', versionBumpErrorHandler))
    .pipe(gulp.dest('./'));
}

/**
 * @name bumpVersionMinor
 * @description
 */
function bumpVersionMinor() {
  return gulp.src(['./package.json', './config.xml'])
    .pipe(bump({ type: 'minor' }).on('error', versionBumpErrorHandler))
    .pipe(gulp.dest('./'));
}

/**
 * @name bumpVersionPatch
 * @description
 */
function bumpVersionPatch() {
  return gulp.src(['./package.json', './config.xml'])
    .pipe(bump({ type: 'patch' }).on('error', versionBumpErrorHandler))
    .pipe(gulp.dest('./'));
}

/**
 * @name bumpVersionRc
 * @description
 */
function bumpVersionRc() {
  return gulp.src(['./package.json', './config.xml'])
    .pipe(bump({ type: 'prerelease' }).on('error', versionBumpErrorHandler))
    .pipe(gulp.dest('./'));
}

/**
 * @name defaultTask
 * @description
 */
function defaultTask(done) {
  runSequence(
    'clean',
    'iconfont',
    [
      'fonts',
      'styles',
      'images',
      'vendor'
    ],
    'localEnvCheck',
    'index',
    build ? 'noop' : 'watchers',
    build ? 'noop' : 'serve',
    build ? 'noop' : 'debug_window',
    emulate ? ['ionic:emulate', 'watchers'] : 'noop',
    run ? 'ionic:run' : 'noop',
    done);
}

/**
 * @name debug_window_sizeTask
 * @description
 */
function debug_window_sizeTask() {
  if (!!args.deviceSize) {
    gutil.log('\033[1;177m Detected device flag: \033[0m' + args.deviceSize);
    var iOSDeviceSizeW, iOSDeviceSizeH;
    switch (args.deviceSize) {
      case 'iPhone5':
        iOSDeviceSizeW = '320';
        iOSDeviceSizeH = '568';
        break;
      case 'iPhone5@2x':
        iOSDeviceSizeW = '640';
        iOSDeviceSizeH = '1136';
        break;
      case 'iPhone6':
        iOSDeviceSizeW = '375';
        iOSDeviceSizeH = '667';
        break;
      case 'iPhone6@2x':
        iOSDeviceSizeW = '750';
        iOSDeviceSizeH = '1334';
        break;
    }
    env({
      vars: {
        iOSDeviceSizeW: iOSDeviceSizeW,
        iOSDeviceSizeH: iOSDeviceSizeH
      }
    });
  }
  else {
    gutil.log('\033[1;177m Missing or invalid device-size flag detected.\033[0m');
    gutil.log('\033[1;177m Booting into iPhone 5 resolution.\033[0m');
    env({
      vars: {
        iOSDeviceSizeW: '320',
        iOSDeviceSizeH: '568'
      }
    });
  }
  return;
}

/**
 * @name debug_windowTask
 * @description
 */
function debug_windowTask() {
  gutil.log('\033[1;177m Initializing Chrome debugger window... \033[0m');
  return gulp.src('*.js', {read: false})
    .pipe(plugins.shell('./debug.sh'))
    .on('error', function(err) {
      console.log('----- ERROR ------\nError launching debug script! Please ensure the file is present in the same directory as your gulpfile.');
      process.exit();
    })
    .on('end', function() {
      gutil.log('\033[1;177m Debugger window initialized successfully! \033[0m');
      gutil.log('Running on port ' + args.port);
    });
}

/**
 * @name connectNgdocs
 * @description
 */
function connectNgdocs() {
  var connect = require('gulp-connect');
    connect.server({
      root: 'docs',
      livereload: false,
      fallback: 'docs/index.html',
      port: 8083
    });
}

/**
 * @name connectReport
 * @description
 */
function connectReport() {
  var connect = require('gulp-connect');
    connect.server({
      root: 'report/complexity',
      livereload: false,
      fallback: 'report/complexity/index.html',
      port: 8083
    });
}

/**
 * @name ngdocs
 * @description
 */
function ngdocs() {
  var gulpDocs = require('gulp-ngdocs');
  var options = {
    scripts: ['app/scripts/**/*.js'],
    html5Mode: true,
    startPage: '/api',
    title: "BPCMobileApp",
    image: "",
    imageLink: "",
    titleLink: "/api"
  }
  return gulp.src('app/scripts/**/*.js')
    .pipe(gulpDocs.process(options))
    .pipe(gulp.dest('./docs'));
}

