var dataSet = {}

function normalize() {
}

function deduplicate() {
}

function integrity_violation_check() {
}

function discern() {
}

function classify() {
}

module.exports = function(schema) {
  var _schema = schema
  return function(model) {
    normalize()
    deduplicate()
    integrity_violation_check()
    discern()
    classify()
    return dataSet
  }
}
