
// const extract = require('./extract')
// const transform = require('./transform')
// const load = require('./load')

//var extracted = extract('./.idea')

const fs = require('fs')
const pos = require('pos');
const moment = require('moment')
const print = console.log
const table = console.table


/**
 * @name extractor
 * @description Load a file with tables provided, then generate
 * anticipation/prediction statements to state for-X/by-Y relations to develop
 * feature modules based on non-normative matrix of key performative anchorings.
 */
function extractor() {

  var fp = './.idea'

  fs.readFile(fp, 'utf-8', function(error, response) {

    var data

    try {
      data = response
    } catch(e) {
      console.log(e)
    }

    var s = data.split('\n')
	var descriptions = []
	var maxDays = 30
	var today = moment()

	var nonNormativeValueTable = {
		'1': '12 days',
		'2': '3 days',
		'0': '7 days',
		'8': '10 days',
		'5': '4 days',
		'25': '12 days',
		'16': '14 days'
	}

	for (var i = 0; i < s.length; i++) {
	  let sent = s[i].split('   ')
	  if (sent) {
		var x = sent[1]
		var y = sent[26]
		var p, _p, q
		if (x && y) {
		  p = nonNormativeValueTable[parseInt(y)]
		  if (p) {
			_p = p.split(' ')
			q = {
			  u : _p[1],
			  v : parseInt(_p[0])
			}
			var daye = moment()
			  .add(q.u, q.v)
			  .format('MM/DD/YYYY')
		    var prediction = [
			  'For', 
			  x.slice(1), 
			  'by', 
			  daye
			].join(' ')
		    //prediction = 'For ' + x + ' by ' + (y / 30) * 10
		    print(prediction)
 		  }	
		}
	  }

	  /*
	  let words = new pos.Lexer().lex(sent);
	  let tags = new pos.Tagger()
	    .tag(words)
	    .map(function(tag) {
		  return tag[0] + '/' + tag[1];
		})
        .join(' ');
	  */

	  //descriptions.push(tags)
	}

	//console.log(descriptions)
  })
}

extractor()

/*
Promise.all([
  extracted,
  transform('./schema.prepare')('model'),
  load()
]).then(completed, failed)
  .catch(captured)
*/

///////////////

/**
 */
function completed() {
}

/**
 */
function failed() {
}

/**
 */
function captured() {
}


