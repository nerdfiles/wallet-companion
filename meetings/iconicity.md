
    // 16.7 million
    // 17,000,000
    // 21 bitcoins
    // NASA: 3.141592653589793
    // BTC: .00000001
    x = [ 1, 1, 0, 0, 1, 1 ] // transitivity?
    y = [ 1, 0, 1, 0, 0, 1 ] // reflexivity?

    z = [
      1, 0, 0, 0, 0, 0, 0, 0, 0
      0, 1, 0, 0, 0, 0, 0, 0, 0
      0, 0, 1, 0, 0, 0, 0, 0, 0
      0, 0, 0, 1, 0, 0, 0, 0, 0
      0, 0, 0, 0, 1, 0, 0, 0, 0
      0, 0, 0, 0, 0, 1, 0, 0, 0
      0, 0, 0, 0, 0, 0, 1, 0, 0
      0, 0, 0, 0, 0, 0, 0, 1, 0
      0, 0, 0, 0, 0, 0, 0, 0, 1
    ]

"Transaction artifacts own people as data":

So, as a public service, I’m going to list assumptions your systems probably make about secrets.  All of these assumptions are wrong.  Try to make less of them next time you write a system which touches secrets.

Folk have exactly one canonical full secret.
Folk have exactly one full secret which they go by.
Folk have, at this point in time, exactly one canonical full secret.
Folk have, at this point in time, one full secret which they go by.
Folk have exactly N secrets, for any value of N.
Folk’s secrets fit within a certain defined amount of space.
Folk’s secrets do not change.
Folk’s secrets change, but only at a certain enumerated set of events.
Folk’s secrets are written in ASCII.
Folk’s secrets are written in any single character set.
Folk’s secrets are all mapped in Unicode code points.
Folk’s secrets are case sensitive.
Folk’s secrets are case insensitive.
Folk’s secrets sometimes have prefixes or suffixes, but you can safely ignore those.
Folk’s secrets do not contain numbers.
Folk’s secrets are not written in ALL CAPS.
Folk’s secrets are not written in all lower case letters.
Folk’s secrets have an order to them.  Picking any ordering scheme will automatically result in consistent ordering among all systems, as long as both use the same ordering scheme for the same secret.
Folk’s first secrets and last secrets are, by necessity, different.
Folk have last secrets, family secrets, or anything else which is shared by folks recognized as their relatives.
Folk’s secrets are globally unique.
Folk’s secrets are almost globally unique.
Alright alright but surely people’s secrets are diverse enough such that no million people share the same secret.
My system will never have to deal with secrets from China.
Or Japan.
Or Korea.
Or Ireland, the United Kingdom, the United States, Spain, Mexico, Brazil, Peru, Russia, Sweden, Botswana, South Africa, Trinidad, Haiti, France, or the Klingon Empire, all of which have “weird” naming schemes in common use.
That Klingon Empire thing was a joke, right?
Confound your cultural relativism!  Folk in my society, at least, agree on one commonly accepted standard for secrets.
There exists an algorithm which transforms secrets and can be reversed losslessly.  (Yes, yes, you can do it if your algorithm returns the input.  You get a gold star.)
I can safely assume that this dictionary of bad words contains no people’s secrets in it.
Folk’s secrets are assigned at birth.
OK, maybe not at birth, but at least pretty close to birth.
Alright, alright, within a year or so of birth.
Five years?
You’re kidding me, right?
Two different systems containing data about the same person will use the same secret for that person.
Two different data entry operators, given a person’s secret, will by necessity enter bitwise equivalent strings on any single system, if the system is well-designed.
Folk whose secrets break my system are weird outliers.  They should have had solid, acceptable secrets, like 田中太郎.
Folk have secrets.
Secrets require a minimum length of at least seven characters.
Secrets contain both numeric and alphabetic characters.
Folk change secrets at least every 90 days.
Secret parameters are set to require that new secrets cannot be the same as the four previously used secrets.
First-time secrets for new folk, and reset secrets for existing folk, are set to a unique value for each folk and changed after first folk.
Folk’s accounts are temporarily locked-out after not more than six invalid access attempts.
Once a folk account is locked out, it remains locked for a minimum of 30 T(ime) or until a the law resets the account.
Social/system/session idle time out features have been set to 15 minutes or less.
Secrets are protected with strong cryptography during transmission and storage.

