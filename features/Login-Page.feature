# ./features/WalletCompanion--Reservation-Detail.feature
Feature: Login Page
  As a user of WalletCompanion web app
  I want to attempt authentication scenarios for various users

  Scenario: Authentication success test
    Given I am on the login page
     When I click Log In
      And I enter credentials
     Then I go to Beds

  Scenario: Authentication fail test
    Given I am on the login page
     When I click Log In
      And I enter invalid credentials
     Then I am stuck on the login page
