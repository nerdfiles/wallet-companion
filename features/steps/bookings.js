/* global expect: false, protractor: false, browser: false, by: false, element: false, $: false */
// ./features/steps/authentication.js
'use strict'
const backend = require('../support/backend-mock')
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const protractor = require('protractor')
chai.use(chaiAsPromised)
const expect = chai.expect
const {defineSupportCode} = require('cucumber')
var EC=protractor.ExpectedConditions

defineSupportCode(({Given, Then, When}) => {
  When('I enter username {stringInDoubleQuotes}', WhenEnterUsername)
  When('I enter password {stringInDoubleQuotes}', WhenEnterPassword)
  When('I select a property', WhenSelectProperty)
  When('I submit my credentials', { timeout: 10000 }, WhenSubmitCredentials)
  When('I filter by Bed Type', { timeout: 30000 }, WhenSelectFiltersByBedType)
  Then('I book a bed', ThenBookBed)
  Then('I apply my filter', ThenApplyMyFilter)

  function WhenEnterUsername(entry) {
    var username = element(by.css('input[name="username"]'))
    username.clear()
    return username.sendKeys(entry)
  }

  function WhenEnterPassword(entry) {
    var password = element(by.css('input[name="password"]'))
    password.clear()
    return password.sendKeys(entry)
  }

  function WhenSubmitCredentials() {
    var btn = element(by.css('button[type="submit"]'))
    return btn.click().then(function() {
      return browser.waitForAngular()
    })
  }

  function WhenSelectProperty() {
    var listing = element(by.css('div.listing'))
    return listing.click().then(function() {
      return browser.waitForAngular()
    })
  }

  function WhenSelectFiltersByBedType(done) {
    var context = this
    browser.sleep(5000)

    var filterBtn = element(by.xpath('//*[@id="beds"]/ion-header-bar/button'))
    browser.wait(EC.presenceOf(filterBtn), 15000)
    filterBtn.click().then(function() {
      browser.waitForAngular()
      var bedTypeHeaderBtn = element(by.css('#bed-type-selection'))
      context.testScreen('bs')
      browser.wait(EC.presenceOf(bedTypeHeaderBtn), 15000)
      bedTypeHeaderBtn.click()
      var optPrivateRoom = element(by.cssContainingText('#filters > div.block.option-block > div.bed-types > div:nth-child(1) > span', 'Single (Private Room)'))
      browser.wait(EC.presenceOf(optPrivateRoom), 15000)
      optPrivateRoom.getText().then(function(content) {
        console.log(content)
      })
      optPrivateRoom.click().then(done)
    })
  }

  function ThenApplyMyFilter() {
    console.log('Apply filter...')
    return browser.waitForAngular()
    // var submitFilterBtn = element(by.css('button.submit-button'))
    // return submitFilterBtn.click().then(function() {
    //   var filterTitle = element(by.xpath('/html/body/div[5]/div[2]/div/ion-header-bar/h1'))
    //   expect(filterTitle.isDisplayed()).to.be.false
    //   return browser.waitForAngular()
    // })
  }

  function ThenBookBed() {
    var btn = element(by.css('button'))
    return btn.click().then(function() {
      return browser.waitForAngular()
    })
  }
})


