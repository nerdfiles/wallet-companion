/* jshint asi: true */
/* global expect: false, protractor: false, browser: false, by: false, element: false, $: false */
// ./features/steps/app.js

'use strict';

const {defineSupportCode} = require('cucumber');

let testSuite = ({Given, When, Then}) => {

  Given(/^I am developing the app$/, { timeout : 10000 }, GivenApp);
  When(/^I maximize the viewport$/, { timeout: 10000 }, WhenMaximize);

  ///////////////

  /**
   * @name WhenMaximize
   * @returns {Object}
   */
  function WhenMaximize() {

    browser.waitForAngular();

    browser.driver.manage().window().maximize();

    return browser.wait(function() {
      return element(by.css('#login')).isPresent().then(function(isPresent) {
        console.log('Found #login modal window?: ', isPresent);
        return isPresent;
      })
    }, 5000);

  }

  /**
   * @name GivenApp
   * @returns {Object}
   */
  function GivenApp() {

    return browser.get('/#!/app/login');

  }
}

defineSupportCode(testSuite);
