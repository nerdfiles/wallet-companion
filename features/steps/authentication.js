/* global expect: false, protractor: false, browser: false, by: false, element: false, $: false */
// ./features/steps/authentication.js
'use strict'
const backend = require('../support/backend-mock')
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const protractor = require('protractor')
chai.use(chaiAsPromised)
const expect = chai.expect
const {defineSupportCode} = require('cucumber')
const pry = require('pryjs')

defineSupportCode(({Given, Then, When}) => {

  var adminEmail = ''
  var adminPassword = ''
  var ownerEmail = 'test1@gmail.com'
  var ownerPassword = 'barging scourger capriole cowherd'
  var travellerEmail = 'test2@gmail.com'
  var travellerPassword = 'barging scourger capriole cowherd'

  var failEmail = 'teapot1@supersupersupersupersuper.com'
  var failPassword = 'superteapot'

  var credentials = {
    'traveller': {
      username: travellerEmail,
      password: travellerPassword
    },
    'owner': {
      username: ownerEmail,
      password: ownerPassword
    },
    'admin': {
      username: adminEmail,
      password: adminPassword
    }
  }

  Given('I am a {stringInDoubleQuotes}', { timeout: 20000 }, GivenIAm)
  Given('I am on the login page', { timeout: 20000 }, GivenLoginPage)
  When('I click Log In', { timeout: 10000 }, WhenLogin)
  When('I enter credentials', { timeout: 10000 }, WhenValidEntry)
  When('I enter invalid credentials', { timeout: 10000 }, WhenInvalidEntry)
  Then('I go to Beds', { timeout: 10000 }, GoToBedsOrProperties)
  Then('I am stuck on the login page', ThenInvalidEntry)

  ///////////////

  /**
   * @name ThenInvalidEntry
   */
  function ThenInvalidEntry() {
    console.log('Invalid login credentials')
  }

  /**
   * @name GivenIAm
   * @param {string} type User type
   * @returns {object} Event object
   */
  function GivenIAm(type) {
    var authenticationData = credentials[type]
    browser.get('/#!/app/login')
    browser.waitForAngular()
    var loginBtn = element(by.xpath('/html/body/div[3]/div[2]/div/div[2]/div/button[2]'))
    return loginBtn.getText().then((content) => {
      expect(content).to.equal('Log In')
      loginBtn.sendKeys(protractor.Key.ENTER)
      browser.waitForAngular()
      var username = element(by.css('input[name="username"]'))
      username.clear()
      username.sendKeys(authenticationData.username) // 'test1@gmail.com' is a bed owner account
      var password = element(by.css('input[name="password"]'))
      password.clear()
      password.sendKeys(authenticationData.password)
      var btn = element(by.xpath('//*[@id="login"]/ion-content/div[1]/div/login/div/form/div[3]/button'))
      return btn.click().then(function() {
        // @IDEA backend can be used for testing units
        // backend.pseudoLogin(travellerEmail, travellerEmail)
        return browser.waitForAngular()
      })
    })
  }

  /**
   * @name GivenLoginPage
   */
  function GivenLoginPage() {
    // eval(pry.it)
    browser.get('/#!/app/login')
    return browser.waitForAngular()
  }

  /**
   * @name WhenLogin
   */
  function WhenLogin() {
    var loginBtn = element(by.xpath('/html/body/div[3]/div[2]/div/div[2]/div/button[2]'))
    return loginBtn.getText().then((content) => {
      expect(content).to.equal('Log In')
      return loginBtn.sendKeys(protractor.Key.ENTER)
    })
  }

  /**
   * @name WhenInvalidEntry
   */
  function WhenInvalidEntry() {
    var username = element(by.css('input[name="email"]'))
    username.clear()
    username.sendKeys(failEmail)
    var password = element(by.css('input[name="password"]'))
    password.clear()
    password.sendKeys(failPassword)
    var btn = element(by.xpath('//*[@id="login"]/ion-content/div[1]/div/login/div/form/div[3]/button'))
    return btn.click().then(function() {
      browser.getCurrentUrl().then(function(content) {
        expect(content).to.have.string('login')
      })
      return browser.waitForAngular()
    })
  }

  /**
   * @name WhenValidEntry
   */
  function WhenValidEntry() {
    var username = element(by.css('input[name="username"]'))
    username.clear()
    username.sendKeys(travellerEmail) // 'test1@gmail.com' is a bed owner account
    var password = element(by.css('input[name="password"]'))
    password.clear()
    password.sendKeys(travellerPassword)
    var btn = element(by.xpath('//*[@id="login"]/ion-content/div[1]/div/login/div/form/div[3]/button'))
    return btn.click().then(function() {
      // @IDEA backend can be used for testing units
      // backend.pseudoLogin(travellerEmail, travellerPassword)
      return browser.waitForAngular()
    })
  }

  /**
   * @name GoToBedsOrProperties
   * @description State from previous clause is preserved, and can be reused
   */
  function GoToBedsOrProperties() {
    browser.getTitle().then(function(content) {
      // console.log('Form submit successful')
      expect(content).to.equal('tgm')
    })
    return browser.getCurrentUrl().then(function(content) {
      // console.log('Login successful')
      expect(content).not.to.be.undefined
      return browser.waitForAngular()
    })
  }

})

