/* global expect: false, protractor: false, browser: false, by: false, element: false, $: false */
// ./features/steps/bookings--listing.js
'use strict'
const backend = require('../support/backend-mock')
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
const protractor = require('protractor')
chai.use(chaiAsPromised)
const expect = chai.expect
const {defineSupportCode} = require('cucumber')
var EC=protractor.ExpectedConditions

defineSupportCode(({Given, Then, When}) => {
  When('I view my Bookings', { timeout: 10000 },WhenSelectBookings)
  Then('I see a Listing', { timeout: 10000 }, ThenViewListing)

  /**
   * @name WhenSelectBookings
   * @returns {*} undefined
   */
  function WhenSelectBookings() {
    // Select the relevant menu item for the given user type
    var menuItem = element(by.css('[ui-sref="app.bookings"]'));
    var listing = element(by.xpath('//*[@id="beds"]/ion-content/div[1]/div/div/div[4]'))
    browser.wait(EC.presenceOf(menuItem), 5000)
    browser.wait(EC.presenceOf(listing), 5000)
    return menuItem.click().then(function() {
      return browser.waitForAngular()
    })
  }

  /**
   * @name ThenViewListing
   * @returns {*} undefined
   */
  function ThenViewListing(done) {
    var listingItems = element.all(by.css('.list--listing'))
    browser.wait(EC.presenceOf(listingItems.first()))
    listingItems.first().getText().then(function(content) {
      expect(content).to.not.equal('')
      listingItems.first().click().then(function() {
        done()
      })
    })
  }
})


