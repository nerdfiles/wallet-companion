/* global asi: true, expect: false, protractor: false, browser: false, by: false, element: false, $: false */
/**
 * @fileOverview ./features/steps/forgot-password.js
 * @description Forgot Password generally is a cold-load condition wherein the
 * user is not interested in a particular page, but they are in fact
 * interested in leveraging a fall-back state of the overall application, in
 * hopes of being directed to the right place. Generally, they will navigate
 * to "Forgot Password" from the homepage, because this is where it is
 * customarily loaded, or on the login page. But the app is being called on to
 * access its login page, so there is no assumption that the login page is
 * known, with the given test.
 */

'use strict';

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const expect = chai.expect;
const {defineSupportCode} = require('cucumber');
var colors = require('colors');
var EC = protractor.ExpectedConditions;

let testSuite = ({Given, Then, When}) => {

  When(/^I navigate to the forgot password page$/, WhenForgotPassword);
  When(/^I click on Forgot Password\?$/, WhenResetPassword);
  Then(/^I should see a small form asking to "([^"]*)"$/, ThenResetPassword);

  ////////////

  /**
   * @name WhenForgotPassword
   * @param done
   * @returns {undefined}
   */
  function WhenForgotPassword(done) {

    var input = {}

    var selector = '.login-modal button.login'

    var _selector = element.all(by.css(selector))

    browser.waitForAngular()

    browser.wait(EC.presenceOf(_selector), 5000)

    element(by.css(selector))
      .click()
        .then(() => {
          console.log('Found element: ', input)
          done()
        })
  }

  /**
   * @name WhenResetPassword
   * @param {function} done Callback
   * @returns {undefined}
   */
  function WhenResetPassword(done) {

    var input = {}

    var selector = '#loginForm > span[href]'

    // why does this span have an href and not just a hyperlink/anchor? the
    // avoidance of standards is total. (averting the necessity of style for
    // anchor pseudostates like :hover, etc. by institutionalizing
    // wrongsemantics)
    var _selector = element.all(by.css(selector))

    browser.wait(EC.presenceOf(_selector), 5000)

    element(by.css(selector))
      .click()
        .then(() => {
          console.log('Found element: ', input)
          done()
        })
  }

  /**
   * @name ThenResetPassword
   * @param {function} done Callback
   * @param {string} text Label on element.
   */
  function ThenResetPassword(text, done) {

    var selector = '#forgot-password > ion-content > div > form > p';

    var _selector = element(by.css(selector))

    browser.wait(EC.presenceOf(_selector), 5000)

    element(by.css(selector))
      .getText()
        .then((outcome) => {
          console.log('Actual: ', text.rainbow)
          console.log('Expected: ', outcome.trap)
          expect(outcome).to.equal(text)
          done()
        })

  }

};

defineSupportCode(testSuite);

