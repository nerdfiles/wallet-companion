// ./features/steps/scratch.js
/* global expect: false, protractor: false, browser: false, by: false, element: false, $: false */
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect
const {defineSupportCode} = require('cucumber')
var EC=protractor.ExpectedConditions

defineSupportCode(({Given, Then, When}) => {

  Given(/^I am on the BPC web app Home page$/, { timeout: 10000 }, GivenHomePage)
  Given('I am on the BPC web app Bookings mobile page', GivenBookingsPage)
  Given('I am on the BPC web app Listings Create mobile page', GivenListingsCreate)

  When('I have a username', { timeout: 20000 }, WhenUsername)
  When('I click on {stringInDoubleQuotes}', { timeout: 20000 }, WhenCreateAccount)
  When('I accept {stringInDoubleQuotes}', { timeout: 20000 }, WhenAccept)

  Then('I confirm user account', { timeout: 20000 }, ThenConfirmUserAccount)
  Then('I see a {stringInDoubleQuotes} Account Type option', ThenChooseAccountType)
  Then('I see the Traveller Account Creation option', ThenChooseTravellerAccountType)
  Then('I see CREATE ACCOUNT', { timeout: 20000 }, ThenCreateAccount)
  Then('I create a password', { timeout: 30000 }, ThenCreatePassword)
  Then('I skip to beds', ThenSkip)
  Then('I add payment method', { timeout: 10000 }, ThenAddPaymentMethod)
  Then('I fill in my info', { timeout: 10000 }, ThenFill)
  Then('I add billing info', { timeout: 10000 }, ThenBilling)
  Then('I browse the app', { timeout: 10000 }, ThenBrowse)
  Then('I load beds', { timeout: 30000 }, ThenLoadBeds)

  ///////////////

  /**
   * @name ThenBilling
   * @returns {undefined}
   */
  function ThenBilling() {
    var line1 = element.all(by.xpath('//*[@id="line1"]'))
    var line2 = element.all(by.xpath('//*[@id="line2"]'))
    var city = element.all(by.xpath('//*[@id="city"]'))
    var state = element.all(by.xpath('//*[@id="state"]'))
    var stateOption = element(by.cssContainingText('option', 'Ontario'))
    var countryOption = element(by.cssContainingText('option', 'CANADA'))

    var stateSelector = '//*[@id="defaultStateSelect"]/option[10]'
    var stateOptionElement = element(by.xpath(stateSelector))

    var countrySelector = '//*[@id="country"]/option[41]'
    var countryOptionElement = element(by.xpath(countrySelector))

    line1.clear()
    line1.sendKeys('12345 Street')

    line2.clear()
    line2.sendKeys('100')

    city.clear()
    city.sendKeys('Toronto')

    state.clear()
    state.sendKeys('Ontario')

    var countrySelector = 'document.getElementById("country").scrollIntoView();'
    browser.driver
      .executeScript(countrySelector)

    browser.wait(EC.presenceOf(stateOptionElement), 5000)
    stateOption.click()

    browser.wait(EC.presenceOf(countryOptionElement), 5000)
    countryOption.click()

    var confirmBillingLocation = element.all(by.xpath('//*[@id="confirm-billing-location"]'))

    return confirmBillingLocation.get(0)
      .click()
      .then(function() {
        return browser.waitForAngular()
      })
  }

  /**
   * @name ThenFill
   * @returns {undefined}
   */
  function ThenFill() {

    // Elements to scroll into view
    var confirmPaymentMethodButton = "document.getElementById('confirm-payment-method').scrollIntoView();"
    var firstnameInput = "document.getElementById('firstname').scrollIntoView();"
    var cardInput = "document.getElementById('cardNumber').scrollIntoView();"

    browser.driver
      .executeScript(firstnameInput)

    var fn = element.all(by.xpath('//*[@id="firstname"]'))
    var ln = element.all(by.xpath('//*[@id="lastname"]'))
    var cn = element.all(by.xpath('//*[@id="cardNumber"]'))
    // var cem = element.all(by.xpath('//*[@id="cardExpiryMonth"]'))
    var cem = element(by.cssContainingText('option', '3 - March'))
    // var cey = element.all(by.xpath('//*[@id="cardExpiryYear"]'))
    var cey = element(by.cssContainingText('option', '2020'))
    var cvc = element.all(by.xpath('//*[@id="cardCvc"]'))
    var cpc = element.all(by.xpath('//*[@id="cardPostalCode"]'))

    fn.clear()
    fn.sendKeys('First1')

    ln.clear()
    ln.sendKeys('Last1')

    browser.driver
      .executeScript(cardInput)

    cn.clear()
    cn.sendKeys('4242424242424242')

    cem.click()

    cey.click()

    cvc.clear()
    cvc.sendKeys('999')

    cpc.clear()
    cpc.sendKeys('M1S 7U8')

    browser.driver
      .executeScript(confirmPaymentMethodButton)

    var confirmPaymentMethod = element.all(by.xpath('//*[@id="confirm-payment-method"]'))

    return confirmPaymentMethod.get(0).click().then(function() {
      return browser.waitForAngular()
    })
  }

  /**
   * @name ThenAddPaymentMethod
   * @returns {undefined}
   */
  function ThenAddPaymentMethod() {
    var pay = element.all(by.css('.addCardToUser'))
    var elm = pay.get(0)
    return elm.click().then(function() {
      return browser.waitForAngular()
    })
  }

  /**
   * @name ThenSkip
   * @returns {undefined}
   */
  function ThenSkip() {
    var skip = element.all(by.css('.cancel'))
    var elm = skip.get(0)
    return elm.click().then(function() {
      return browser.waitForAngular()
    })

  }

  /**
   * @name ThenChooseTravellerAccountType
   * @returns {undefined}
   */
  function ThenChooseTravellerAccountType() {
    var pseudoBtnTraveller = element.all(by.xpath('//*[@id="create"]/ion-content/div[1]/div/account-type/div/div[2]/div/div/span[1]'))
    var pseudoBtnTravellerLabel = pseudoBtnTraveller.get(0).getText()

    return pseudoBtnTravellerLabel.then((content) => {

      expect(content).to.not.equal('')
      expect(content).to.equal('I am a Traveller')

      return pseudoBtnTraveller.click().then(function() {

        return browser.waitForAngular()
      })
    })
  }

  /**
   * @function generateEmail
   * @returns {undefined}
   * @inner
   */
  function generateEmail() {

    var r = Math.ceil(Math.random() * 100000)

    return 'test_email_' + r.toString() + '@example.com'
  }

  /**
   * @name ThenCreatePassword
   * @returns {undefined}
   */
  function ThenCreatePassword() {

    browser.sleep(1000)
    var username = element.all(by.xpath('//*[@id="email"]'))
    var password = element.all(by.xpath('//*[@id="password"]'))
    var confirmPassword = element.all(by.xpath('//*[@id="confirmPassword"]'))

    username.clear()
    username.sendKeys(generateEmail())

    password.clear()
    password.sendKeys('123456')

    confirmPassword.clear()
    confirmPassword.sendKeys('123456')

    browser.driver.executeScript("document.getElementById('create-traveller').scrollIntoView();")
    var ct = element.all(by.xpath('//*[@id="create-traveller"]'))
    var elm = ct.get(0)

    return elm.click()
      .then(function() {

        // Sleeping to wait for HTTP response
        browser.sleep(4000)
        return browser.waitForAngular()
      })
  }

  /**
   * @name ThenCreateAccount
   * @returns {undefined}
   */
  function ThenCreateAccount() {

    // var btn = element.all(by.xpath('//*[@id="create-user"]/ion-content/div[1]/div/div/form/button'))
    var btn = element.all(by.xpath('//*[@id="create-user-profile"]'))
    var elm = btn.get(0)
    elm.getText()
      .then(function(text) {
        expect(text).to.equal('Next')
      })

    var fn = element.all(by.xpath('//*[@id="firstname"]'))
    var ln = element.all(by.xpath('//*[@id="lastname"]'))
    var em = element.all(by.xpath('//*[@id="email"]'))
    var tp = element.all(by.xpath('//*[@id="phone"]'))
    var dci = element.all(by.xpath('//*[@id="defaultCity"]'))
    var ds = element.all(by.xpath('//*[@id="defaultState"]'))
    var dpc = element.all(by.xpath('//*[@id="defaultPostalCode"]'))
    var countryOption = element(by.cssContainingText('option', 'CANADA'))

    fn.clear()
    fn.sendKeys('Test User')

    ln.clear()
    ln.sendKeys('Test User')

    em.clear()
    em.sendKeys(generateEmail())

    browser.driver.executeScript("document.getElementById('phone').scrollIntoView();")
    tp.clear()
    tp.sendKeys('9999999999')

    dci.clear()
    dci.sendKeys('Toronto')

    ds.clear()
    ds.sendKeys('Ontario')

    browser.driver
      .executeScript("document.getElementById('defaultCountry').scrollIntoView();")

    dpc.clear()
    dpc.sendKeys('M1S 7U8')

    countryOption.click()

    browser.driver
      .executeScript("document.getElementById('create-user-profile').scrollIntoView();")

    return elm.click()
      .then(function() {
        return browser.waitForAngular()
      })
  }

  /**
   * @name ThenChooseAccountType
   * @param {string} text String on button.
   */
  function ThenChooseAccountType(text) {

    var pseudoBtnBedOwner = element.all(by.css('.account-type.row.bed-owner .super'))
    var pseudoBtnBedOwnerLabel = pseudoBtnBedOwner.getText()

    return pseudoBtnBedOwnerLabel.then((content) => {

      expect(content[0]).to.not.equal('')
      expect(content[0]).to.equal(text)

      return browser.waitForAngular()
    })
  }

  /**
   * @name WhenAccept
   * @param {string} text Text label on element.
   */
  function WhenAccept(text, done) {

    browser.driver.executeScript("document.getElementById('acceptance').scrollIntoView();")

    var rdo = '#acceptance'

    var elm = element.all(by.css(rdo)).get(0)

    browser.sleep(500)

    elm.click().then(done)
  }

  /**
   * @name WhenCreateAccount
   * @param {string} text Text label on element.
   */
  function WhenCreateAccount(text) {
    var btn = element(by.css('button.create'))
    var btnLabel = btn.getText()

    return btnLabel.then((content) => {

      expect(content).to.equal('CREATE ACCOUNT')

      return btn.click()
    })
  }

  /**
   * @name GivenHomePage
   */
  function GivenHomePage() {

    browser.get('/#!/app/login')

    return browser.waitForAngular()
  }

  /**
   * @name WhenUsername
   * @description
   * @returns {undefined}
   */
  function WhenUsername(done) {
    var username = element(by.name('username'))
    username.getText()
      .then((content) => {
        expect(content).to.not.equal('')
      })
    username.clear()
    username.sendKeys('test1@gmail.com')
    var name = element(by.binding('username'))
    name.getText()
      .then((content) => {
        expect(content).to.equal('test1@gmail.com')
      })
      .then(done)
  }

  /**
   * @name ThenBrowse
   * @returns {undefined}
   */
  function ThenBrowse(done) {
    var btn = element(by.css('.popup-buttons button'))
    browser.wait(EC.presenceOf(btn), 5000)
    browser.wait(EC.visibilityOf(btn), 5000)
    btn.getText().then((content) => {
      expect(content).to.equal('OK')
      btn.click().then(done)
    })
  }

  /**
   * @name GivenBookingsPage
   * @returns {undefined}
   */
  function GivenBookingsPage() {
    browser.get('/#!/app/bookings')
    return browser.waitForAngular()
  }

  /**
   * @name GivenListingsCreate
   * @returns {undefined}
   */
  function GivenListingsCreate() {
    browser.get('/#!/app/listings/create')
    return browser.waitForAngular()
  }

  /**
   * @name ThenLoadBeds
   * @returns {undefined}
   */
  function ThenLoadBeds() {
    var thirdItemSelector = '#beds > ion-content > div.scroll > div > div > div:nth-child(3)'
    var thirdItem = element(by.css(thirdItemSelector))
    browser.wait(EC.presenceOf(thirdItem), 20000)
    browser.wait(EC.visibilityOf(thirdItem), 20000)
    thirdItem.getText()
      .then(function(content) {
        expect(content).to.not.equal('')
      })
    var thirdItemSelector = '//*[@id="beds"]/ion-content/div[1]/div/div/div[3]/i'
    var thirdItemIcon = element(by.xpath(thirdItemSelector))
    browser.wait(EC.visibilityOf(thirdItemIcon), 20000)
    return thirdItemIcon.click()
      .then(function() {
        return browser.waitForAngular()
      })
  }

  /**
   * @name ThenConfirmUserAccount
   * @returns {Object:Promise} Return confirmation popup dialog Promise.
   */
  function ThenConfirmUserAccount() {
    var popupButtonSelector = 'button.button-default'
    var popupButton = element(by.css(popupButtonSelector))
    browser.wait(EC.visibilityOf(popupButton), 5000)
    return popupButton.click()
      .then(function() {
        return browser.waitForAngular()
      })
  }
})

