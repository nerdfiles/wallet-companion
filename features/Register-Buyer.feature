# ./features/WalletCompanion--Register-Traveller.feature
Feature: Register Traveller
  As a user of WalletCompanion web app
  I want to register an account for a traveller

  Scenario: Register as a Traveller with Payment Methods
    Given I am on the login page
     When I click on "Create Account"
      And I accept "TOS"
     Then I see the Traveller Account Creation option
      And I see CREATE ACCOUNT
      And I create a password
      And I confirm user account
      And I add payment method
      And I add billing info
      And I fill in my info
      And I browse the app

#   Scenario: Register as a Traveller
#     Given I am on the login page
#      When I click on "Create Account"
#       And I accept "TOS"
#      Then I see the Traveller Account Creation option
#       And I see CREATE ACCOUNT
#       And I create a password
#       And I confirm user account
#       And I skip to beds
#       And I load beds
