const fs = require('fs')
const path = require('path')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect
const {defineSupportCode} = require('cucumber')

defineSupportCode(({Before, After}) => {

  Before(function (scenario, done) {
    let world = this

    world.testScreen = (name) => {
      var fileName = path.join(__dirname, name + '.png')
      browser.takeScreenshot().then(function (base64png) {
        var img = new Buffer(base64png, 'base64')
        fs.writeFile(fileName, img, {encoding: 'base64'}, function (error) {
          if (error) {
            console.log(error)
          }
        })
      }, function (error) {
          done(error)
      })
    }

    world.waitUntilReady = ($element) => {
      browser.wait(() => {
        return $element.isPresent()
      }, 10000)
      browser.wait(() => {
        return $element.isDisplayed()
      }, 10000)
    }
    done()
  })

  After(function (scenario, done) {
    let world = this

    if (scenario.isFailed()) {
      var fileName = path.join(__dirname, 'last-test.png')
      browser.takeScreenshot().then(function (base64png) {
        var img = new Buffer(base64png, 'base64')
        fs.writeFile(fileName, img, {encoding: 'base64'}, function (error) {
          if (error) {
            console.log(error)
          }
        })
      }, function (error) {
          done(error)
      })
    }

    // world.browserModules = browser.forkNewDriverInstance(true, true)
    // world.testModule = browser.addMockModule('stub', ```
    //   angular.module("WalletCompanionMobileAppNameCheck", [])
    //     .factory("stub", [() => {
    //       let stub = {} return stub
    //     }])
    // ```
    // )

    // browser.manage().logs().get('browser')
    //   .then((log) => {
    //     var message = JSON.parse(log[i].message)
    //       .message
    //       .parameters[0]
    //       .value
    //     browser.getCurrentUrl()
    //       .then((url) => {
    //         console.log(url)
    //       })
    //   })

    world.driver.quit()
    done()
  })
})
