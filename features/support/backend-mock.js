var exports = module.exports = {}
var nock = require('nock')
var _ = require('lodash')
var server = nock(process.env.backendServer || 'https://telegraf-money.herokuapp.com')

var api = {
  users: {
    post: '/api/v1/users',
    getList: '/api/v1/users',
    get: '/api/v1/users/59405f6cc5ffad0007eaa5a1',
    delete: '/api/v1/users',
    update: '/api/v1/users/59405f6cc5ffad0007eaa5a1'
  },
  properties: {
    post: '/api/v1/properties',
    getList: '/api/v1/properties',
    get: '/api/v1/properties/59527170eece0ceb9beca1da',
    delete: '/api/v1/properties',
    update: '/api/v1/properties/59527170eece0ceb9beca1da'
  },
  reservations: {
    post: '/api/v1/reservations',
    getList: '/api/v1/reservations',
    get: '/api/v1/properties/5952b9b17e2673000715566e',
    delete: '/api/v1/reservations',
    update: '/api/v1/reservations/5952b9b17e2673000715566e'
  },
  authentication: {
    logout: '/api/v1/logout',
    login: '/api/v1/auth'
  }
}

server.defaultReplyHeaders({
  'Content-Type'                : 'application/json',
  'Access-Control-Allow-Origin' : '*'
})


/**
 * @name createUser
 * @returns {undefined}
 */
exports.createUser = function(user) {
  var yin = {
    email           : email,
    password        : password,
    confirmPassword : "pulsar convalescent propelling hematic",
    username        : "test999999999999@gmail.com",
    role            : "Seller",
    profile: {
      name:{
        first:"testuser9999999999",
        last:"testuser9999999999"
      },
      phone:"2812812811",
      location:{
        city       : "Houston",
        state      : "Texas",
        postalCode : "77039",
        country    : "USA"
      }
    }
  }
  _.extend(ying, user)

  var yang = {
    data: {
    }
  }

  server
    .post(api.users.post, yin)
    .reply(200, yang)
}

/**
 * @name createProperty
 * @returns {*} undefined
 * @usage
 * backend.createProperty()
 */
exports.createProperty = function(property) {
  var yin = {}
  _.extend(yin, property)

  var yang = {
    data: {
    }
  }

  server
    .post(api.properties.post, yin)
    .reply(200, yang)
}

/**
 * @name createReservation
 * @returns {*} undefined
 */
exports.createReservation = function(reservation) {
  var yin = {
  }
  _.extend(yin, reservation)

  var yang = {
    data: {
    }
  }

  server
    .post(api.reservations.post, yin)
    .reply(200, yang)
}

/**
 * @name getUser
 * @returns {*} undefined
 */
exports.getUser = function() {
  var yang = {}
  server
    .get(api.users.get)
    .reply(200, yang)
}

/**
 * @name getProperty
 * @returns {*} undefined
 */
exports.getProperty = function() {
  var yang = {}
  server
    .get(api.properties.get)
    .reply(200, yang)
}

/**
 * @name getReservation
 * @returns {*} undefined
 */
exports.getReservation = function() {
  var yang = {}
  server
    .get(api.reservations.get)
    .reply(200, yang)
}

/**
 * @name updateUser
 * @returns {*} undefined
 */
exports.updateUser = function(user) {
  var yin = {}
  _.extend(yin, user)
  var yang = {}
  server
    .put(api.users.update)
    .reply(200, yang)
}

/**
 * @name updateProperty
 * @returns {*} undefined
 */
exports.updateProperty = function(property) {
  var yin = {}
  _.extend(yin, property)
  var yang = {}
  server
    .put(api.properties.update, ying)
    .reply(200, yang)
}

/**
 * @name updateReservation
 * @returns {*} undefined
 */
exports.updateReservation = function(reservation) {
  var yin = {}
  _.extend(yin, reservation)
  var yang = {}
  server
    .put(api.reservations.update)
    .reply(200, yang)
}

/**
 * @name deleteUser
 * @returns {*} undefined
 */
exports.deleteUser = function() {
  server
    .delete(api.users.delete)
    .reply(200)
}

/**
 * @name deleteProperty
 * @returns {*} undefined
 */
exports.deleteProperty = function() {
  server
    .delete(api.properties.delete)
    .reply(200)
}

/**
 * @name deleteReservation
 * @returns {*} undefined
 */
exports.deleteReservation = function() {
  server
    .delete(api.reservations.delete)
    .reply(200)
}

/**
 * @name pseudoLogin
 * @returns {*} undefined
 */
exports.pseudoLogin = function (email, password) {
  var yin = {
    email    : email,
    password : password
  }

  var yang = {
    data: {
      email: email
    }
  }

  server
    .post(api.authentication.login, yin)
    .reply(200, yang)
}

/**
 * @name pseudoLogout
 * @returns {*} undefined
 */
exports.pseudoLogout = function (email) {
  server
    .post(api.authentication.logout, {
      email: email
    })
    .reply(200)
}

