const nock = require('nock')
var { defineSupportCode } = require('cucumber')

defineSupportCode(function({Before, After}) {

  Before(function (scenario, callback) {
    var world = this
    nock.cleanAll()
    world.global = {}
    world.timeSpent = process.hrtime()

    browser.executeScript('\
      navigator.geolocation.getCurrentPosition = function (success) { \
          var position = { \
            "coords" : { \
              "latitude": "37", \
              "longitude": "-115" \
            } \
          }; \
          success(position); \
        }')
    callback()
  })

  After(function (scenario, callback) {
    var world = this
    var diff = process.hrtime(world.timeSpent)
    console.log('>> Execution time: %d nanoseconds', diff[0] * 1e9 + diff[1])
    callback()
  })
})
