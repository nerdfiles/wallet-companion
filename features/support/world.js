require('chromedriver')
var seleniumWebdriver = require('selenium-webdriver')
var {defineSupportCode} = require('cucumber')
var FirefoxProfile = require('firefox-profile')
var profile = new FirefoxProfile()

/**
 * @name WalletCompanion
 * @see https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities, https://sites.google.com/a/chromium.org/chromedriver/capabilities
 * @returns {undefined}
 */
function WalletCompanion() {
  var context = this
  // var capabilities = seleniumWebdriver.Capabilities.firefox()
  // profile.setPreference("geo.prompt.testing", true)
  // profile.setPreference("geo.prompt.testing.allow", true)
  // profile.setPreference("geo.enabled", true)
  // profile.setPreference("geo.wifi.uri", 'data:application/json,{"location": {"lat": 37, "lng": -115}, "accuracy": 100.0}')
  // profile.encoded(function(encodedProfile) {
  //   capabilities.set('firefox_profile', encodedProfile)
  //   context.driver = new seleniumWebdriver.Builder()
  //     .withCapabilities(capabilities)
  //     .build()
  // })

  this.driver = new seleniumWebdriver.Builder()
    .forBrowser('chrome')
    .build()
}

defineSupportCode(function({setWorldConstructor}) {
  setWorldConstructor(WalletCompanion)
})
