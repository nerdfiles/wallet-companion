# ./features/WalletCompanion--Forgot-Password.feature
Feature: Reset Forgotten Password
  As a user of Wallet Companion web app
  I want to reset a forgotten password

  Scenario: Navigating to Forgot Password page
    Given I am on the login page
     When I navigate to the forgot password page
     When I click on Forgot Password?
     Then I should see a small form asking to "Enter your email and a reset password link will be emailed to you."


