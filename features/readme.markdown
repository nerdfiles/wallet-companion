# Features

Features should be named by ticket number or by module.

## Ticket Number

    ./BC-999.feature

## Module

    ./BPC--Some-Page

The convention for naming is to help distinguish DRY versus non-reusable code.
