# ./features/WalletCompanion--Register-Bed-Owner.feature
Feature: Register Bed Owner
  As a user of WalletCompanion web app
  I want to register an account for a bed owner

  Scenario: Register as a Bed Owner
    Given I am on the login page
     When I click on "Create Account"
     Then I see a "I am a Bed Owner" Account Type option

