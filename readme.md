# [Wallet Companion][wc]

## Install

    $ npm i

## Development

    $ gulp

## Build

    $ gulp --build && ionic build ios

## Test

## Deploy

    $ surge www wallet.companion.money

### Overview

Launch tests using:

    $ npm run test/aat

### HTTP Intercept Testing

Using [nock](https://www.npmjs.com/package/nock). There is an available 
backend module for testing authentication which can be expanded to cover more 
of the Wallet Companion API.

### WebDriver

Separately, run your local Selenium server at http://localhost:4444:

    $ sudo webdriver-manager start

#### Exiting Browser Instances

If your Selenium browser instances do not close, try the following, as they
might stack up in your OS window manager if not exited properly:

    $ ps ax | grep firefox | cut -d' ' -f1 | sudo xargs kill -9

#### Notes on WebDriver

We will likely provision and configure our own highly available Selenium server 
through AWS EC2. At the moment, these tests could theoretically live anywhere 
our application successfully installs. We may consider a Make file to bind 
into our npm scripts in order to provision WebDriver accordingly.

Assuming `npm i`:

    $ sudo webdriver-manager update

The above command will install `chromedriver` and `geckodriver`.

### Automated Acceptance Testing with Cucumber and Protractor

#### Tests

Gherkin-style User Behavior tests are available in `./features/`.

#### Steps

Steps for User Behavior tests are available in `./features/steps`.

## Report

### Complexity

    $ npm run report/complexity

View [ES analysis](https://github.com/es-analysis/plato):

    $ npm run serve/report/complexity

## Documentation

### ngdocs

Generate docs:

    $ npm run docs

Serve, view docs:

    $ npm run serve/docs 

## Hotfix

    $ cd app/scripts
    $ source source bin/jshint_missing_semicolon.sh && jshint.missing_semicolon

### When in doubt

    $ ionic reset state

Or beforehand:

    $ ionic platforms rm ios

Also check out missing plugin errors or this stackoverflow issue for `File` or
`CDVPlugin.h`: https://stackoverflow.com/a/11117169

Be sure to install CDVLocation and sqlite3 in Xcode.

## OS X and Android

    $ brew tap caskroom/cask
    $ brew cask install android-sdk

## Cordova

    $ cordova platform add android
    $ gulp --build && ionic build android

### Android

    $ sdkmanager "build-tools;26.0.0"

Or:

    $ sdkmanager "platform-tools" "platforms;android-26"

Add to `$HOME/.bash_profile`:

    export ANDROID_HOME=/Users/<User>/Library/Android/sdk

Or:

    export ANDROID_HOME=/usr/local/share/android-sdk

Then:

    export PATH="$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools:$PATH"

    $ cordova prepare android
    $ cordova build android 

Added to `config.xml`: 

    <preference name="loadUrlTimeoutValue" value="700000" />

Try:

    adb kill-server
    adb forward --remove-all
    adb start-server

Try:

    $ chmod +x hooks/after_prepare/010_add_platform_class.js

Try:

    $ cordova platform rm android && cordova platform add android


## Needful commands

Autoselect for npm list (install percol):

    $ npm run $(npm run | percol)

HFactor analysis:

    $ sh ../stats-on-apps.sh # ask for this script if wanted

## Android Device Setup

`android-25` chosen:

    $ android update sdk -u --filter platform-tools,android-25
    $ sdkmanager --verbose "system-images;android-25;google_apis;x86"
    $ avdmanager -v create avd -n x86 -k "system-images;android-25;google_apis;x86" -g "google_apis".


[wc]: https://bitbucket.org.nerdfiles/wallet-companion
