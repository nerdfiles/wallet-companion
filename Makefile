.PHONY: all install test build run build-ios emulate-ios tarball

all: run

install:
	npm install && bower install

test: install
	npm run test/aat/users

build: build-ios

run:
	gulp

build-ios:
	gulp --build && ionic build ios

emulate-ios:
	gulp --build && ionic emulate ios

tarball: tgm-app-mobile.tar.gz

tgm-app-mobile.tar.gz:
	tar -czvf tgm-app-mobile.tar.gz \
	Makefile package.json readme.md resty.sh gulpfile.js bower.json \
	browser.js ionic.project href.map config.xml \
	app/ bin/ config/constants.js features/ helpers/ \
	keys/ plugins/ report/ test/ resources/ platforms/
