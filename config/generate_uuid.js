/**
 * @filepath ./config/generate_uuid.js
 */

/**
 * @constant
 * @default
 */
const GLUE = ''

// @tools

const log = console.log
const { Random, MersenneTwister19937 } = require('random-js')
const random = new Random(MersenneTwister19937.autoSeed())
const MIN = 1
const MAX = Number.MAX_SAFE_INTEGER

/**
 * @constant
 * @default
 */
const DEBUG = true

// @internals

/**
 * @name groupRandom
 */
const groupRandom = function () {
  const args = arguments
  const GLUE = args[args.length - 1]
  return [
    ...args
  ].join(GLUE)
}

/**
 * @name rand
 * @description not cryptographically secure but safe to run on computer.
 */
const rand = () => {
  const BASE = 16
  const MID = 2
  const SOURCE = random.integer(MIN, MAX)
  const RANDO = SOURCE.toString(BASE)
  const LENGTH = RANDO.length
  const STRIP = LENGTH / MID
  const OUT = RANDO.substring(STRIP)
  return OUT
}

/**
 * Array.prototype.rand.
 */
if (typeof Array.prototype.rand !== 'function') {
  Array.prototype.rand = function () {
    const OUT = this
    log(OUT)
    return OUT
  }
}

const guid = () => {
  /**
   * @constant
   * @default
   */
  const SEP = '-'

  // passing in a SEP glues together a Nothing, obvs
  const group1 = groupRandom(rand(), rand(), rand(), GLUE)
  const group3 = groupRandom(rand(), rand(), rand(), GLUE)

  const group2 = [
    rand(),
    rand(),
    rand()
  ].join(SEP)

  const identifier = [
    group1,
    group2,
    group3
  ].join(SEP)

  return identifier
}

const ID = guid()
// @see https://github.com/WebOfTrustInfo/rwot11-the-hague/blob/master/advance-readings/did-primer.md
const DID = 'urn:uuid:fe0cde11-59d2-4621-887f-23013499f905'

log('DEBUG:', DID)
DEBUG && log('DEBUG:', ID)

const NON = ID.split(GLUE).length
log('DID:', DID)
const CAP = DID.split(GLUE).length
log(random.dice(NON, CAP))

module.exports = guid

// EOF
