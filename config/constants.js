'use strict';

/**
 * @ngdoc service
 * @name WalletCompanion.CONSTANTS
 * @description
 * # CONSTANTS
 */

(function() {
  angular.module('WalletCompanion')

    // development
    .constant('API_ENDPOINT', {
      host: '__HOST__',
      port: null,
      path: '',
      needsAuth: false
    })
    .constant('MAPS_API', {
      key: 'AIzaSyBSWXeZ3hE9rX-EBGLQXrjyU8LLiI1yaBM'
    })
    .constant('STRIPE', {
      publishKey: ''
    });
})();
