
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')


  /**
   * @ngdoc filter
   * @name tgmFormatMonthDayYear
   * @returns {date} Formatted date for use in template.
   */
  .filter('tgmFormatMonthDayYear', function tgmFormatMonthDayYear() {
    return function(dateString) {
      return moment(dateString).format('MMM D, YYYY');
    };
  });

})();

