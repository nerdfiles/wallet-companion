
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatMonthYear
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatMonthYear', function tgmFormatMonthYear() {
      return function(dateString) {
        return moment(dateString).format('MMM YYYY');
      };
    });

})();

