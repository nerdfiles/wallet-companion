
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name rangeFilter
     */
    .filter('tgmCardRange', rangeFilter)

  /**
   * rangeFilter
   *
   * @returns {undefined}
   */
  function rangeFilter() {

    /**
     * @function filter
     * @param {object} array TBD
     * @param {object} lower TBD
     * @param {object} upper TBD
     * @returns {function} Filtering
     */
    function filter(array, lower, upper) {
      for (var i = lower; i <= upper; ++i) {
        array.push(i);
      }
      return array;
    }
    return filter;
  }

})();

