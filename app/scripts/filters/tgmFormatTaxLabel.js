
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatTaxLabel
     */
    .filter('tgmFormatTaxLabel', function tgmFormatTaxLabel() {
      return function(number) {
        return (number * 100) + '%';
      };
    });

})();

