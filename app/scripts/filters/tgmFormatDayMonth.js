
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatDayMonth
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatDayMonth', function tgmFormatDayMonth() {
      return function(dateString) {
        return moment(dateString).format('D MMM');
      };
    })

})();

