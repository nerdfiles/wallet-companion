
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatMonthDay
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatMonthDay', function tgmFormatMonthDay() {
      return function(dateString) {
        return moment(dateString).format('MMM D');
      };
    });

})();

