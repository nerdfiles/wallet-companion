
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmCapitalize
     * @returns {string} Capitalized string.
     */
    .filter('tgmCapitalize', function tgmCapitalize() {
      return function(contentString) {
        if (!contentString) {
          return;
        }
        var str = contentString.split('-');
        var _str = _.map(str, function(word) {
          var firstChar = word.slice(0, 1).toUpperCase();
          return firstChar + word.slice(1);
        });
        return _str.join(' ');
      };
    })

})();
