
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatDate
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatDate', function tgmFormatDate() {
      return function(dateString) {
        return moment(dateString).format('MMM D, YYYY');
      };
    });

})();

