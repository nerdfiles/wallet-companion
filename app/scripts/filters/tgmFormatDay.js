
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatDay
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatDay', function tgmFormatDay() {
      return function(dateString) {
        return moment(dateString).format('dddd');
      };
    });

})();
