
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmTruncate
     */
    .filter('tgmTruncate', function tgmTruncateLabel() {
      return function(str, t, q) {
        if (!str) { return; }
        // @see grammuelle grids in organic css
        var a = str.split('');
        return a.splice(0, t).join('') + q;
      };
    });

})();

