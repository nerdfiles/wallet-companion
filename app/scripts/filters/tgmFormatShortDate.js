
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatShortDate
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatShortDate', function tgmFormatShortDate() {
      return function(dateString) {
        return moment(dateString).format('MM/D/YYYY');
      };
    });

})();

