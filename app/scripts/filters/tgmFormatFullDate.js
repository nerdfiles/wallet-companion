
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc filter
     * @name tgmFormatFullDate
     * @returns {date} Formatted date for use in template.
     */
    .filter('tgmFormatFullDate', function tgmFormatFullDate() {
      return function(dateString) {
        return moment(dateString).format('dddd, MMM D, YYYY');
      };
    });

})();

