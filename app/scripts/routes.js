// @fileOverview .//app/scripts/routes.js

/**
 * @ngdoc overview
 * @returns {undefined}
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .config([
      '$urlRouterProvider',
      '$stateProvider',
      routes
    ]);

  /**
   * @name routes
   * @returns {undefined}
   * @description
   * Routes for Telegraf Wallet front end matter.
   */
  function routes($urlRouterProvider, $stateProvider) {

    var __app__ = {
      app: {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/shell.html',
        controller: 'MainController',
        controllerAs: 'vm'
      }
    };

    /**
     * @usage moduleSelector('app')('MainController')('templates/shell.html')
     *          .then(function(res) {
     *            $stateProvider.state(res.app.name, res.app);
     *          });
     */
    function plant(namespace, config) {

      if ( ! namespace )
        defer.reject({ error: '404' })

      var spec = {};
      var defer = {};

      spec[namespace] = {
        url: config.url,
        name: config.name,
        controllerAs: config.controllerAs || 'vm'
      };

      if (config && config.abstract) {
        spec[namespace].abstract = true
      }

      return function(controllerName) {
        spec[namespace]['controller'] = controllerName;
        return {
          with: function (templateUrl) {
            spec[namespace]['templateUrl'] = templateUrl;

            return function() {
              defer.resolve(spec)

            }
          }
        }
      }

      return defer.promise
    }

    /**
        plant('app', configuration)('MainController').with('templates/shell.html')
          .then(function(res) {
            $stateProvider.state(res.app.name, res.app);
          });

        plant('app.login', initialConfiguration)('LoginController').with('templates/shell.html')
          .then(function(res) {
            $stateProvider.state(res.app.name, res.app);
          })
    **/

    // Application routing
    $stateProvider

      /**
       * @accountType all
       * @viewType abstractView
       */
      .state('app', {
        url          : __app__.app.url,
        abstract     : __app__.app.abstract,
        templateUrl  : __app__.app.templateUrl,
        controller   : __app__.app.controller,
        controllerAs : __app__.app.controllerAs
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.login', {
        url: '/login',
        cache: false,
        parent: 'app',
        views: {
          'viewContent': {
            templateUrl: 'templates/views/splash/home.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        },
        params: {
          initial: null
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.support', {
        url: '/support',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/splash/support.html',
            controller: 'SupportController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.forgotPassword', {
        url: '/forgot-password',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/splash/forgot-password.html',
            controller: 'ForgotPasswordController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.stats', {
        url: '/stats',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/stats/main.html',
            controller: 'StatsController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.createUser', {
        url: '/create',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/splash/create.html',
            controller: 'AccountCreationController',
            controllerAs: 'vm'
          }
        },
        params: {
          type: null,
          userType: null
        }
      })

      /**
       * @accountType buyer
       * @viewType createView
       */
      .state('app.createMessage', {
        url: '/messages/create/:id?',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/create.html',
            controller: 'ContractCreationController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType baseView
       */
      .state('app.createMessagePaymentSelect', {
        url: '/messages/create-payment-select',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/payment-select.html',
            controller: 'PaymentCreationController',
            controllerAs: 'vm'
          }
        },
        params: {
          name: {},
          user: {}
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.help', {
        url: '/help',
        cache: false,
        views: {
          viewContent: {
            templateUrl  : 'templates/views/help.html',
            controller   : 'HelpController',
            controllerAs : 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType listView
       */
      .state('app.coins', {
        url: '/currencies/:id?',
        cache: false,
        parent: 'app',
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/coins.html',
            controller: 'HomeController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType mapView,listView
       */
      .state('app.mapView', {
        url: '/mapView',
        cache: false,
        parent: 'app',
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/map.html',
            controller: 'MapViewController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType seller
       * @viewType createView
       * @description Should be invoked by individual contract with a "contract
       * member." Any user can be a contract member to another buyer who has
       * ownership".
       */
      .state('app.createNewOffer', {
        url: '/missives/create-new-offer',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/create-new-offer.html',
            controller: 'NewOfferCreationController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType seller
       * @viewType createView
       * @description Should be invoked by individual contract with a "contract
       * member." Any user can be a contract member to another buyer who has
       * ownership".
       */
      .state('app.createMissive', {
        url: '/missives/create',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/create.html',
            controller: 'MissiveCreationController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType seller
       * @viewType listView
       */
      .state('app.myOffers', {
        url: '/offers',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/offers.html',
            controller: 'MyOffersController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType seller
       * @viewType listView
       */
      .state('app.offers', {
        url: '/offers',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/missives/offers.html',
            controller: 'MyOffersController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType listView
       */
      .state('app.messagesTimeline', {
        url: '/messages/timeline',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/timeline.html',
            controller: 'ContractTimelineController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType listView
       */
      .state('app.history', {
        url: '/history',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/base.html',
            controller: 'ContractListController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType detailView
       * @description Alias for app.messages (buyer only template)
       */
      .state('app.historyDetail', {
        url: '/history/:id',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/detail.html',
            controller: 'ContractDetailController',
            controllerAs: 'vm'
          }
        }
      })


      /**
       * @accountType buyer
       * @viewType listView
       */
      .state('app.messages', {
        url: '/messages',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/base.html',
            controller: 'ContractListController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType detailView
       * @description Alias for app.messages (buyer only template)
       */
      .state('app.message', {
        url: '/messages/:id',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/messages/detail.html',
            controller: 'ContractDetailController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       * @description
       */
      .state('app.address', {
        url: '/address',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/address.html',
            controller: 'AddressController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType listView
       * @description
       * Is this an alias for app.messages (buyer only template)?
       */
      // .state('app.history', {
      //   url: '/history',
      //   cache: false,
      //   views: {
      //     'viewContent': {
      //       templateUrl: 'templates/views/wallet/history.html',
      //       controller: 'ContractListController',
      //       controllerAs: 'vm'
      //     }
      //   }
      // })

      /**
       * @accountType all
       * @viewType listView
       */
      .state('app.exchange', {
        url: '/exchange',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/exchange.html',
            controller: 'ExchangeController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType listView
       * @TODO should be notifications
       */
      .state('app.approval', {
        url: '/approval',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/approval.html',
            controller: 'ApprovalController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType listView
       */
      .state('app.balances', {
        url: '/balances',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/balances.html',
            controller: 'BalancesController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType baseView
       */
      .state('app.importKey', {
        url: '/import-key',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/import.html',
            controller: 'ImportKeyController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType createView
       */
      .state('app.send', {
        url: '/send',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/send.html',
            controller: 'SendController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType detailView
       * @description
       * A receipt page for a transfer.
       */
      .state('app.receive', {
        url: '/receive',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/wallet/receive.html',
            controller: 'ReceiveController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType detailView
       */
      .state('app.profile', {
        url: '/profile',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/profile.html',
            controller: 'ProfileController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType detailView
       */
      .state('app.profileEdit', {
        url: '/profile/edit',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/profile.html',
            controller: 'ProfileController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.tos', {
        url: '/profile/tos',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/tos.html',
            controller: 'TosController',
            controllerAs: 'vm'
          }
        },
        params: {
          intro: {
            init: false
          }
        }
      })

      /**
       * @accountType all
       * @viewType detailView
       */
      .state('app.viewCard', {
        url: '/profile/view-card/:id?',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/view-card.html',
            controller: 'ViewCardController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType buyer
       * @viewType listView
       */
      .state('app.paymentMethods', {
        url: '/profile/payment-methods',
        cache: false,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/payment-methods.html',
            controller: 'PaymentMethodsController',
            controllerAs: 'vm'
          }
        },
        params: {
          name: {},
          user: {}
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.about', {
        url: '/about',
        cache: true,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/about.html',
            controller: 'AboutController',
            controllerAs: 'vm'
          }
        }
      })

      /**
       * @accountType all
       * @viewType baseView
       */
      .state('app.tools', {
        url: '/tools',
        cache: true,
        views: {
          'viewContent': {
            templateUrl: 'templates/views/profile/tools.html',
            controller: 'ToolsController',
            controllerAs: 'vm'
          }
        }
      });

    // redirects to default route for undefined routes
    $urlRouterProvider.otherwise('/app/login');

  }
})();

