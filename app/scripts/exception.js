
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {
  'use strict'

  angular
    .module('WalletCompanion')
    .factory('$exceptionHandler', exceptionHandlerDecorate)

  /**
   * @ngdoc service
   * @name exceptionHandlerDecorate
   * @param {object} $log Internal.
   * @returns {*} undefined
   */
  exceptionHandlerDecorate.$inject = [
    '$log', 
    '$window', 
    '$analytics'
  ]

  /**
   * exceptionHandlerDecorate
   *
   * @param $log
   * @returns {undefined}
   */
  function exceptionHandlerDecorate($log, $window, $analytics) {

    return finallyCatcher

    /**
     * finallyCatcher
     *
     * @param exception
     * @param cause
     * @returns {undefined}
     */
    function finallyCatcher(exception, cause) {

      $window.setTimeout(function() {

        try {
          $log.info('exception caught: ', {
            message: exception.message,
            exception: exception,
            cause: cause
          })
        } catch(e) {
          $analytics.eventTrack('$exception', {
            category: 'error',
            label: exception.message + '::' + JSON.stringify(e)
          })

          $log.info(exception)

          throw e
        }

      }, 4)
    }
  }

})()

