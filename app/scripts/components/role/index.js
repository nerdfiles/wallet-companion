/**
 * @ngdoc directive
 * @description A directive to be placed where role determination is needed
 * across templates.
 */
(function() {
  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmLocalSession', tgmLocalSession);

  RoleController.$inject = [
    '$scope',
    'UserService',
    'ErrorService',
    '$state'
  ];

  function RoleController($scope, UserService, ErrorService, $state) {
    var vm = $scope.$parent.vm;

    UserService.loadLocalSession()
      .then(loadLocalSessionCompleted, function(e) {
        console.log(e);
      });

    /**
     * loadLocalSessionCompleted
     *
     * @param userData
     * @returns {undefined}
     */
    function loadLocalSessionCompleted(userData) {
      if (userData && userData.role) {
        $scope.$emit('userRole', userData.role);
      }
    }
  }

  /**
   * tgmLocalSession
   *
   * @returns {object} Directive factory.
   */
  function tgmLocalSession() {

    var dv = {
      restrict    : 'AE',
      scope       : {},
      controller  : RoleController,
      templateUrl : 'templates/components/local-session.html'
    };

    return dv;
  }
})();
