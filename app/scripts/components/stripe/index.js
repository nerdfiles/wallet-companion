/**
 * StripeJs
 */
(function() {
  'use strict'

  angular
    .module('WalletCompanion')
    .directive('tgmStripeJs', tgmStripeJsDirective)
    .provider('WalletCompanionStripeJs', tgmStripeJsProvider)

  var baseUrl = 'https://js.stripe.com/v2/'

  tgmStripeJsDirective.$inject = ['WalletCompanionStripeJs', 'Restangular', '$window', '$rootScope']

  /**
   * stripeJsDirective
   *
   * @param {object} StripeJs TBD
   * @returns {*} undefined
   */
  function tgmStripeJsDirective(WalletCompanionStripeJs, Restangular, $window, $rootScope) {
    return {
      scope : {},
      link  : link
    }
    ////////////
    function link($scope, $element, $attrs) {
      WalletCompanionStripeJs.load().then(() => {
        var stripeKey = Restangular.one('../../stripe').one('key')
        stripeKey.get().then(function (res) {
          var key
          if (res && res.data) {
            key = res.data.key || 'pk_test_ZH6EJHiQK2dcSnucxPQzfRnp'
          }
          Stripe.setPublishableKey(key)
          $rootScope.$emit('stripe:ready', function() {
            console.log('$window.stripe is loaded')
            return Stripe
          })
        })
      }, (err) => {
        console.log('Could not load Stripe.js')
      })
    }
  }

  /**
   * StripeJsProvider
   *
   * @returns {*} undefined
   */
  function tgmStripeJsProvider() {
    this.load = (WalletCompanionStripeJs) => {
      return WalletCompanionStripeJs.load()
    }
    this.load.$inject = ['WalletCompanionStripeJs']
    this.$get = ($document, $q) => {
      return new StripeJsService($document, $q)
    }
    this.$get.$inject = ['$document', '$q']
  }

  /**
   * StripeJsService
   *
   * @param {object} $document Internal.
   * @param {object} $q Internal.
   * @param {object} providerDefaults TBD
   * @returns {*} undefined
   */
  function StripeJsService($document, $q) {
    this.load = () => {
      return loadLibrary($document, $q)
    }
  }

  var loaded = false

  /**
   * loadLibrary
   *
   * @param {object} $document Internal.
   * @param {object} $q Internal.
   * @returns {*} undefined
   */
  function loadLibrary($document, $q) {
    var deferred = $q.defer()
    var doc = $document[0]
    var script = doc.createElement('script')
    var container = doc.getElementsByTagName('head')[0]

    if (window.stripe && loaded) {
      deferred.resolve()
      return deferred.promise
    }

    script.type = 'text/javascript'
    script.src = baseUrl
    script.onload = onload
    script.onreadystatechange = onreadystatechange
    script.onerror = onerror
    container.appendChild(script)
    return deferred.promise

    ////////////

    /**
     * onload
     *
     * @returns {undefined}
     */
    function onload() {
      loaded = true
      deferred.resolve()
    }

    /**
     * onreadystatechange
     *
     * @returns {undefined}
     */
    function onreadystatechange() {
      var rs = this.readyState
      if (rs === 'loaded' || rs === 'complete') {
        deferred.resolve()
      }
    }

    /**
     * onerror
     *
     * @param err
     * @returns {undefined}
     */
    function onerror(err) {
      deferred.reject({ error: err })
    }
  }
})()
