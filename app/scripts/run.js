
/**
 * @ngdoc filter
 *
 * @returns {undefined}
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .run(runBlock);

  angular
    .module('WalletCompanion')
    .factory('localLedgerManager', [
      '$q',
      '$timeout',
      localLedgerManager
    ]);

  runBlock.$inject = [
    '$ionicPlatform',
    '$cordovaKeyboard',
    '$window',
    '$log',
    '$q',
    'localStorageService',
    'Restangular',
    'httpErrorHandler',
    '$rootScope',
    'UserService',
    'cuid'
  ];

  /**
   * @name localLedgerManager
   * @description
   * LLM is a CQRS interface for localizing distribued ledger environments to
   * support event streams of variegated ACID event sources within a
   * virtualized cognitive layer that describes supervenience relations between
   * cybernetic forces and material conditions of the internetworking nodes as
   * they are physically manifest. The database might be initialized at
   * the "runtime" of the application, but the "configure boot" system pre-
   * hooks to the initialization of the application in parallel such that HTTP
   * pipelines might be co-initialized within the event store namespace at
   * "post-boot".
   */
  function localLedgerManager($q, $timeout) {

    var serviceInterface = {

      db: db,
      accounts: function(ns) {},
      balances: function(ns) {},
      transactions: function(ns) {},
      loadAddress: function(ns) {

      }

    };

    function db() {

      var defer = $q.defer();

      try {
        $timeout(function() {
          defer.resolve( $window.db );
        }, 4);
      } catch(err) {
        defer.reject({ error: err });
      }

      return defer.promise;
    }

    return serviceInterface;
  }

  /**
   * @name runBlock
   * @param {object} $ionicPlatform Internal.
   * @param {object} $window Internal.
   * @param {object} $log Internal.
   * @returns {*} undefined
   * @returns {undefined}
   */
  function runBlock($ionicPlatform, $cordovaKeyboard, $window, $log, $q, localStorageService, Restangular, httpErrorHandler, $rootScope, UserService, cuid) {

    // ## == Run Init
    var loaded = false;

    // ## == Run Setup
    $window.db = {};
    $window.db.init = initializeLocalSessionDatabase;

    $ionicPlatform.ready(ready);

    /////////////////

    /**
     * @name ready
     */
    function ready() {

      if (
        $window.cordova &&
        $window.cordova.plugins &&
        $window.cordova.plugins.Keyboard
      ) {
        $window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        $cordovaKeyboard.disableScroll(true);
      }

      if ( $window.StatusBar ) {
        $window.StatusBar.styleDefault();
      }
    }

    /**
     * @name $off
     */
    $rootScope.constructor.prototype.$off = function (eventName, callback) {

      var events

      if (this.$$listeners) {

        events = this.$$listeners[eventName]

        if (events && events.length) {

          events.forEach(function(i, ev) {
            if (ev === callback) {
              events.splice(i, 1)
            }
          })

          //for (var i = 0; i < events.length; i++) {
          //  if (events[i] === callback) {
          //    events.splice(i, 1)
          //}
        }
      }
    }

    /**
     * @name initializeLocalSessionDatabase
     * @returns {object:Promise} Promisified DB API.
     * @description We attempt to pass the instantiated DB API around through
     * Promised, trying its methods at each juncture in order to initiate a
     * 'local' session that is not bound to the scope of the running
     * application.
     */
    function initializeLocalSessionDatabase() {

      var deferred = $q.defer();

      $ionicPlatform.ready(function() {

        if (
          $window.db &&
          $window.db._ &&
          loaded === true
        ) {

          return deferred.resolve($window.db._);
        }

        if (window.sqlitePlugin) {

          if ($window.db._) {

          } else {

            $window.db._ = window.sqlitePlugin.openDatabase({
              name: 'tgm__test33.db',
              location: 'default'
            });

          }
        } else if (_.isFunction($window.openDatabase)) {

          // Chromium browsers
          $window.db._ = $window.db._ ? $window.db._ : $window.openDatabase('tgm__test33', '', 'Local session storage', 5*1024*1024);

        } else {

          // Mozilla browsers
          $window.db._ = $window.db._ ? $window.db._ : $window.indexedDB.open('tgm__test33', 'Local session storage');

        }

        _.extend($window.db._, {
          loaded: loaded
        });

        $window.db._.execute = __execute__;

        /**
         * @name __execute__
         * @description
         * Execution of the localized storage context for ORM-like layered
         * REST-ful mapping.
         */
        function __execute__(db, sql, data) {

          var def = $q.defer();
          var _data = data ? data : [];

          if (_.isFunction($window._.onsuccess)) {

            /**
             * @name onsuccess
             * @param $event
             * @returns {undefined}
             */
            $window._.onsuccess = function($event) {

              var db = $event.result;
              var objectStore;

              try {
                objectStore = $event.result.objectStore(_data);
              } catch(err) {
                return def.reject({ error: err });
              }

              if (_.size(_data)) {
                return def.reject({ error: '404' });
              }

              for (var index = 0; index < _data.length; index++) {
                var id = _data[index];
                objectStore.add(id).onsuccess = function($event) {
                  console.log('Local store update: ', $event);
                };
              }

              def.resolve(objectStore);
            };

          } else {

            /**
             * @name transaction
             * @param t
             * @returns {undefined}
             */
            $window.db._.transaction(function(t) {

              t.executeSql(sql, _data, function(tx, res) {
                return def.resolve(res);
              }, function(tx, error) {
                return def.resolve({ role: 'admin' });
              });
            }, function(e) {
              return def.reject(e);
            });

          }

          return def.promise;
        }

        var sqlStatement = 'CREATE TABLE IF NOT EXISTS local_session (token text, user_id integer, role text, client_logged_in text, created_at text, updated_at text, profile_verification text, profile_completion text)'

        try {

          if (
            $window.db &&
            $window.db._ &&
            $window.db._.loaded === false
          ) {

            if (_.isFunction($window.db._.createObjectStore)) {

              $window.db._.onsuccess = function($event) {
                var db = $event.result;
                if (db.version !== '1') {
                  var createdObjectStoreCount = 0;
                  var token = cuid();
                  var objectStores = [
                    {
                      token                : token,
                      user_id              : undefined,
                      role                 : 'interim',
                      client_logged_in     : moment(),
                      created_at           : moment(),
                      updated_at           : moment(),
                      profile_verification : moment(),
                      profile_completition : moment()
                    },
                  ];

                  function objectStoreCreated(event) {
                    if (++createdObjectStoreCount == objectStores.length) {
                      db.setVersion('1').onsuccess = function(event) {
                        return deferred.resolve(db);
                      };
                    }
                  }

                  for (var index = 0; index < objectStores.length; index++) {
                    var params = objectStores[index];
                    $window.db._ = db.createObjectStore(params.token, params.user_id, params.role);
                    $window.db._.onsuccess = objectStoreCreated;
                  }
                }
              }
            } else {

              $window
                .db
                ._
                .execute($window.db._, sqlStatement)
                .then((res) => {

                  loaded = true;

                  $window.db._.loaded = true;

                  return deferred.resolve($window.db._);

                }, (err) => {

                  if ($window.db._) {
                    return deferred.resolve($window.db._);
                  }

                  return deferred.reject({
                    error: err
                  });
                });
            }
          } else {

            if ($window.db && $window.db._) {

              deferred.resolve($window.db._);

            }
          }

        } catch(err) {

          console.log({ error: err });

          return deferred.reject({ error: err });
        }
      });

      return deferred.promise;
    }

    // Restangular configuration
    Restangular.setErrorInterceptor(httpErrorHandler.intercept);

    // add possible global event handlers here
    // the following should only run when under ios conditions since unload
    // implies more than a refresh so we want to aggressively clear user
    // credentials

    $window.addEventListener('beforeunload', function() {
      console.log('Clearing local session')
      // @TODO
      // UserService.deleteLocalSession().then(function() {
      // }, function(e) {
      //   console.log(e)
      //   console.log('Clearing local session')
      // })
    });
  }

})();

