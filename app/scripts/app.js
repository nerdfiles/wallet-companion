
/**
 * @ngdoc overview
 * @name WalletCompanion
 * @description
 * @channel
 * 2018-03-11_13:59: Why haven't I renamed this file to 'interface.js' yet? It
 * likely has to do with the fact that `wiredep` explodes when I change base
 * files still.
 */

(function() {

  'use strict';

  /**
   * toHex
   *
   * @returns {undefined}
   */
  String.prototype.toHex = function() {

    var hex, i;
    var result = '';

    for (i = 0; i < this.length; i++) {
      hex = this.charCodeAt(i).toString(16);
      result += ("000"+hex).slice(-4);
    }

    return result
  };

  /**
   * fromHex
   *
   * @returns {undefined}
   */
  String.prototype.fromHex = function() {

    var j;
    var hexes = this.match(/.{1,4}/g) || [];
    var back = '';

    for (j = 0; j < hexes.length; j++) {
      back += String.fromCharCode(parseInt(hexes[j], 16));
    }

    return back;
  };

  var moduleDeps = [
    'ngAnimate',
    'ngMap',
    'restangular',
    'ionic',
    'ngCordova',
    'ngResource',
    'oc.lazyLoad',
    'ngCookies',
    'rzModule',
    'ion-google-autocomplete',
    'ui.rCalendar',
    'ionic-timepicker',
    'LocalStorageModule',
    'ngStorage',
    'uuid4',
    'images-resizer',
    'pasvaz.bindonce',
    'angular-jsonrpc-client',
    'angulartics',
    'angulartics.segment',
    'angulartics.scroll',
    'ngAria',
    'monospaced.qrcode'
  ];

  var CryptoManager = [
    '$window',
    __CryptoManager__
  ];

  var ExternalApiManager = [
    'Restangular',
    __ExternalApiManager__
  ]

  restangularConfig.$inject = [
    'RestangularProvider',
    'API_ENDPOINT'
  ]

  appConfig.$inject = [
    '$httpProvider',
    '$ionicConfigProvider',
    '$qProvider',
    '$cordovaInAppBrowserProvider',
    'ionicTimePickerProvider',
    'localStorageServiceProvider',
    '$analyticsProvider',
    'NgMapProvider'
  ]

  jsonrpcConfig.$inject = [
    'jsonrpcConfigProvider'
  ]


  // Application Initialization

  angular
    .module('WalletCompanion', moduleDeps, function($ariaProvider) {
      $ariaProvider.config({
        tabindex: false
      })
    })

    /**
     * @name restangularConfig
     * @param {object} RestangularProvider TBD
     * @param {object} API_ENDPOINT TBD
     * @returns {*} undefined
     */
    .config(restangularConfig)

    /**
     * @ngdoc service
     * @name appConfig
     */
    .config(appConfig)

    /**
     * @ngdoc service
     * @name jsonrpcConfig
     */
    .config(jsonrpcConfig)

    /**
     * @ngdoc service
     * @name cuid
     */
    .factory('cuid', [
      '$window', function($window) {
        return $window.cuid;
      }
    ])

    /**
     * @ngdoc service
     * @name cuid
     */
    .factory('R', [
      '$window', function($window) {
        console.log(R);
        return $window.R;
      }
    ])

    .factory('Buffer', [
      '$window',
      function($window) {
        return window.buffer.Buffer;
      }
    ])

    .factory('EthTx', [
      '$window',
      function($window) {
        console.log('Loaded: ', $window);
        return $window.ethereumjs;
      }
    ])

    /**
     * @ngdoc service
     * @name moment
     */
    .factory('moment', ['$window', function($window) {
      return $window.moment
    }])

    /**
     * @ngdoc service
     * @name numeral
     */
    .factory('numeral', ['$window', function($window) {
      return window.numeral
    }])

    .factory('contractManager', ['web3', function(web3) {

      // {
      //   'name': 'SetMessage',
      //   'type': 'function',
      //   'inputs': [{'name': 'newMessage', 'type': 'string32'}]
      // },
      // {
      //   'name': 'Message',
      //   'type': 'function',
      //   'inputs': []
      // },

      var abi = [
        {
          'constant': true,
          'inputs': [],
          'name': 'name',
          'outputs': [{'name': '', 'type': 'string'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_spender', 'type': 'address'}, {'name': '_value', 'type': 'uint256'}],
          'name': 'approve',
          'outputs': [{'name': 'success', 'type': 'bool'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_disable', 'type': 'bool'}],
          'name': 'disableTransfers',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'totalSupply',
          'outputs': [{'name': '', 'type': 'uint256'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'MiningRewardPerETHBlock',
          'outputs': [{'name': '', 'type': 'uint256'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_from', 'type': 'address'}, {'name': '_to', 'type': 'address'}, {
            'name': '_value',
            'type': 'uint256'
          }],
          'name': 'transferFrom',
          'outputs': [{'name': 'success', 'type': 'bool'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'decimals',
          'outputs': [{'name': '', 'type': 'uint8'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_amount', 'type': 'uint256'}],
          'name': 'ChangeMiningReward',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'version',
          'outputs': [{'name': '', 'type': 'string'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'standard',
          'outputs': [{'name': '', 'type': 'string'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_token', 'type': 'address'}, {'name': '_to', 'type': 'address'}, {
            'name': '_amount',
            'type': 'uint256'
          }],
          'name': 'withdrawTokens',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [{'name': '', 'type': 'address'}],
          'name': 'balanceOf',
          'outputs': [{'name': '', 'type': 'uint256'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [],
          'name': 'acceptBuyership',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_to', 'type': 'address'}, {'name': '_amount', 'type': 'uint256'}],
          'name': 'issue',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'buyer',
          'outputs': [{'name': '', 'type': 'address'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'symbol',
          'outputs': [{'name': '', 'type': 'string'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_from', 'type': 'address'}, {'name': '_amount', 'type': 'uint256'}],
          'name': 'destroy',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_to', 'type': 'address'}, {'name': '_value', 'type': 'uint256'}],
          'name': 'transfer',
          'outputs': [{'name': 'success', 'type': 'bool'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'transfersEnabled',
          'outputs': [{'name': '', 'type': 'bool'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [],
          'name': 'TransferMinersReward',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'newBuyer',
          'outputs': [{'name': '', 'type': 'address'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [{'name': '', 'type': 'address'}, {'name': '', 'type': 'address'}],
          'name': 'allowance',
          'outputs': [{'name': '', 'type': 'uint256'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': true,
          'inputs': [],
          'name': 'lastBlockRewarded',
          'outputs': [{'name': '', 'type': 'uint256'}],
          'payable': false,
          'type': 'function'
        }, {
          'constant': false,
          'inputs': [{'name': '_newBuyer', 'type': 'address'}],
          'name': 'transferBuyership',
          'outputs': [],
          'payable': false,
          'type': 'function'
        }, {
          'inputs': [{'name': '_name', 'type': 'string'}, {'name': '_symbol', 'type': 'string'}, {
            'name': '_decimals',
            'type': 'uint8'
          }], 'payable': false, 'type': 'constructor'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': false, 'name': '_token', 'type': 'address'}],
          'name': 'MineableCoinTokenGenesis',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': false, 'name': '_amount', 'type': 'uint256'}],
          'name': 'Issuance',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': false, 'name': '_amount', 'type': 'uint256'}],
          'name': 'Destruction',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': false, 'name': '_amount', 'type': 'uint256'}],
          'name': 'MiningRewardChanged',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': true, 'name': '_from', 'type': 'address'}, {
            'indexed': true,
            'name': '_to',
            'type': 'address'
          }, {'indexed': false, 'name': '_value', 'type': 'uint256'}],
          'name': 'MiningRewardSent',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': false, 'name': '_prevBuyer', 'type': 'address'}, {
            'indexed': false,
            'name': '_newBuyer',
            'type': 'address'
          }],
          'name': 'BuyerUpdate',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': true, 'name': '_from', 'type': 'address'}, {
            'indexed': true,
            'name': '_to',
            'type': 'address'
          }, {'indexed': false, 'name': '_value', 'type': 'uint256'}],
          'name': 'Transfer',
          'type': 'event'
        }, {
          'anonymous': false,
          'inputs': [{'indexed': true, 'name': '_buyer', 'type': 'address'}, {
            'indexed': true,
            'name': '_spender',
            'type': 'address'
          }, {'indexed': false, 'name': '_value', 'type': 'uint256'}],
          'name': 'Approval',
          'type': 'event'
        }
      ];

      var _address = '0xb6ed7644c69416d67b522e20bc294a9a9b405b31';
      var contract = new web3.eth.contract(abi);

      var newContract = [
        'pragma solidity ^0.4.6;',
        'contract tank {',
          'function canon(uint a) constant returns(uint d) {',
            'return a + "a"',
          '}',
        '}'
      ].join('\n');

      //var compiled = web3.eth.compile.solidity(newContract);
      //var code = compiled.code;
      //var abi = compiled.info.abiDefinition;

      var ci;
      try {
        ci = contract.at(_address);
      } catch(e) {
        console.log(e);

      }

      web3.eth.defaultAccount = _address;

      return {
        _address                 : _address,
        _ethereum_contract_fixed : 100000000,
        ethereum_contract        : contract,
        contractInstance         : ci
      };
    }])

    .factory('Crypto', CryptoManager)

    .factory('web3', function($window) {

      var web3;
      var httpHost = 'https://api.myetherapi.com/eth';
      //var url = 'ws://socket.etherscan.io/wshandler';
      //var testUrl = 'ws://localhost:8546';
      //var _testUrl = 'http://localhost:8546';

      try {
        web3 = new window.Web3(new window.Web3.providers.HttpProvider(httpHost));

        //web3 = new window.Web3(new window.Web3.providers.HttpProvider(_rpcHost));
        //web3 = new $window.Web3(new $window.Web3.providers.HttpProvider(_rpcHost));
        //web3 = new $window.Web3($window.Web3.givenProvider || new $window.Web3.providers.WebsocketProvider(url));
      } catch(e) {
        console.log({ error: e });
      }

      console.log(web3);

      return web3;

    })

    .factory('debitCoinContract', [
      'web3',
      'contractManager',
      debitCoinContract
    ])

    .factory('keythereum', ['$window', function($window) {
      return $window.keythereum;
    }])

    .factory('ExternalApi', ExternalApiManager)

    .factory('_', ['$window', function($window) {
      return $window._;
    }])

    .factory('FW', ['$window', function($window) {
      var m = new $window.Mnemonic(128);
      return {
        PASSCODE: null,
        WALLET_ENTITY: {},
        WALLET_KEYS: {},
        WALLET_HEX: (function() {
          var h;
          try {
            //h  = m.toString();
            h  = m;
          } catch(e) {
            console.log(e);
          }
          return h;
        })()
      }
    }])

    .factory('Random', ['$window', function($window) {
      var r;
      var Random = $window.Random;

      function Mode(engine) {
        return Random.call(this, engine);
      }

      Mode.prototype = Object.create(Random.prototype);
      Mode.prototype.constructor = Mode;

      /**
       * @usage
       *
       * var randomize = new Mode();
       * console.log('Test mode');
       * var l = randomize.mode();
       * console.log( l );
       */
      Mode.prototype.mode = function() {
        var s = Math.floor(Math.random() * 10000);
        s = this.integer(0, 10000);
        return s;
      };

      try {
        r = $window.Random;
      } catch(e) {
        console.log(e);
      }

      return {
        Random: r,
        Mode: Mode
      };
    }])

    .factory('bitcore', ['$window', function($window) {
      var b;
      try {
        b = $window.bitcore;
      } catch(e) {
        console.log(e);
      }

      return b;
    }])

    .factory('Message', ['$window', function($window) {
      var m;
      try {
        m = $window.Message;
      } catch(e) {
        console.log(e);
      }

      return m;
    }]);

  ////////////

  /**
   * @name debitCoinContract
   * @param web3
   * @param contractManager
   * @returns {undefined}
   */
  function debitCoinContract(web3, contractManager) {

    if (web3.isConnected() === false) {
      return;
    }

    var abi = JSON.parse(contractManager.ethereum_contract);
    var contract = web3.eth.contract(abi);
    var sig =  contract.at(contractManager);

    return {
      abi      : abi,
      contract : contract,
      sig      : sig
    };

  }

  /**
   * @name __CryptoManager__
   */
  function __CryptoManager__($window) {
    var c;
    try {
      c = $window.CryptoJS;
    } catch(e) {
      console.log(e);
    }

    return c;
  }

  /**
   * @name__ExternalApiManager
   */
  function __ExternalApiManager__(Restangular) {
    return function(domain) {
      return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(domain);
      });
    }
  }

  /**
   * jsonrpcConfig
   *
   * @param jsonrpcConfigProvider
   * @returns {undefined}
   */
  function jsonrpcConfig(jsonrpcConfigProvider) {
    var basicAuth = '0x891033A5669274C4a87B96EF1EfC7E246c4A1CA3'
    jsonrpcConfigProvider.set({
      servers: [
        {
          name    : '1',
          url     : 'https://api.myetherapi.com/rpc',
          headers : {
            'Authorization': 'Basic ' + basicAuth
          }
        },
        {
          name    : 'mainnet',
          url     : 'https://coindaddy.io:4001/rpc',
          headers : {
            'Authorization': 'Basic ' + basicAuth
          }
        },
        {
          name    : 'testnet',
          url     : 'https://coindaddy.io:14001/rpc',
          headers : {
            'Authorization': 'Basic ' + basicAuth
          }
        }
      ]
    });
  }

  /**
   * restangularConfig
   *
   * @param RestangularProvider
   * @param API_ENDPOINT
   * @returns {undefined}
   */
  function restangularConfig(RestangularProvider, API_ENDPOINT) {

    RestangularProvider.setBaseUrl(API_ENDPOINT.host);
    RestangularProvider.setFullResponse(true);
    RestangularProvider.setDefaultHttpFields({'withCredentials': true});
    RestangularProvider.setResponseExtractor(responseExtractor);
    RestangularProvider.setRequestInterceptor(configureRemove);

    /**
     * configureRemove
     *
     * @param {object} response TBD
     * @param {object} operation TBD
     * @returns {*} undefined
     */
    function configureRemove(response, operation) {
      if (operation === 'remove') {
        return undefined;
      }

      // if (operation === 'get' && String(JSON.stringify(response)).includes('countries')) {
      //   return undefined;
      // }

      return response;
    }

    /**
     * responseExtractor
     *
     * @param {object} response TBD
     * @param {object} operation TBD
     * @returns {*} undefined
     * @description API Middleware.
     */
    function responseExtractor(response, operation) {
      if (operation === 'getList') {
        var newResponse = response.offers || response.contracts || response.coins || response.tickets;
        return newResponse
      }
      return response
    }
  }

  /**
   * appConfig
   *
   * @param $httpProvider
   * @param $ionicConfigProvider
   * @param $qProvider
   * @param $cordovaInAppBrowserProvider
   * @param ionicTimePickerProvider
   * @param localStorageServiceProvider
   * @returns {undefined}
   */
  function appConfig($httpProvider, $ionicConfigProvider, $qProvider, $cordovaInAppBrowserProvider, ionicTimePickerProvider, localStorageServiceProvider, $analyticsProvider, NgMapProvider) {

    NgMapProvider.setDefaultOptions({
      marker: {
        optimized: true
      }
    });

    $analyticsProvider.virtualPageviews(false);

    var options, timePickerObj;

    localStorageServiceProvider
      .setPrefix('tgm')
      .setNotify(true, true);

    // $ionicConfigProvider.views.transition('none')
    $ionicConfigProvider.views.swipeBackEnabled(false);

    $ionicConfigProvider.scrolling.jsScrolling(false);

    $ionicConfigProvider.platform.android.views.maxCache(5);
    $ionicConfigProvider.platform.ios.views.maxCache(5);

    options = {
      location   : 'yes',
      clearcache : 'yes',
      toolbar    : 'yes'
    };

    $cordovaInAppBrowserProvider.setDefaultOptions(options);

    $httpProvider.defaults.withCredentials = true;

    timePickerObj = {
      inputTime  : Math.round(moment().minute() / 15) * 15,
      format     : 12,
      step       : 15,
      setLabel   : 'Set',
      closeLabel : 'Close'
    };

    ionicTimePickerProvider.configTimePicker(timePickerObj);
  }
})();

