
/**
 * @ngdoc function
 * @name WalletCompanion.directive:userProfile
 * @description User profile directive.
 */
(function() {

  'use strict';

  angular.module('WalletCompanion')
    .directive('tgmUserProfile', userProfile);

  userProfile.$inject = [];

  /**
   * userProfile
   *
   * @returns {undefined}
   */
  function userProfile() {

    var directiveOpts = {
      restrict     : 'E',
      replace      : true,
      templateUrl  : 'templates/directives/profile/profile.html',
      controller   : directiveControllerFn,
      controllerAs : 'dm'
    };

    return directiveOpts;

  }

  directiveControllerFn.$inject = [
    '$scope',
    '$state',
    'ApiService'
  ];

  /**
   * directiveControllerFn
   *
   * @param $scope
   * @param $state
   * @param ApiService
   * @returns {undefined}
   */
  function directiveControllerFn($scope, $state, ApiService) {

    // ## Controller Init
    var dm = this;

    // ## Controller Defaults
    var vm = $scope.$parent.vm;

    // ## Controller Data
    vm.sectionTitle = 'Profile';

    // ## Controller Capabilities
    vm.addProfilePhoto = addProfilePhoto;

    ////////////

    /**
     * @name addProfilePhoto
     * @returns {*} undefined
     */
    function addProfilePhoto(photoID) {
      var profilePhotos = {
        photos: []
      };
      profilePhotos.photos.push(photoID);
      ApiService.users.actions(vm.user._id)('addPhoto', profilePhotos)
        .then(function(res) {
        });
    }
  }
})();
