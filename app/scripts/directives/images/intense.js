
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
  .directive('tgmIntense', [
    '$window',
    function tgmIntense($window) {

      var t;

      var dv = {
        restrict: 'A',
        scope: {
          offer: '=tgmIntenseOffer'
        },
        controller: ['$scope', '$timeout', function($scope, $timeout) {

          var vm = this;

          $scope.$watch('offer', function(newVal, oldVal) {
            // $timeout.cancel(t);
          });

          $scope.$on('figure', function($event, data) {
            // $timeout.cancel(t);
          });
        }],
        link: link
      };

      return dv;

      function link($scope, $element, $attrs, $ctrl) {

        var figure = document.querySelector('figure');

        console.log(figure);

        // angular.element(figure).off('click');

        // angular.element(figure).remove();

        var body = document.querySelector('body');

        angular.element(body).css({
          'pointer-events': 'unset'
        });

        var _$element = $element.find('.intense');

        $scope.$on('tgm:intenseOffer', function($event, data) {

        // $scope.$watch('tgmIntenseOffer', function(newVal, oldVal) {
        // $window.sentinel.on('.intense', function(_$element) {

          var a = angular.element(data);
          // a.remove();

          console.log('tgmIntenseOffer: ', data);
          var _$element = data;

          var img;

          try {
            img = _$element;

            console.log(img);

            Intense(img, { invertInteractionDirection: true });

          } catch(e) {
            console.log(e);
          }

          angular.element(_$element).on('click', function() {

            t = window.setTimeout(function() {

              var figure = document.querySelectorAll('figure');

              console.log(figure);

              try {
                figure = _.last(Array.prototype.slice.call(figure));
              } catch(e) {
                console.log(e);
                figure = [];
              }

              $scope.$emit('figure', figure);

              var oldImg = angular.element(figure).find('img');

              angular.element(oldImg).css({
                'object-fit': 'scale-down',
                'transform': 'translate3d(-25%, 0, 0)'
              });

              angular.element(oldImg).off('click');
            }, 4);
          });
        });
      }
    }
  ])



})();

