
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    .directive('tgmResizeImage', tgmResizeImage)

  tgmResizeImage.$inject = [
    '$rootScope',
    '$compile',
    '$document',
    '$timeout',
    '$ionicPopup',
    'ApiService',
    'MissiveService',
    '$ionicLoading',
    '$q',
    'MessageService',
    'Restangular',
    '$ionicGesture'
  ];

  function tgmResizeImage($rootScope, $compile, $document, $timeout, $ionicPopup, ApiService, MissiveService, $ionicLoading, $q, MessageService, Restangular, $ionicGesture) {

    var changeLoaded = undefined;
    var _loaded = false;
    var __loaded = false;
    var cachedLength = 0;
    var directiveOpts = {
      restrict: 'A',
      controllerAs: 'vm',
      controller: ['$scope', '$ionicPopup', '$ionicGesture', '$log', '$timeout', '$q', tgmResizeImageController],
      link: directiveLinkFn,
      priority: 2,
      scope: {
        offer: '='
      }
    };

    /**
     * tgmResizeImageController
     *
     * @param $scope
     * @param $ionicPopup
     * @param $ionicGesture
     * @returns {undefined}
     */
    function tgmResizeImageController($scope, $ionicPopup, $ionicGesture, $log, $timeout, $q) {

      try {
        var $element = document.querySelector('img.upload-photo-staple');
        // $ionicGesture.on('pinch', function() {
        //   $timeout(function() {
        //     console.log($scope);
        //     $scope.$apply();
        //   }, 4);
        // }, $element);
      } catch(e) {
        $log.info(e);
      }
    }

    var loaded = false;
    var style = null;

    return directiveOpts;

    ////////////

    /**
     * directiveLinkFn
     *
     * @returns {undefined}
     */
    function directiveLinkFn($scope, $element, $attrs, $ctrl) {

      var photos = $scope.offer && $scope.offer.photos;

      $scope.$on('$destroy', function() {
        loaded = false;
      });

      /**
       * b64decode
       *
       * @param str
       * @returns {undefined}
       */
      function b64decode(str) {
        return decodeURIComponent(atob(str)
          .split('')
          .map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          }).join(''));
      }

      /**
       * b64encode
       *
       * @param str
       * @returns {undefined}
       */
      function b64encode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
          return String.fromCharCode('0x' + p1);
        }));
      }

      /**
       * resizeNewImage
       *
       * @returns {undefined}
       */
      function resizeNewImage(config) {

        var deferred = $q.defer();

        $ionicLoading.show();

        _loaded = false;

        var imgRef = _.last(config.imgRef);

        var cropOffers = {
          x       : ($scope.updateX - 500),
          y       : ($scope.updateY - 250),
          gravity : 'North'
        };

        /**
         * inf
         *
         * @param x
         * @param y
         * @returns {undefined}
         */
        function inf(x, y, mode) {

          var def = $q.defer();

          var pos = function() {
            return {
              l: (x > 0),
              p: y > 0
            };
          };

          var _pos = pos();

          if (mode) {

            def.resolve('North');

            return def.promise;
          }

          if (
            _pos.p === false
          ) {
            def.resolve('South');
          } else {
            def.resolve('North');
          }

          return def.promise;
        }

        MissiveService.setTempObj({
          width : $scope.updatesize,
          top   : $scope.updateY,
          left  : $scope.updateX
        }, 'cropOffers');

        var h;

        try {
          h = parseInt($scope.updateY, 10);
        } catch(e) {
          h = 0;
          console.log(e);
        }

        var resizeOffers = {
          width: ($scope.updatesize > 0) ? $scope.updatesize : 500
        };

        ApiService.assets.action(imgRef.image._id)('resize', resizeOffers)
          .then(function(res) {

          inf($scope.updateX, $scope.updateY, 'North')
            .then(function(pos) {

              var $$element = $element[0];
              var localImageHeight = $$element.height;
              var localImageWidth = $$element.width;
              var localImageXPosition = $$element.x;
              var localImageYPosition = $$element.y;
              var localY = !_.isNaN(cropOffers.y) ? Math.abs(cropOffers.y) : 0;
              var localX = !_.isNaN(cropOffers.x) ? Math.abs(cropOffers.x) : 0;
              var updatedX = Math.abs(localX - localImageWidth - localImageXPosition);
              var updatedY = Math.abs(localY - localImageHeight);

              _.extend(cropOffers, {
                x: updatedX,
                y: updatedY,
                gravity: pos
              });

              delete cropOffers.x;

              var _cropFromSouth = {
                y: localImageYPosition,
                gravity: 'North'
              };

              var _cropFromEast = {
                x: updatedX,
                gravity: 'West'
              };

              ApiService.assets.action(res.data._id)('crop', cropOffers)
                .then(function(_res) {

                  ApiService.assets.action(_res.data._id)('crop', _cropFromEast)
                    .then(function(__res) {

                      ApiService.assets.action(__res.data._id)('crop', _cropFromSouth)
                        .then(function(outcome) {
                          var res = outcome;
                          deferred.resolve(res);
                          $ionicLoading.hide();
                          $ionicPopup.show({
                           title: 'Resize successful!',
                           subTitle: 'Try selecting another image or resizing again.',
                           buttons: [
                             {
                                text: 'OK',
                                onTap: function(e) {
                                  __loaded = false;
                                }
                              }
                            ]
                          });
                        }, function(e) {
                          deferred.reject(e)
                          $ionicLoading.hide();
                          $ionicPopup.show({
                           title: 'HTTP Error',
                           subTitle: 'There was an error in your request. Please try again.',
                           buttons: [
                             {
                                text: 'OK',
                                onTap: function(e) {
                                }
                              }
                            ]
                          });
                        }).catch(function(e) {
                          deferred.reject(e)
                          $ionicLoading.hide();
                        });
                    }, function(e) {
                      deferred.reject(e)
                      console.log(e);
                    });
                }, function(e) {
                  deferred.reject(e)
                  console.log(e);
                });
          }, function(e) {

            console.log(e);

            $ionicPopup.show({
             title: 'HTTP Error',
             subTitle: 'There was an error in your request. Please try again.',
             buttons: [
               {
                  text: 'OK',
                  onTap: function(e) {
                  }
                }
              ]
            });

            $ionicLoading.hide();

            deferred.reject(e)
          });
        });

        return deferred.promise;
      }

      $scope.$on('configMessage', configMessage);

      /**
       * configMessage
       *
       * @param $event
       * @param config
       * @returns {undefined}
       */
      function configMessage($event, config) {

        if (_loaded === true) {
          return;
        }

        $ionicPopup.show({
          title    : config.title,
          subTitle : config.subTitle,
          buttons  : [
            {
              text  : 'OK',
              onTap : function(e) {
                resizeNewImage(config).then(function(res) {
                  console.log(res);

                  var newUrl = (res && res.data) ? res.data.url : '';
                  MessageService.resetPhoto()
                  MessageService.addPhoto([Restangular.stripRestangular(res.data)])
                  $element[0].setAttribute('src', newUrl);

                  var newPhoto = MessageService.getPhoto();
                  console.log('newPhoto: ', newPhoto);
                  $scope.$emit('photo:saveEnd', newPhoto);
                  MissiveService.removeActiveObj();
                }, function(e) {
                  console.log(e);
                });
              }
            }
          ]
        });

        _loaded = true;
      }

      $scope.$on('photo:saved', function($event) {

        if (__loaded === true) {
          return;
        }

        if ($scope.imageList) {
          resizeNewImage({ imgRef: $scope.imageList })
            .then(function(res) {
              console.log(res);
              var newUrl = (res && res.data) ? res.data.url : '';
              MessageService.resetPhoto()
              MessageService.addPhoto([Restangular.stripRestangular(res.data)])
              $element[0].setAttribute('src', newUrl);

              var newPhoto = MessageService.getPhoto();
              console.log('newPhoto: ', newPhoto);
              $scope.$emit('photo:saveEnd', newPhoto);
              MissiveService.removeActiveObj();
            }, function(e) {
              console.log(e);
            });
        }

        __loaded = true;

      });

      $scope.pinchComplete = function() {
        console.log(_loaded);
      };

      $scope.$on('photo-changed', function($event, data) {

        _loaded = false;

        // if (changeLoaded === undefined) {
        //   return;
        // }

        var image = $element[0].getAttribute('src');

        var newUrl = data ? _.last(data).image.url : '';
        if (!newUrl) {
          return console.log('No url found on image');
        }

        $scope.imageList = data;

        var startX = 0,
            startY = 0,
            startW = 0,
            w = 0,
            x = 0,
            y = 0;

        var isResizing = false;

        var _img = document.createElement('img');

        _img.setAttribute('src', newUrl);

        $element[0].setAttribute('src', newUrl);

        $element[0].setAttribute('width', _img.getAttribute('width'));

        $element[0].setAttribute('prevent-modification', true);

        $rootScope.$on('resizeImage', function($event, data) {
          $element[0].setAttribute('prevent-modification', false);
        });

        $rootScope.$on('resizeFormUpdate', function($event, positionRef) {

          $scope.updatesize = positionRef.width;
          $scope.updateX = positionRef.x;
          $scope.updateY = positionRef.y;

          $element.css({
            width : (positionRef.width) + 'px',
            top   : positionRef.y + 'px',
            // left  : positionRef.x + 'px'
          });

        });

        $element.css({
          position : 'relative',
          width    : $scope.updatesize + 'px',
          top      : $scope.updateY + 'px',
          left     : $scope.updateX + 'px',
          cursor   : 'pointer'
        });

        $element.on('pinch pinchin pinchout', function($event) {

          if (isResizing) {
            $document.on('touchmove mousemove', drag);
            $document.on('touchend mouseup', dragend);
            return;
          }

          var s = $event.scale;

          startW = s * $scope.updatesize;

          if (startW < 500) {
            isResizing = true;
          }

          $document.on('pinch pinchin pinchout', pinch);
        });

        $element.on('touchstart mousedown', function($event) {

          if (isResizing) {

            $document.on('pinch pinchin pinchout', pinch);

            return;
          }

          $event.preventDefault();

          var e = $event.touches ? $event.touches[0] : $event;

          startX = e.pageX - x;
          startY = e.pageY - y;

          if (startX > 480 && startY > 270) {
            isResizing = true;
          }

          $document.on('touchmove mousemove', drag);
          $document.on('touchend mouseup', dragend);
        });

        /**
         * pinch
         *
         * @param $event
         * @returns {undefined}
         */
        function pinch($event) {

          var s = $event.scale;

          w = s * startW;

          if (isResizing) {

            $scope.updatesize = w;

            $scope.$apply();
          } else {

            $scope.updatesize = w;

            $scope.$apply();

            $element.css({
              width: w + 'px'
            });
          }

        }

        /**
         * drag
         *
         * @param event
         * @returns {undefined}
         */
        function drag($event) {

          var e = $event.touches ? $event.touches[0] : $event;

          y = e.pageY - startY;
          x = e.pageX - startX;

          if (isResizing) {

            $scope.updatesize = (x);
            $scope.$apply();
          } else {

            $scope.updatesize = (x);
            $scope.updateX = x;
            $scope.updateY = y;

            $scope.$apply();

            $element.css({
              top: y + 'px'
              // left: x + 'px'
            });
          }
        }

        /**
         * dragend
         *
         * @returns {undefined}
         */
        function dragend() {
          isResizing = false;

          $document.off('touchmove mousemove', drag);
          $document.off('touchend mouseup', dragend);
        }

        $scope.showingDragged = true;

        $scope.$emit('photo:dragend', $scope.showingDragged);

        $element[0].setAttribute('on-pinch', 'pinchComplete()');

        angular.element($element[0]).on('touchend mouseup', function($event) {

          $ionicLoading.hide();

          $scope.$broadcast('configMessage', {
            title        : 'Good to go?',
            subTitle     : 'Resize and position, then drag and Drop to finalize.',
            imgRef       : data
          });
        });

        changeLoaded = true;
      });

      if (!loaded && photos && photos.length) {
        cachedLength = photos.length;

        if (_.last(photos).image && !_.last(photos).image.error) {

          var s = photos.sort(function(a, b) {
            if (b.image && a.image)
              return (new Date(b.image.createdAt).getTime()) - (new Date(a.image.createdAt).getTime())
          }).reverse();

          if (_.keys(_.last(s).image).length) {

            var img = _.last(s).image;
            var url;

            try {
              url = img.url;
            } catch(e) {
              console.log(e);
            }

            if (!url) {
              return console.log('No url found on image');
            }

            $timeout(function() {
              loaded = true;
              $element[0].setAttribute('ng-src', url);
              $element[0].setAttribute('prevent-modification', true);
              $compile($element)($scope);
              $scope.$apply();
            }, 4);

            changeLoaded = false;
            return;
          }

          try {
            $element[0].setAttribute('ng-src', _.last(s).image[0].url);
            $element[0].setAttribute('prevent-modification', false);
            angular.element($element[0]).off('dragend');
            loaded = true;
            changeLoaded = false;
          } catch(e) {
            console.log('No url found on image', e);
          }
        }
      }
    }
  }

})();

