/**
 * @ngdoc directive
 * @name WalletCompanion.directive:tgmRead
 * @description A directive for augmenting or extending an object by a given
 * read-only style action which should only if that object has been motified.
 * @example
 * <div
 *   tgm-read
 *   listening-to="$parent.vm.reservation"
 *   type="reservations"
 *   key="getInvoice"
 *   callback="vm.callback"
 * >
 * </div>
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
    .directive('tgmRead', tgmGetInvoice);

  tgmGetInvoice.$inject = [
    '$rootScope',
    'ApiService',
    '$ionicLoading'
  ];

  function tgmGetInvoice($rootScope, ApiService, $ionicLoading) {

    var directive = {
      scope        : {
        type        : '@',
        key         : '@',
        callback    : '&'
      },
      controller   : controller,
      controllerAs : 'dm',
      link         : link
    };

    return directive;

    ////////////

    controller.$inject = [
      '$scope',
      '$rootScope',
      'ApiService'
    ];

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.directive:tgmRead
     * @name controller
     * @param {object} $scope Internal.
     * @param {object} $rootScope Internal.
     * @param {object} ApiService TBD
     * @returns {*} undefined
     */
    function controller($scope, $rootScope, ApiService) {
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.directive:tgmRead
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link($scope, $element, $attrs, $ctrl) {
      var dm = this;
      var vm = $scope.$parent.vm;
      var user = vm.user;
      $scope.$on('reservation-loaded', function($event, reservationData) {
        ApiService[$scope.type]
          .action(reservationData._id)($scope.key, {})
          .then(vm.callback);
      });
    }
  }
})();
