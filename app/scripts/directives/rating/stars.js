/**
 * link
 *
 * @param $scope
 * @param $element
 * @param $attrs
 * @param $ctrl
 * @returns {undefined}
 */
function link ($scope, $element, $attrs, $ctrl) {
  $scope.list = []

  return (t) => {
    $scope.$watch('tgmStarsList', function(newVal) {
      $scope.list.length = 0
      if (newVal) {
        for (var i = 0; i < newVal; ++i) {
          $scope.list.push({ name: 'star' })
        }
        t(function() {
          $scope.isLoaded = true
        }, 500)
      }
    })
  }
}

/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
function app ($timeout) {

  var dv = {
    link: link($timeout),
    templateUrl: 'templates/stars.html',
    scope: {
      tgmStarsList: '='
    }
  }

  return dv
}

const directive = [
  '$timeout',
  app
]

(function() {
  'use strict'

  angular
    .module('WalletCompanion')
    .directive('tgmStars', directive)
})()

