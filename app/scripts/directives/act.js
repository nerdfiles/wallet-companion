/**
 * @ngdoc directive
 * @name WalletCompanion.directive:tgmAction
 * @example
 * <div tgm-action>
 *   <button ng-click="act('reservations', 'addBed')(vm.user, vm.reservation)">
 *     Add Bed
 *   </button>
 * </div>
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
    .directive('tgmAction', tgmAction);

  tgmAction.$inject = [
    '$rootScope',
    'ActionService',
    '$ionicLoading'
  ];

  function tgmAction($rootScope, ActionService, $ionicLoading) {

    controller.$inject = [
      '$rootScope'
    ];

    function controller($rootScope) {
    }

    var directive = {
      scope        : {},
      controller   : controller,
      controllerAs : 'dm',
      link         : link
    };

    return directive;

    ////////////
    /**
     * @ngdoc method
     * @methodOf WalletCompanion.directive:tgmAction
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link($scope, $element, $attrs, $ctrl) {
      var dm = this;
      dm.act = ActionService.act;
    }
  }
})();
