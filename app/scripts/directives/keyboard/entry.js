/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmKeyboardEntry', [
      '$timeout',
      tgmKeyboardEntry
    ]);

    /**
     * @name tgmKeyboardEntry
     * @param $timeout
     * @returns {undefined}
     */
    function tgmKeyboardEntry($timeout) {

      var dv = {
        link: link,
        scope: {
          entry: '&'
        }
      };

      return dv;

      ////////////

      /**
       * link
       *
       * @param $scope
       * @param $element
       * @param $attrs
       * @param $ctrl
       * @returns {undefined}
       */
      function link($scope, $element, $attrs, $ctrl) {

        angular
          .element($element)
          .on('keypress', keypress);

        /**
         * keypress
         *
         * @returns {undefined}
         */
        function keypress(e) {

          if (e.which === 13) {

            try {

              $timeout(function() {

                var ionContent = angular.element(document.querySelector('ion-content'));
                ionContent.attr('style', null);
                $scope.entry();

              }, 4);

            } catch(e) {
              console.log(e);
            }
          }

        }
      }
    }

})();

