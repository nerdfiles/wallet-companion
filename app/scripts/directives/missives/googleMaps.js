
/**
 * @ngdoc directive
 * @name WalletCompanion.directive:filterOffers
 * @description A directive that houses a Google Maps implementation.
 * @usage
 * <google-maps></google-maps>
 */
 (function() {
   'use strict'

   angular
     .module('WalletCompanion')
     .directive('googleMaps', googleMaps)

   googleMaps.$inject = ['$compile', '$timeout', 'NgMap']

   function googleMaps($compile, $timeout, NgMap) {

     var m
     var directiveOpts = {
       restrict: 'E',
       replace: true,
       scope: true,
       templateUrl: 'templates/directives/missives/google-maps.html',
       link: directiveLinkFn,
       controllerAs: 'dm'
     }

     return directiveOpts

     ////////////

     function directiveLinkFn($scope, $element, $attrs) {

        $scope.map = {
          defaults: {
            tileLayer: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png',
            maxZoom: 14,
            minZoom: 10,
            reuseTiles: true,
            unloadInvisibleTiles: true,
            zoomControl: false,
            zoomControlPosition: 'bottomleft'
          },
          markers : [],
          styles: [
              {
                  "featureType": "administrative",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "landscape",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      },
                      {
                          "hue": "#0066ff"
                      },
                      {
                          "saturation": 74
                      },
                      {
                          "lightness": 100
                      }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "road",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      },
                      {
                          "weight": 0.6
                      },
                      {
                          "saturation": -85
                      },
                      {
                          "lightness": 61
                      }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "road.arterial",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#eaeaea"
                      }
                  ]
              },
              {
                  "featureType": "transit",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      },
                      {
                          "color": "#5f94ff"
                      },
                      {
                          "lightness": 26
                      },
                      {
                          "gamma": 5.86
                      }
                  ]
              }
          ],
          events: {
            map: {
              enable: ['context'],
              logic: 'emit'
            }
          },
          controls: {
            custom: []
          }
        }

       $scope.$on('offerModalOpened', function(e, offer) {
         initializeMap(offer)
       })

       $scope.$on('offerModalClosed', function() {
         deleteMap()
       })

       $scope.$on('$destroy', function() {
         deleteMap()
       })

       /**
        * initializeMap
        *
        * @param offer
        * @returns {undefined}
        */
       function initializeMap(offer) {

         console.log('initializing map based on following offer object: ', offer)

         var element = `<ng-map
                     id="previewMap"
                     zoom="15"
                     disable-default-u-i="true"
                     styles="map.styles"
                     map-type-control-options="[
                       'mapTypeIds': ['Styled']
                     ]"
                     map-type-id="'Styled'"
                   >
                   </ng-map>`

         $timeout(function() {

           var existingMapElement = $element[0].querySelector('ng-map')
           if (existingMapElement) {
             existingMapElement.remove()
           }

           var mapElement = angular.element(element)

           $element.append(mapElement)

           $compile($element)($scope)
         }, 4)

         $timeout(function() {
           NgMap.getMap({ id: 'previewMap' }).then(function(map) {

             var styledMapType = new google.maps.StyledMapType($scope.map.styles, {
               name: 'Styled'
             })
             map.mapTypes.set('Styled', styledMapType)

             var lat, lng

             try {
               lat = offer.geometry.position.latitude
               lng = offer.geometry.position.longitude
             } catch(e) {
               lat = offer.location.lat
               lng = offer.location.lon
             }

             m = new google.maps.Marker({
               icon: {
                 scaledSize : new google.maps.Size(21, 31),
                 size       : new google.maps.Size(21, 31),
                 origin     : new google.maps.Point(0, 0),
                 anchor     : new google.maps.Point(10, 21),
                 url        : './images/icons/iconPin@3x.png'
               }
             })

             var position = new google.maps.LatLng(lat, lng)

             $timeout(function() {

               var circle = new google.maps.Circle({
                 strokeColor   : '#0092BC',
                 strokeOpacity : 0.5,
                 strokeWeight  : 5,
                 fillOpacity   : 0,
                 map           : map,
                 center        : { lat: lat, lng: lng },
                 radius        : 130
               })

               m.setPosition(position)
               m.setMap(map)
               map.setCenter(position)

             }, 4)

             google.maps.event.trigger(map, "resize")
           })
         }, 1000)
       }

       /**
        * deleteMap
        *
        * @returns {undefined}
        */
       function deleteMap() {

         NgMap.getMap({ id: 'previewMap' })
           .then(function(map) {
             m.setMap(null)
           }, function(e) {
             console.log(e)
           })

       }
     }
   }
 })()
