
/**
 * @ngdoc directive
 * @name WalletCompanion.directive:offerDetails
 * @description Offer Detail view directive that is used inside of modal
 * windows provided by Ionic.
 */
(function() {
  'use strict'

  angular
    .module('WalletCompanion')
    .directive('offerDetails', offerDetails)

  offerDetails.$inject = ['_']

  function offerDetails(_) {
    var directiveOpts = {
      restrict     : 'E',
      replace      : true,
      scope        : true,
      templateUrl  : 'templates/directives/missives/offer-details.html',
      controller   : directiveControllerFn,
      link         : link,
      controllerAs : 'dm'
    }

    return directiveOpts

    ////////////

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attrs
     * @param $ctrl
     * @returns {undefined}
     */
    function link($scope, $element, $attrs, $ctrl) {
      $scope._ = _
    }
  }

  directiveControllerFn.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    'moment',
    '$log'
  ]

  /**
   * directiveControllerFn
   *
   * @param $scope
   * @param $state
   * @param $stateParams
   * @returns {undefined}
   */
  function directiveControllerFn($scope, $state, $stateParams, moment, $log) {
    var dm = this
    var vm = $scope.$parent.vm
    $scope.moment = moment
    $scope.$log = $log
  }
})()

