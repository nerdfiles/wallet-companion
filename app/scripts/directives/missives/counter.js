(function() {

  'use strict';

  angular
    .module('WalletCompanion')
   .directive('tgmCounter', tgmCounter);

   tgmCounter.$inject = ['$timeout'];

   /**
    * tgmCounter
    *
    * @param $timeout
    * @returns {undefined}
    */
   function tgmCounter($timeout) {

     var directive = {
       link: link,
       templateUrl: 'templates/directives/max-label.html',
       scope: {
         counterContent: '=',
         formContainer: '='
       }
     };

     return directive;

     ////////////////

     /**
      * link
      *
      * @param $scope
      * @param $element
      * @param $attrs
      * @returns {undefined}
      */
     function link($scope, $element, $attrs) {
       $scope.max = 500;
       $scope.content = 'Description';
       $scope.$watch('counterContent', function(newVal, oldVal) {

         var _newVal = [];

         if (newVal) {

           try {
             _newVal = _newVal.concat(newVal.split(''));
           } catch(e) {

             console.log({ error: e });
           } finally {

             $timeout(function() {

               try {
                 if (_newVal.length > $scope.max) {
                   $scope.formContainer.description.$setValidity('max-val', false);
                 } else {
                   $scope.formContainer.description.$setValidity('max-val', true);
                 }
               } catch(e) {

                 console.log({ error: e });
               }

               $scope.size = (_newVal.length).toString();
               $scope.$apply();
             }, 4);
           }

         }
       });
     }
   }

}());

