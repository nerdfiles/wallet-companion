// ./app/directives/missives/filterOffers.js
/**
 * @ngdoc function
 * @name WalletCompanion.directive:filterOffers
 * @description
 * # missives
 */

(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('filterOffers', filterOffersDirective);

  filterOffersDirective.$inject = [
    '$compile',
    '$timeout',
    'MessageService',
    '$ionicPlatform',
    'ScriptLoader',
    'NgMap',
    '$window'
  ];

  function filterOffersDirective($compile, $timeout, MessageService, $ionicPlatform, ScriptLoader, NgMap, $window) {

    var directiveOpts = {
      restrict: 'E',
      scope: true,
      replace: true,
      templateUrl: 'templates/directives/missives/filter-offers.html',
      link: link,
      controller: controller,
      controllerAs: 'dm'
    };

    return directiveOpts;

    ////////////

    /**
     * @name link
     * @param scope
     * @param el
     * @param attrs
     * @returns {undefined}
     */
    function link(scope, el, attrs) {

      if ( !ScriptLoader.isLoaded() ) {
        scope.$on('scriptsLoaded', function() {
          initializePlacesInput();
        });
      } else {
        initializePlacesInput();
      }

      /**
       * @name initializePlacesInput
       * @returns {undefined}
       */
      function initializePlacesInput() {
        $ionicPlatform.ready(function() {
          $timeout(function() {
            var savedFilter = MessageService.getSavedFilter();
            var value;
            var input;
            input = angular.element(el[0].querySelector('#autocomplete'));
            input.attr('google-autocomplete-suggestion', '');
            input.attr('ng-model-options', '{ debounce: 250 }');
            if (savedFilter) {
              console.log(savedFilter);
              try {
                value = savedFilter.offerFilter.location.address;
              } catch(e) {
                value = '';
                console.log(e);
              }
              input.attr('value', value)
            }
            $compile(input)(scope);
          }, 4);
        });
      }
    }
  }

  controller.$inject = [
    '$scope',
    '$ionicPlatform',
    '$cordovaGeolocation',
    'MessageService',
    '$timeout',
    'ApiService',
    'Restangular',
    '$ionicPopup',
    '$q',
    '$window',
    '$ionicLoading'
  ];

  function controller($scope, $ionicPlatform, $cordovaGeolocation, MessageService, $timeout, ApiService, Restangular, $ionicPopup, $q, $window, $ionicLoading) {

    var dm = this;
    var vm = $scope.$parent.vm;

    dm.filterOpts = vm.currentFilters;

    console.log(dm);

    dm.canLoadMore = true;
    dm.currentHotelResultPage = 1;

    dm.coinTypesExpanded = false;

    dm.sliderOpts = {
      maxRadius: {
        floor: 1,
        ceil: 50,
        showSelectionBar: true
      }
    };

    dm.submitForm = submitForm;

    /**
     * resetFilterOpts
     *
     * @returns {undefined}
     */
    dm.resetFilterOpts = function() {
      dm.filterOpts.calendar.dates.from = '';
      dm.filterOpts.calendar.dates.to = '';
      dm.filterOpts.location.address = '';
      dm.filterOpts.location.geo = {};
      dm.filterOpts.location.useCurrent = true;
      dm.filterOpts.location.withinDistance = 10;
      dm.canLoadMore = true;
      dm.currentHotelResultPage = 1;
      _.each(dm.filterOpts.offer.coinTypes, function(coinTypeRef) {
        coinTypeRef.selected = false;
      });
      delete vm.selectedFrom;
      delete vm.selectedTo;
      delete vm.calendarControl.selectedDate;
    };

    /**
     * toggleBedTypes
     *
     * @returns {undefined}
     */
    dm.toggleCoinTypes = function() {
      dm.coinTypesExpanded = !dm.coinTypesExpanded;
    };

    /**
     * checkFormattedAddress
     *
     * @param f
     * @returns {undefined}
     */
    function checkFormattedAddress(f) {
      if (typeof f !== 'string') {
        return false;
      }
      var _f = f.split(' ');
      if (_f.length > 3) {
        return true;
      } else {
        return false;
      }
    }


    /**
     * onAddressSelection
     *
     * @param location
     * @returns {undefined}
     */
    dm.onAddressSelection = function(location) {

      var savedFilter = MessageService.getSavedFilter();

      if (savedFilter) {
        console.log('Saved filter: ', savedFilter);
      }

      if (dm.filterOpts.location && dm.filterOpts.location.address) {
        dm.filterOpts.location.address = '';
      }

      dm.filterOpts.location.address = location.formatted_address;

      dm.filterOpts.location.geo.coords = [
        location.geometry.location.lat(),
        location.geometry.location.lng()
      ];

      // if (!checkFormattedAddress(location.formatted_address)) {
      //   dm.filterOpts.location.address = '';
      //   dm.filterOpts.location.placeholder = 'Try a street address...';
      //   dm.filterForm.$setPristine(true);
      // }


      var savedAddress = MessageService.setSavedAddress({
        formatted_address: location.formatted_address
      });

      MessageService.setSavedFilter(dm.filterOpts)
        .then(function() {
          if (checkFormattedAddress(location.formatted_address)) {
            $scope.$emit('location-loaded', location);
            $scope.oldLocation = location.formatted_address;
          }
        }, angular.noop);

    };

    /**
     * submitForm
     *
     * @param opts
     * @returns {undefined}
     */
    function submitForm(opts) {

      vm.currentFilters = opts;
      vm.offers = [];
      vm.hotels = [];
      $scope.$emit('loading:start');

      console.log(dm);

      dm.canLoadMore = true;
      dm.currentHotelResultPage = 1;

      var payload;

      $ionicPlatform.ready(function() {

        var options = {
          enableHighAccuracy : true,
          timeout            : 5000,
          maximumAge         : 0
        };

        $cordovaGeolocation.getCurrentPosition(options)
          .then(function(data) {

            try {
              if (opts && opts.location && opts.location.useCurrent && data) {
                opts.location.geo.coords = [
                  data.coords.latitude,
                  data.coords.longitude
                ];
              }
            } catch(e) {
              console.log('Unable to load coords')
            }

            payload = MessageService.getFilterObj(opts);

            try {
              payload.formatted_address = opts.location.formatted_address;
              payload.location.address = opts.location.formatted_address;
            } catch(e) {
              console.log(e);
            }

            MessageService.setSavedFilter(payload)
              .then(function(res) {
                console.log('Saved filter', res);
                loadData(res);
              }, function(e) {
                console.log(e);
              });

          }, function(e) {

            try {
              payload.formatted_address = opts.location.formatted_address;
              payload.location.address = opts.location.formatted_address;
            } catch(e) {
              console.log(e);
            }


            payload = MessageService.getFilterObj(opts);

            MessageService.setSavedFilter(payload)
              .then(function(res) {
                console.log('Saved filter', res);
                loadData(res);
              }, function(e) {
                console.log(e);
              });
          });
      });

      /////////////

      /**
       * loadData
       *
       * @param payload
       * @returns {undefined}
       */
      function loadData(payload) {

        var s = MessageService.getSavedFilter();

        var override = {
        };

        var _p = s ? s : payload;

        if ($scope.oldLocation !== _p.offerFilter.location.address) {
          _.extend(override, {
            limit: 20,
            offset: 0
          });
        }

        $scope.currentHotelResultPage = 0;

        MessageService.getFilteredOffers(_p)
         .then(function(offers) {

           offers.forEach(function(offer) {
             if (offer.coins) {

               offer.coins.forEach(function(coin) {
                 coin.availability.from = moment(coin.availability.from);
                 coin.availability.to = moment(coin.availability.to);
                 coin.selected = false;
               });

               offer.coins = {
                 single: offer.coins.filter(function(coin) {
                   return (coin.coinType === 'single');
                 }),
                 bunk: offer.coins.filter(function(coin) {
                   return (coin.coinType === 'bunk');
                 })
               };
             }
           });

           vm.offers = offers;

           // NOTE: Use event emitter to trigger the ionicLoading spinner
           vm.filterOffersModal.hide();

           var from = moment().format('YYYY-MM-DD'), to = moment().add(1, 'days').format('YYYY-MM-DD');

           if (
             payload
             && payload.coinFilter
             && payload.coinFilter.availability
             && _.keys(payload.coinFilter.availability).length
           ) {
             from = payload.coinFilter.availability.from;
             to = payload.coinFilter.availability.to;
           }

           ApiService.hotels.list({
             limit  : override.limit,
             offset : override.offset,
             lat    : payload.offerFilter.location.coordinates[0],
             lon    : payload.offerFilter.location.coordinates[1],
             from   : from,
             to     : to
           }).then(function(res) {

             var hotels = Restangular.stripRestangular(res.data);

             if (!_.size(hotels)) {

               $scope.$emit('ready:dataHotels', {
                 data : res,
                 type : 'loading hotels'
               });

               return console.log('No hotels found: ', res);
             }

             dm.canLoadMore = true;
             _.each(hotels, function(hotel) {

               if (hotel.rooms) {

                 hotel.rooms.forEach(function(room) {

                   room.availability = {};

                   try {

                     room.availability.from = (
                       opts
                       && opts.calendar
                       && opts.calendar.dates
                       && opts.calendar.dates.from
                     )
                       ? moment(opts.calendar.dates.from).format('YYYY-MM-DD')
                       : moment().format('YYYY-MM-DD');

                   } catch(e) {
                     console.log(e);
                     // $ionicLoading.hide();
                   }

                   try {

                     room.availability.to = (
                       opts
                       && opts.calendar
                       && opts.calendar.dates
                       && opts.calendar.dates.to
                     )
                       ? moment(opts.calendar.dates.to).format('YYYY-MM-DD')
                       : moment().add(1, 'days').format('YYYY-MM-DD');

                   } catch(e) {
                     $ionicLoading.hide();
                     console.log(e);
                   }

                   room.selected = false;
                 });

                 hotel.rooms = {
                   single: hotel.rooms.filter(function(roomRef) {
                     return true;
                   })
                 };

                 console.log(hotel.rooms);
               } else {
                 console.log('No hotel rooms found');
               }
             });

             _.each(hotels, function(offer) {

               offer.rooms = {
                 single: _.map(offer.rooms, function(roomRef) {
                   return roomRef[0];
                 })
               };
             });

             $scope.$emit('ready:dataHotels', {
               data: hotels,
               type: 'loading hotels'
             });

           }, function(e) {
             $scope.$emit('ready:failed', {
               tag: 'hotels',
               error: e });
           });

         }, function(e) {
           $scope.$emit('ready:failed', {
             tag: 'getFilteredOffers',
             error: e });
         })
         .catch(function(e) {
           $scope.$emit('ready:failed', {
             tag: 'getFilteredOffers',
             error: e });
         });
      }
    }

    /**
     * @name expandCalendar
     * @param method
     * @returns {undefined}
     */
    dm.expandCalendar = function(method) {
      vm.calendarControl.method = method;
      vm.calendarModal.show();
    };
  }
})();

