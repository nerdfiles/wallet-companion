/**
 * @ngdoc function
 * @name WalletCompanion.directive:hotels
 * @description
 * # hotels
 */
(function(cuid) {

  'use strict'

  angular.module('WalletCompanion')
    .directive('hotels', hotelsDirective)

  hotelsDirective.$inject = []

  function hotelsDirective() {

    var directiveOpts = {
      restrict     : 'E',
      replace      : true,
      scope        : true,
      templateUrl  : 'templates/directives/missives/hotels.html',
      controller   : directiveControllerFn,
      controllerAs : 'dm'
    }

    return directiveOpts

    //////////////

    directiveControllerFn.$inject = [
      '$scope',
      '$state',
      '$timeout',
      '$cordovaGeolocation',
      '$ionicPlatform',
      'MessageService',
      'ApiService',
      'Restangular',
      '$log',
      '$window',
      '$q'
    ]

    function directiveControllerFn($scope, $state, $timeout, $cordovaGeolocation, $ionicPlatform, MessageService, ApiService, Restangular, $log, $window, $q) {

      // ## Controller Init

      var dm = this
      var vm = $scope.$parent.vm

      // ## Controller Data

      if (!vm.hotels) {
        vm.hotels = []
      }

      // ## Controller Defaults

      vm.opts = {}
      vm.filterOpts = {}
      $scope.loading = true
      dm.canLoadMore = true
      $scope.noMoreItemsAvailable = false
      $scope.currentPage = 0

      // ## Controller Capabilities

      dm.currentHotelResultPage = 1
      $scope.currentHotelResultPage = 0
      $scope.$log = $log
      $scope.loadMore = loadMore
      vm.loadHotels = loadHotels
      vm.loadNextPage = loadNextPage

      // ## Controller Events

      $scope.$on('httpErrorMessage', function($event, data) {
        $scope.loading = false
        // dm.canLoadMore = false
      })

      vm.loadHotels()

      /////////////

      /**
       * @name loadMore
       * @returns {undefined}
       */
      function loadMore() {

        $scope.loading = true

        if ($scope.initialized !== true) {
          console.log('$scope is not initialized.')
          return
        }

        if ($scope.currentPage === 5) {
          $scope.$broadcast('scroll.infiniteScrollComplete')
          $scope.noMoreItemsAvailable = true
          return
        }

        var res = MessageService.getSavedFilter()
        var from, to

        if (
          res &&
          res.coinFilter &&
          res.coinFilter.availability &&
          _.keys(res.coinFilter.availability).length
        ) {
          from = res.coinFilter.availability.from
          to = res.coinFilter.availability.to
        }

        var latLon = {}

        try {
          _.extend(latLon, {
            lat    : res.offerFilter.location.coordinates[0],
            lon    : res.offerFilter.location.coordinates[1]
          })
        } catch(e) {
          console.log(e)
          _.extend(latLon, {
            error: e
          })
        }

        // if (res !== undefined && res !== false) {
        //   try {
        //     latLon = {
        //       lat    : res.offerFilter.location.coordinates[0],
        //       lon    : res.offerFilter.location.coordinates[1]
        //     }
        //   } catch(e) {
        //     console.log(e)
        //     latLon = {
        //       error: e
        //     }
        //   }
        // } else {
        //   latLon = {
        //     lat: 0,
        //     lon: 0
        //   }
        // }

        var _latLon = _.extend({}, latLon)

        var metadataControls = _.extend({
          limit  : 20,
          offset : $scope.currentPage,
          from   : from ? from : moment().format('YYYY-MM-DD'),
          to     : to ? to : moment().add(1, 'days').format('YYYY-MM-DD')
        }, _latLon)

        ApiService.hotels
          .list(metadataControls)
          .then(function(res) {

            var items

            dm.canLoadMore = true
            $timeout(function() {
              // $scope.$broadcast('scroll.infiniteScrollComplete')
              // $scope.currentPage += 1
              items = Restangular.stripRestangular(res.data)
              vm.hotels = vm.hotels.concat(items)
              try {
                vm.hotels = _.uniqBy(vm.hotels, 'id')
              } catch(e) {
                vm.hotels = _.uniqBy(vm.hotels, '_cid')
              }
              $scope.loading = false
              $scope.$apply()
            }, 4)

          }, function(e) {
            console.log(e)
            // dm.canLoadMore = false
            $timeout(function() {
              $scope.loading = false
              $scope.$apply()
            }, 4)
          })
      }

      /**
       * @function loadHotels
       * @param page
       * @param data
       * @returns {undefined}
       * @inner
       */
      function loadHotels(page, data) {
        var type = 'loading hotels'
        $ionicPlatform.ready(function() {
          $scope.$emit('loading:start')
          init(page).then(function(res) {
            var hotels = res.data
            $scope.$emit('ready:dataHotels', {
              data: hotels,
              type: type
            })
            $scope.loading = false
            $scope.$broadcast('scroll.infiniteScrollComplete')
            $scope.initialized = true
          }, function(e) {
            $scope.loading = false
            $scope.$broadcast('scroll.infiniteScrollComplete')
            $scope.initialized = true

          })
        })
      }

      /**
       * @name loadNextPage
       * @returns {undefined}
       */
      function loadNextPage() {
        if ( $scope.currentPage <= 4 ) {
          $scope.currentPage += 1
          vm.loadHotels($scope.currentPage)
        }
      }

      // function loadNextPage() {
      //   var nextPage = parseInt($scope.currentHotelResultPage, 10) + 1
      //   $scope.currentHotelResultPage = nextPage
      //   console.log(nextPage)
      //   vm.loadHotels(nextPage)
      // }

      /**
       * @function init
       * @returns {undefined}
       * @inner
       */
      function init(page) {

        var deferred = $q.defer()
        var oldPage

        // if (vm.hotels && vm.hotels.length) {
        //   vm.hotels.length = 0
        // }

        var options = {
          enableHighAccuracy : true,
          timeout            : 5000,
          maximumAge         : 0,
          priority           : 102
        }

        $cordovaGeolocation.getCurrentPosition(options)
           .then(function(pos) {

             var res

             if (
               vm.currentFilters && 
               vm.currentFilters.location &&
               vm.currentFilters.location.useCurrent === false
             ) {
               try {
                 vm.currentFilters.location.geo = _.extend({}, {
                   coords: [
                     vm.currentFilters.location.coordinates[0],
                     vm.currentFilters.location.coordinates[1],
                   ]
                 })
               } catch(e) {
                 console.log(e)
               }
             } else {
               vm.currentFilters.location.geo = _.extend({}, {
                 coords: [
                   pos.coords.latitude,
                   pos.coords.longitude
                 ]
               })
             }

             try {
               $scope.filterOpts = MessageService.getFilterObj(vm.currentFilters)
             } catch(e) {
               console.log(e)
             }

             $scope.filterOpts = _.extend($scope.filterOpts, {
               limit  : 20,
               offset : 0
             })

             try {
               res = ApiService.hotels.list($scope.filterOpts, page)
               // res = ApiService.hotels.list($scope.filterOpts, dm.currentHotelResultPage)
               // res = ApiService.hotels.list($scope.filterOpts, $scope.currentHotelResultPage)
             } catch(e) {
               console.log(e)
             }

             return res

           }, function(e) {

             $timeout(function() {
               deferred.reject(e)
               $scope.loading = false
               $scope.$apply()
             }, 4)

             return e
           })
           .catch(function(e) {

             $timeout(function() {
               deferred.reject(e)
               $scope.loading = false
               $scope.$apply()
             }, 4)
           })
           .then(function(hotelOffers) {

             if (!hotelOffers || !hotelOffers.data) {
               return deferred.reject(hotelOffers)
             }

             oldPage = page

             console.log(hotelOffers)

             vm.updatedHotels = Restangular.stripRestangular(hotelOffers.data)
             if (vm.hotels) {
               vm.updatedHotels = vm.updatedHotels.concat(vm.hotels)
             }
             if (vm.hotels && vm.hotels.length === vm.updatedHotels.length) {
               $scope.impasse = true
             }

             try {
               var tmpData = Restangular.stripRestangular(hotelOffers.data)
               vm.hotels = tmpData.reduceRight(function(index, item) {
                 index.unshift(item)
                 return index
               }, vm.hotels)
             } catch(e) {
               console.log(e)
               deferred.reject(e)
             }

             console.log('hotel offers returned from server: ', vm.hotels)

             if (!vm.hotels || (vm.hotels && !vm.hotels.length)) {

               $timeout(function() {
                 deferred.reject({ error: 'No hotels found' })
                 $scope.loading = false
                 $scope.$apply()
               }, 4)

               return console.log('No hotels found')
             }

             vm.hotels.forEach(function(offer) {

               if (offer.rooms && !offer.rooms.length) {
                 return console.log('No rooms available')
               }

               if (offer && offer.rooms) {

                 try {
                   offer.rooms.forEach(function(room) {
                     room.availability = {}
                     room.availability.from = ($scope.filterOpts.coinFilter.availability &&
                       $scope.filterOpts.coinFilter.availability.from) ?
                         moment($scope.filterOpts.coinFilter.availability.from).format('YYYY-MM-DD') :
                         moment().format('YYYY-MM-DD')
                     room.availability.to = ($scope.filterOpts.coinFilter.availability &&
                       $scope.filterOpts.coinFilter.availability.to) ?
                         moment($scope.filterOpts.coinFilter.availability.to).format('YYYY-MM-DD') :
                         moment().add(1, 'days').format('YYYY-MM-DD')
                     room.selected = false
                   })
                 } catch(e) {
                   console.log(e)
                 }

                 offer.rooms = {
                   single: offer.rooms && offer.rooms.filter(function(roomRef) {
                     var test = /(T|t)win|(S|s)uperior|(S|s)tandard|(Q|q)ueen|(T|t)tudio|(S|s)uite|(K|k)ing|(S|s)ingle|(D|d)ouble/
                     var validate = test.test(roomRef.desc)
                     return (validate)
                   }),
                   bunk: offer.rooms && offer.rooms.filter(function(roomRef) {
                     var test = /(S|s)hared/
                     var validate = test.test(roomRef.desc)
                     return (validate)
                   })
                 }
               }
             })

             vm.hotels.forEach(function(offer) {
               offer.rooms = {
                 single: _.map(offer.rooms, function(roomRef) {
                   roomRef[0]._cid = cuid()
                   console.log(roomRef[0])
                   return roomRef[0]
                 }),
                 bunk: _.filter(_.map(offer.rooms, function(roomRef) {
                   if ( false )
                   {
                     roomRef[0]._cid = cuid()
                     return roomRef[0]
                   }
                 }))
               }
             })

             $timeout(function() {

               vm.hotels.forEach(function(offer) {
                 offer.rooms = {
                   single: _.map(offer.rooms.single, function(roomRef) {
                     roomRef._cid = cuid()
                     var test = /(T|t)win|(S|s)uperior|(S|s)tandard|(Q|q)ueen|(T|t)tudio|(S|s)uite|(K|k)ing|(S|s)ingle|(D|d)ouble/
                     var validate
                     try {
                       validate = test.test(roomRef.desc)
                     } catch(e) {
                       validate = false
                     }
                     if (validate === true) {
                       return roomRef
                     } else {
                       return null
                     }
                   }),
                   bunk: _.map(offer.rooms.bunk, function(roomRef) {
                     roomRef._cid = cuid()
                     var test = /(S|s)hared/
                     var validate
                     try {
                       validate = test.test(roomRef.desc)
                     } catch(e) {
                       validate = false
                     }
                     if (validate === true) {
                       return roomRef
                     } else {
                       return null
                     }
                   })
                 }
               })

               if (
                 hotelOffers && 
                 hotelOffers.data &&
                 hotelOffers.data.length
               ) {
                 // console.log(vm.hotels.rooms.single.length)
                 deferred.resolve(hotelOffers)
               } else {
                 console.log(vm.hotels)
                 if (
                   hotelOffers && 
                   hotelOffers.data &&
                   !hotelOffers.data.length
                 ) {
                   deferred.reject({
                     error: 'no_hotels_found'
                   })
                   vm.canLoadMore = false
                 }
               }

             }, 4)

           }, function(e) {

             $timeout(function() {
               deferred.reject(e)
               $scope.loading = false
               $scope.$apply()
             }, 4)
           }).catch(function(e) {

             $timeout(function() {
               deferred.reject(e)
               $scope.loading = false
               $scope.$apply()
             }, 4)
           })

        return deferred.promise
      }
    }
  }
})(window.cuid)

