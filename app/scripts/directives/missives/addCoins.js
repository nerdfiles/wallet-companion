/**
 * @ngdoc function
 * @name WalletCompanion.directive:addCoins
 * @description
 * # addCoins
 */
 (function() {

  'use strict';

  angular
   .module('WalletCompanion')
   .directive('addCoins', addCoins);

   addCoins.$inject = [];

   directiveControllerFn.$inject = [
     '$scope',
     '$rootScope',
     '$state',
     'ionicTimePicker',
     '$ionicPopup',
     'cuid'
   ];

   /**
    * addCoins
    *
    * @returns {undefined}
    */
   function addCoins() {

     var directiveOpts = {
       restrict     : 'E',
       replace      : true,
       scope        : true,
       templateUrl  : 'templates/directives/missives/add-coins.html',
       controller   : directiveControllerFn,
       controllerAs : 'dm'
     };

     return directiveOpts;

     ////////////

   }

  /**
   * directiveControllerFn
   *
   * @param $scope
   * @param $rootScope
   * @param $state
   * @param ionicTimePicker
   * @param $ionicPopup
   * @returns {undefined}
   */
  function directiveControllerFn($scope, $rootScope, $state, ionicTimePicker, $ionicPopup, cuid) {

    var dm = this;
    var vm = $scope.$parent.vm;
    dm.newCoinFormObject = {};

    $rootScope.$on('newCoin', function($event, privacy) {
     dm.newCoinFormObject = {};
     dm.newCoinFormObject.coinType = privacy;
     dm.newCoinFormObject.tokenType = privacy ? 'erc721' : 'erc20';
    });

    $rootScope.$on('loadCoin', function($event, data) {
     dm.newCoinFormObject = data;
    });

    dm.coinHandler = {
     currentSelection: null
    };

    /**
    * switchBathType
    *
    * @returns {undefined}
    */
    dm.switchCoinType = function(val) {
     dm.newCoinFormObject.coinType = val;
    };

    /**
    * submitForm
    *
    * @returns {undefined}
    */
    dm.submitForm = function() {

     var form = dm.newCoinFormObject;

     dm.newCoinFormObject = {};

     coinDispatcher(form);

     function coinDispatcher(coin) {
       coin.localId = cuid();
       var privacy = (coin.tokenType !== '' ? 'erc721' : 'erc20');
       var queryString =  coin.coinType + '-' + privacy;
       var privacyObj = {
         shortName: coin.coinType,
         queryString: queryString
       };

       $rootScope.$broadcast('addNewCoinSelection', privacyObj, coin);

       vm.coinManagementModal.hide();
     }

    };

    /**
    * expandCalendar
    *
    * @param method
    * @returns {undefined}
    */
    dm.expandCalendar = function(method) {
     vm.calendarControl.method = method;
     // vm.calendarControl.selectedDate = '';

     // $scope.$emit('resetCalendar');
     vm.calendarModal.show();
    };

    /**
    * hideCalendar
    *
    * @param mode
    * @returns {undefined}
    */
    dm.hideCalendar = function(mode) {
     vm.calendarControl.method = '';
     // vm.calendarControl.selectedDate = '';
     vm.calendarModal.hide();
    };

    /**
    * expandTimepicker
    *
    * @param mode
    * @returns {undefined}
    */
    dm.expandTimepicker = function(mode) {

     if (typeof dm.newCoinFormObject.availability === 'undefined') {
       dm.newCoinFormObject.availability = {};
     }

     var ipObj1 = {
      callback: function (val) {
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);

          function addZero(i) {
            if (i < 10) {
              i = '0' + i;
            } else if (i === 1) {
              i = '00';
            }
            return i;
          };

          dm.newCoinFormObject.availability[mode] = addZero(selectedTime.getUTCHours()) + ':' + addZero(selectedTime.getUTCMinutes());
        }
      }
    };

    ionicTimePicker.openTimePicker(ipObj1);
    };

    $scope.$on('coinTypePrivacySelected', function(e, privacy) {
     dm.newCoinFormObject.coinType = privacy;
     dm.newCoinFormObject.tokenType = privacy ? 'erc721' : 'erc20';
    });

    $scope.$on('configError', configError);

    function configError($event, config) {
      $ionicPopup.show({
        title: config.title,
        subTitle: config.subTitle,
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
            }
          }
        ]
     });
    }

    $scope.$on('calendarSelection', function(e, val) {
     dm.newCoinFormObject.availability = _.extend({}, dm.newCoinFormObject.availability);
     val = moment(val);

     dm.newCoinFormObject.availability[vm.calendarControl.method] = val;
     var invalid = false;

     if (dm.newCoinFormObject.availability.to && dm.newCoinFormObject.availability.from) {

       if (moment(dm.newCoinFormObject.availability.to).isBefore(moment(dm.newCoinFormObject.availability.from))) {
         dm.newCoinFormObject.availability.to = '';

         $scope.$broadcast('configError', {
           title: 'Invalid Date',
           subTitle: 'Please select a checkout date after the checkin date.'
         });

         invalid = true;
       }

       if (moment(dm.newCoinFormObject.availability.from).isAfter(moment(dm.newCoinFormObject.availability.to))) {
         dm.newCoinFormObject.availability.from = '';

         $scope.$broadcast('configError', {
           title: 'Invalid Date',
           subTitle: 'Please select a checkin date before the checkout date.'
         });

         invalid = true;
       }

       if (!invalid) {
         vm.calendarModal.hide();
       }
     } else {
       vm.calendarModal.hide();
     }
    });
  }
})();
