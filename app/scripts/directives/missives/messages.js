/**
 * @ngdoc function
 * @name WalletCompanion.directive:messages
 * @description
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('messages', messages);

  messages.$inject = [];

  controller.$inject = [
    '$scope',
    '$state',
    '$timeout',
    '$cordovaGeolocation',
    '$ionicLoading',
    '$ionicPlatform',
    'MessageService',
    '$window',
    '$q',
    'GeoService',
    '$ionicPopup'
  ];

  /**
   * messages
   *
   * @returns {undefined}
   */
  function messages() {

    var directiveOpts = {
      restrict     : 'E',
      replace      : true,
      scope        : true,
      templateUrl  : 'templates/directives/missives/messages.html',
      controller   : controller,
      controllerAs : 'dm'
    };

    return directiveOpts;

    //////////////
  }

  /**
   * controller
   *
   * @param $scope
   * @param $state
   * @param $timeout
   * @param $cordovaGeolocation
   * @param $ionicLoading
   * @param $ionicPlatform
   * @param MessageService
   * @param $window
   * @param $q
   * @param GeoService
   * @returns {undefined}
   */
  function controller($scope, $state, $timeout, $cordovaGeolocation, $ionicLoading, $ionicPlatform, MessageService, $window, $q, GeoService, $ionicPopup) {

    var dm = this;

    var vm = $scope.$parent.vm;

    // ## Controller Capabilities

    vm.loadOffers = loadOffers;

    // ## Controller Events

    // Imagine that this is like an `ng-init`, but since we're lazy we're
    // going to keep it inside the controller. ¯\(°_o)/¯
    vm.loading = true;
    vm.loadOffers();

    $scope.$on('httpErrorMessage', function($event, data) {
      console.log(data);
      vm.loading = false;
    });

    $scope.$on('loadOffers', function($event, data) {
      if (data.refresh) {
        loadOffers();
      }
    });

    ////////////

    /**
     * @function loadOffers
     * @returns {undefined}
     * @inner
     */
    function loadOffers() {

      var type = 'Loading offers';
      $ionicPlatform.ready(function() {
        init().then(function(res) {
          console.log(type);
          vm.loading = false;
          $scope.$emit('ready:data', type);
        }, function(e) {
          console.log(type);
          vm.loading = false;
          $scope.$emit('ready:failed', type);
        });
      });
    }

    /**
     * @function init
     * @returns {undefined}
     * @inner
     */
    function init() {

      var def = $q.defer();

      if (vm.offers.length) {
        vm.offers.length = 0;
      }

      var options = {
        enableHighAccuracy : true,
        timeout            : 5000,
        maximumAge         : 0,
        priority           : 102
      };

      $cordovaGeolocation
        .getCurrentPosition(options)
        .then(function(pos) {

          if (
            vm.currentFilters &&
            vm.currentFilters.location &&
            vm.currentFilters.location.useCurrent === false
          ) {
            try {
              vm.currentFilters.location.geo = _.extend({}, {
                coords: [
                  vm.currentFilters.location.coordinates[0],
                  vm.currentFilters.location.coordinates[1],
                ]
              })
            } catch(e) {
              console.log('Position metadata:', pos)
              console.log('Error: ', e)
            }
          } else {
            try {
              vm.currentFilters.location.geo = _.extend({}, {
                coords: [
                  pos.coords.latitude,
                  pos.coords.longitude
                ]
              });
            } catch(e) {
              console.log('Position metadata: ', pos);
              console.log('Error: ', e);
            }
          }

          $scope.filterOpts = MessageService.getFilterObj(vm.currentFilters);

          MessageService
            .setSavedFilter($scope.filterOpts)
            .then(angular.noop, angular.noop);

          return MessageService.getFilteredOffers($scope.filterOpts);
        }, function(e) {

          def.reject({ error: 'no_location' });

          $timeout(function() {
            vm.loading = false;
            $scope.$apply();
          }, 4);

          return e
        })
        .catch(function(e) {

          def.reject({ error: 'no_location' });

          $timeout(function() {
            vm.loading = false;
            $scope.$apply();
          }, 4);

        })
        .then(function(offers) {

          if (offers && !offers.length) {

            def.resolve([]);

            $timeout(function() {
              vm.offers = [];
              vm.loading = false;
              $scope.$apply();
            }, 4);

            console.log('No offers found');

            $ionicPopup.show({
              title: 'No offers found!',
              subTitle: 'Try restricting your search to a different location.',
              buttons: [
                {
                  text: 'Close'
                }
              ]
            });

            return { error: '404' };
          }

          offers.forEach(function(offer) {
            if (offer.coins && !offer.coins.length) {
              return console.log('No coins available for offer: ', offer)
            }
            if (offer.coins) {
              offer.coins.forEach(function(coin) {
                coin.availability.from = moment(coin.availability.from);
                coin.availability.to = moment(coin.availability.to);
                coin.selected = false;
              });

              offer.coins = {
                scrypt: offer.coins.filter(function(coin) {
                  return (coin.coinType === 'scrypt')
                }),
                sha256: offer.coins.filter(function(coin) {
                  return (coin.coinType === 'sha256')
                })
              };
            }
          });

          _.each(offers, function(offer) {
            offer._price = 0
            _.each(offer.coins.scrypt, function(coin) {
              offer._price += coin.price;
            })
            _.each(offer.coins.sha256, function(coin) {
              offer._price += coin.price;
            });
          });

          def.resolve(offers);

          $timeout(function() {
            vm.offers = offers;
            vm.loading = false;
            $scope.$apply();
          }, 4);

        }, function(e) {
          def.reject(e)
          $timeout(function() {
            vm.offers = []
            vm.loading = false
            $scope.$apply()
          }, 4)
        })
        .catch(function(e) {
          def.reject(e)
          $timeout(function() {
            vm.offers = []
            vm.loading = false
            $scope.$apply()
          }, 4)
        })

      return def.promise
    }
  }
})()

