
/**
 * @ngdoc directive
 * @name WalletCompanion.directive:myOffers
 * @description
 * # offer details
 */
 (function() {

   'use strict';

   angular
     .module('WalletCompanion')
     .directive('myOffers', myOffers);

   myOffers.$inject = ['$ionicLoading'];

   directiveControllerFn.$inject = [
     '$scope',
     '$rootScope',
     '$state',
     '$timeout',
     '$ionicPopup',
     '$ionicLoading',
     'Restangular',
     'UserService',
     'MessageService',
     'MissiveService'
   ];

   /**
    * directiveControllerFn
    *
    * @param $scope
    * @param $rootScope
    * @param $state
    * @param $timeout
    * @param $ionicPopup
    * @param $ionicLoading
    * @param Restangular
    * @param UserService
    * @param MessageService
    * @param MissiveService
    * @returns {undefined}
    */
   function directiveControllerFn($scope, $rootScope, $state, $timeout, $ionicPopup, $ionicLoading, Restangular, UserService, MessageService, MissiveService) {

     var dm = this;
     var vm = $scope.$parent.vm;

     dm.deleteOffer = deleteOffer;

     getMyOffers();

     dm.createOffer = createOffer;
     dm.editOffer = editOffer;


     /**
      * createOffer
      *
      * @returns {undefined}
      */
     function createOffer() {

       MissiveService.resetActiveObj('offer');

       // $state.go('app.createMissive');
       $state.go('app.createNewOffer');
     }

     /**
      * editOffer
      *
      * @param offer
      * @returns {undefined}
      */
     function editOffer(offer) {

       $state.go('app.createMissive');

       console.log('Loaded offer: ', offer);

       MissiveService.resetActiveObj('offer');

       $timeout(function() {
         $rootScope.$broadcast('editOffer', offer)
       }, 4);
     }

     /**
      * getMyOffers
      *
      * @returns {undefined}
      */
     function getMyOffers() {

       UserService
        .getCurrentUser(true)
        .then(function(user) {
          if (typeof user == 'undefined') {
            vm.offers = []
            return
          }
          var opts = {
            'offerFilter': {
              'buyer': user.id
            }
          }

          return MessageService.getFilteredOffers(opts)
        }, function(e) {
          console.log(e)
        })
        .then(function(offers) {

          console.log('my offers: ', offers)

          vm.offers = offers

          if (!_.size(vm.offers)) {
            $ionicLoading.hide()
          }

          return $ionicLoading._getLoader()
        }, function (e) {
          console.log(e)
        })
        .catch(function(e) {
          console.log('error in myOffers: ', e)
        })
        .finally(function(loader) { console.log(loader)
          $ionicLoading.hide()
        }, function(e) {
          console.log(e)
        })
     }

     /**
      * deleteOffer
      *
      * @param i
      * @returns {undefined}
      */
     function deleteOffer(i) {

       $ionicPopup
         .confirm({
           title: 'Delete Offer',
           template: 'Are you sure you want to delete ' + vm.offers[i].profile.title + '?'
         })
         .then(function(res) {
           if (res) {
             showConfirmation(true)
           } else {
             showConfirmation(false)
           }
         });

       /**
        * showConfirmation
        *
        * @param success
        * @returns {undefined}
        */
       function showConfirmation(success) {
        var confirmationMsg
        if (success) {
          var offer = Restangular.all('offers/' + vm.offers[i]._id + '/action')

          offer.customPOST({ 'action': { 'key': 'setHidden' } })
            .then(function(res) {
              if (!res.error) {
                confirmationMsg = 'Offer deleted successfully!'
                vm.offers.splice(i, 1)
              } else {
                confirmationMsg = 'An error was encountered when deleting your offer.'
              }
            })
            .catch(function(e) {
              confirmationMsg = 'An error was encountered when deleting your offer.';
            })
            .finally(function() {
              $ionicPopup.show({
                template: '<span>' + confirmationMsg + '</span>',
                title: 'Offer Deletion',
                scope: $scope,
                buttons: [
                  { text: 'Ok' },
                ]
              });
            });
        }
      }
     }
   }

   /**
    * myOffers
    *
    * @param $ionicLoading
    * @returns {undefined}
    */
   function myOffers($ionicLoading) {

     var directiveOpts = {
       restrict     : 'E',
       replace      : true,
       scope        : true,
       templateUrl  : 'templates/directives/missives/my-offers.html',
       link         : directiveLinkFn,
       controller   : directiveControllerFn,
       controllerAs : 'dm'
     };

     return directiveOpts;

     ////////////

     /**
      * directiveLinkFn
      *
      * @param scope
      * @param el
      * @param attrs
      * @returns {undefined}
      */
     function directiveLinkFn(scope, el, attrs) {
       $ionicLoading._getLoader()
         .then(function(loader) {
           //if (!loader || !loader.isShown) {
             // $ionicLoading.show()
           //}
         });
     }

   }
 })();

