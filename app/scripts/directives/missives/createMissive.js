
/**
 * @ngdoc function
 * @name WalletCompanion.directive:createMissive
 * @description
 * # create missive
 */
 (function() {

   'use strict';

   var httpErrorMessageLoaded = false;

   angular
     .module('WalletCompanion')
     .directive('createMissive', createMissive);


   createMissive.$inject = [
     '$compile',
     '$timeout',
     'ScriptLoader',
     'ApiService',
     '_',
     '$log',
     '$rootScope',
     '$ionicPopup'
   ];

   directiveControllerFn.$inject = [
     '$scope',
     '$state',
     '$ionicPlatform',
     '$ionicPopup',
     '$ionicLoading',
     '$cordovaCamera',
     'MissiveService',
     'MessageService',
     '$timeout',
     '$rootScope',
     'NgMap',
     '$log',
     '$q',
     '_',
     'ApiService'
   ];

   /**
    * directiveControllerFn
    *
    * @param $scope
    * @param $state
    * @param $ionicPlatform
    * @param $ionicPopup
    * @param $ionicLoading
    * @param $cordovaCamera
    * @param MissiveService
    * @param MessageService
    * @param $timeout
    * @param $rootScope
    * @param NgMap
    * @param $log
    * @param $q
    * @param _
    * @param ApiService
    * @returns {undefined}
    */
   function directiveControllerFn($scope, $state, $ionicPlatform, $ionicPopup, $ionicLoading, $cordovaCamera, MissiveService, MessageService, $timeout, $rootScope, NgMap, $log, $q, _, ApiService) {

     // ## Controller Init
     var dm = this;


     // ## Controller Hypermedia Control Data

     var vm = $scope.$parent.vm

     // ## Controller Reference Data

     $scope.map = {
       defaults: {
         tileLayer: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png',
         maxZoom: 18,
         zoomControlPosition: 'bottomleft'
       },
       markers : {},
       events: {
         map: {
           enable: ['context'],
           logic: 'emit'
         }
       },
       controls: {
         custom: [ L.control.zoom({
           position: 'topright'
         }) ]
       }
     };

     var _tiles = {
       tiles: {
         // url: "http://{s}.tile.openstreetmap.org/" +(L.Browser.retina ? '@2x': '')+ "/{z}/{x}/{y}.png",
         url: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
         options: {
           attribution: "",
           minZoom: 10,
           maxZoom: 21
         }
       }
     };

     angular.extend($scope, _tiles);

     angular.extend($scope.map, {
       center: {
         lat  : 43.666667,
         lng  : -79.416667,
         zoom : 8
       }
     });

     $scope.$watch('vm.currentMapLatLng', function(newVal) {

       if (newVal) {
         console.log(newVal)
         $timeout(function() {
           $scope.map.center.lat = newVal[0]
           $scope.map.center.lng = newVal[1]
           $scope.$apply()
         }, 4)

         NgMap.getMap('currentMapLatLng').then(function(map) {
           if (_.isArray(newVal[0])) {
             map.setCenter({
               lat: newVal[0],
               lng: newVal[1]
             })
           }
         })
       }
     });

     $scope.$on('imageTypeError', function($event, data) {

       $ionicLoading.hide()

       var typeMessage = (data && data.size)
         ? 'Please upload an image smaller than 2MB'
         : 'Please select JPG files only.'
       var config = {
         title: 'Incorrect File Format',
         subTitle: typeMessage
       }

       $ionicPopup.show({
         title: config.title,
         subTitle: config.subTitle,
         buttons: [
           {
             text: 'OK',
             onTap: function(e) {
             }
           }
         ]
       })
     });

     dm.deserializeCoins = deserializeCoins;

     dm.newOffer = (MissiveService.getTempObj('offer') || {});

     console.log('directiveControllerFn:newOffer::', dm.newOffer);

     if (dm.newOffer && !dm.newOffer.photos || !dm.newOffer.photos.length) {
       dm.newOffer.photos = [{ image: MessageService.getPhoto() }];
       MissiveService.setTempObj(dm.newOffer, 'offer');
     }

     var _ac = MissiveService.getActive();
     var ac = _ac.currentStep;
     if (ac === 2) {
       console.log(dm.newOffer.photos);
     }

     $rootScope.cropOffers = {
       'min-height' : '150px',
       'max-width'  : '100%',
       width        : '260px',
       top          : 'auto',
       left         : 'auto'
     };

     var cropOffers = MissiveService.getTempObj('cropOffers') || {}

     if (cropOffers.width) {
       $rootScope.cropOffers = {
         'min-height' : '150px',
         'max-width'  : '100%',
         width        : cropOffers.width + 'px',
         top          : (cropOffers.top < 0 ? 0  : cropOffers.top) + 'px',
         left         : (cropOffers.left < 0 ? 0 : cropOffers.left) + 'px'
       };

       console.log('crop: ', $rootScope.cropOffers);
     }

     $scope.$on('photo-uploaded', function($event, data) {

       dm.newOffer.photos = [{ image: _.last(data) }];

       $rootScope.$emit('resizeFormUpdate', {
         updatesize : '500',
         x          : '0px',
         y          : '0px',
         sourceData : data
       });

       MissiveService.setTempObj(dm.newOffer, 'offer');
     });

     vm.newOffer = (vm.newOffer || {});

     /*  PROPERTY TYPES  */
     dm.coinTypesExpanded = false
     dm.coinTypes = [
       {
         name: 'Collectibles',
         shortName: 'collectibles',
         selected: false,
         disabled: false
       },
       {
         name: 'Computer Equipment',
         shortName: 'computer-equipment',
         selected: false,
         disabled: false
       },

       {
         name: 'Ethereum',
         shortName: 'eth',
         selected: false,
         disabled: false
       },
       {
         name: 'Bitcoin',
         shortName: 'btc',
         selected: false,
         disabled: false
       },
       {
         name: 'Monero',
         shortName: 'xmr',
         selected: false,
         disabled: false
       },
       {
         name: 'Conata',
         shortName: 'cot',
         selected: false,
         disabled: false
       }
     ];

     if (!Object.keys(dm.newOffer).length) {
       dm.newOffer = {
         coins: {
           sha256: [],
           scrypt: []
         }
       };
     }

     if (!dm.coins) {
       dm.coins = {
         types: {
           scrypt: [
             {
               name: 'Single Coin - ERC20',
               shortName: 'scrypt-erc20',
               expanded: false,
               selection: []
             },
             {
               name: 'Single Coin - ERC721',
               shortName: 'scrypt-erc721',
               expanded: false,
               selection: []
             }
           ],
           sha256: [
             {
               name: 'Shared Coin - ERC721',
               shortName: 'sha256-erc721',
               expanded: false,
               selection: []
             }
           ]
         }
       };

       loadCoinSelections();
     }

     /**
      * photoUploadModified
      *
      * @param $event
      * @returns {undefined}
      */
     $rootScope.$on('photoUploadModified', function($event) { });

     /**
      * savePhotoResize
      *
      * @returns {undefined}
      */
     dm.savePhotoResize = function() {
       $scope.$broadcast('photo:saved');
     };

     $scope.$on('photo:saveEnd', function($event, data) {
       vm.newOffer.photos = [{ image: { url: data.url } }];
       MissiveService.setTempObj(vm.newOffer, 'offer');
     });

     $scope.$on('photo:dragend', function($event, data) {
       $scope.showingDragged = data;
     });

     /**
      * resizeFormUpdate
      *
      * @returns {undefined}
      */
     dm.resizeFormUpdate = function() {

       console.log('resizeFormUpdate: ', dm.resizeImageWidth);

       $rootScope.$broadcast('resizeFormUpdate', {
         width : dm.resizeImageWidth,
         x     : dm.resizeImageX,
         y     : dm.resizeImageY
       });
     };

     /**
      * @ngdoc method
      * @name resizeImageMode
      * @param resizeImage
      * @returns {undefined}
      * @description After the user updates on a dragend.
      */
     dm.resizeImageMode = function(resizeImage) {

       console.log('resizeImageMode: ', resizeImage);

       $rootScope.$broadcast('resizeImage', resizeImage);
     };

     /**
      * editCoin
      *
      * @param coin
      * @returns {undefined}
      */
     dm.editCoin = function (coin) {
       $rootScope.$broadcast('loadCoin', coin);
       vm.coinManagementModal.show();
     };

     /**
      * toggleOfferTypes
      *
      * @returns {undefined}
      */
     dm.toggleOfferTypes = function() {
       dm.coinTypesExpanded = !dm.coinTypesExpanded;
     };

     /**
      * selectOffer
      *
      * @param i
      * @param e
      * @returns {undefined}
      */
     dm.selectOffer = function(i, e) {

       if (dm.coinTypes[i].disabled === true) {
         e.stopPropagation();
         return;
       }

       dm.coinTypes.forEach(function(x) {
         x.selected = false;
       });

       dm.coinTypes[i].selected = true;
       dm.newOffer.coinType = dm.coinTypes[i];
     };

     /**
      * previewListing
      *
      * @returns {undefined}
      */
     dm.previewListing = function() {
       var tempObj = MissiveService.getTempObj('offer');
       var photo = MessageService.getPhoto();

       $scope.$emit('offer:loaded', tempObj);

       // tempObj.coins.sha256 = []; @TODO ? coin types are scrypt|sha256
       tempObj.coins.scrypt =  []; // @TODO the are token types
       tempObj.coins.sha256 =  [];

       tempObj.coins
         && tempObj.coins.length
         && _.each(tempObj.coins, function(coinRef) {
           try {
             if (
               (coinRef.coinType && coinRef.coinType.indexOf('scrypt') !== -1)
               || (
                 coinRef.selection[0]
                 && coinRef.selection[0].coinType
                 && coinRef.selection[0].coinType.indexOf('scrypt') !== -1
               )
               || (
                 coinRef.selection[0]
                 && coinRef.selection[0].coinType
                 && coinRef.selection[0].coinType.indexOf('scrypt') !== -1
               )
             ) {
               if (coinRef.selection && coinRef.selection.length) {
                 // tempObj.coins.scrypt.concat(coinRef.selection);
                 _.each(coinRef.selection, function (coinItem) {
                   tempObj.coins.scrypt.push(coinItem);
                 })
               } else {
                 tempObj.coins.scrypt.push(coinRef);
               }
             } else {
               if (coinRef.selection && coinRef.selection.length) {
                 // tempObj.coins.scrypt.concat(coinRef.selection)
                 _.each(coinRef.selection, function (coinItem) {
                   tempObj.coins.scrypt.push(coinItem);
                 })
               } else {
                 tempObj.coins.scrypt.push(coinRef);
               }
             }

           } catch(e) {
             console.log(e);
           }
         });

        vm.offers = tempObj.coins ? tempObj.coins.scrypt.concat(tempObj.coins.sha256) : [];
        vm.offersForDateRangeDisplay = _.size(vm.offers) ? [_.last(vm.offers)] : [];

        vm.cachedYear = undefined;
        vm.cachedInitYear = undefined;

        _.each(vm.offers, function(coin) {

          var initYear;
          var year;

          if (coin && coin.availability && coin.availability.from)
            initYear = moment(coin.availability.from).format('YYYY').toString();
          else
            initYear = moment().format('YYYY').toString();
          if (coin && coin.availability && coin.availability.to)
            year = moment(coin.availability.to).format('YYYY').toString();
          else
            year = moment(initYear).format('YYYY').toString();

          vm.cachedYear = (!vm.cachedYear || vm.cachedYear > parseInt(year, 10))
            ? parseInt(year, 10)
            : vm.cachedYear;
          vm.cachedInitYear = (!vm.cachedInitYear || vm.cachedInitYear < parseInt(initYear, 10))
            ? parseInt(initYear, 10)
            : vm.cachedInitYear;

        });

        vm.cachedDate = {};

        _.each(vm.offers, function(coin) {

          var to = moment(coin.availability.to).format('MMM DD').toString();
          var from = moment(coin.availability.from).format('MMM DD').toString();

          vm.cachedDate.first = !vm.cachedDate.first
            || moment(coin.availability.from).isBefore(vm.cachedDate.first)
              ? from
              : vm.cachedDate.first
          vm.cachedDate.last = !vm.cachedDate.last
            || moment(coin.availability.to).isBefore(vm.cachedDate.last)
              ? to
              : vm.cachedDate.last
        });

       vm.currentProfile = {
         title       : tempObj.title,
         subtitle    : tempObj.subtitle,
         description : tempObj.description,
         location    : tempObj.location,
         photo       : photo[0],
         photos      : dm.newOffer.photos
       };

       vm.editMode = tempObj.editing;
       vm.currentOffer = {
         _id      : tempObj._parentId,
         assets   : tempObj.assets,
         coins    : tempObj.coins,
         coinType : tempObj.coinType.shortName || tempObj.coinType.name
       };
       vm.viewOfferModal.show();
     };

     /*  BEDS  */
     /**
      * serializeCoins
      *
      * @returns {undefined}
      */
     dm.serializeCoins = function() {

       var coinRef = _.filter(_.reduce(dm.coins.types, function(typeRef, i) {
         return typeRef.concat(i);
       }), function(categoryRef) {
         return categoryRef.selection.length;
       });

       dm.newOffer.coins = coinRef;

       var _scrypt = [];
       var _sha256 = [];

       _scrypt.concat(_.filter(coinRef, function(coin) {
         return (coin.selection && coin.selection[0] &&
           coin.selection[0].coinType &&
           coin.selection[0].coinType.indexOf('scrypt') !== -1);
       }));

       _sha256.concat(_.filter(coinRef, function(coin) {
         return (coin.selection && coin.selection[0] &&
           coin.selection[0].coinType &&
           coin.selection[0].coinType.indexOf('sha256') === -1);
       }));

       $scope.$emit('coins-selected', dm.newOffer.coins);
     };

     /**
      * deserializeCoins
      *
      * @param coins
      * @returns {undefined}
      */
     function deserializeCoins(coins) {
       if (!coins || !coins.length) { return };
       var types = dm.coins.types;
       var _types = types.scrypt.concat(types.sha256);
       matchCoinTypes(_types);

       /**
        * matchCoinTypes
        *
        * @param types
        * @returns {undefined}
        */
       function matchCoinTypes(types) {

         types.forEach(function(typeRef) {
           var loadedCoins = coins.filter(function(coinRef) {
             var coinType;
             var payType;
             var tokenType;
             var opts = {};
             try {
               coinType = coinRef.selection[0].coinType;
               tokenType = coinRef.selection[0].tokenType;
               payType = coinRef.selection[0].payType;
             } catch(e) {
               coinRef.selection = [];
             } finally {
               if (coinRef && !coinRef.selection.length) {
                 coinType = coinRef.coinType;
                 payType = coinRef.payType;
                 opts.availability = coinRef.availability;
                 opts.coinType = coinRef.coinType;
                 opts.tokenType = coinRef.tokenType;
                 opts.payType = coinRef.payType;
                 // @TODO opts.coinType = coinRef.payType !== '' ? 'scrypt' : 'sha256';
                 opts.description = coinRef.description;
                 opts.price = coinRef.price;
                 opts.quantity = coinRef.quantity;
                 opts._id = coinRef._id;
                 coinRef.selection.push(opts);
               }
             }

             var sep = '-';
             var glue = '';
             var coinOptions = [
               coinType,
               sep,
               ((payType) ? 'erc20' : 'erc271')
             ].join(glue)
             return (coinOptions === typeRef.shortName)
           })

           try {
             var selection = loadedCoins

             _.each(selection, function(coinRef) {
               _.each(coinRef.selection, function(coin) {
                 var typeOpt = [
                   (coin.coinType),
                   ((coin.tokenType) ? 'erc20' : 'erc721')
                 ].join('-')

                 if (typeRef.shortName === typeOpt) {
                   typeRef.selection.push(coin)
                 }
               })
             })
           } catch(e) {
             console.log(e)
           }
         })
       }
     }

     dm.expandCoins = expandCoins;

     /**
      * expandCoins
      *
      * @param mint
      * @returns {undefined}
      */
     function expandCoins(mint) {
       $scope.$parent.$broadcast('coinTypePrivacySelected', mint)
       $rootScope.$broadcast('newCoin', mint)
       vm.coinManagementModal.show()
       $timeout(function() {
         var desc = document.getElementById('description')
         var _desc = angular.element(desc)
         _desc[0].focus()
       }, 4)
     }

     $scope.$on('configError', configError)

     $scope.$on('loadCoinSelections', loadCoinSelections);

     /**
      * @name loadCoinSelections
      * @returns {undefined}
      */
     function loadCoinSelections() {
       if (
         MissiveService.getActive().currentStep === 4 &&
         typeof dm.newOffer !== 'undefined' &&
         typeof dm.newOffer.coins !== 'undefined'
       ) {

         // var types = dm.coins.types

         try {
           dm.deserializeCoins(dm.newOffer.coins)
           // dm.deserializeCoins(types.scrypt)
         } catch(e) {
           console.log(e)
         }
       }
     }

     function configError($event, config) {
       $ionicPopup.show({
         title: config.title,
         subTitle: config.subTitle,
         buttons: [
           {
             text: 'OK',
             onTap: function(e) {
             }
           }
         ]
       })
     }

     /**
      * find
      *
      * @param components
      * @param type
      * @returns {undefined}
      */
     function find(components, type) {
       var listing = _.filter(components, function(component) {
         var i = _.filter(component.types, function(name) {
           return name === type
         })
         return i.length ? i : undefined
       })

       return listing.length ? _.first(listing) : undefined
     }

     function checkFormattedAddress(f) {
       if (typeof f !== 'string') {
         return false
       }
       var _f = f.split(' ')
       if (_f.length > 3) {
         return true
       } else {
         return false
       }
     }

     /*  LOCATION  */
     dm.onAddressSelection = function(location) {

       if (dm.newOffer.location && dm.newOffer.location.address) {
         dm.newOffer.location.address = ''
       }

       var address_components = location.address_components

       var city = find(address_components, 'locality')
       var state = find(address_components, 'administrative_area_level_1')
       var zipCode = find(address_components, 'postal_code')
       var country = find(address_components, 'country')

       ApiService.constants.getCountries().then(function(countries) {

         var _country = _.filter(countries, function(c) {
           return c === country.short_name
         })

         dm.newOffer.location = {
           address: location.formatted_address,
           geometry: [
             location.geometry.location.lng(),
             location.geometry.location.lat()
           ],
           city: city,
           state: state,
           zipCode: zipCode,
           country: _country[0]
         }

         console.log('Chosen address: ', location)

         if (!checkFormattedAddress(location.formatted_address)) {
           dm.newOffer.location.address = ''
           dm.newOffer.location.placeholder = 'Try a full address...'
           dm.locationForm.$setPristine(true)
         }

         if (checkFormattedAddress(location.formatted_address)) {
           $scope.$emit('location-loaded', location)
         }
       }, function(e) {
         console.log(e)
       })
     }

     /*  FLOW+NAVIGATION  */
     console.log(vm.listingMissive);
     if (vm.listingMissive.flow === '') {
       vm.listingMissive = MissiveService.setActive(true, 'createMissive', 'default', 0)
       resetCompletionChecklist()
     } else {
       resetCompletionChecklist()
     }

     dm.viewMod = viewMod
     vm.sectionTitle = MissiveService.getTitle()

     /**
      * next
      *
      * @returns {undefined}
      */
     dm.next = function($event) {
       var fs = MissiveService.getActive()
       var cs = fs.currentStep
       var def = $q.defer()

       if (
         cs === 2 &&
         typeof dm.newOffer !== 'undefined' &&
         typeof dm.newOffer.coins !== 'undefined'
       ) {

         $ionicPopup.show({
           templateUrl: 'templates/popups/listing-location-photos-confirm.html',
           title: 'Like what you see?',
           subTitle: '',
           scope: $scope,
           buttons: [
             {
               text: 'Re-Take',
               onTap: function(e) {
                 def.resolve(e)
               }
             },
             {
               text: 'Confirm',
               onTap: function(e) {
                 def.reject(e)
               }
             }
           ]
         })
       } else {
         def.reject({ status: 'next' })
       }

       def.promise.then(function(res) {

       }, function(e) {

         _.extend(dm.newOffer, vm.newOffer)

         MissiveService.validateChecklistItems(dm.completionChecklist)
          .then(function(res) {
            if (Object.keys(res).length) {
              dm.newOffer.checklist = res
            }

            function hasShortName(coinRef) {
              return _.size(_.filter(coinRef, function(b) {
                var _b
                if (b && _.isFunction(b.hasOwnOffer)) {
                  _b = b.hasOwnOffer('shortName');
                  return _b;
                } else {
                  return false;
                }
              }))
            }

            console.log('next::', dm.newOffer)

            try {

              if (dm.newOffer._id && hasShortName(dm.newOffer.coins)) {
                dm.newOffer.coins = _.map(dm.newOffer.coins, function(coinRef) {
                  return coinRef.selection[0]
                })
              }
            } catch(e) {

              console.log(e);
            }

            MissiveService.setTempObj(dm.newOffer, 'offer')
            MissiveService.next()

          }, function(e) {
            console.log(e);
          })
       })
     }

     $scope.$watch('dm.newOffer.coins', function(newVal) {
       $scope.$emit('coins:loaded', newVal)
     }, true)

     /**
      * removePrivateCoin
      *
      * @param p
      * @param i
      * @returns {undefined}
      */
     dm.removeScryptCoin = function(p, i) {
       console.log(p)
       console.log(i)
       console.log(dm.coins.types.scrypt)
       dm.coins.types.scrypt[p].selection.splice(i, 1)

       if (!dm.coins.types.scrypt[0].selection.length
         && !dm.coins.types.scrypt[1].selection.length)
         $scope.$emit('coins:loaded', undefined)
     }

     /**
      * removeSharedCoin
      *
      * @param p
      * @param i
      * @returns {undefined}
      */
     dm.removeSHA256Coin = function(p, i) {
       dm.coins.types.scrypt[p].selection.splice(i, 1)

       if (!dm.coins.types.scrypt[0].selection.length
         && !dm.coins.types.scrypt[1].selection.length)
         $scope.$emit('coins:loaded', undefined)
     }

     $scope.$on('addNewCoinSelection', function(e, mint, coin) {
       var coinsValid = false
       dm.coins.types[mint.shortName].forEach(function(type) {
         if (type.shortName === mint.queryString) {
           type.selection.push(coin)
         }
       })

       var s = _.filter(dm.coins.types.sha256, function(sha256) {
         return sha256.selection.length
       })
       var b = _.filter(dm.coins.types.scrypt, function(scrypt) {
         return scrypt.selection.length
       })

       if (s.length || b.length) {
         coinsValid = true
       }

       dm.coinsValid = coinsValid
       $scope.$emit('coins:loaded', dm.coinsValid)
     })

     $scope.$on('goBack', function($event) {
       MissiveService.setTempObj(dm.newOffer, 'offer')
       MissiveService.previous()
     })

     $scope.$on('editOffer', function($event, offer) {
       dm.newOffer = MessageService.deserializeOffer(offer)
       dm.newOffer.photo = _.find(dm.newOffer.photos, function(asset) {
         return asset.image && asset.image.url
       })
       dm.newOffer.photos = dm.newOffer.photos
       dm.newOffer._parentId = offer._id
       dm.newOffer.editing = true
     })

     $scope.$on('deserializeAssets', function($event, res) {
       $timeout(function() {
         try {

           vm.newOffer.assets = dm.newOffer ? dm.newOffer.assets : res.item.assets ;
         } catch(e) {

           console.log(e);
         }
         vm.deserializeAssets(vm.newOffer.assets);
         vm.assetsModal.show();
       }, 4);
     });

     /*  PHOTO UPLOAD  */
     /**
      * @ngdoc method
      * @methodOf directiveControllerFn
      * @name choosePhoto
      * @returns {*} undefined
      */
     dm.choosePhoto = function() {
       $ionicPlatform
         .ready(choosePictureWhenReady, false)

       /**
        * @function choosePictureWhenReady
        * @returns {*} undefined
        */
       function choosePictureWhenReady () {

         var cameraConfig = {
           targetWidth        : 200,
           targetHeight       : 200,
           allowEdit          : false,
           destinationType    : Camera.DestinationType.DATA_URL,
           correctOrientation : true,
           quality            : 70,
           sourceType         : Camera.PictureSourceType.PHOTOLIBRARY,
           encodingType       : Camera.EncodingType.JPEG
         }

         $cordovaCamera
          .getPicture(cameraConfig)
            .then(getPictureCompleted, getPictureFailed)

         /**
          * @function getPictureCompleted
          * @param {object} imageData TBD
          * @returns {*} undefined
          */
         function getPictureCompleted (imageData) {

           vm.newOffer.photo = 'data:image/jpegbase64,' + imageData
         }

         /**
          * @function getPictureFailed
          * @param {object} error TBD
          * @returns {*} undefined
          */
         function getPictureFailed (error) {
           console.log(error)
         }
       }
     }

    /**
     * viewMod
     *
     * @returns {undefined}
     */
    function viewMod($event, item, $index) {
      MissiveService.setActive(false, 'createMissive', 'buyer', $index)
      dm.deserializeCoins(dm.newOffer.coins)
    }

    /**
     * resetCompletionChecklist
     *
     * @returns {undefined}
     */
    function resetCompletionChecklist() {

      dm.completionChecklist = (MissiveService.getTempObj('offer')
      ? MissiveService.getTempObj('offer').checklist
      : [])

      if (
        typeof dm.completionChecklist === 'undefined'
        || !dm.completionChecklist.length
      ) {
        dm.completionChecklist = [
          {
            name: 'Set open offer type',
            description: 'Select offer type',
            shortName: 'residence_type',
            complete: false
          },
          {
            name: 'Geocache',
            description: 'Is an address necessary?',
            shortName: 'address',
            complete: false
          },
          {
            name: 'Define you terms',
            description: 'Highlight your listing',
            shortName: 'description',
            complete: false
          },
          {
            name: 'Manage coin inventory',
            description: 'Manage coin rates and inventory',
            shortName: 'coin_inventory',
            complete: false
          },
          {
            name: 'Set availability',
            description: 'Let buyers know when your open offer is available',
            shortName: 'availability',
            complete: false
          }
        ]
      }
    }
  }

   /**
    * createMissive
    *
    * @param $compile
    * @param $timeout
    * @param ScriptLoader
    * @param ApiService
    * @param _
    * @param $log
    * @param $rootScope
    * @param $ionicPopup
    * @returns {undefined}
    */
   function createMissive($compile, $timeout, ScriptLoader, ApiService, _, $log, $rootScope, $ionicPopup) {

     var directiveOpts = {
       restrict     : 'E',
       replace      : true,
       scope        : true,
       templateUrl  : 'templates/directives/missives/create-missive.html',
       link         : directiveLinkFn,
       controller   : directiveControllerFn,
       controllerAs : 'dm'
     };

     return directiveOpts;

     ////////////

     /**
      * directiveLinkFn
      *
      * @param $scope
      * @param $element
      * @param $attrs
      * @returns {undefined}
      */
     function directiveLinkFn($scope, $element, $attrs) {

       $scope.$log = $log;

       $scope._ = _;

       if ( !ScriptLoader.isLoaded() ) {

         $scope.$on('scriptsLoaded', function() {
           initializePlacesInput()
         });

       } else { initializePlacesInput(); }


       /**
        * @name initializePlacesInput
        * @returns {undefined}
        */
       function initializePlacesInput() {

         $timeout(function() {
           var input = angular.element($element[0].querySelector('#offer-autocomplete'))
           input.attr('google-autocomplete-suggestion', '')
           input.attr('ng-model-options', '{ debounce: 250 }')
           $compile(input)($scope)
         }, 4);
       }

     }
  }
})();

