
/**
 * @ngdoc function
 * @name WalletCompanion.directive:addAssets
 * @description
 * # addAssets
 */
 (function() {

   'use strict';

   angular
     .module('WalletCompanion')
     .directive('addAssets', addAssets);

     /**
      * addAssets
      *
      * @returns {undefined}
      */
     function addAssets() {

     var directiveOpts = {
       restrict     : 'E',
       replace      : true,
       scope        : true,
       templateUrl  : 'templates/directives/missives/add-assets.html',
       controller   : directiveControllerFn,
       link: function() {},
       controllerAs : 'dm'
     };

     return directiveOpts;

     /**
      * directiveControllerFn
      *
      * @param $scope
      * @param $state
      * @returns {undefined}
      */
     function directiveControllerFn($scope, $state) {

       var dm = this;
       var vm = $scope.$parent.vm;

       dm.assets = [
         {
           name: 'VAT',
           shortName: 'vat',
           selected: false
         },
         {
           name: 'SIC',
           shortName: 'sic',
           selected: false
         },
      ];

       vm.deserializeAssets = function(assets) {
         if (assets) {
           dm.assets.forEach(function(x) {
             vm.newOffer.assets.forEach(function(y) {
               if (y === x.shortName) { x.selected = true; }
             });
           });
         }
       };

       vm.serializeAssets = function() {
         var res = dm.assets.filter(function(x) {
           return (x.selected);
         });
         res = res.map(function(x) {
           return (x.shortName);
         });
         vm.newOffer.assets = (res || []);
         vm.assetsModal.hide();
       };

       vm.clearAssets = function() {
         dm.assets.forEach(function(x) { x.selected = false; });
         vm.assetsModal.hide();
       };


     }
  }
})();
