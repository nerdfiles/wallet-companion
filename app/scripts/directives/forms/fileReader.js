
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

  /**
   * @ngdoc directive
   * @name fileReader
   * @param {object} $q Internal.
   * @returns {object} AngularJS directive component for accessing FileReader
   * API in the the web browser.
   */
  .directive('tgmFileReader', ['$q', '$ionicPopup', '$ionicLoading', '$analytics', fileReader])

  /**
   * fileReader
   *
   * @param $q
   * @returns {undefined}
   */
  function fileReader($q, $ionicPopup, $ionicLoading, $analytics) {

    var slice = Array.prototype.slice;
    var directive = {
      restrict : 'A',
      require  : '?ngModel',
      scope    : {
        uploadPhoto: '&'
      },
      link     : link
    };

    return directive;

    ////////////

    /**
     * link
     *
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ngModel Internal.
     * @returns {*} undefined
     */
    function link($scope, $element, $attrs, $ngModel) {
      if (!$ngModel) {
        return;
      }

      $element.bind('change', onChange);

      /**
       * onChange
       *
       * @param {object} e TBD
       * @returns {*} undefined
       */
      function onChange(e) {

        var $target = e.target;

        $ionicLoading.show();

        $q.all(slice.call($target.files, 0)
          .map(readFile))
          .then(values, function(e) {
            $scope.$emit('uploadPhoto', e);
          });
        ////////////

        /**
         * __resize__
         *
         * @param {object} img Image file.
         * @returns {string} Data URL as base64.
         */
        function __resize__(img, config) {

          var max_width = config ? config.max_width : 260;
          var max_height = config ? config.max_height : 140;

          if (!img) {
            return {
              error: 'no_image'
            };
          }

          var canvas = document.createElement('canvas');

          var width = img.width;

          var height = img.height;

          if (width > height) {

            if (width > max_width) {
              height = Math.round(height *= max_width / width);
              width = max_width;
            }
          } else {

            if (height > max_height) {
              width = Math.round(width *= max_height / height);
              height = max_height;
            }
          }

          canvas.width = width;

          canvas.height = height;

          var ctx = canvas.getContext("2d");

          // @see https://developer.mozilla.org/en/docs/Web/API/CanvasRenderingContext2D/drawImage
          // ctx.drawImage(image,
          //   sx, sy, sw, sh,
          //   dx, dy, dw, dh);
          ctx.drawImage(img, 0, 0, width, height);

          return canvas.toDataURL("image/jpeg", 0.7);
        }

        /**
         * @name values
         * @param {array} values File inputs.
         * @returns {*} undefined
         */
        function values(__values__) {

          if ($target.multiple) {
            $ngModel.$setViewValue(__values__);
          } else {
            $ngModel.$setViewValue(__values__.length ? __values__[0] : null);
          }

          $scope.$emit('uploadPhoto', {
            files: $target.files,
            image: __values__
          });
        }

        /**
         * @name readFile
         * @param {object} file TBD
         * @returns {object:Promise} File data.
         */
        function readFile(file) {

          var deferred = $q.defer();

          var reader = new FileReader();

          /**
           * @function onload
           * @param {object} e Event object.
           * @returns {*} undefined
           */
          reader.onload = function (e) {
            var imgRef = e.target.result;

            if (!(/image/i).test(file.type) || !(/jpeg/i).test(file.type)) {
              // $scope.$emit('imageTypeError');
              $analytics.eventTrack('image.type', {
                category: 'error',
                label: 'photo-upload'
              });
              deferred.reject({ error: 'file_type' });
            }

            if (file.size > 2097152 || file.fileSize > 2097152) {
              // $scope.$emit('imageTypeError', { size: true });
              $analytics.eventTrack('image.size', {
                category: 'error',
                label: 'photo-upload'
              });
              deferred.reject({ error: 'file_size' });
            }

            var blobRef = new Blob([imgRef]);

            var blobURL = window.URL.createObjectURL(blobRef);

            var image = new Image();

            image.src = blobURL;

            /**
             * onload
             *
             * @returns {undefined}
             */
            image.onload = function() {

              try {

                var resized = __resize__(image);

                deferred.resolve(resized);

                $analytics.eventTrack('image.onload', {
                  category: 'success',
                  label: 'photo-upload'
                });
              } catch(e) {
                deferred.reject(e);
              }
            };

            $ionicLoading.hide();
          };

          /**
           * @name onerror
           * @param {object} e Event object.
           * @returns {*} undefined
           */
          reader.onerror = function (e) {
            console.log(e);
            $analytics.eventTrack('fileReader.onerror', {  category: 'error', label: 'photo-upload' });
            deferred.reject(e);
            $ionicLoading.hide();
          };

          reader.readAsArrayBuffer(file);
          // reader.readAsDataURL(file);

          return deferred.promise;
        }
      }
    }
  }

})();

