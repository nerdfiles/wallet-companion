
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc directive
     * @name fileUpload
     * @returns {object} AngularJS directive component for accessing an input
     * field with type='file'.
     */
    .directive('tgmFileUpload', [fileUpload])

  /**
   * fileUpload
   *
   * @returns {undefined}
   */
  function fileUpload() {

    var directive = {
      restrict: 'A',
      scope: {
        activate: '=',
        prepared: '='
      },
      link: link
    };

    return directive;

    ////////////

    /**
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link($scope, $element, $attrs, $ctrl) {
      angular.element($element).on('click', function() {

        if ($scope.prepared) {
          var img = $element.find('img');
          var pm;
          var preventModification;
          try {
            pm = (img[0].getAttribute('prevent-modification') === "true");
          } catch(e) {
          }
          if (pm !== true) {
            $scope.activate.$$element[0].click();
          }
        }
      });
    }
  }


})();

