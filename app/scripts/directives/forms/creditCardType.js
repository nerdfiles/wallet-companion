
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

  /**
   * @ngdoc directive
   * @name creditCardTypeDirective
   * @returns {object} Directive factory.
   * @see No rolling your own here: https://gist.github.com/lrvick/8789669
   */
  .directive('tgmCreditCardType', creditCardTypeDirective)

  /**
   * creditCardTypeDirective
   *
   * @returns {undefined}
   */
  function creditCardTypeDirective() {

    var directive = {
      require : 'ngModel',
      link    : link
    };

    ////////////

    return directive;

    /**
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link ($scope, $element, $attrs, $ctrl) {
      $ctrl.$parsers.unshift(creditCardTypeDirectiveParser);

      /**
       * @function creditCardTypeDirectiveParser
       * @param {number} value
       * @returns {string} Card type
       * @description Validity parser for CC number.
       */
      function creditCardTypeDirectiveParser (value) {
        $scope.ccInfo.type =
          (/^5[1-5]/.test(value)) ? 'mastercard'
          : (/^4/.test(value)) ? 'visa'
          : (/^3[47]/.test(value)) ? 'amex'
          : (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? 'discover'
          : undefined;
        $ctrl.$setValidity('cc', !!$scope.ccInfo.type);
        return value;
      }
    }
  }


})();

