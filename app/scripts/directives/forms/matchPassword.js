
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc directive
     * @name matchPassword
     * @returns {*} undefined
     * @usage
     *  <input
     *    name="confirmPassword"
     *    tgm-match-password
     *    target-element="dm.FormName.password"
     *  />
     */
    .directive('tgmMatchPassword', matchPassword)

  /**
   * matchPassword
   *
   * @returns {undefined}
   */
  function matchPassword () {

    var directive = {
      scope: {
        targetPassword: '='
      },
      require : '?ngModel',
      link    : link
    };

    return directive;

    ////////////

    /**
     * link
     *
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ngModelCtrl Internal.
     * @returns {*} undefined
     */
    function link ($scope, $element, $attrs, $ngModelCtrl) {
      /**
       * passwordComparirator
       *
       * @param {object} length TBD
       * @returns {*} undefined
       */
      function passwordComparirator (length) {
        var targetPassword = $scope.targetPassword;
        if ($ngModelCtrl.$viewValue !== targetPassword) {
          $ngModelCtrl.$setValidity('match', false);
          return false;
        } else {
          $ngModelCtrl.$setValidity('match', true);
          return targetPassword;
        }
      }
      $ngModelCtrl.$parsers.push(passwordComparirator);
    }
  }

})();

