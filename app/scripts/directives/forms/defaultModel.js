
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
  .directive('defaultModel', ['$log', '$timeout', defaultModel]);

  /**
   * defaultModel
   *
   * @param $log
   * @param $timeout
   * @returns {undefined}
   */
  function defaultModel($log, $timeout) {

    var dv = {
      restrict : 'A',
      link     : link,
      require  : '?ngModel'
    }

    return dv

    //////////////

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attrs
     * @param $ctrl
     * @returns {undefined}
     */
    function link($scope, $element, $attrs, $ctrl) {
      var val = parseInt($attrs.value, 10);
      if ($attrs && $attrs.ngModel) {
        $scope.$watch($attrs.ngModel, function(newVal, oldVal) {
          if (!newVal) {
            $timeout(function() {
              $ctrl.$setViewValue(val);
              $ctrl.$render();
            }, 4);
          }
        })
      }
    }
  }

})();

