
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

  /**
   * @ngdoc directive
   * @name cardExpirationDirective
   */
  .directive('tgmCardExpiration', cardExpirationDirective)


  /**
   * cardExpirationDirective
   *
   * @returns {undefined}
   */
  function cardExpirationDirective () {
    var directive = {
      require : 'ngModel',
      link    : link
    };

    ////////////

    return directive;

    /**
     * @ngdoc method
     * @methodOf cardExpirationDirective
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link ($scope, $element, $attrs, $ctrl) {
      $scope.$watch('[user.ccInfo.month,user.ccInfo.year]', cardExpirationWatch, true);

      /**
       * @function cardExpirationWatch
       *
       * @param {object} value TBD
       * @returns {*} undefined
       */
      function cardExpirationWatch (value) {
        $ctrl.$setValidity('invalid', true);

        if (
          $scope.user.ccInfo.year == $scope.currentYear &&
          $scope.user.ccInfo.month <= $scope.currentMonth
        ) {
          $ctrl.$setValidity('invalid', false);
        }

        return value;
      }
    }
  }

})();

