/**
 * @ngdoc directive
 * @name WalletCompanion.directive:createBuyer
 * @description Create buyer
 */
(function() {

   'use strict';

   var now = new Date();

   angular
    .module('WalletCompanion')
    .directive('createBuyer', createBuyer);

   createBuyer.$inject = [];

   directiveControllerFn.$inject = [
     '$scope',
     '$state',
     '$locale',
     'MissiveService',
     'Restangular',
     'UserService',
     'ApiService',
     '$ionicPlatform',
     '$cordovaCamera',
     '$ionicPopup',
     '$rootScope',
     '$ionicLoading',
     '$analytics',
     'ErrorService',
     'localLedgerManager',
     'LedgerService',
     'WalletService',
     'FW',
     'bitcore',
     'keythereum',
     'web3',
     'Buffer'
   ];

   /**
    * createBuyer
    *
    * @returns {undefined}
    */
   function createBuyer() {

     var directiveOpts = {
       restrict     : 'E',
       replace      : true,
       templateUrl  : 'templates/directives/login/buyer-create-account.html',
       controller   : directiveControllerFn,
       controllerAs : 'dm'
     };

     return directiveOpts;

   }

   /**
    * directiveControllerFn
    *
    * @param $scope
    * @param $state
    * @param $locale
    * @param MissiveService
    * @param Restangular
    * @param UserService
    * @param ApiService
    * @param $ionicPlatform
    * @param $cordovaCamera
    * @param $ionicPopup
    * @param $rootScope
    * @param $ionicLoading
    * @param $analytics
    * @param ErrorService
    * @param localLedgerManager
    * @param LedgerService
    * @param WalletService
    * @param FW
    * @param bitcore
    * @param keythereum
    * @param web3
    * @returns {undefined}
    */
   function directiveControllerFn($scope, $state, $locale, MissiveService, Restangular, UserService, ApiService, $ionicPlatform, $cordovaCamera, $ionicPopup, $rootScope, $ionicLoading, $analytics, ErrorService, localLedgerManager, LedgerService, WalletService, FW, bitcore, keythereum, web3) {

     // ## Controller
     var dm = this;
     var vm = $scope.$parent.vm;

     $scope.$on('$destroy', function() {
       dm.user = {}
     });

     dm.errors = [];
     dm.showPass = false;
     dm.handler = { showPass: function() { dm.showPass = !dm.showPass; } };
     vm.accountMissive = {};
     vm.accountMissive = MissiveService.getActive();
     vm.countries = {};

     if (vm.accountMissive.flow === '') {
       vm.accountMissive = MissiveService.setActive(true, 'createUser', 'buyer', 0);
     }

     vm.sectionTitle = MissiveService.getTitle();
     dm.next = next;
     dm.submit = submit;

     $scope.$on('httpErrorMessage', function($event, data) {
       $ionicLoading.hide();
       dm.errors.length = 0;
       dm.errors.push(data);
     });

     dm.user = MissiveService.getTempObj('user');

     if (vm.countries && vm.countries.length) {
       console.log(dm.user);
     }

     //activated()

     /**
      * activated
      *
      * @returns {undefined}
      */
     function activated() {

       localLedgerManager.loadAddress()
         .then(loadAddressCompleted, loadAddressRejected)
         .catch(loadAddressError)

       function loadAddressCompleted(res) {}
       function loadAddressError(err) {}
       function loadAddressRejected(rej) {}
     }

     ApiService.constants.getCountries().then(function(countries) {
       var co = [];
       var c = _.map(countries, function(key, name) {
         var p = {};
         p.id = key;
         p.name = name;
         co.push(p);
       });
       co.sort(function(a, b) {
         return a.name > b.name ? 1 : -1;
       });
       vm.countries = co;
       var c;

       try {
         c = dm.user.profile.location.country;
         dm.loadProvincesAndStates(c);
       } catch(e) {
         console.log(e);
       }
     }, function(e) {
       ErrorService.notify(e, 'info');
     });

     /**
      * loadProvincesAndStates
      *
      * @param country
      * @returns {undefined}
      */
     dm.loadProvincesAndStates = function(country) {
       ApiService.constants.getProvinceOrState(country)
         .then(function(result) {
           vm.provinces = result
         }, function(e) {
           vm.provinces = {};
           vm.provinces.$error = {
             not_found: true
           };
         });

     };

     /**
      * @ngdoc method
      * @methodOf directiveControllerFn
      * @name next
      * @returns {*} undefined
      */
     function next() {
       if (
         dm.createProfileForm &&
         dm.createProfileForm.$valid
       ) {

         MissiveService.initSignup(dm.createProfileForm.$valid);

         if (!_.keys(dm.user).length) {
           dm.user = MissiveService.getTempObj('user');
         }
       }

       MissiveService.setTempObj(dm.user, 'user');
       MissiveService.next();
     }

     /**
      * @ngdoc method
      * @methodOf directiveControllerFn
      * @name submit
      * @returns {*} undefined
      */
     function submit() {

       $ionicLoading.hide();

       if (
         dm.createUsernameForm && dm.createUsernameForm.$invalid ||
         !MissiveService.isSigningUp() && !dm.user.username ||
         dm.createLocationForm || dm.createPaymentForm
       ) {
         return;
       }

       if (!_.keys(dm.user).length) {
         dm.user = MissiveService.getTempObj('user');
       }

       dm.user.email = dm.user.username;

       if (!dm.user.email) {
         return;
       }

       $ionicLoading.show();

       UserService
         .createAccount(dm.user)
           .then(createAccountCompleted, createAccountFailed);

       /**
        * @function createAccountFailed
        * @param {object} error Error object.
        * @returns {*} undefined
        */
       function createAccountFailed (error) {

         try {
           $analytics.eventTrack('api/v1/user', {
             category: 'error',
             label: 'POST'
           });
         } catch(e) {
           console.log(e)
         }

         console.log(error);

         $ionicLoading.hide();

         $ionicPopup.show({
           title: 'Could not create account',
           subTitle: 'Please try again later',
           buttons: [
             {
               text: 'OK',
               onTap: function(e) {
                 MissiveService.resetActiveObj('user');
                 $state.go('app.login', {}, {
                   reload: true,
                   notify: true,
                   inherit: true
                 });
               }
             }
           ]
         });

       }

       /**
        * @function createAccountCompleted
        * @param {object} data TBD
        * @returns {*} undefined
        */
       function createAccountCompleted(user) {

         $ionicLoading.hide();

         $scope.$emit('userRole', user.role);

         FW.PASSCODE = dm.user.password;

         UserService
           .logNewUser(user)
           .then(function(res) {

             console.log('Logged new user: ', res)

             try {
               $analytics.eventTrack('api/v1/user', {
                 category: 'success',
                 label: 'POST'
               });
             } catch(e) {
               console.log(e);
             }

             activate();

             /**
              * @name activate
              * @returns {undefined}
              */
             function activate() {

               UserService
                 .suggestPassphrase()
                 .then(function(passphraseRef) {

                   var _passphraseRef;

                       var hex = FW.WALLET_HEX;

                       var params = {
                         keyBytes: 32,
                         ivBytes: 16,
                         pbkdf2: {
                           c: 2000,
                           dklen: 32,
                           hash: "sha256"
                         }
                       };

                       // Some cryptocurrency workflow for setting up trust I suppose?
                       var btc_privateKey = new bitcore.HDPrivateKey();
                       var btc_exported = btc_privateKey.toWIF();
                       console.log(btc_exported);

                       var retrieved = new HDPrivateKey(btc_privateKey);
                       var derived = btc_privateKey.derive("m/0");
                       // var derivedByNumber = hdPrivateKey.derive(1).derive(2, true);
                       // var derivedByArgument = hdPrivateKey.derive("m/1/2");
                       // derivedByEndpoint given ledger calculus; convert
                       // everything after m/[...] to a x/y rational number
                       // PDF matrice invert, "don't sartorialize, cantorialize."
                       // convert UR-URL structure to isomorphic description of some CNN output
                       var address = derived.privateKey.toAddress();

                       FW.WALLET_ENTITY_BTC = btc_exported;
                       _.extend(FW.WALLET_KEYS_BTC, btc_privateKey);

                       keythereum.create(params, function(dk) {

                         var options = {
                           kdf: "pbkdf2",
                           cipher: "aes-128-cbc",
                           kdfparams: {
                             c: 262144,
                             dklen: 32,
                             prf: "hmac-sha256"
                           }
                         };

                         var entityRef = keythereum.dump(hex.phrase, dk.privateKey, dk.salt, dk.iv, options);
                         console.log('CREATED', entityRef);

                         FW.WALLET_ENTITY = entityRef;

                         _.extend(FW.WALLET_KEYS, dk);

                         WalletService
                           .encryptWallet()
                           .then(function(res) {

                             MissiveService.setActive(true, 'createUser', 'buyer', 0);

                             $state.go('app.address', {
                               name: vm.accountMissive,
                               user: user
                             }, {
                               reload: true,
                               notify: true,
                               inherit: true
                             });
                           });

                       });

                 });
             }

             //var __addWalletButton__ = {
             //  text: 'Add Wallet Address',
             //  onTap: angular.noop
             //};

             //var __createPaymentMethod__ = {
             //  text: 'Create Payment Method',
             //  onTap: function(e) {

             //    MissiveService.setActive(true, 'createUser', 'buyer', 0);

             //    $state.go('app.paymentMethods', {
             //      name: vm.accountMissive,
             //      user: user
             //    }, {
             //      reload: true,
             //      notify: true,
             //      inherit: true
             //    });
             //  }
             //};

             //$ionicPopup.show({
             //  title: 'Created account',
             //  _subTitle: '<p>Please write down your mnemonic in case you may need to recover your key</p><p>' + FW.WALLET_HEX + '</p>',
             //  subTitle: 'A new private key awaits!',
             //  buttons: [
             //    __addWalletButton__
             //    //__createPaymentMethod__
             //  ]
             //});


           }, function(err) {

             console.log({ error: err });

             $ionicLoading.hide();

             $ionicPopup.show({
               title: 'Count not create account',
               subTitle: 'Please try creating your account again',
               buttons: [
                 {
                   text: 'OK',
                   onTap: function(e) {
                     MissiveService.resetActiveObj('user');
                     $state.go('app.login');
                   }
                 }
               ]
             });
           })

           .catch(function(err) {

             console.log({ error: err });

             $ionicLoading.hide();

             $ionicPopup.show({
               title: 'Count not create account',
               subTitle: 'Please try creating your account again',
               buttons: [
                 {
                   text: 'OK',
                   onTap: function(e) {
                     MissiveService.resetActiveObj('user');
                     $state.go('app.login');
                   }
                 }
               ]
             });
           });
       }
     }
   }
 })();

