
/**
 * @ngdoc function
 * @name WalletCompanion.directive:accountType
 * @description
 * # Account creation type-picker
 */

(function() {
  'use strict'

  angular
    .module('WalletCompanion')
    .directive('accountType', accountType)

  accountType.$inject = ['MissiveService']

  function accountType(MissiveService) {

    var directiveOpts = {
      restrict: 'E',
      templateUrl: 'templates/directives/login/account-type.html',
      controller: directiveController
    }

    return directiveOpts

    directiveController.$inject = [
      '$scope',
      '$state',
      'UserService'
    ]

    function directiveController($scope, $state, UserService) {

      MissiveService.resetActiveObj('user')

      $scope.types = {
        member : 'buyer',
        admin  : 'admin'
      }

      $scope.createAccount = function(accountType) {

        MissiveService.setActive(true, 'createUser', accountType, 0)

        UserService.updateCurrentUser({
          role: accountType
        }).then(function(user) {

          if (user.verification.hasAcceptedTerms !== false) {

            $state.go('app.createUser', {
              type: user.role
            })
          } else {
            $state.go('app.login')
          }

        }, function(e) {
          console.log(e)
        })
      }
    }
  }
})()

