
/**
 * @ngdoc directive
 * @name WalletCompanion.directives:login
 * @description
 * Login menu directive.
 */
(function() {

   'use strict';

   angular
    .module('WalletCompanion')
    .directive('login', loginDirective);

  loginDirective.$inject = [
    'Random'
  ];

   /**
    * loginDirective
    *
    * @returns {Object} An angularJS directive.
    */
   function loginDirective(Random) {

     var directiveOpts = {
       restrict     : 'E',
       templateUrl  : 'templates/directives/login/login.html',
       controller   : directiveControllerFn,
       controllerAs : 'dm'
     };

     return directiveOpts;
   }

   // @TODO currently FW and WalletService loaded here break iOS login page
   directiveControllerFn.$inject = [
     '$scope',
     '$rootScope',
     '$state',
     '$ionicLoading',
     'UserService',
     'FW',
     'WalletService',
     'keythereum',
     '$q',
     'Random'
   ];

   /**
    * directiveControllerFn
    *
    * @param $scope
    * @param $rootScope
    * @param $state
    * @param $ionicLoading
    * @param UserService
    * @param FW
    * @param WalletService
    * @returns {undefined}
    */
   function directiveControllerFn($scope, $rootScope, $state, $ionicLoading, UserService, FW, WalletService, keythereum, $q, Random) {

     var dm = this;
     var vm = $scope.$parent;
     dm.showPass = false;
     dm.error = false;

     dm.handler = {
       showPass: function() {
         dm.showPass = !dm.showPass;
       }
     };

     var configureRoutes = {
       default : 'app.balances',
       buyer   : 'app.balances'
     };

     $scope.$on('httpErrorMessage', function($event, data) {
       console.log('error: ', data);
       $ionicLoading.hide();
       dm.error = true;
       if (data && data.status === -1) {
         dm.errorMsg = 'A connection error occurred. Please try again later.';
       } else {
         dm.errorMsg = 'Please check your credentials and try again.';
       }
     });

     dm.qrcode = {
       'content' : null,
       'version' : '4',
       'level'   : 'Q',
       'size'    : '200'
     };


     UserService
       .suggestPassphrase()
       .then(function(passphraseRef) {

          var u = passphraseRef.split(' ');
          var _u = _.shuffle(u);
          var last = _u.pop();
          _u = _.shuffle(_u);
          _u.push(last)
          _u = _.shuffle(_u);
          var _last = _u.join(' ');

          try {
            dm.qrcode.content = _last;
          } catch (e) {
            console.log(e);
          }

       }, function(e) {
         console.log({ error: e });
       });

     dm.login = login;

     ////////////

     /**
      * login
      *
      * @param {object} user TBD
      * @returns {*} undefined
      */
     function login(user) {

       $ionicLoading.show();

       // Set user passcode globally for immediate interaction with next
       // route whomsted'vely should invoke an authorization schema at a
       // given service. If they leave the global password live, it's their
       // business.
       FW.PASSCODE = user.password;

       $q
         .all([
           UserService.login(user)
         ])
         .then(loginCompleted, loginFailed)
         .catch(loginErrorHandler);

       /**
        * loginCompleted
        *
        * @param {object} res TBD
        * @returns {*} undefined
        */
       function loginCompleted(outcome) {

         WalletService
           .decryptWallet()
           .then(function() {

             delete FW.PASSCODE;

             var res = outcome[0];

             if (!res || res.error) {
               $ionicLoading.hide();
               dm.error = true;
               return;
             }

             $ionicLoading.hide();

             $scope.$emit('userRole', res.role);

             if (res.role === 'admin' ) {
               $state.go(configureRoutes.default, {}, {
                 notify: true,
                 reload: true,
                 inherit: false
               });
             } else if (res.role === 'buyer') {
               $state.go(configureRoutes.buyer, {}, {
                 notify: true,
                 reload: true,
                 inherit: false
               });
             } else {
               $state.go(configureRoutes.buyer, {}, {
                 notify: true,
                 reload: true,
                 inherit: false
               });
             }
           }, function(e) {
             delete FW.PASSCODE;
             $ionicLoading.hide();
             dm.error = true;
             if (e && e.status === -1) {
               dm.errorMsg = 'A connection error occurred. Please try again later.';
             } else {
               dm.errorMsg = 'Please check your credentials and try again.';
             }
           }).catch(function(e) {
             delete FW.PASSCODE;
             $ionicLoading.hide();
             dm.error = true;
             if (e && e.status === -1) {
               dm.errorMsg = 'A connection error occurred. Please try again later.';
             } else {
               dm.errorMsg = 'Please check your credentials and try again.';
             }
           });
       }

       /**
        * loginFailed
        *
        * @param {object} e TBD
        * @returns {*} undefined
        */
       function loginFailed(e) {
         console.log('error: ', e);
         $ionicLoading.hide();
         dm.error = true;
         if (e && e.status === -1) {
           dm.errorMsg = 'A connection error occurred. Please try again later.';
         } else {
           dm.errorMsg = 'Please check your credentials and try again.';
         }
       }

      /**
       * loginErrorHandler
       *
       * @param e
       * @returns {undefined}
       */
       function loginErrorHandler(e) {
         console.log({ error: e });
       }
     }
   }
 })();

