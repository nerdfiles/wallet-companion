
/**
 * @name tgmPreOrderActionsTimeline
 * @param $rootScope
 * @description PreOrderAction Offers Time Series data.
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
    .directive('tgmReservationsTimeline', tgmReservationsTimeline);

  tgmReservationsTimeline.$inject = [
    '$rootScope',
    '$timeout'
  ];

  function tgmReservationsTimeline($rootScope, $timeout) {
    var dv = {
      restrict: 'EA',
      scope: {
        annotationSeries : '=',
        graphData        : '=',
        timestamps       : '='
      },
      link: link
    };
    return dv;

    ////////////

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attrs
     * @returns {undefined}
     */
    function link ($scope, $element, $attrs) {

      $scope.$watchCollection('[annotationSeries, timestamps, graphData]', function (newVal, oldVal) {

        if (!newVal[2] || !newVal[0].length || !newVal[1] || !newVal[2].length) {
          return;
        }

        var graph = new window.Rickshaw.Graph({
          element  : document.getElementById('graph'),
          renderer : 'bar',
          height   : 300,
          series   : [{
            data  : newVal[0],
            color : 'lightgrey',
            name  : 'Request Graph'
          }]
        });

        var graphData = new window.Rickshaw.Graph({
          element  : document.getElementById('graphData'),
          renderer : 'line',
          height   : 300,
          series   : [{
            data  : newVal[2],
            color : 'lightgrey',
            name  : 'Data'
          }]
        });

        graph.render();

        graphData.render();

        var annotator = new window.Rickshaw.Graph.Annotate({
          graph   : graph,
          element : document.getElementById('timeline')
        });

        var messages = newVal[1];
        var l = messages.length;
        for (var i = 0; i < l; ++i) {
          annotator.add(new Date(messages[i].created_at).getTime(), (messages[i].stage + ' on ' + messages[i].created_at + ': ' + messages[i].title));
          if (i === (l - 1)) {
            $timeout(function () {
              annotator.update();
            }, 4);
          }
        }

      });
    }
  }
})();

