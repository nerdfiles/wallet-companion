/**
 * @fileOverview ./app/scripts/directives/wallet/messenger.js
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmMessengerWallet', tgmMessengerWallet);

  tgmMessengerWallet.$inject = [
    '$timeout',
    'localStorageService'
  ];

  function tgmMessengerWallet($timeout, localStorageService) {

    var activated;

    activated = __activated__();

    var ctrl = ['$scope', function($scope) {
      console.log($scope);
    }];

    return activated;

    function __activated__() {
      var dv = {
        controller: ctrl,
        link     : link,
        restrict : 'E',
        scope    : {
          enabled: '=',
          available: '=',
          installed: '='
        },
        templateUrl: 'templates/directives/wallet/messenger.html'
      }
      return dv;

      /*
       * DETAILS
       **/
      function link($scope, $element, $attr) {

      }
    }
  }
}())

