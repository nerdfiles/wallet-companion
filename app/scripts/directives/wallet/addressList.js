/**
 * @fileOverview ./app/scripts/directives/wallet/about.js
 * @usage 
 * <tgm-about content="vm.someContent"></tgm-about>
 * ... later, from within some controller ...
 * $scope.$broadtcast('about:content:loaded')
 */
(function() { 

    'use strict'

    angular.module('tgmAddressList', [
        tgmAddressListDirective
    ])

    ctrl.$inject = [
        '$scope',
        'cuid'
    ]

    // Initialize with top-level content first, then give route to content via broadcast.
    // @usage Inside the directive: <span>{{vm.content|content}}</span>
    function ctrl($scope, cuid) {

        var id = cuid()
        var vm = this

		vm.getClasses = getClasses 
		vm.getBalance = getBalance

        $scope.$on('about:content:loaded', function($event, $data) {
            vm.content = $data.content
            console.log(['about::', id, '::', $event].join(''))
        })

        console.log('Initializing...')

		function getClasses(values) {
			var cls = 'fw-addresslist-address ';
			if(values.address==FW.WALLET_ADDRESS.address)
				cls += 'current-address';
			return cls;
		}

		function getBalance(values, asset) {
			var store  = Ext.getStore('Balances'),
				prefix = values.address.substr(0,5),
				data   = store.data.all,
				qty    = 0;
			// Loop through all store data and try to find balance
			Ext.each(data, function(item){
				var rec = item.data;
				if(rec.prefix==prefix && rec.asset==asset)
					qty = rec.quantity;
			});
			return numeral(qty).format('0,0.00000000');

	   }

    }

    function tgmAddressListDirective() {
        var dv = {
            restrict: 'E',
            templateUrl: '',
            scope: {
                content: '='
            },
            controller: ctrl,
            link: function($scope, $element, $attrs) {
                console.log('DOM element loaded')

				$scope.initialize = initialize
				$scope.onAddAddress = onAddAddress
				$scope.onListSearch = onListSearch

				// Called when view is first initialized
				function initialize(){
					var me     = this,
						cfg   = me.config;
					// Setup alias to main controller
					me.main = FW.app.getController('Main');
					// Setup some aliases for the various components
					me.tb     = me.down('fw-toptoolbar');
					me.search = me.getComponent('listSearch');
					// Call the parent handler
					me.callParent();
					// Handle setting up the list listeners
					me.setListeners({
						itemtap: function(cmp, index, target, record, e, eOpts){
							// Change wallet address to selected address
							me.main.setWalletAddress(record.data.address, true);
							me.main.showMainView();
						},
						// Whenever list is shown, update list to only display records which match wallet prefix and network
						show: function(){
							me.onListSearch();
						}
					});         
					// Handle applying any background class to the list in the correct place (so docked components don't overlay over background)
					if(cfg.bgCls)
						me.element.down('.x-dock-body').addCls(cfg.bgCls);
				}


				// Handle searching list for specific matches
				function onListSearch(str){
					var me     = this,
						store  = me.getStore(),
						regexp = new RegExp(str,"ig"),
						filter = Ext.create('Ext.util.Filter', {
									filterFn: function(item){
										var o = item.data;
										// Only show addresses for the current wallet 
										if(o.prefix==FW.WALLET_PREFIX && o.network==FW.WALLET_NETWORK){
											if(str){
												if(o.address && String(o.address).match(regexp)!=null)
													return true;
												if(o.label && String(o.label).match(regexp)!=null)
													return true;
												return false;
											}
											return true;
										}
										return false;
									}, 
									root: 'data'
								});
					store.clearFilter();
					store.filter(filter);
					me.refresh();        
				}


				// Handle prompting user what action they would like to take, then perform that action
				function onAddAddress(){
					var me = this;
					// @TODO  $ionicModal
					Ext.Msg2.show({
						buttons:[{
							itemId: 'add',
							iconCls: 'fa fa-plus',
							text: 'Add New Address'
						},{
							itemId: 'import',
							iconCls: 'fa fa-upload',
							text: 'Import Private Key'
						},{
							itemId: 'cancel',
							iconCls: 'fa fa-cancel',
							ui: 'decline',
							text: 'Cancel'
						}],
						fn: function(btn){
							// Handle adding 1 new address to the wallet
							if(btn=='add'){
								// Confirm with user that they want to generate new address
								// @TODO $ionicDialog
								Ext.Msg.confirm('Add Address', 'Are you sure?', function(btn){
									if(btn=='yes'){
										// Defer message a bit to prevent knkown issue in 
										// sencha touch library 
										// (https://www.sencha.com/forum/showthread.php?279721)
										// @TODO $q
										Ext.defer(function(){
											addr = me.main.addWalletAddress(1,null,true,true);
										},10);
									}
								});                    
							}
							if(btn=='import'){
								var cb = function(address, privkey){
									if(address && privkey){
										console.log('address,privkey=',address,privkey);
										me.main.addWalletPrivkey(privkey, true);
									}
								}
								me.main.promptAddressPrivkey(cb);
							}
						}
					});
				}

            }
        }

        return dv
    }
})()

