
/**
 * @name tgmQrcode
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmQrcode', [
    '$timeout',
    Qrcode
  ]);

  /**
   * Qrcode
   *
   * @param $timeout
   * @returns {undefined}
   */
  function Qrcode($timeout) {

    var dv = {
      restrict: 'E',
      scope: {
        version : '=tgmVersion',
        level   : '=tgmLevel',
        size    : '=tgmSize',
        content : '=tgmContent'
      },
      link: link,
      templateUrl: 'templates/directives/wallet/qrcode.html'
    };

    return dv;

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attr
     * @returns {undefined}
     */
    function link($scope, $element, $attr) {
      console.log($scope);
    }
  }
}())

