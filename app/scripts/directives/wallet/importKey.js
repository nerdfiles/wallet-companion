(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('importKey', [
      importKey
    ])

  importKeyController.$inject = [
    '$scope'

  ]

  function importKeyController($scope) {
    console.log($scope)

    var vm = this
  }

  function importKey() {

    var dv = {

      restrict    : 'E',
      templateUrl : 'templates/directives/login/import-key.html',
      controller  : importKeyController,
      scope       : {},
      link        : link
    }

    return dv

    function link($scope, $element, $attr) {

    }

  }
}())

