/**
 * @fileOverview ./app/scripts/directives/wallet/about.js
 * @usage 
 * <tgm-about content="vm.someContent"></tgm-about>
 * ... later, from within some controller ...
 * $scope.$broadtcast('about:content:loaded')
 */
(function() { 

    'use strict'

    angular.module('tgmAddressItem', [
        tgmAddressItemDirective
    ])

    ctrl.$inject = [
        '$scope',
        'cuid'
    ]

    // Initialize with top-level content first, then give route to content via broadcast.
    // @usage Inside the directive: <span>{{vm.content|content}}</span>
    function ctrl($scope, cuid) {
        var id = cuid()
        var vm = this
        console.log('Initializing...')
        $scope.$on('about:content:loaded', function($event, $data) {
            vm.content = $data.content
            console.log(['about::', id, '::', $event].join(''))
        })
    }

    function tgmAddressItemDirective() {
        var dv = {
            restrict: 'E',
            templateUrl: '',
            scope: {
                content: '='
            },
            controller: ctrl,
            link: function($scope, $element, $attrs) {
                console.log('DOM element loaded')
            }
        }
        return dv
    }

})()

