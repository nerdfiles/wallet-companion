/**
 * @ngdoc directive
 * @name WalletCompanion.directives:tgmNetwork
 * @description
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmNetwork', NetworkDirective);

  NetworkDirective.$inject = [
    '_'
  ];

  NetworkDirectiveController.$inject = [
    '$scope',
    '$timeout'
  ];

  /**
   * @function NetworkDirectiveController
   * @inner
   * @memberof tgmNetwork.NetworkDirective
   * @description
   */
  function NetworkDirectiveController($scope, $timeout) {

    var vm = this;
    vm.displayHostsInConsole = displayHostsInConsole;

    $scope.$on('hosts:loaded', function(event, res) {
      $timeout(function() {
        console.log('hosts:loaded ', res);
        vm.hosts = res.collection;
        displayHostsInConsole(vm.hosts);
        $scope.$apply();
      }, 4);
    });

    ////////////////

    /**
     * @ngdoc method
     * @name displayHostsInConsole
     * @methodOf WalletCompanion.directives:tgmNetwork#NetworkDirectiveController
     */
    function displayHostsInConsole(hosts) {
      console.log(hosts);
    }
  }

  /**
   * @function NetworkDirective
   * @memberof tgmNetwork
   * @inner
   * @description
   */
  function NetworkDirective(_) {

    var dv = {
      controller   : NetworkDirectiveController,
      controllerAs : 'vm',
      link         : NetworkDirectiveLink,
      restrict     : 'E',
      scope        : {
        'hosts': '=',
        'rfcNs': '@'
      },
      templateUrl  : 'templates/directives/wallet/network.html'
    };

    return dv;

    /**
     * @function NetworkDirectiveLink
     * @description
     * @memberof tgmNetwork.NetworkDirective
     * @inner
     */
    function NetworkDirectiveLink($scope, $element, $attrs) {
      console.log('@conformant ', $scope.rfcNs);
      console.log('@hosts ', $scope.hosts);
      $scope.$emit('hosts:loaded', { collection: $scope.hosts });
    }
  }
})();

