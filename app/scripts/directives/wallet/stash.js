/**
 * @fileOverview ./app/scripts/directives/wallet/stash.js
 * @description
 * Inherits legit, and other git-like capabilities associated with
 * "gitchain"-styles applications (so, prototype blockchain apps written for
 * and via git protocological matter).
 * @BEGIN
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmMessengerStash', tgmMessengerStash);

  var ctrl = ['$scope', function($scope) {
    var vm = this;
    console.log(vm);
  }];

  tgmMessengerStash.$inject = [
    '$timeout'
  ];

  function tgmMessengerStash($timeout) {

    var dv = {
      controller: ctrl,
      controllerAs: 'vm',
      link: link,
      restrict: 'E',
      scope: {
        'balanceList': '='
      },
      templateUrl: 'templates/directives/wallet/stash.html'
    };

    /*
     * @EXPORTS
     **/

    return dv;

    ///////////

    function link($scope, $element, $attr) {

      console.log($scope);

    }
  }
}())

