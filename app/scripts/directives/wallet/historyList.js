/**
 * @name tgmHistoryList
 * @description
 * A directive for listing historical references to exchanges between
 * cryptocurrencies.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmHistoryList', [
      '$timeout',
      'WalletService',
      '$q',
      tgmHistoryList
    ]);

  /**
   * tgmHistoryList
   *
   * @param $timeout
   * @param WalletService
   * @param $q
   * @param LedgerService
   * @returns {undefined}
   */
  function tgmHistoryList() {

    var dv = {
      restrict: 'E',
      controller: [
        '$timeout',
        'WalletService',
         '$q',
        'LedgerService',
        function($timeout, WalletService, $q, LedgerService) {

          WalletService.authorize()
            .then(function(res) {
              var address = res.address;
              LedgerService.getTransactionHistory(address)
                .then(function(res) {
                  $timeout(function() {
                    $scope.historyList = res.collection;
                    $scope.$apply();
                  }, 4)
                }, function(rej) {
                  console.log(rej);
                })
                .catch(function(err) {
                  console.log(err);
                });

            }, function(rej) {
              console.log(rej);
            })
            .catch(function(err) {
              console.log(err);
            });
      }],
      scope: {
        historyList: '='
      },
      templateUrl: 'templates/directives/wallet/history.html'
    };

    return dv;

  }
}());

