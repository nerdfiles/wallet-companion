
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    .directive('profileBubble', [
      'ApiService',
      '$timeout',
      '$q',
      '$window',
      '$document',
      '_',
      'ErrorService',
      function(ApiService, $timeout, $q, $window, $document, _, ErrorService) {

        var offers = [];

        return {
          restrict: 'A',
          templateUrl: 'templates/components/profile.html',
          scope: {
            propId: '='
          },
          link: function($scope, $element, $attrs, $ctrl) {

            $scope._ = _;

            var offer = {};

            offer.offerRef = $scope.propId;

            var img = $element.find('img');

            // angular.element($window).bind("scroll", function() {

            //   [img].each(function(i) {
                // var t = angular.element(this);
                // if (t.position().top > (angular.element($window).scrollTop() + angular.element($window).height())) {

                  ApiService.offers.get(offer.offerRef.offer)
                    .then(function(res) {
                      _.extend(offer, res.data.offer);
                      console.log('Loaded: ', offer);
                      $scope.offer = offer;
                    }, function(e) {
                      ErrorService.notify(e, 'info');
                    });
                // }
              // });

              // $scope.$apply($attrs.profileBubble);

              // $scope.$on('$destroy', function() {
              //   $window.unbind('scroll');
              // });
            // });
          }
        };
      }
    ]);

})();
