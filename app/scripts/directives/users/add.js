/**
 * @ngdoc directive
 * @name WalletCompanion.directive:addPaymentMethod
 */
(function() {
  'use strict'

  angular.module('WalletCompanion')
    .directive('tgmAddPayment', addPaymentMethod)

  addPaymentDirectiveController.$inject = [
    '$scope',
    '$window',
    '$rootScope',
    'UserService',
    '$state',
    '$ionicPopup',
    'MissiveService',
    'ReservationService',
    'Restangular',
    'ApiService',
    '$ionicLoading',
    'ErrorService',
    '$timeout'
  ]

  /**
   * @ngdoc method
   * @methodOf WalletCompanion.directive:addPaymentMethod
   * @name addPaymentDirectiveController
   * @param {object} $scope Internal.
   * @returns {*} undefined
   */
  function addPaymentDirectiveController($scope, $window, $rootScope, UserService, $state, $ionicPopup, MissiveService, ReservationService, Restangular, ApiService, $ionicLoading, ErrorService, $timeout) {

    var vm = this

    $scope.paymentMissive = {}
    $scope.paymentMissive = MissiveService.getActive()
    vm.countries = []

    ApiService.constants.getCountries().then(function(countries) {
      var co = []
      var c = _.map(countries, function(key, name) {
        var p = {}
        p.id = key
        p.name = name
        co.push(p)
      })
      co.sort(function(a, b) {
        return a.name > b.name ? 1 : -1
      })
      vm.countries = co

      try {
        var c = $scope.user.profile.location.country
        vm.loadProvincesAndStates(c)
      } catch(e) {
        console.log(e)
      }
      // vm.loadProvincesAndStates('CA')
    }, function(e) {
      ErrorService.notify(e, 'info')
    })

    vm.loadProvincesAndStates = function(country) {
      ApiService.constants.getProvinceOrState(country)
        .then(function(result) {
          $timeout(function() {
            vm.provinces = result
            vm.provinces.$error = undefined
            $scope.$apply()
          }, 4)

          try {
            vm.createLocationForm.state.$setViewValue('')
          } catch(e) {
          }

          try {
            vm.createLocationForm.defaultStateSelect.$setViewValue('')
          } catch(e) {}


        }, function(e) {

          try {
            vm.createLocationForm.state.$setViewValue('')
          } catch(e) {
          }

          try {
            vm.createLocationForm.defaultStateSelect.$setViewValue('')
          } catch(e) {}

          $timeout(function() {
            vm.provinces = []
            vm.provinces.$error = {
              not_found: true
            }
            $scope.$apply()
          }, 4)
        })
     }

    if (
      $scope.$parent.vm.paymentMissive
      && $scope.$parent.vm.paymentMissive.flow === ''
    ) {
      $scope.paymentMissive = MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
    }

    if (
      $scope.$parent.vm.paymentMissive
      && $scope.$parent.vm.paymentMissive.flow === 'createUser'
    ) {
      $scope.$on('$locationChangeSuccess', function(e, name) {
        if (
          $scope.$parent.vm.paymentMissive.currentStep !== 0 || $scope.$parent.vm.paymentMissive.flow === ''
        ) {
          $scope.$parent.vm.paymentMissive = MissiveService.setActive(true, 'createUser', 'buyer', 0)
        }
      })
    }

    UserService.getCurrentUser(true).then(function(user) {
      _.extend($scope.user, user)
    }, function(e) {
      console.log(e)
    })

    $scope.validateCardDate = function(month, year) {

      if (month < 10)
        month = '0' + month

      if (moment().format('YYYY-MM') < year + '-' + month) {

        $scope.vm.createPaymentForm.cardExpiryMonth.$setValidity("past",true)
        $scope.vm.createPaymentForm.cardExpiryYear.$setValidity("past",true)

      }
      else {

        $scope.vm.createPaymentForm.cardExpiryMonth.$setValidity("past",false)
        $scope.vm.createPaymentForm.cardExpiryYear.$setValidity("past",false)

      }

    }

    $scope.$on('$destroy', function() {

      $ionicLoading.hide()

      try {
        delete $scope._expiryDate
        delete $scope.currentMonth
        delete $scope.currentYear
        $scope.selectedMonth = undefined
        $scope.selectedYear = undefined

        UserService.removeCcInfo()
      } catch(e) {
      }

      try {
        $rootScope.$off('httpErrorMessage')
      } catch(e) {
        console.log(e)
      }
    })

    $scope.$on('httpErrorMessage', function($event, data) {

      try {
        delete $scope._expiryDate
        delete $scope.currentMonth
        delete $scope.currentYear
        $scope.selectedMonth = undefined
        $scope.selectedYear = undefined

        UserService.removeCcInfo()
      } catch(e) {
      }

      $ionicLoading.hide()

      if ($scope.paymentMissive.flow === 'createUser') {
        $state.go('app.coins', {}, { notify: true, reload: true })
      } else {
        MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
        $state.go('app.paymentMethods', {}, { notify: true, reload: true })
      }

      var config = {
        title: 'Payment Method Error',
        subTitle: 'We could not add your payment method at this time. Please try again later.'
      }

      $ionicPopup.show({
        title: config.title,
        subTitle: config.subTitle,
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {

              $scope.user.billing = {}
              $scope.user.ccInfo = {}
              UserService.removeCcInfo()
            }
          }
        ]
      })
    })

    $scope.$on('addPaymentMethod:loaded', function($event, data) {

      var ccInfo
      var user = data.$user

      if ($scope.vm.createPaymentForm.$valid == false)
        return

      /**
       * loadToken
       *
       * @param {object} user TBD
       * @returns {*} undefined
       */
      function loadToken(user) {

        UserService.removeCcInfo()

        var card

        return function(status, response) {

          if (response.error) {
            return $scope.$broadcast('paymentMethodError', response)
          }

          var id = response.id

          _.extend(user.stripe, { token: id })

          $ionicLoading.show()

          UserService.getUserPaymentMethods(user._id, user)
            .then(function(paymentData) {
              _.extend(user.paymentMethods, { list: paymentData })

              UserService.addPaymentMethod(user._id, user.stripe.token)
                .then(function(cardData) {

                  $scope.user.billing = {}
                  $scope.user.ccInfo = {}

                  $ionicLoading.hide()

                  $scope.$emit('resetForm', $scope.user.billing)
                  $scope.$emit('toggleList', false)

                  var selectedCard = Restangular.stripRestangular(cardData.data)
                  ccInfo = {};
                  card = selectedCard;
                  var selectedPaymentMethod = {
                    paymentMethod: card
                  };

                  ReservationService.updateCurrentMessage(selectedPaymentMethod);

                  $ionicPopup.show({
                    title: 'Payment method added successfully',
                    subTitle: 'Please select a payment method',
                    buttons: [
                      {
                        text: 'OK',
                        onTap: function(e) {

                          $scope.$broadcast('cleanup')

                          ReservationService.getCurrentMessage()
                            .then(function(res) {

                              var paymentMissive = $scope.paymentMissive.flow !== 'createUser'
                                ?  MissiveService.getActive()
                                : $scope.paymentMissive

                            if (res.listingId) {
                              $state.go('app.createMessage', { id: res.listingId }, { reload: true })
                              MissiveService.setActive(true, 'createMessage', 'buyer', 2)
                            } else {
                              if ($scope.paymentMissive.flow === 'createPaymentMethod') {
                                MissiveService.next()
                              } else if ($scope.paymentMissive.flow === 'createUser') {
                                $rootScope.$emit('completeForm')
                                $scope.paymentMissive = MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
                                $state.go('app.coins', null, { notify: true, reload: true })
                              } else {
                                $rootScope.$emit('completeForm')
                                $scope.paymentMissive = MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
                                $state.go('app.paymentMethods', null, { notify: true, reload: true })
                              }
                            }

                            $scope.user = {}

                          }, function(e) {

                            $scope.user = {}

                            if ($scope.paymentMissive.flow === 'createPaymentMethod') {
                              $rootScope.$emit('completeForm')
                              MissiveService.resetActiveObj()
                              $state.go('app.coins', null, { notify: true, reload: true })
                              return
                            }

                            $rootScope.$emit('completeForm')
                            MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
                            $state.go('app.paymentMethods', null, { notify: true, reload: true })

                          })

                          $scope.user.billing = {}
                          $scope.user.ccInfo = {}
                          if (vm.createPaymentForm) {
                            vm.createPaymentForm.$setPristine()
                          }
                        }
                      }
                    ]
                  })

                }, function(e) {

                  $ionicLoading.hide()
                  $scope.$broadcast('paymentMethodError')
                  ccInfo = {}

                  $scope.user.billing = {}
                  $scope.user.ccInfo = {}
                  console.log(e)
                  $state.go('app.paymentMethods', {}, { notify: true, reload: true })
                  $scope.user = {}
                })

            }, function(e) {
              $ionicLoading.hide()
              $scope.$broadcast('paymentMethodError')
              $state.go('app.paymentMethods', {}, { notify: true, reload: true })
              console.log(e)
              $scope.user = {}
              $scope.user.billing = {}
              $scope.user.ccInfo = {}
            })

        }
      }

      var billingInfo = _.extend({}, data.billingInfo || {
          state: '',
          line1: '',
          line2: '',
          city: '',
          country: ''
        })

      ccInfo = {
        number          : data.ccInfo.number,
        cvc             : data.ccInfo.cvc,
        exp_month       : data.ccInfo.month,
        exp_year        : data.ccInfo.year,
        address_zip     : data.ccInfo.postalCode,
        address_state   : billingInfo.state,
        address_line1   : billingInfo.line1,
        address_line2   : billingInfo.line2,
        address_city    : billingInfo.city,
        address_country : billingInfo.country
      }

      if (
        $window.stripe &&
        _.isFunction($window.stripe.card.createToken)
      ) {
        $window.stripe.card.createToken(ccInfo, loadToken($scope.user))

        if (vm.createPaymentForm) {
          vm.createPaymentForm.$setPristine()
        }
      }
    })

  }

  addPaymentMethod.$inject = ['$rootScope', '$locale', '$window', 'WalletCompanionStripeJs', 'UserService', 'MissiveService', '$timeout', '$ionicPopup', '$ionicLoading']

  function addPaymentMethod($rootScope, $locale, $window, WalletCompanionStripeJs, UserService, MissiveService, $timeout, $ionicPopup, $ionicLoading) {

    var stripeLoaded
    var now = new Date()
    var directive = {
      templateUrl  : 'templates/directives/users/add.html',
      controller   : addPaymentDirectiveController,
      controllerAs : 'vm',
      link         : link,
      scope        : {
        addPaymentMethod : '&',
        user             : '=',
        addCardToUser    : '&',
        completeForm     : '=',
        next             : '&',
        flow             : '='
      }
    }

    return directive

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.directive:addPaymentMethod
     * @name link
     * @param {object} $scope Internal.
     * @param {object} $element Internal.
     * @param {object} $attrs Internal.
     * @param {object} $ctrl Internal.
     * @returns {*} undefined
     */
    function link($scope, $element, $attrs, $ctrl) {
      $scope.currentYear = now.getFullYear()
      $scope.currentMonth = now.getMonth() + 1
      $scope.months = $locale.DATETIME_FORMATS.MONTH
      $scope.ccInfo = { type: undefined }
      $scope.addCardToUser = addCardToUser
      $scope.completeForm = false
      $scope.showPaymentForm = undefined

      $scope.$on('cleanup', function() {
        $scope.ccInfo = { type: undefined }

        delete $scope._expiryDate
        delete $scope.currentMonth
        delete $scope.currentYear

        $scope.selectedMonth = undefined
        $scope.selectedYear = undefined

        // UserService.removeCcInfo()
      })

      $scope.validateCcInfo = function(form) {
        $rootScope.$emit('swap', form)

        $scope.selectedMonth = undefined
        $scope.selectedYear = undefined
        delete $scope._expiryDate
        delete $scope.currentMonth
        delete $scope.currentYear

        UserService.removeCcInfo()
      }

      if ($scope.paymentMissive.currentStep === 1) {
        $timeout(function() {
          $scope.vm.createPaymentForm.showBillingForm = false
          $rootScope.$emit('swap', $scope.vm.createPaymentForm)
        }, 1000)
      }

      $scope.years = []
      $scope.era = 13
      var yy = parseInt(moment().format('YYYY').toString(), 10)
      while ($scope.era > 0) {
        yy++
        $scope.years.push({
          val: '' + (yy),
          id: yy
        })
        $scope.era--
      }


      $scope.selectedMonth = undefined
      $scope.selectedYear = undefined
      $scope.updateCardExpiryDate = function() {
        var ed
        $scope.user = _.extend({ ccInfo: {} }, $scope.user)
        try {
          ed = $scope.user._expiryDate.split('/')
          var md = ed[0].startsWith('0') ? ed[0].split('0')[1] : ed[0]
          var _md = md === "0" ? 0 : (parseInt(md, 10) - 1)

          $scope.vm.createPaymentForm.cardExpiryDate.$setValidity('required', false)

          $scope.selectedMonth = $scope.months[_md]
          $scope._selectedMonth = _md

          $scope.selectedYear = _.filter($scope.years, function(y) {
            return y.val === ("20"+ed[1])
          })[0].id

          if ($scope.selectedMonth) {
            $scope.vm.createPaymentForm.cardExpiryMonth.$setValidity('required', true)
          } else {
            $scope.vm.createPaymentForm.cardExpiryMonth.$setValidity('required', false)
          }

          if ($scope.selectedYear) {
            $scope.vm.createPaymentForm.cardExpiryYear.$setValidity('required', true)
          } else {
            $scope.vm.createPaymentForm.cardExpiryYear.$setValidity('required', false)
          }

          if ($scope.selectedYear && $scope.selectedMonth)
            $scope.vm.createPaymentForm.cardExpiryDate.$setValidity('required', true)

        } catch(e) {
          $scope.vm.createPaymentForm.cardExpiryMonth.$setValidity('required', false)
          $scope.vm.createPaymentForm.cardExpiryYear.$setValidity('required', false)
          $scope.vm.createPaymentForm.cardExpiryDate.$setValidity('required', false)
        } finally {
          init()
        }

        function init() {
          $timeout(function() {
            $scope.user.ccInfo.month = $scope._selectedMonth
            $scope.user.ccInfo.year = $scope.selectedYear
            $scope.$apply()
          }, 4)
        }
      }

      $scope.$watch('vm.createLocationForm.$valid', function(newVal, oldVal) {
        try {
          $scope.vm.createLocationForm.untouched = true
        } catch(e) {
        }
        if (oldVal === false && newVal) {
          $timeout(function() {
            try {
              $scope.vm.createLocationForm.showBillingForm = true
              $rootScope.$emit('swap', $scope.vm.createLocationForm)
            } catch(e) {
            }
          }, 1000)
        } else if (oldVal === true && newVal === false) {
          $timeout(function() {
            try {
              $scope.vm.createLocationForm.showBillingForm = false
              $scope.vm.createLocationForm.untouched = false
              $rootScope.$emit('swap', $scope.vm.createLocationForm)
            } catch(e) {
            }
          }, 1000)
        }
      })

      $scope.$watch('vm.createPaymentForm.$valid', function(newVal, oldVal) {

        $timeout(function() {
          try {
            $scope.vm.createPaymentForm.showBillingForm = true
            $rootScope.$emit('swap', $scope.vm.createPaymentForm)
          } catch(e) {
          }
        }, 1000)
      })

      if ($scope.vm.createLocationForm) {
        $scope.vm.createLocationForm.$setPristine()
      }

      $scope.$on('paymentMethodError', function($event, data) {

        $ionicLoading.hide()

        var config = {
          title: 'Payment Method Error',
          subTitle: data.error.message
        }

        $ionicPopup.show({
          title: config.title,
          subTitle: config.subTitle,
          buttons: [
            {
              text: 'OK',
              onTap: function(e) {
                $ionicLoading.hide()
              }
            }
          ]
        })
      })

      $scope.$on('resetForm', function($event, data) {

        // $scope.user = {}
        MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0)
      })

      // $scope.$on('beforeEnter', function($event, data) {
      //   $scope.showBillingForm = data
      // })

      $rootScope.$on('showBillingForm', function($event) {
        $scope.showBillingForm = true
      })

      $rootScope.$on('hideBillingForm', function($event, data) {
        var vBtn = angular.element(document.querySelector('.virtual'))
        vBtn.remove()
        $scope.showBillingForm = false
        $scope.showingForm = true
      })

      $scope.$on('resetBillingForm', function() {
        $scope.vm.createLocationForm.$setPristine(true)
      })

      $scope.$on('hidePaymentForm', function($event, data) {
        $scope.showPaymentForm = false
      })

      $rootScope.$on('initForm', function($event, user) {
        console.log('initForm: ', user)
        _.extend($scope.user, user)
        $scope.showBillingForm = undefined
        $scope.showingForm = undefined
      })

      $rootScope.$on('stripe:ready', function($event, stripe) {
        if (!stripeLoaded) {
          $window.stripe = stripe()
        }
        stripeLoaded = true
      })

      $scope.$on('user-loaded', function($event, user) {
        var entity = user
        var id
        if (!entity || (entity && !entity._id)) {
          return
        }
        id = entity._id
        if (!$scope.userLoaded && entity && id) {

          $scope.$emit('user-init', user)

          console.log('Loaded user: ', user)

          UserService.getUserPaymentMethods(id, user)
            .then(function(res) {
              console.log(res)
              $scope.$parent.vm.cards = res
              $scope.userLoaded = true
            }, function(e) {
              console.log(e)
              $scope.user.error = e
              $scope.userLoaded = true
            })
        }
      })

      ////////////

      /**
       * @ngdoc method
       * @methodOf link
       * @name addCardToUser
       * @returns {*} undefined
       */
      function addCardToUser() {

        console.log($scope.paymentMissive)

        if ($scope.paymentMissive.flow === 'addPaymentMethod' &&
          $scope.paymentMissive.currentStep === 0
        ) {} else {
          // MissiveService.next()
        }

        // $scope.vm.createLocationForm.showBillingForm = true
        $rootScope.$emit('swap', $scope.vm.createLocationForm)
        $scope.showBillingForm = true
        $scope.showForm = !$scope.showForm
        $rootScope.$emit('hideList')
        // $scope.user
        //   && $scope.$emit('resetForm', $scope.user.billing)
        $scope.$emit('toggleList', $scope.showForm)
        $rootScope.$emit('addPaymentMethod:init')
      }
    }
  }
})()
