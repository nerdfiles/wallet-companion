// ./app/scripts/directives/animated.js

/**
 * @ngdoc directive
 * @description
 * @usage
 * <button
 *  tgm-animated
 *  tgm-animated-load-with"pulse"
 *  tgm-animated-click-type="tada"
 * >{{label}}</button>
 */
(function() {
  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmAnimated', [
      '$timeout',
      tgmAnimated
    ]);

  function tgmAnimated($timeout) {

    var dv = {
      restrict: 'A',
      scope: {
        'tgmAnimatedLoadWith': '=',
        'tgmAnimatedClickType': '='
      },
      link: link
    };

    return dv;

    ////////////

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attrs
     * @param $ctrl
     * @returns {undefined}
     */
    function link($scope, $element, $attrs, $ctrl) {

      $element.addClass('animated');

      var loadWith = $scope.tgmAnimatedLoadWith || 'pulse';

      $element.addClass(loadWith);

      $element.on('click', function($event) {

        $element.removeClass(loadWith);

        var type = $scope.tgmAnimatedClickType || 'rubberBand';

        $element.removeClass(type);

        $timeout(function() {
          $element.addClass(type);
        }, 4);
      });
    }
  }
})();

