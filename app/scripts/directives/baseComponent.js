/**
 * @ngdoc directive
 * @name tgm.directives:tgmWebComponent
 * @description
 * Specify Module.
 * @spec ./baseComponent.spec.js
 * @usage
 * <tgm-web-component></tgm-web-component>
 */
(function() {

  'use strict'

  /**
   * @module {name} tgmWebComponent
   */
  var module = {

    // Application Configuration

    appName : 'WalletCompanion',

    // Layer Configuration

    controller : [
      '$scope',
      '$timeout',
      webComponentController
    ],
    controllerAs : 'vm',
    dependencies : [
      '$timeout',
      webComponentDirective
    ],

    // Demand Configuration

    name        : 'tgmWebComponent',
    restrict    : 'A',
    scope       : {},
    templateUrl : ''
  }

  /**
   * @module {inject} webComponentDirective
   */
  angular
    .module(module.appName)
    .directive(module.name, module.dependencies)

  ////////////////

  /**
   * @name webComponentDirective
   * @module {detail} #cclrst
   */
  function webComponentDirective($timeout) {

    var dv = {
      controller   : module.controller,
      controllerAs : module.controllerAs,
      link         : link,
      restrict     : module.restrict,
      scope        : module.scope,
      templateUrl  : module.templateUrl
    }

    return dv

    ////////////////

    /**
     * @function link
     * @inner
     */
    function link($scope, $element, $attr) {
      $scope.$$element = $element
      console.log($scope)
    }
  }

  /**
   * @name webComponentController
   * @description
   * @module {contact} webComponentController
   */
  function webComponentController($scope, $timeout) {
    console.log($scope)
  }

}())

