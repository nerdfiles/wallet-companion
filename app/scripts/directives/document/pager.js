
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    .factory('paged', function() {
      var p = {};
      return {
        p: p
      };
    })

    .directive('pager', [
      'paged',
      '$rootScope',
      function(paged, $rootScope)
    {

      return {
        restrict: 'A',
        link: function($scope, $element) {
          $rootScope.$on('updateBody', function($event, data) {
            var body = angular.element(document.querySelector('body'));
            if (paged.p.mode) {
              body.removeClass(paged.p.mode.replace('.', '--'));
            }
            try {
              body.addClass(data.replace('.', '--'));
              paged.p.mode = data;
            } catch(e) {}
          });
          $rootScope.$on('$stateChangeStart', function(e, toState, fromState) {
            var body = angular.element(document.querySelector('body'));
            if (paged.p.mode) {
              body.removeClass(paged.p.mode.replace('.', '--'));
            }
            if (paged.p.page) {
              body.removeClass(paged.p.page.replace('.', '--'));
            }
            try {
              body.addClass(toState.name.replace('.', '--'));
              paged.p.page = toState.name;
            } catch(e) {}
          });
        }
      };
    }])

})();

