
/**
 * @ngdoc directive
 * @returns {undefined}
 * @description
 * Fix Button for Stripe Payment 
 */

(function() {

  'use strict';

  angular
    .module('WalletCompanion')

    .directive('fixButton', [
      '$rootScope',
      '$timeout',
      'MissiveService',
      '$compile',
      fixedButton
    ]);

  function fixedButton($rootScope, $timeout, MissiveService, $compile) {
    return {
      link: function($scope, $element) {

        $rootScope.$on('swap', function($event, form) {

          $timeout(function() {

            var doc = document.querySelector('ion-content#paymentMethods');
            console.log(doc);

            var container = angular.element(document.querySelector('ion-content#create-reservation') || doc);
            var confirmPaymentMethodBtn = angular.element(document.getElementById('confirm-payment-method'));
            var confirmBillingLocationBtn = angular.element(document.getElementById('confirm-billing-location'));

            var vv = document.querySelector('button.virtual');
            var __vv = angular.element(vv);
            __vv.remove();

            var virtualBtn = document.createElement('button');

            virtualBtn.innerHTML = confirmBillingLocationBtn &&
              confirmBillingLocationBtn.length ?
                confirmBillingLocationBtn.text() :
                confirmPaymentMethodBtn.text();

            virtualBtn.className = 'button next next-button virtual';

            // virtualBtn.setAttribute('id', 'v');
            Object.assign(virtualBtn.style, {});

            virtualBtn.setAttribute('ng-disabled', false)

            if (form && form.$valid) {

              virtualBtn.setAttribute('ng-disabled', false);

            } else {

              console.log(form);

              virtualBtn.setAttribute('ng-disabled', true);

              if (form && form.reset) {
                angular.element(virtualBtn).remove();
              } else if (form && form.prev) {
                virtualBtn.setAttribute('ng-disabled', false);
              }
            }

            $compile(virtualBtn)($scope);

            try {
              if (confirmBillingLocationBtn && _.keys(confirmBillingLocationBtn).length) {
                console.log(confirmBillingLocationBtn);
                Object.assign(confirmBillingLocationBtn[0].style, { visibility: 'hidden' });
              }
            } catch(e) {}

            try {
              Object.assign(confirmPaymentMethodBtn[0].style, { visibility: 'hidden' });
            } catch(e) {}

            angular.element(virtualBtn).on('click', function($event) {

              try {
                if (confirmBillingLocationBtn && _.keys(confirmBillingLocationBtn).length) {
                  confirmBillingLocationBtn[0].click();
                }
              } catch(e) {}

              try {
                if (confirmPaymentMethodBtn && _.keys(confirmPaymentMethodBtn).length) {
                  confirmPaymentMethodBtn[0].click();
                }
              } catch(e) {}
            });

            var fs = MissiveService.getActive();

              try {
                var vBtn = document.querySelector('.virtual');
                vBtn.remove();

              } catch(e) {
                console.log(e);
              }

              container.append(virtualBtn);
          }, 4);
        })
      }
    };
  }
})();

