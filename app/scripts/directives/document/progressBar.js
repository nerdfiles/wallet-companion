
/**
 * @ngdoc directive
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')

    /**
     * @ngdoc directive
     * @name progressBar
     */
    .directive('tgmProgressBar', ['MissiveService', progressBar])

  /**
   * progressBar
   *
   * @param MissiveService
   * @returns {undefined}
   */
  function progressBar(MissiveService) {
    var directive = {
      scope        : {
        steps : '='
      },
      templateUrl  : 'templates/components/progress-bar.html',
      controller   : ['$scope', function ($scope) {
      }],
      controllerAs : 'dm',
      link         : link
    };
    return directive;
    ////////////
    function link ($scope, $element, $attrs, $ctrl) {
      $scope.currentMissive = MissiveService.getActive();
      console.log('ℹ️', $scope.currentMissive);
    }
  }

})();

