/**
 * @name tgmClipboardCapture
 * @description
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .directive('tgmClipboardCapture', [
      '$document',
      '$ionicPopup',
      clipboardCapture
    ]);

  /**
   * clipboardCapture
   *
   * @param $document
   * @param $ionicPopup
   * @returns {undefined}
   */
  function clipboardCapture($document, $ionicPopup) {

    var dv = {
      link: link,
      restrict: 'A',
      scope: {
        'clipboardData': '@'
      }
    };

    return dv;

    /**
     * link
     *
     * @param $scope
     * @param $element
     * @param $attr
     * @returns {undefined}
     */
    function link($scope, $element, $attr) {

      $document.bind('copy', toInit);
      $element.on('click', toCopy);

      /**
       * @name toInit
       * @param $event
       * @returns {undefined}
       */
      function toInit($event) {
        $event.clipboardData.setData('text/plain', $scope.clipboardData);
        $event.preventDefault();
      }

      /**
       * @name toCopy
       * @returns {undefined}
       */
      function toCopy() {

        document.execCommand('copy');

        $ionicPopup.show({
          title: 'Address copied!',
          subTitle: 'Send a message to hello@companion.money to get started!',
          buttons: [{ text: 'Close' }]
        });
      }
    }
  }
}());

