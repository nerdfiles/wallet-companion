'use strict';

/**
 * @ngdoc service
 * @name WalletCompanion.CONSTANTS
 * @description
 * # CONSTANTS
 */

(function() {
  angular.module('WalletCompanion')

    // development
    .constant('API_ENDPOINT', {
      host: 'https://localhost:3000/api/v1',
      port: null,
      path: '',
      needsAuth: false
    })
    .constant('MAPS_API', {
      key: 'AIzaSyBSWXeZ3hE9rX-EBGLQXrjyU8LLiI1yaBM'
    })
    .constant('STRIPE', {
      publishKey: ''
    });
})();
