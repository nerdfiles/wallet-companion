/**
 * @fileOverview ./app/scripts/services/LedgerService.js
 * @name tgm.services:LedgerService
 * @description
 * A module for programmable ledger-centric 'service' words to detail a
 * front end implemention given to microservice grammar (commands) and
 * restful endpoint (segmentarities; 'field'-like things) compositions[0] for
 * management of stateless, cached server-side abstractions (models).
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 * ---
 * [0]: http://microservices.io/patterns/data/cqrs.html
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('LedgerService', [
      '$q',
      'web3',
      'FW',
      LedgerService
    ]);

  function LedgerService($q, web3, FW) {

    var serviceInterface = {
      firstAccountBalance         : firstAccountBalance,
      listAccounts                : listAccounts,
      getTransactionHistory       : getTransactionHistory,
      checkAllBalances            : checkAllBalances,
      latestTransaction           : latestTransaction,
      latestBlocks                : latestBlocks,
      latestTransactionForAddress : latestTransactionForAddress
    };

    return serviceInterface;

    /**
     * @name latest
     * @returns {undefined}
     */
    function latestTransaction() {

      var defer = $q.defer();
      var filter = web3.eth.filter('pending');

      filter.watch(function(error, result) {
        if (!error) {
          web3.eth.getTransaction(result, function(error, data) {
            defer.resolve({
              item: {
                from  : data.from,
                to    : data.to,
                value : web3.fromWei(data.value, 'ether').toString() + 'ETH'
              }
            });
          });
        } else {
          defer.reject({ error: error });
        }
      });

      return defer.promise;
    }

    /**
     * @name latest
     * @returns {undefined}
     */
    function latestTransactionForAddress(address) {

      var defer = $q.defer();
      var filter = web3.eth.filter('pending');

      filter.watch(function(error, result) {
        if (!error) {
          web3.eth.getTransaction(result, function(error, data) {
            if (data && data.from && data.from.includes(address)) {
              defer.resolve({
                item: {
                  from  : data.from,
                  to    : data.to,
                  value : web3.fromWei(data.value, 'ether').toString() + 'ETH'
                }
              });
            }
          });
        } else {
          defer.reject({ error: error });
        }
      });

      return defer.promise;
    }

    /**
     * @name latestBlocks
     * @returns {undefined}
     */
    function latestBlocks() {

      var defer = $q.defer();
      var filter = web3.eth.filter('latest');

      filter.watch(function(error, result) {
        if (!error) {
          web3.eth.Block(result, function(error, data) {
            defer.resolve({
              hash   : data.hash.substr(0,20),
              number : data.number,
              miner  : data.miner,
              uncles : data.uncles.length
            });
          });
        } else {
          defer.reject({ error: error });
        }

      });

      return defer.promise;
    }

    /**
     * @name listAccounts
     */
    function listAccounts() {

      var defer = $q.defer()
      var accounts

      console.log( 'Listing (ETH): ', web3 )

      web3.eth.getAccounts().then(function(res) {
        accounts = res
        if (accounts &&_.size(accounts)) {
          console.log( 'Found ', accounts );
          defer.resolve({ collection: accounts });
        } else if (accounts && ! _.size(accounts)) {
          defer.resolve({ collection: accounts });
        } else {
          defer.reject({ error: '404' });
        }
      }, function(rej) {
        console.log(rej);
      })
      .catch(function(err) {
        console.log(err);
      });

      return defer.promise;
    }

    /**
     * @name firstAccountBalance
     * @description
     * First Account Balance of the given PKI pair post-login.
     */
    function firstAccountBalance() {

      var defer = $q.defer()
      var firstAccount

      try {
        firstAccount = web3.eth.getBalance( web3.eth.accounts[0] );
      } catch(err) {
        var error = { error: err };
        defer.reject( error );
      } finally {
        console.log( 'Found ', firstAccount );
        if (firstAccount) {
          defer.resolve( firstAccount );
        }
      }

      return defer.promise;
    }

    /**
     * @name checkAllBalances
     * @returns {undefined}
     */
    function checkAllBalances(accountRef) {
     web3.eth.accounts.forEach(function(e) {
        console.log(web3.fromWei(web3.eth.getBalance(accountRef), 'ether') + ' ether');
     });
    }

    /**
     * @name getTransactionHistory
     * @param address
     * @param startBlockNumber
     * @returns {undefined}
     */
    function getTransactionHistory(address, startBlockNumber) {

      var defer = $q.defer();

      if (!address) {
        defer.reject({ error: '404' });
      }

      var transactions = []
      var endBlockNumber = web3.eth.blockNumber;
      var block;

      if (endBlockNumber === null) {
        console.log('Using endBlockNumber: ' + endBlockNumber);
      }

      if (startBlockNumber === null) {
        startBlockNumber = endBlockNumber - 1000;
        console.log('Using startBlockNumber: ' + startBlockNumber);
      }

      console.log([
        'Searching for transactions to ',
        address,
        ' within blocks ',
        startBlockNumber,
        ' and ',
        endBlockNumber
      ].join(''));

      for (var i = startBlockNumber; i <= endBlockNumber; ++i) {
        try {
          block = web3.eth.getBlock(i, true);
        } catch(e) {
          console.log(e);
        } finally {
          if (block && block.transactions) {
            block.transactions.forEach(function(e) {
              if (e && address === e.to) {
                transactions.push({
                  sender: e.from,
                  value: e.value
                });
              }
            });
          }
        }
      }

      defer.resolve({ collection: transactions });

      return defer.promise;
    }
  }
}());

