
(function() {

  'use strict'

  //'TalkService.MessageBox',
  //'DeviceService.Device',
  //'DeviceService.Connection'

  angular
    .module('WalletCompanion')
    .service('AppService', [
      '$timeout',
      'localStorageService',
      'FW',
      '$ionicPopup',
      'Crypto',
      '$http',
      '$window',
      '$interval',
      'CryptocurrencyService',
      AppService
    ])

  function AppService($timeout, localStorageService, FW, $ionicPopup, Crypto, $http, $window, $interval, CryptocurrencyService) {

    var log = function() { return console.log }

    var serviceInterface = this

    _.extend(serviceInterface, {
      cbError           : cbError,
      clearAppCache     : clearAppCache,
      copyToClipboard   : copyToClipboard,
      getUrlHostname    : getUrlHostname,
      getDeviceType     : getDeviceType,
      isNative          : (typeof $window.cordova === 'undefined') ? false : true,
      isUrl             : isUrl,
      openUrl           : openUrl,
      promptLogout      : promptLogout,
      getStore          : getStore,
      saveStore         : saveStore,
      showView          : showView,
      getCmp            : getCmp,
      showTool          : showTool,
      processLaunchData : processLaunchData,
      launch            : launch,
      encode : encode,
      decode : decode,
      apply  : apply

    })

    function encode() {}
    function decode() {
    }

    function apply() {}

    function launch() {

      var me   = this,
          sm   = localStorageService, // Alias for state manager
          vp   = AppService.Viewport,
          wall = sm.getItem('wallet'),
          pass = sm.getItem('passcode');
      // Setup alias to counterparty controller
      me.counterparty   = CryptocurrencyService.getCurrency('Counterparty');
      // Setup flag to indicate if we are running as a native app.
      me.isNative = (typeof cordova === 'undefined') ? false : true;
      // Setup alias to device type
      me.deviceType = me.getDeviceType();

      // Initalize some runtime values
      FW.PASSCODE       = parseInt('0000', 10);                           // Default passcode used to encrypt wallet
      FW.WALLET_HEX     = null;                           // HD wallet Hex key
      FW.WALLET_KEYS    = {};                             // Object containing of address/private keys
      FW.WALLET_NETWORK = sm.getItem('network') || 1;     // (1=Mainnet, 2=Testnet)
      FW.WALLET_PREFIX  = sm.getItem('prefix')  || null;  // 4-char wallet hex prefix (used to quickly find addresses associated with this wallet in datastore)
      FW.WALLET_ADDRESS = sm.getItem('address') || null;  // Current wallet address info
      FW.TOUCHID        = sm.getItem('touchid') || false; // TouchID Authentication enabled (iOS 8+)
      FW.NETWORK_INFO   = {};                             // latest network information (price, fees, unconfirmed tx, etc)
      FW.API_KEYS       = {
          BLOCKTRAIL: 'efb0aae5420f167113cc81a9edf7b276d40c2565'
      }

      // Define default server/host settings
      FW.SERVER_INFO    = {
          mainnet: {
              cpHost: 'public.coindaddy.io',          // Counterparty Host
              cpPort: 4001,                           // Counterparty Port
              cpUser: 'rpc',                          // Counterparty Username
              cpPass: '1234',                         // Counterparty Password
              cpSSL: true                             // Counterparty SSL Enabled (true=https, false=http)
          },
          testnet: {
              cpHost: 'public.coindaddy.io',          // Counterparty Host
              cpPort: 14001,                          // Counterparty Port
              cpUser: 'rpc',                          // Counterparty Username
              cpPass: '1234',                         // Counterparty Password
              cpSSL: true                             // Counterparty SSL Enabled (true=https, false=http)
          }
      };

      // Define default miners fees (pull dynamic fee data from blocktrail.com API)
      var std = 0.0001
      FW.MINER_FEES = {
          standard: std,
          medium: std * 2,
          fast: std * 5
      };

      // Load any custom server information
      var serverInfo = sm.getItem('serverInfo');
      if(serverInfo){
          var o = Ext.decode(serverInfo);
          if(o)
              FW.SERVER_INFO = o;
      }

      // Detect if we have a wallet
      if(wall){
          // Define function to run once we have successfully authenticated
          var successFn = function(pass){
              if(pass)
                  FW.WALLET_PASSCODE = pass;
              me.decryptWallet();
              me.setWalletNetwork(FW.WALLET_NETWORK);
              me.setWalletAddress(FW.WALLET_ADDRESS, true);
              ViewService.showMainView();
              // Load network info every 10 minutes
              var network  = sm.getItem('networkInfo'),
                  tstamp   = sm.getItem('networkInfoUpdated'),
                  interval = 600000; // 10 minutes
              if(network)
                  FW.NETWORK_INFO = Ext.decode(network);
              // Parse in last known network fees
              if(FW.NETWORK_INFO.fee_info){
                  var o = FW.NETWORK_INFO.fee_info;
                  FW.MINER_FEES.medium = o.low_priority;
                  FW.MINER_FEES.fast   = o.optimal;
              }
              // Refresh if we have no network data, or it is older than interval
              if(!tstamp || (tstamp && (parseInt(tstamp)+interval) < Date.now()))
                  me.updateNetworkInfo(true);
              // Update prices every 10 minutes
              $interval(function(){ me.updateNetworkInfo(true); }, interval);
              // Handle processing any scanned data after 1 second
              $timeout(function(){ me.processLaunchData(); }, 1000);
          }
          if(FW.TOUCHID && me.isNative){
              // Handle Touch ID authentication
              TouchService.authenticateTouchID(successFn, null, 'Please scan your fingerprint', true);
          } else if(pass){
              // Handle passcode authentication
              PasscodeService.authenticatePasscode(successFn, null, 'Please enter your passcode', true);
          } else {
              successFn();
          }
      } else {
        // Show the welcome/setup view
        ViewService.showWelcomeView();
      }
    }

    function processLaunchData(){
      var me   = this
      var data = FW.LAUNCH_DATA

      // Only proceed if we have a decrypted wallet
      if (FW.WALLET_HEX && data) {
        var o = me.getScannedData(String(data));
        me.processQRCodeScan(o); // Treat input as a scanned QR Code
        FW.LAUNCH_DATA = false;  // Reset data so it is gone on next check
      }
    }


    function showTool(tool,cfg){
        var me   = this;
        // console.log('showTool tool,cfg=',tool,cfg);
        // Show main view (probably already visible)
        ViewService.showMainView();
        // Switch main view to the 'Tools' tab
        var main  = Ext.getCmp('mainView'),
            tools = Ext.getCmp('toolsView');
        main.setActiveItem(tools);
        // Display the correct tool
        if(tool=='bet')
            tools.showBetTool(cfg);
        if(tool=='broadcast')
            tools.showBroadcastTool(cfg);
        if(tool=='dividend')
            tools.showDividendTool(cfg);
        if(tool=='exchange')
            tools.showExchangeTool(cfg);
        if(tool=='issue')
            tools.showIssueTool(cfg);
        if(tool=='notarize')
            tools.showNotarizeTool(cfg);
        if(tool=='otcmarket')
            tools.showOTCMarketTool(cfg);
        if(tool=='receive')
            tools.showReceiveTool(cfg);
        if(tool=='send')
            tools.showSendTool(cfg);
        if(tool=='sign')
            tools.showSignTool(cfg);
    }

    /**
     * @name getCmp
     * @description
     */
    function getCmp(viewspace) {
      return function() {
        log(viewspace)
      }
    }

    function showView(id, xclass, cfg){
        var view = false
        var vp   = AppService.Viewport;
        // console.log('showView xclass,cfg=',xclass,cfg);
        if(id)
          view = AppService.getCmp(id)
        // If we found existing view, update data and use it
        if(view){
          if(cfg)
            view.updateView(cfg)
        } else {
          view = vp.add(AppService.apply({ xclass: xclass }, cfg))
        }
        // Show view using correct method
        if(view.isInnerItem()){
          vp.setActiveItem(view)
        } else {
          view.show()
        }
    }

    function getStore(id) {
      var store = {
        sync: function() {
          console.log(id)
        }
      }
      return store
    }

    // Handle saving a datastore to disk
    function saveStore(id) {
      var store = serviceInterface.getStore(id);
      if (store)
        store.sync();
    }


    function getDeviceType(){
      var me = this;
      if(!me.deviceType){
        var w  = Math.max(window.innerWidth,window.innerHeight);
        me.deviceType = (w>=1000) ? 'tablet' : 'phone';
      }
      return me.deviceType;
    }

    // Handle toggling hiding/showing the sidemenu
    function showMainMenu(side) {
      var me   = this
      var vp   = AppService.Viewport
      var main = serviceInterface.getCmp('mainView')
      var side = (side) ? side : 'right'

      // Handle creating the menuTree if we don't already have one
      if(!me.menu)
        self.menu = AppService.create('FW.view.MainMenu');
      // Set the sidemenu to this menu, and show the menu
      vp.setMenu(me.menu, { side:side, cover: true });
      vp.showMenu(side);
    }

    /**
     * Confirm with user that they want to generate new wallet
     */
    function promptLogout() {
      $ionicPopup.confirm('Logout / Clear Data', 'Are you sure?')
        .then(function(btn){
          if (btn == 'yes') {
            localStorageService.clear()
            location.reload()
          }
        })
    }

    // Detect if a string is a valid URL
    function isUrl(str) {
      var self = this
      log(self)
      var re = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i
      return re.test(str)
    }

    // fixes issue with app not updating localStorageService properly at times
    function clearAppCache(reload) {
      var ls = localStorageService // @PREFER
      var keys = []
      var patt = [
          'app.js',
          'app.json',
          'resources/'
      ]

      // Loop through all keys and find any that match our patterns
      for(var i = 0, ln = ls.length; i < ln; i += 1){
        var key = ls.key(i);
        if(key){
          patt.forEach(function(pattern){
            if(key.indexOf(pattern)!== -1)
            keys.push(key);
          });
        }
      }

      keys.forEach(function(key){
        console.log('removing appCache item ' + key);
        ls.removeItem(key);
      });

      // Handle reloading window after 1 second
      if (reload)
        $timeout(function(){
          window.location.reload();
        },1000);
    }

    // Handle opening urls
    function openUrl(url){
        var me = this,
            re = /^(http|https):\/\//i
        if(url){
            // Handle adding http:// to any invalid URLs
            if(!re.test(url))
                url = 'http://' + url
            if(me.isUrl(url)){
                var loc = (me.isNative) ? '_system' : '_blank'
                window.open(url,loc)
            }
        }
    }

    // Handle extracting hostname from a url
    function getUrlHostname(url) {
      var arr  = url.split('/')
      // Remove protocol (http/https)
      var host = (url.indexOf('://') > -1) ? arr[2] : arr[0]
      // Remove Port
      host = host.split(':')[0]
      return host
    }

    // Handle copying a string to the clipboard
    function copyToClipboard(str) {
      if (AppService.isNative) {
        $window.cordova.plugins.clipboard.copy(str)
      }
    }

    function cbError(msg, callback){
      $ionicPopup.alert('Error',msg)
      if (typeof callback === 'function')
        callback()
    }
  }
})()

