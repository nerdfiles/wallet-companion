// ./app/scripts/services/httpErrorHandler.js
/**
 * @ngdoc service
 * @name WalletCompanion.httpErrorHandler
 * @description httpErrorHandler
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('httpErrorHandler', httpErrorHandler);

  httpErrorHandler.$inject = [
    '$rootScope',
    '$q',
    '$http',
    '$analytics',
    'UserService',
    '_'
  ];

  /**
   * httpErrorHandler
   *
   * @param $rootScope
   * @param $q
   * @param $http
   * @param $analytics
   * @returns {undefined}
   */
  function httpErrorHandler($rootScope, $q, $http, $analytics, UserService, _) {

    var service =  {
      intercept: intercept
    };

    return service;

    ///////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.httpErrorHandler
     * @name intercept
     * @returns {*} undefined
     */

    function intercept(response, deferred, responseHandler) {
      var error = null

      console.log('httpErrorHandler.intercept: Caught HTTP' + response.status + 'error!');
      console.log('httpErrorHandler.intercept: Dumping payload:', response.data);

      if (_.includes(response.data.error.type, 'AuthRequiredError')) {
        UserService
          .deleteLocalSession()
          .then(function(res) {
            console.log(res);
          }, function(err) {
            console.log({ error: err });
          });
      }

      if (response.status === 401) {
        // Do refresh access token logic here
        $http(response.config).then(responseHandler, deferred.reject);
      }

      if (response.data && response.data.error) {
        error = _.extend({}, response.data.error)

        $analytics.eventTrack('httpErrorMessage', {
          category: 'error',
          label: 'protocol [' + (JSON.stringify(response.data.error)) + ']'
        });
      }

      $rootScope.$broadcast('httpErrorMessage', error);
      $rootScope.$broadcast('httpErrorMissive', error);

      return false;
    }

  }

})();

