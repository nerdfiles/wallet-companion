/**
 * @fileOverview ./app/scripts/services/ProfileService.js
 * @name ProfileService
 * @description
 * Users and Offers (distributed objects) bear "profiles". The idea here is
 * that second-class properties of distributed objects should be treated with
 * model services which underpin profile services such that profiling of
 * digital traits is achieved.
 * @BEGIN
 */
(function() {

  'use strict';

  /*
   * @EXPORTS
   **/
  angular
    .module('WalletCompanion')
    .service('ProfileService', [
      '$q',
      ProfileService
    ]);

  ProfileService.$inject = [
  ];
  /**
   * ProfileService
   *
   * @returns {undefined}
   */
  function ProfileService() {

    var serviceInterface = this;

    serviceInterface.init = init;
    serviceInterface.resolve = resolve;
    serviceInterface.signal = signal;
    serviceInterface.recognize = recognize;

    //////////

    function init(model) {}
    function resolve() {}
    function signal() {}
    function recognize() {}
  }
}());
