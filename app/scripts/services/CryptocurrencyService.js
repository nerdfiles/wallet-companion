// ./app/scripts/services/CurrenciesService.js
/**
 * @ngdoc service
 * @name WalletCompanion.CurrenciesService
 * @description API Service provides methods for configuring REST interface
 * method accessibility and configuration.
 */

(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .factory('CryptocurrencyService', CryptocurrencyService)

  CryptocurrencyService.$inject = [
    '$window',
    '$http',
    'jsonrpc'
  ];

  function CryptocurrencyService($window, $http, jsonrpc) {

    var service = {
      counterparties: {
        action: __action__,
      },
    };

    return service

    ////////////

    /**
     * @name __name__
     * @usage
     *
     * angular.controller('CtrlName', [
     *   'CurrenciesService',
     *   function CtrlName(CurrenciesService) {
     *     CurrenciesService
     *       .counterparties
     *       .action('create_send', {})('testnet)
     *   }
     * ])
     *
     * @description
     * Currencies action service for RPC Interactions Transmedia Factor
     * implement.
     */
    function __action__(methodName, formData) {
      return function(network) {
        jsonrpc.request(network, methodName, formData)
          .then(function(res) {
            console.log(res)
          })
          .catch(function(error) {
            console.log(error)
          })
      }
    }
  }

})()

