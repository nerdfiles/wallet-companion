
/**
 * @ngdoc service
 * @name WalletCompanion.ScriptLoader
 * @description Returns 'true' if the object being passed in is valid and
 * non-empty, 'false' otherwise
 */
(function() {
  'use strict';

  angular
    .module('WalletCompanion')
    .factory('ScriptLoader', ScriptLoader);

  ScriptLoader.$inject = [
    '$window',
    '$q',
    '$ionicPlatform'
  ];

  function ScriptLoader($window, $q, $ionicPlatform) {

    var apiKey = 'AIzaSyBSWXeZ3hE9rX-EBGLQXrjyU8LLiI1yaBM';

    /**
     * isLoaded
     *
     * @returns {undefined}
     */
    function isLoaded() {
      return (typeof google !== 'undefined');
    }

    //Google's url for async maps initialization accepting callback function
    var asyncUrl = 'https://maps.google.com/maps/api/js?key=' + apiKey + '&libraries=geometry,places&callback=',
      deferred = $q.defer();

    //Callback function - resolving promise after maps successfully loaded
    $window.scriptInitialized = deferred.resolve; //

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ScriptLoader
     * @name asyncLoad
     * @param {string} asyncUrl URL to load.
     * @param {string} callbackName Callback name.
     * @returns {*} undefined
     * @description Async loader.
     */
    var asyncLoad = function(asyncUrl, callbackName) {

      var script = document.createElement('script');

      script.setAttribute('src', asyncUrl + callbackName);
      script.setAttribute('defer', '');

      document.getElementsByTagName('head')[0]
        .appendChild(script);

      script.onload = function() {
        $window.google = google;
      };

      script.onerror = function(error) {
        $window.google = {};
        console.log('Could not load GMaps');
      };
    };

    $ionicPlatform.ready(function() {
      asyncLoad(asyncUrl, 'scriptInitialized');
    });

    //Usage: ScriptLoader.initialize().then(callback)
    return {
      initialize: deferred.promise,
      isLoaded: isLoaded
    };
  }
})();
