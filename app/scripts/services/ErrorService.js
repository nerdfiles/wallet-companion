// ./app/scripts/services/ErrorService.js
/**
 * @ngdoc service
 * @name WalletCompanion.ErrorService
 * @description ErrorService
 */
(function() {
  'use strict';
  angular
    .module('WalletCompanion')
    .service('ErrorService', ErrorService);

  ErrorService.$inject = [
    'API_ENDPOINT',
    'ApiService',
    'Restangular',
    '$q'
  ];

  function ErrorService(API_ENDPOINT, ApiService, Restangular, $q) {

    var service = this;

    service.notify = notify;
    service.error = error;

    /**
     * error
     *
     * @param e
     * @param type
     * @returns {undefined}
     */
    function error(e, type) {

      var deferred = $q.defer();

      if (!type) {
        deferred.reject(e);
      } else {
        deferred.resolve(e);
      }

      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ErrorService
     * @name notify
     * @returns {function} Callback function for use in Promise.
     */
    function notify(callback) {
      return (data) => {
        var activity, deferred = $q.defer();

        if (data && data.error) {
          console.log(data);
          activity = callback(data.error);
          deferred.reject(activity);
          return deferred.promise;
        } else if (_.isArray(data) && !_.size(data)) {
          console.log(data);
          activity = callback(null);
          deferred.reject(activity);
          return deferred.promise;
        }

        try {
          activity = callback(data);
          deferred.resolve(activity);
        } catch(err) {
          console.log(data);
          activity = callback(err);
          deferred.reject(activity);
        }
        return deferred.promise;
      };
    }
  }
})();
