(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .service('BalanceService', [BalanceService]);

  function BalanceService() {

    var serviceInterface = {
      getBalance           : getBalance,
      updateAddressBalance : updateAddressBalance,
      getSatoshis          : getSatoshis,
      getAddressBalances   : getAddressBalances
    };

    return serviceInterface;

    /**
     * @name getAddressBalances
     * @description
     * Handle getting address balance information
     * @TODO
     * Use $q.Promise.all, etc.
     */
    function getAddressBalances(address, callback) {

        var me     = this,
            addr   = (address) ? address : FW.WALLET_ADDRESS.address,
            prefix = addr.substr(0,5),
            store  = Ext.getStore('Balances'),
            net    = (FW.WALLET_NETWORK==2) ? 'tbtc' : 'btc',
            hostA  = (FW.WALLET_NETWORK==2) ? 'tbtc.blockr.io' : 'btc.blockr.io',
            hostB  = (FW.WALLET_NETWORK==2) ? 'testnet.xchain.io' : 'xchain.io';

        // Get Address balance from blocktrail
        me.ajaxRequest({
            url: 'https://api.blocktrail.com/v1/' + net + '/address/' + address + '?api_key=' + FW.API_KEYS.BLOCKTRAIL,
            success: function(o){
                if(o.address){
                    var quantity  = (o.balance) ? numeral(o.balance * 0.00000001).format('0.00000000') : '0.00000000',
                        price_usd = me.getCurrencyPrice('bitcoin','usd'),
                        price_btc = me.getCurrencyPrice('counterparty','btc'),
                        values    = {
                            usd: numeral(parseFloat(price_usd * quantity)).format('0.00000000'),
                            btc: '1.00000000',
                            xcp: (price_btc) ? numeral(1 / price_btc).format('0.00000000') : '0.00000000'
                        };
                    me.updateAddressBalance(address, 1, 'BTC','', quantity, values);
                    me.saveStore('Balances');
                    // App store is rejecting app with donate button, so hide it if BTC balance is 0.00000000... shhh :)
                    if(Ext.os.name=='iOS'){
                        var cmp = Ext.getCmp('aboutView');
                        if(cmp){
                            if(quantity=='0.00000000'){
                                cmp.donate.hide();
                            } else {
                                cmp.donate.show();
                            }
                        }
                    }
                }
                // Handle processing callback now
                if(callback)
                    callback();
            },
            failure: function(o){
                // If the request to blocktrail API failed, fallback to slower blockr.io API
                me.ajaxRequest({
                    url: 'https://' + hostA + '/api/v1/address/info/' + address,
                    success: function(o){
                        if(o.data){
                            var quantity  = (o.data.balance) ? numeral(o.data.balance).format('0.00000000') : '0.00000000',
                                price_usd = me.getCurrencyPrice('bitcoin','usd'),
                                price_btc = me.getCurrencyPrice('counterparty','btc'),
                                values    = {
                                    usd: numeral(price_usd * quantity).format('0.00000000'),
                                    btc: '1.00000000',
                                    xcp: (price_btc) ? numeral(1 / price_btc).format('0.00000000') : '0.00000000'
                                };
                            me.updateAddressBalance(address, 1, 'BTC','', quantity, values);
                            me.saveStore('Balances');
                        }
                    },
                    callback: function(){
                        // Handle processing callback now
                        if(callback)
                            callback();
                    }
                });
            }
        });

        // Get Asset balances
        me.ajaxRequest({
            url: 'https://' + hostB + '/api/balances/' + address,
            success: function(o){
                if(o.data){
                    Ext.each(o.data, function(item){
                        var type = (item.asset=='XCP') ? 1 : 2;
                        me.updateAddressBalance(address, type, item.asset, item.asset_longname, item.quantity, item.estimated_value);
                    });
                } else {
                    // Show 0.00000000 for XCP balance if we have none (prevent display on iOS)
                    if(!(me.isNative && Ext.os.name=='iOS'))
                        me.updateAddressBalance(address, 1, 'XCP', '', '0.00000000');
                }
                me.saveStore('Balances');
            }
        }, true);
    }

    // Handle creating/updating address balance records in datastore
    function updateAddressBalance(address, type, asset, asset_longname, quantity, estimated_value){
      // console.log('updateAddressBalance address, type, asset, asset_longname, quantity, estimated_value=',address, type, asset, asset_longname, quantity, estimated_value);
      var me   = this,
        addr   = (address) ? address : FW.WALLET_ADDRESS,
        prefix = addr.substr(0,5),
        store  = Ext.getStore('Balances');
        record = store.add({
          id: prefix + '-' + asset,
          type: type,
          prefix: prefix,
          asset: asset,
          asset_longname: asset_longname,
          display_name: (asset_longname!='') ? asset_longname : asset,
          quantity: quantity,
          estimated_value: estimated_value
        });
      // Mark record as dirty, so we save it to disk on the next sync
      record[0].setDirty();
    }

    /**
     * @ngdoc method
     * @name getSatoshis
     * @description
     * Handle converting an amount to satoshis
     */
    function getSatoshis(amount){
        var num = numeral(amount);
        if(/\./.test(amount))
            num.multiply(100000000);
        return parseInt(num.format('0'));
    }

    /**
     * @name getBalance
     * @description
     */
    function getBalance(asset){

      var balanceRef;

      WalletService.getUser().then(function() {

      });

      /*
        var balances = Ext.getStore('Balances'),
            balance  = 0,
            prefix   = FW.WALLET_ADDRESS.address.substr(0,5)

        balances.each(function(item){
            var rec = item.data
            if (rec.prefix == prefix && rec.asset == asset ){
                balance = rec.quantity
                return false
            }
        })
      ()
      */

      return balanceRef;
    }


  }
}())

