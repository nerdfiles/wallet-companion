/**
 * @ngdoc service
 * @name WalletCompanion.GMapsService
 * @description GMaps service.
 */
(function(){

  'use strict';

  angular.module('WalletCompanion')
    .factory('GMaps', GMapsService);

  var initialized = false;

  GMapsService.$inject = [
    '$window',
    '$http',
    '$q',
    'MAPS_API',
    'ScriptLoader'
  ];

  function GMapsService($window, $http, $q, MAPS_API, ScriptLoader) {

    var apiKey = MAPS_API.key;
    var service = {
      getDistance : getDistance,
      initialize  : initialize,
      geocode     : geocode
    };

    return service;

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.GMapsService
     * @name getDistance
     * @param {object} orig LatLng object.
     * @param {object} dest LatLng object.
     * @returns {*} undefined
     */
    function getDistance(orig, dest) {

      var gLib = $window.google.maps;

      orig = new gLib.LatLng(orig.lat, orig.lng);

      dest = new gLib.LatLng(dest.lat, dest.lng);

      var computed = gLib.geometry.spherical.computeDistanceBetween(orig, dest);

      return computed;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.GMapsService
     * @name initialize
     * @returns {*} undefined
     */
    function initialize() {

      var deferred = $q.defer();

      if (initialized === true) {
        deferred.resolve();
      } else {
        ScriptLoader.initialize
          .then(function() {
            deferred.resolve();
            initialized = true;
          });
      }

      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.GMapsService
     * @name geocode
     * @param {object} p Object.
     * @param {object} filters Object.
     * @returns {*} undefined
     */
    function geocode(p, filters) {

      var deferred = $q.defer();
      var geocoder = new $window.google.maps.Geocoder();
      var postalCode = p.profile.location.postalCode || null;
      var address = p.profile.location.address || null;
      var city = p.profile.location.city || null;
      var subregion = p.profile.location.state || null;
      var country = p.profile.location.country || null;
      var glue = ' ';
      var compiledAddress = [
        address,
        city, subregion, postalCode,
        country
      ].join(glue);

      geocoder.geocode({'address': compiledAddress}, function(results, status) {

        var res;
        var distance;
        var loc;
        var destCoords;

        try {

          if (status && status === google.maps.GeocoderStatus.OK) {

            res = _.first(result);
            loc = res.geometry.location;
            destCoords = {
              lat: loc.lat(),
              lng: loc.lng()
            };

            distance = getDistance(filters.current, destCoords);

            p.profile.location.geocode = {
              location    : loc,
              coordinates : destCoords,
              distance    : distance
            };

            deferred.resolve(p);
          } else {
            deferred.resolve(p);
          }
        } catch(e) {
          deferred.reject({ error: e });
        }
      });

      return deferred.promise;
    }

  }
})();
