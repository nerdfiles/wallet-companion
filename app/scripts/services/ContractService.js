
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .service('ContractService', [
      '$q',
      '$timeout',
      'web3',
      ContractService
    ]);

  /**
   * ContractService
   *
   * @param $timeout
   * @returns {undefined}
   */
  function ContractService($q, $timeout, web3) {

    var serviceInterface = this;

    serviceInterface.getContracts = getContracts;

    /**
     * getContracts
     *
     * @returns {undefined}
     */
    function getContracts() {

      var defer = $q.defer();

      if (!web3.isConnected()) {
        defer.reject({ error: '404' });
      }

      defer.resolve({ collection: [] });

      return defer.promise;
    }

  }
}());


