/**
 * @ngdoc service
 * @name WalletCompanion.UserService
 * @description
 * @BEGIN
 */
(function () {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('UserService', UserService);

  UserService.$inject = [
    '$rootScope',
    '$window',
    '$ionicPlatform',
    '$http',
    '$q',
    'Restangular',
    'ApiService',
    'API_ENDPOINT',
    '$ionicLoading',
    'ErrorService',
    'LedgerService'
  ];

  /**
   * UserService
   *
   * @param $rootScope
   * @param $window
   * @param $ionicPlatform
   * @param $http
   * @param $q
   * @param Restangular
   * @param ApiService
   * @param API_ENDPOINT
   * @param $ionicLoading
   * @param ErrorService
   * @param LedgerService
   * @returns {undefined}
   */
  function UserService($rootScope, $window, $ionicPlatform, $http, $q, Restangular, ApiService, API_ENDPOINT, $ionicLoading, ErrorService, LedgerService) {

    var _api = API_ENDPOINT;
    var endpoint = _api.port ? [
      _api.host,
      ':',
      _api.port,
      _api.path
    ].join('') : [
      _api.host,
      _api.path
    ].join('');

    var isLoggedIn = false;
    var currentUser = {};
    var client_logged_in = moment().unix();
    var db;

    var service = {
      createAccount             : createAccount,
      createLocalSession        : createLocalSession,
      loadLocalSession          : loadLocalSession,
      deleteLocalSession        : deleteLocalSession,
      getSession                : getSession,
      isLoggedIn                : isLoggedIn,
      login                     : login,
      logout                    : logout,
      suggestPassphrase         : suggestPassphrase,
      findAddresses             : findAddresses,
      getCurrentUser            : getCurrentUser,
      getUser                   : getUser,
      getUserPaymentMethods     : getUserPaymentMethods,
      getCurrentUserForPayments : getCurrentUserForPayments,
      addPaymentMethod          : addPaymentMethod,
      getLocalTime              : getLocalTime,
      updateCurrentUser         : updateCurrentUser,
      logNewUser                : logNewUser,
      removeCurrentUser         : removeCurrentUser,
      addBillingInfo            : addBillingInfo,
      removeCcInfo              : removeCcInfo,
      forgot                    : forgot,
      support                   : support
    };

    var localTime;
    setLocalTime();

    /*
     * @EXPORTS
     **/

    return service;

    ////////////

    /**
     * @name forgot
     * @param user
     * @returns {undefined}
     */
    function forgot(user) {
      var deferred = $q.defer();
      var forgot = Restangular.all('../../forgot');

      forgot.post({
        email: user.username
      })
        .then(function(res) {

          deferred.resolve(res.data);

        }, function(e) {

          ErrorService.notify(e)
            .then(function(res) {

              deferred.reject(res);
            }, function(e) {
              console.log(e);
            });
        });

      return deferred.promise;

    }

    /**
     * @name support
     * @param {object} feedback With Subject and Message attached.
     * @returns {undefined}
     * @description Why
     */
    function support(feedback) {
      var deferred = $q.defer();
      var support = Restangular.all('../../app/support');
      return support.post(feedback);
    }

    /**
     * @ngdoc method
     * @name removeCcInfo
     * @returns {undefined}
     */
    function removeCcInfo() {
      try {
        delete currentUser._expiryDate;
      } catch(e) {
        console.log(e);
      }
      try {
        delete currentUser.ccInfo;
      } catch(e) {
        console.log(e);
      }
      try {
        delete currentUser.billing;
      } catch(e) {
        console.log(e);
      }
    }

    /**
     * @name addBillingInfo
     * @param user
     * @returns {undefined}
     */
    function addBillingInfo(user) {
      if (user.billing) {
        currentUser.billing = _.extend({}, user.billing);
      }
    }

    /**
     * @name updateCurrentUser
     * @returns {undefined}
     */
    function updateCurrentUser(userData) {

      var deferred = $q.defer();

      if (userData && userData.verification && userData.verification.hasAcceptedTerms) {
        _.extend(currentUser, userData);
        deferred.resolve(currentUser);
        return deferred.promise;
      }

      if (!userData._id) {
        _.extend(currentUser, userData);
        deferred.resolve(currentUser);
        return deferred.promise;
      }

      getCurrentUser(true).then(function(user) {

        if (!user._id) {
          _.extend(currentUser, userData);
          return deferred.resolve(currentUser);
        }

        var users = Restangular.one('users', user._id);

        users.put(userData).then(function(data) {
          deferred.resolve(data.data.user);
        }, function(e) {

          console.log('User not found', e);
          deferred.reject(e);
        });

      }, function(e) {

        deferred.reject(currentUser);

        if (!userData._id) {
          _.extend(currentUser, userData);
          currentUser.loaded = false;
        } else {
          deferred.reject(currentUser);
        }
      });

      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getLocalTime
     * @returns {*} undefined
     */
    function getLocalTime() {
      return localTime;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name setLocalTime
     * @returns {*} undefined
     */
    function setLocalTime() {
      localTime = moment().subtract(60, 'minutes').unix();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getCurrentUser
     * @returns {*} undefined
     */
    function getCurrentUserForPayments(then) {
      if (then) {
        var deferred = $q.defer();

        // deferred.resolve(currentUser);

        loadLocalSession().then(ErrorService.notify(function(userData) {
          var user_id = userData._id || userData.user_id;

          if (!user_id && !currentUser.loaded) {
            currentUser.loaded = false;
            return deferred.resolve(currentUser);
          }

          getUser(user_id).then(function(currentUser) {
            currentUser.loaded = true;
            deferred.resolve(currentUser);
          }, function(e) {

            deleteLocalSession().then(function(log){
              console.log(log);
            }, function(e) {
              console.log(e);
            });

            currentUser.loaded = false;
            deferred.resolve(currentUser);
          });

        }, function(e) {
          console.log(e);
          currentUser.loaded = false;
          deferred.resolve(currentUser);
        }), (e) => {

          deleteLocalSession().then(function(log){
            console.log(log);
          }, function(e) {
            console.log(e);
          });

          currentUser.loaded = false;
          deferred.resolve(e);
        });

        return deferred.promise;
      } else {
        return currentUser;
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getCurrentUser
     * @returns {*} undefined
     */
    function getCurrentUser(then) {
      if (then) {
        var deferred = $q.defer();

        if (currentUser && currentUser._id) {
          console.log('Found hot user: ', currentUser);
          deferred.resolve(currentUser);
          return deferred.promise;
        }

        loadLocalSession().then(ErrorService.notify(function(userData) {
          var user_id = userData._id || userData.user_id;

          if (!user_id && !currentUser.loaded) {
            currentUser.loaded = false;
            return deferred.resolve(currentUser);
          }

          getUser(user_id).then(function(_currentUser) {

            _.extend(currentUser, _currentUser);

            currentUser.loaded = true;

            deferred.resolve(currentUser);
          }, function(e) {

            deleteLocalSession().then(function(log){
              console.log(log);
            }, function(e) {
              console.log(e);
            });

            currentUser.loaded = false;
            deferred.resolve(currentUser);
          });

        }, function(e) {
          console.log(e);
          currentUser.loaded = false;
          deferred.reject(currentUser);
        }), (e) => {

          deleteLocalSession().then(function(log){
            console.log(log);
          }, function(e) {
            console.log(e);
          });

          currentUser.loaded = false;
          deferred.reject(e);
        });

        return deferred.promise;
      } else {
        return currentUser;
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name addPaymentMethod
     * @param {object} user_id TBD
     * @param {object} token TBD
     * @returns {*} undefined
     */
    function addPaymentMethod(user_id, token) {
      var action = Restangular.service('action', Restangular.one('users', user_id));
      return action.post({
        action: {
          key: 'addPaymentMethod',
          params: {
            token: token
          }
        }
      }).then(function(data) {
        return data;
      }, function(e) {
        return e;
      });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getUser
     * @param {object} user_id TBD
     * @returns {*} undefined
     */
    function getUser(user_id) {
      var users = Restangular.one('users', user_id);
      return users.get().then(function(res) {
        return res.data.user;
      }, function(e) {
        return e;
      });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getUsers
     * @returns {*} undefined
     */
    function getUsers() {
      var users = Restangular.all('users');
      return users.getList().then(function(res) {
        return res.data.user;
      }, function(e) {
        return e;
      });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getUserPaymentMethods
     * @param {object} user_id TBD
     * @param {object} user TBD
     * @returns {*} undefined
     */
    function getUserPaymentMethods(user_id, user) {
      var users = Restangular.one('users', user_id);
      return users.customPOST({
        stripe: { customerId: user.stripe.customerId }
      }, 'payment-methods', {}).then(function(res) {
        return res.data.methods.data;
      }, (e) => {
        return e;
      });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name isLoggedIn
     * @returns {*} undefined
     */
    function isLoggedIn() {
      return isLoggedIn;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name getSession
     * @returns {*} undefined
     */
    function getSession() {
      return currentUser;
    }

    function findAddresses() {

      var defer = $q.defer();

      LedgerService.listAccounts()
        .then(function(res) {
          defer.resolve(res)
        })
        .catch(function(err) {
          defer.reject({ error: err })
        })
      return defer.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name suggestPassphrase
     * @returns {object:Promise} Promise object for Passphrase.
     */
    function suggestPassphrase() {
      return Restangular
        .one('../../passphrase')
        .get().then((response) => {
          var activity, outcome;
          try {
            activity = response.data.passphrase.join(' ');
          } catch (e) {
            activity = e;
          } finally {
            outcome = activity;
          }
          return outcome;
        }, function(e) {
          return e;
        }).catch((error) => {
          console.log(error);
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name login
     * @param {object} user TBD
     * @returns {*} undefined
     */
    function login(user) {

      var deferred = $q.defer();

      if (typeof user === 'undefined' || !user.email || !user.password) {
        deferred.reject({
          'error': 'user_pass_incomplete'
        });
      }
      else {
        var auth = Restangular.all('auth');
        var authenticatedSession = {};
        auth
          .post({
            email: user.email,
            password: user.password
          })
          .then((res) => {

            if (!res.data.user) {
              deferred.reject({
                'error': 'user_not_found'
              });
            } else {

              isLoggedIn = true;

              currentUser = res.data.user;

              $q
                .all([
                  deleteLocalSession(),
                  createLocalSession(),
                  loadLocalSession()
                ])
                .then(() => {})
                .finally(() => {
                  deferred.resolve(res.data.user);
                });

            }
          }, function(e) {
            deferred.reject({ error: e });
          })
          .catch(function(e) {
            deferred.reject({ error: e });
          });
      }
      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name logout
     * @param {object} user TBD
     * @returns {*} undefined
     */
    function logout(user) {
      currentUser = {};
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name createAccount
     * @param {object} userData TBD
     * @returns {*} undefined
     */
    function createAccount(userData) {
      var users = Restangular.all('users');
      return users.post(userData)
        .then((res) => {
          return login(userData).then(function() {
            return res.data.user;
          }, function(e) {
            console.log(e);
            return e;
          });
        }, (e) => {
          console.log(e);
          return e;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name createLocalSession
     * @returns {*} undefined
     */
    function createLocalSession() {
      var deferred = $q.defer();

      var glue = ' ';
      var sqlLines = [
        'INSERT INTO local_session',
        '(token,user_id,role,client_logged_in,created_at,updated_at,profile_verification,profile_completion)',
        'VALUES (?,?,?,?,?,?,?,?)'
      ];
      var sqlStatement = sqlLines.join(glue);
      var data = [
        '(not_implemented)',
        currentUser.id,
        currentUser.role,
        client_logged_in.toString(),
        currentUser.createdAt,
        currentUser.updatedAt,
        currentUser.verification.isVerified.toString(),
        currentUser.verification.isComplete.toString()
      ];

      $window.db.init().then((db) => {
        $window.db._.execute(db, sqlStatement, data).then((res) => {
          deferred.resolve(res);
        }, (err) => {
          deferred.reject(err);
        });
      }, (e) => {
        deferred.reject(e);
      });
      return deferred.promise;

    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name loadLocalSession
     * @returns {object:Promise} A local session object.
     */
    function loadLocalSession() {

      var deferred = $q.defer();

      // @TODO Datetime comparison breaks between months...
      var sqlStatement = 'SELECT * FROM local_session WHERE Datetime(client_logged_in, \'unixepoch\', \'localtime\') >= Datetime(' + getLocalTime() + ', \'unixepoch\', \'localtime\') ORDER BY Datetime(client_logged_in, \'unixepoch\', \'localtime\') DESC Limit 1';

      $window.db.init().then((db) => {

        var trx = $window.db._.execute(db, sqlStatement);
        var user;

        trx.then((res) => {
          user = _.extend({}, currentUser);

          if (res.rows && res.rows.length) {
            _.extend(user, res.rows[0]);
          } else if (_.isFunction(res.rows.items)) {
            try {
              _.extend(user, res.rows.items(0));
            } catch (e) {
              console.log(e);
            }
          } else {
            user.loaded = false
            user.local = false
            return deferred.resolve(user);
          }

          try {
            user.loaded = true;
            user.local = true;
          } catch(e) {
            console.log(e);
          }

          currentUser = user;
          deferred.resolve(currentUser);

        }, (e) => {

          currentUser.loaded = true;
          currentUser.local = false;
          deferred.resolve(currentUser);

        });

      }, (e) => {

        currentUser.loaded = true;
        currentUser.local = false;
        deferred.resolve(currentUser);
      });

      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.UserService
     * @name deleteLocalSession
     * @param {number} user_id User ID.
     * @returns {object:Promise} Result on SQL operation.
     */
    function deleteLocalSession(user_id) {
      var deferred = $q.defer();
      // user_id = currentUser._id || user_id
      var param = [];

      var sqlStatement = 'DELETE FROM local_session';

      $window.db.init().then((db) => {
        $window.db._.execute(db, sqlStatement).then((res) => {
          deferred.resolve(res);
        }, (err) => {
          deferred.resolve(err);
        });
      }, (err) => {
        deferred.reject(err);
      });

      return deferred.promise;
    }

    /**
     * @name logNewUser
     * @param {object} user User object.
     * @returns {*} undefined
     * @description TODO Make call for user based on user ID
     */
    function logNewUser(user) {
      var deferred = $q.defer();
      currentUser = _.extend(currentUser, user);
      deferred.resolve(currentUser);
      return deferred.promise;
    }

    function removeCurrentUser() {
      var deferred = $q.defer();
      deferred.resolve(true);
      currentUser = {};
      return deferred.promise;
    }
  }
})();
