/**
 * @fileOverview ./app/scripts/services/TalkService.js
 * @description
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .service('TalkService', [
      'AppService',
      'FW',
      '$q',
      'web3',
      'localStorageService',
      TalkService
    ]);

  /**
   * TalkService
   *
   * @param AppService
   * @param FW
   * @param $q
   * @param web3
   * @param localStorageService
   * @returns {undefined}
   */
  function TalkService(AppService, FW, $q, web3, localStorageService) {

    return {
      getProvider: getProvider
    };

    /**
     * getProvider
     *
     * @returns {undefined}
     */
    function getProvider() {
      var defer = $q.defer()
      try {
        defer.resolve({ item: web3.currentProvider });
      } catch(err) {
        defer.reject({ error: err });
      }
      return defer.promise;
    }

  }
}());
