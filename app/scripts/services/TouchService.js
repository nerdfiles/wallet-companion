/**
 * @name TouchService
 * @description
 * TouchService concerns restful endpoint resolutions to higher order modular
 * credit calculus (MCC) across distributed quantum circuits based on
 * distributed semaphores and universional cytome module. (UCM).
 */

(function() {

  'use strict';

  angular
  .module('WalletCompanion')
  .factory('TouchService', [
    'localStorageService',
    'AppService',
    'PasscodeService',
    'FW',
    'touchid',
    TouchService
  ]);

  function TouchService(localStorageService, AppService, PasscodeService, FW, $timeout, touchid) {

    var serviceInterface = {

      /**
       * resetTouchId
       *
       * @returns {undefined}
       * @description
       * Handle resetting the wallet to use default passcode, and re-encrypt
       * wallet.
       */
      resetTouchId: function() {
        var self  = this;
        var sm  = localStorageService;
        var cmp = AppService.getCmp('settingsPanel');
        cmp.toggleField(cmp.touchid, 0);
        sm.removeItem('touchid');
        FW.TOUCHID = false;
      },

      /**
       * enableTouchID
       *
       * @returns {undefined}
       * @description
       * Handle enabling Touch ID authentication
       */
      enableTouchID: function() {
        var self  = this;
        var sm  = localStorageService;
        var cmp = AppService.getCmp('settingsPanel');

        // Define function that enables touchID and disables passcode  toggle
        // fields
        var successFn = function() {
          cmp.toggleField(cmp.touchid, 1, 'Touch ID enabled');
          sm.setItem('touchid', true);
          FW.TOUCHID = true;
          PasscodeService.resetPasscode();
        };
        // Define function that disables touchID toggle field
        var errorFn = function(){
          cmp.toggleField(cmp.touchid, 0);
        }
        self.authenticateTouchID(successFn, errorFn, 'Enable Touch ID Authentication');
      },


      /**
       * disableTouchID
       *
       * @description
       * Handle disabling Touch ID
       * @returns {undefined}
       */
      disableTouchID: function() {
        var self  = this;
        var sm  = localStorageService;
        var cmp = AppService.getCmp('settingsPanel');

        // Define success function that disables touchId
        var successFn = function() {
          cmp.toggleField(cmp.touchid, 0,'Touch ID disabled');
          self.resetTouchId();
        };
        var errorFn = function() {
          cmp.toggleField(cmp.touchid, 1);
        }
        var authenticateTouchIdLabel = 'Disable Touch ID Authentication';
        self.authenticateTouchID(successFn, errorFn, authenticateTouchIdLabel);
      },


      /**
       * authenticateTouchID
       *
       * @param successFn
       * @param errorFn
       * @param text
       * @param force
       * @description
       * Handle Touch ID Authentication (iOS 8+)
       * @returns {undefined}
       */
      authenticateTouchID: function(successFn, errorFn, text, force) {
        var self = this;
        // Define error callback function
        var errorCb = function() {
          if (force) {
            $timeout(function() {
              $ionicPopup.alert(null,'Invalid Authentication', function() {
                self.authenticateTouchID(successFn, errorFn, text, force);
              })
            }, 10);
          } else if (typeof errorFn === 'function') {
            errorFn();
          }
        }

        // Handle requesting Touch ID authentication via plugin
        if (AppService.isNative) {
          touchid.authenticate(successFn, errorCb, text);
        } else {
        // Fake success for non-native
          successFn();
        }
      }
    }

    return serviceInterface;
  }
}());

