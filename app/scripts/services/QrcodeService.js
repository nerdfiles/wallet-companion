
/**
 * @fileOverview ./app/scripts/services/QrcodeService.js
 * @description
 * A module for programmable ledger-centric 'service' words to detail a
 * front end implemention given to microservice grammar (commands) and
 * restful endpoint (segmentarities; 'field'-like things) compositions[0] for
 * management of stateless, cached server-side abstractions (models).
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 * ---
 * [0]: http://microservices.io/patterns/data/cqrs.html
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('QrcodeService', [
      '$q',
      QrcodeService
    ]);

  /**
   * QrcodeService
   *
   * @param $q
   * @returns {undefined}
   */
  function QrcodeService($q) {

    var serviceInterface = {
      scan: scan
    };

    return serviceInterface;

    /**
     * scan
     *
     * @returns {undefined}
     */
    function scan() {
    }
  }

}());

