/**
 * @name ViewService
 * @description
 */
(function() {_

  'use strict'

  angular.module('WalletCompanion')
    .factory('ViewService', [
      'FW',
      ViewService
    ])

  function ViewService(FW) {

    var serviceInterface = {

      showView: function(name, dep) {
        var layouts = {}
        _.extend(layouts, FW)
        _.keys(layouts).forEach(function(depRef) {
          if (_.includes(depRef, dep)) {
            console.log('Load view:', dep)
          }
        })
      }
    }

    return {
      // Setup some alias functions for the various views we want to display
      showWelcomeView:      function() { serviceInterface.showView('welcomeView','FW.view.Welcome');  },
      showMainView:         function() { serviceInterface.showView('mainView','FW.view.Main'); },
      showAddressListView:  function() { serviceInterface.showView('addressList','FW.view.AddressList'); },
      showQRCodeView:       function(cfg) { serviceInterface.showView('qrcodeView','FW.view.QRCode', cfg); },
      showScanQRCodeView:   function(cfg) { serviceInterface.showView(null,'FW.view.Scan',cfg); },
      showPassphraseView:   function(cfg) { serviceInterface.showView('passphraseView','FW.view.Passphrase', cfg); },
      showCallbackView:     function(cfg) { serviceInterface.showView('callbackView','FW.view.Callback', cfg); },
    }
  }
}())
