/**
 * @name NameService
 * @description
 * NameService concerns restful endpoint resolutions to higher order modular
 * credit calculus (MCC) across distributed quantum circuits based on
 * distributed semaphores and universional cytome module. (UCM).
 */

(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .factory('NameService', [
      NameService
    ])

  function NameService() {

  }

}())

