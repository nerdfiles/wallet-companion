/**
 * @fileOverview ./app/scripts/services/LogService.js
 * @name LogService
 * @description A logging service.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .service('LogService', [
      LogService
    ]);

  /**
   * LogService
   *
   * @returns {undefined}
   */
  function LogService() {

    // ## Service Init

    var si = this;

    // ## Service Data

    var logs = [];

    // Service Capabilities

    si.log = log;
    si.register = register;

    /////////////////

    // ## Service Internals

    /**
     * @ngdoc method
     * @name log
     * @param type
     * @param params
     * @returns {undefined}
     */
    function log(type, params) {
      console.log('Fired: ', type);
      logs.forEach(function(callback) {
        callback(params);
      });
    }

    /**
     * @ngdoc method
     * @name register
     * @param callback
     * @returns {undefined}
     */
    function register(callback) {
      logs.push(callback);
    }

  }
})();

