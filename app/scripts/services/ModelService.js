// ./app/scripts/services/ModelService.js
/**
 * @ngdoc service
 * @name WalletCompanion.ModelService
 * @description Model Service:
 * 1. Actions: Augments data model with MODEL.action.{{API_ACTION_NAME}}() (to
 * be used in `ng-click` or `ng-href`, assuming the HTTP METHOD.
 * 2. Caching: Creates a place for synchronized model data that has been requested such
 * that the front end can cache and manage data based on its model as reflected
 * in the REST API.
 * 3. Combine: Create higher order methods like "save" to be used at a controller (rather
 * than using raw `ApiService` methods. Combine REST calls logic in a generic
 * way rather than at the controller.
 * 4. Create a "working item" directory under each model, and initialize
 * collections in a uniform way.
 */

(function() {
  'use strict';
  angular.module('WalletCompanion')
    .service('ModelService', ModelService);

  ModelService.$inject = ['Restangular', 'ApiService', '$q'];
  /**
   * @name ModelService
   * @returns {*} undefined
   */
  function ModelService(Restangular, ApiService, $q) {

    var service = this;
    this.modelResource = {};

    this.models = {
      coins: {
        collection: [],
        item: {}
      },
      tickets: {
        collection: [],
        item: {}
      },
      contracts: {
        collection: [],
        item: {}
      },
      offers: {
        collection: [],
        item: {}
      },
      users: {
        collection: [],
        item: {}
      }
    };

    /**
     * @name save
     * @returns {*} undefined
     */
    this.save = function() {
      if (this.item._id) {
        return this.modelResource[name].update(this.item);
      } else {
        // @TODO add `create` for ApiService
        return this.modelResource[name].create(this.item)
          .then(function(itemId) {
            return itemId;
          }, function(e) {
            // console.log(e);
          });
      }
    };

    /**
     * @name delete
     * @returns {*} undefined
     */
    this.delete = function() {
      // @stub
    };

    /**
     * @name init
     * @param {string} name Model name
     * @param {string} operation Restangular operation.
     * @param {object} data Data for use.
     * @param {object} $service API resource (a factory for getting and
     * initializing data).
     * @returns {*} undefined
     */
    service.init = function(name, operation, data, $service) {
      Restangular.addElementTransformer(name, false, function(element) {
        element.loaded = true;
        return element;
      });
      this.modelResource[name] = $service[name];
      this.models[name] = this.models[name] || data;
    }

    /**
     * @name initCollection
     * @param {string} modelId Model ID
     * @returns {*} undefined
     */
    this.initCollection = function(name, data) {
      service.getModel(name).collection = data;
      return data;
    };

    /**
     * @name initItem
     * @param modelId
     * @returns {*} undefined
     */
    this.initItem = function(schema, url, data, name) {
      this.enactModel(name, data);
      var deferred = $q.defer();
      var id = url.split('/').pop();
      if (id) {
        try {
          this.modelResource[name].get(id)
            .then(function(model) {
              deferred.resolve(model);
            }, function(e) {
              // console.log(e);
            });
        } catch(e) {
          // console.log(e);
        }
      } else {
        service.item = {
          _id: undefined
        };
        deferred.resolve(service.item);
      }
      return deferred.promise;
    };

    /**
     * @name enactModel
     * @param modelName
     * @param data
     * @returns {undefined}
     */
    this.enactModel = function(modelName, data) {
      Restangular.extendModel(modelName, function(model) {
        model.action = {};
        var _name = modelName.replace(/ies$/, 'y');
        _name = _name.replace(/s$/, '');


//         if (data && !data[_name]) {
//           return model;
//         }

//         if (!data[_name] || !data[_name].links) {

//           _.each(data[_name].links, function(link) {
//             if (/action$/.test(link.href)) {

//               /**
//                * @usage ng-click="vm.reservation.completePayment({})"
//                */
//               model.action[link.rel] = function(params) {
//                 service.modelResource[modelName].action(model._id)(link.rel, params)
//               };
//             } else {
//               /**
//                * @usage ng-href="vm.reservation.self()"
//                */
//               model.action[link.rel] = function() {
//                 // console.log('Model has method: ' + link.rel + '(): ', link.href);
//                 return link.href;
//               };
//             }
//           });

//         }
        return model;
      });
    };

    /**
     * @name getModel
     * @param modelId='users'
     * @returns {undefined}
     */
    this.getModel = function(name) {
      var emptyModel = { collection: [], item: {} };
      this.models[name] = this.models[name] ? this.models[name] : emptyModel;
      return this.models[name] || {};
    };

    /**
     * @name modelNamespace
     * @param {string} name Model name.
     * @returns {*} undefined
     */
    function modelNamespace(name, factory) {
      if (!factory) {
        return function(data, operation, schema, url) {
          if (name === schema && operation === 'get' && Object.keys(service.models[name].item).length) {
            return service.models[name].item;
          }
          if (name === schema && operation === 'getList' && service.models[name].collection.length) {
            return service.models[name].collection;
          }
          return data;
        }
      }

      return function(data, operation, schema, url, response, deferred) {
        // diff of time since last call?
        if (name === schema && operation === 'get') {
          if (service.models[name] && !Object.keys(service.models[name].item).length && service.modelResource[name]) {
            service.initItem(schema, url, data, name).then(function(item) {
              var modelName = angular.copy(name);
              var _name = modelName.replace(/ies$/, 'y');
              _name = _name.replace(/s$/, '');
              var newItem = Restangular.stripRestangular(item.data);
              var modelData = newItem[_name];
              var extendedModel = _.extend(newItem, modelData);
              delete extendedModel[_name];
              // console.log('ℹ️', extendedModel);
              service.models[name].item = extendedModel;
            }, function(e) {
              // console.log(e);
            });
          }
        } else if (name === schema && operation === 'getList') {
          if (service.models[name] && !service.models[name].collection.length) {
            service.initCollection(name, data);
          }
        }

        service.init(name, operation, data, factory);
        service.enactModel(name, data);
        return data;
      };
    }

    Restangular.addResponseInterceptor(modelNamespace('users', ApiService));
    Restangular.addResponseInterceptor(modelNamespace('contracts', ApiService));
    Restangular.addResponseInterceptor(modelNamespace('offers', ApiService));
    Restangular.addResponseInterceptor(modelNamespace('tickets', ApiService));
    Restangular.addResponseInterceptor(modelNamespace('coins', ApiService));
    Restangular.addRequestInterceptor(modelNamespace('users'));
    Restangular.addRequestInterceptor(modelNamespace('contracts'));
    Restangular.addRequestInterceptor(modelNamespace('offers'));
    Restangular.addRequestInterceptor(modelNamespace('tickets'));
    Restangular.addRequestInterceptor(modelNamespace('coins'));
  }
})();
