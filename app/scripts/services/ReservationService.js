/**
 * @ngdoc service
 * @name Wallet Companion.ReservationService
 * @description Messages API.
 * @api http://api.companion.money/api/v1/messages
 * A module for programmable contract/message-centric "service" words to detail a
 * front end implemention given to microservice grammar (commands) and
 * restful endpoint (segmentarities; "field"-like things) compositions[0] for
 * management of stateless, cached server-side abstractions (models).
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 */
(function(){

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('ReservationService', ReservationService);

  ReservationService.$inject = [
    '$rootScope',
    '$window',
    '$ionicPlatform',
    'Restangular',
    '$q',
    'cuid'
  ];

  function ReservationService($rootScope, $window, $ionicPlatform, Restangular, $q, cuid) {

    var currentMessage = {};

    var service = {
      createReservation     : createReservation,
      getReservationById    : getReservationById,
      // getReservationById : _getReservationById,
      getReservations       : getReservations,
      updateReservation     : updateReservation,
      deleteReservation     : deleteReservation,
      getCalendarEvents     : getCalendarEvents,
      currentMessage        : currentMessage,
      updateCurrentMessage  : updateCurrentMessage,
      getCurrentMessage     : getCurrentMessage,
      cancelMessage         : cancelMessage,
      removeCurrentMessage  : removeCurrentMessage
    };

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name cancelMessage
     * @returns {*} undefined
     */
    function cancelMessage() {
      currentMessage = {
        _id: cuid()
      };
    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name updateCurrentMessage
     * @param {object} data TBD
     * @returns {*} undefined
     */
    function updateCurrentMessage(data) {
      _.extend(currentMessage, data || {});
    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name getCurrentMessage
     * @returns {object:Promise} Current message object.
     */
    function getCurrentMessage() {
      var deferred = $q.defer();
      if (currentMessage) {
        deferred.resolve(currentMessage);
      } else {
        deferred.reject(currentMessage);
      }
      return deferred.promise;
    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name removeCurrentMessage
     * @returns {*} undefined
     */
    function removeCurrentMessage() {
      currentMessage = {};
    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name getCalendarEvents
     * @returns {array} Array for UI Calendar events.
     */
    function getCalendarEvents() {
      var events = [
        /*
        // Example event - to be refactored into user-specific booking dates in future release
        {
          title: 'Example Event 1',
          startTime: moment().startOf('day').format('YYYY/MM/DD HH:mm'),
          endTime: moment().endOf('day').format('YYYY/MM/DD HH:mm'),
          allDay: false
        } */
      ];
      return events;
    }

    /**
     * @ngdoc method
     * @name createReservation
     * @methodOf Wallet Companion.ReservationService
     * @param {object} messageData TBD
     * @returns {object:Promise} Create Reservation
     */
    function createReservation(messageData) {
      var messages = Restangular.all('messages');
      var message = {};
      message.coins = messageData.coins;
      message.dates = messageData.dates;

      return messages.post(message)
        .then(createReservationCompleted, createReservationFailed);

        /**
         * @function createReservationFailed
         * @param {object} e Error object.
         * @returns {object} Parent $object.
         */
        function createReservationFailed(e) {
          currentMessage = {};
          return e.data;
        }

        /**
         * createReservationCompleted
         *
         * @param {object} res TBD
         * @returns {*} undefined
         */
        function createReservationCompleted(res) {
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res;
          }
        }
    }

    /**
     * @ngdoc method
     * @name getReservations
     * @methodOf Wallet Companion.ReservationService
     * @returns {object:Promise} Reservation.
     */
    function getReservations() {
      var messages = Restangular.all('messages');
      return messages.getList()
        .then(getMessagesCompleted, getMessagesFailed);

      /**
       * @function getMessagesCompleted
       * @param {object} res Restangular $object.
       * @returns {object} Restangular $object data.
       */
      function getMessagesCompleted (res) {
        if (res.error) {
          return {
            'error': res.error.message
          };
        } else {
          if (res.data) {
            return res.data;
          }
        }
      }

      /**
       * @function getMessagesFailed
       * @param {string} e Error object.
       * @returns {*} undefined
       */
      function getMessagesFailed (e) {
        console.log(e);
      }

    }

    /**
     * @ngdoc method
     * @name getReservations
     * @methodOf Wallet Companion.ReservationService
     * @returns {object:Promise} Reservation.
     */
    function _getReservationById(id) {
      var messages = Restangular.all('messages');
      return messages.getList()
        .then(getMessagesCompleted, getMessagesFailed);

      /**
       * @function getMessagesCompleted
       * @param {object} res Restangular $object.
       * @returns {object} Restangular $object data.
       */
      function getMessagesCompleted (res) {
        if (res.error) {
          return {
            'error': res.error.message
          };
        } else {
          if (res.data) {
            var data = _.filter(res.data, function(item) {
              return item._id === id;
            });
            return _.last(Restangular.stripRestangular(data));
          }
        }
      }

      /**
       * @function getMessagesFailed
       * @param {string} e Error object.
       * @returns {*} undefined
       */
      function getMessagesFailed (e) {
        console.log(e);
      }

    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name getReservationById
     * @param {object} id TBD
     * @returns {*} undefined
     */
    function getReservationById(id) {

      var messages = Restangular.one('messages', id);
      return messages.get().then(getReservationByIdCompleted, getReservationByIdFailed);

        /**
         * @function getReservationByIdCompleted
         * @param {object} res XHR object for booking.
         * @returns {*} undefined
         */
        function getReservationByIdCompleted (res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.message;
          }
        }

        /**
         * @function getReservationByIdFailed
         * @param {object} e Error object.
         * @returns {object} Error data (can sometimes be null).
         */
        function getReservationByIdFailed(e) {
          return e;
        }
    }

    /**
     * @ngdoc method
     * @methodOf Wallet Companion.ReservationService
     * @name id
     * @name updateReservation
     * @param {object} messageData Message form data.
     * @returns {object:Promise} Promise object.
     */
    function updateReservation(id, messageData) {
      var messages = Restangular.one('messages', id);
      return messages.put(messageData)
        .then(updateReservationCompleted, updateReservationFailed);

        /**
         * @function updateReservationFailed
         * @param {object} e Error object.
         * @returns {object} Status object data.
         */
        function updateReservationFailed(e) {
          return e.data;
        }
        /**
         * @function updateReservationCompleted
         * @param {object} res Restangular $object.
         * @returns {*} undefined
         */
        function updateReservationCompleted(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.message;
          }
        }
    }

    /**
     * @ngdoc method
     * @name deleteReservation
     * @methodOf Wallet Companion.ReservationService
     * @param {string|number}  id ID for a https://schema.org/Message.
     * @returns {*} undefined
     */
    function deleteReservation(id) {
      var messages = Restangular.one('messages', id);
      return messages.remove()
        .then(deleteReservationCompleted, deleteReservationFailed);

        /**
         * deleteReservationCompleted
         *
         * @param {object} res TBD
         * @returns {*} undefined
         */
        function deleteReservationCompleted(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.message;
          }
        }

        /**
         * deleteReservationFailed
         *
         * @param {object} e TBD
         * @returns {*} undefined
         */
        function deleteReservationFailed(e) {
          return e.data;
        }
    }

    return service;
  }
})();
