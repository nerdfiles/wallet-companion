
/**
 * @ngdoc service
 * @name WalletCompanion.OrgService
 * @description `api/v1/organizations`.
 */
(function(){
'use strict';
  angular.module('WalletCompanion')
  .factory('OrgService', OrgService);

  OrgService.$inject = [
    '$rootScope',
    '$window',
    '$ionicPlatform',
    '$http',
    'Restangular',
    'API_ENDPOINT',
  ];
  function OrgService($rootScope, $window, $ionicPlatform, $http, Restangular, API_ENDPOINT) {
    var _api = API_ENDPOINT;
    var endpoint = _api.port
      ? (_api.host + ':' + _api.port + _api.path)
      : (_api.host + _api.path);
    var isLoggedIn = false;
    var currentOrg = {};
    var service = {
      createOrg  : createOrg,
      getOrg     : getOrg,
      getOrgs    : getOrgs,
      updateOrgs : updateOrgs,
      updateOrg  : updateOrg,
      deleteOrg  : deleteOrg
    };

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name createOrg
     * @param {object} orgData TBD
     * @returns {object:Promise} Promise object.
     */
    function createOrg(orgData) {
      var orgs = Restangular.all('organizations');
      return orgs.post(orgData)
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organizations;
          }
        }, function(e) {
          return e.data;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name getOrg
     * @param {object} id TBD
     * @returns {object:Promise} Promisified response.
     */
    function getOrg(id) {
      var orgs = Restangular.one('organizations', id);
      return orgs.get()
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organization;
          }
        }, function(e) {
          return e.data;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name getOrgs
     * @returns {object:Promise} Promisified response.
     */
    function getOrgs() {
      var orgs = Restangular.all('organizations');
      return orgs.getList()
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organizations;
          }
        }, function(e) {
          return e.data;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name updateOrgs
     * @param {array} orgsData Organization objects in an array.
     * @returns {object:Promise} Promisified response.
     */
    function updateOrgs(orgsData) {
      var orgs = Restangular.all('organizations');
      return orgs.post(orgData)
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organizations;
          }
        }, function(e) {
          return e.data;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name updateOrg
     * @param {object} orgData TBD
     * @returns {object:Promise} Promisified response.
     */
    function updateOrg(id, orgData) {
      var org = Restangular.one('organizations', id);
      return org.put(orgData)
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organization;
          }
        }, function(e) {
          return e.data;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.OrgService
     * @name deleteOrg
     * @param {object} orgData TBD
     * @returns {object:Promise} Promisified response.
     */
    function deleteOrg(id) {
      var org = Restangular.one('organizations', id);
      return org.remove()
        .then(function(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return res.data.organization;
          }
        }, function(e) {
          return e.data;
        });
    }

    return service;
  }
})();
