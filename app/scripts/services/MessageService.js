
/**
 * @ngdoc service
 * @name WalletCompanion.MessageService
 * @description Offers API.
 * @api https://api.backpackercollege.com/api/v1/offers
 */
(function(){

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('MessageService', MessageService);

  MessageService.$inject = [
    '$window',
    '$http',
    '$q',
    'Restangular',
    'GMaps',
    'UserService',
    'ApiService',
    'moment',
    'uuid4'
  ];

  /**
   * MessageService
   *
   * @param $window
   * @param $http
   * @param $q
   * @param Restangular
   * @param GMaps
   * @param UserService
   * @param ApiService
   * @param moment
   * @param uuid4
   * @returns {undefined}
   */
  function MessageService($window, $http, $q, Restangular, GMaps, UserService, ApiService, moment, uuid4) {

    var service = {
      getOfferById          : getOfferById,
      getOffers             : getOffers,
      getFormedObj          : getFormedObj,
      getFilterObj          : getFilterObj,
      getSavedFilter        : getSavedFilter,
      setSavedFilter        : setSavedFilter,
      setSavedAddress       : setSavedAddress,
      getFilteredOffers     : getFilteredOffers,
      getDefaultFilterOpts  : getDefaultFilterOpts,
      createOffer           : createOffer,
      updateOffers          : updateOffers,
      updateOffer           : updateOffer,
      deleteOffer           : deleteOffer,
      getCoinsByOfferId     : getCoinsByOfferId,
      getOfferByMessage     : getOfferByMessage,
      serializeFilters      : serializeFilters,
      deserializeOffer      : deserializeOffer,
      addPhoto              : addPhoto,
      getPhoto              : getPhoto,
      resetPhoto            : resetPhoto
    };

    var offers = Restangular.all('offers');

    var images = [];

    return service;

    ////////////

    /**
     * addPhoto
     *
     * @param image
     * @returns {undefined}
     */
    function addPhoto(image) {
      images.length = 0;
      if (image.length) {
        images = image;
      } else {
        images.push(image);
      }
    }

    /**
     * getPhoto
     *
     * @returns {undefined}
     */
    function getPhoto() {
      return images.length ? images[0] : { error: 'no_photo' };
    }

    /**
     * resetPhoto
     *
     * @returns {undefined}
     */
    function resetPhoto() {
      images.length = [];
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getOfferByMessage
     * @param {object} message A message object.
     * @returns {*} undefined
     */
    function getOfferByMessage(message) {

      var deferred = $q.defer()

      var p = message
        && message.coins
        && message.coins[0]
        && message.coins[0].coin
          ? message.coins[0].coin.offer
          : null

      if (!p) {
        console.log('No offer ID found on message coin object', message)
        deferred.reject(message)
        return deferred.promise
      }

      return offers.getList()

        .then(function(res) {

          var offerData = Restangular.stripRestangular(res.data)

          var d = _.filter(offerData, function(offer) {
            return offer._id === p
          })

          return d;
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getOffers
     * @param {string} id Offer ID.
     * @param {object} filters Filters.
     * @returns {*} undefined
     */
    function getOffers(id, filters) {
      var deferred = $q.defer()
      offers.getList()
        .then(function(res) {

          console.log('raw offer list: ', res)

          res = (res && res.data)
            ? Restangular.stripRestangular(res.data)
            : Restangular.stripRestangular(res)

          var offerList = res

          if (filters.current && filters.withinDistance) {

            iterateOffers(offerList)
              .then(function(valid) {
                deferred.resolve(valid)
              }, function(err) {
                deferred.reject({
                  error: 'no_error'
                })
              })

          } else {

            deferred.reject({
              error: 'no_error'
            })
          }

          /**
           * iterateOffers
           *
           * @param offerList
           * @returns {object:Promise} Geocoded list of offers
           */
          function iterateOffers(offerList) {

            var innerDeferred = $q.defer()
            var asyncQueue = []

            offerList.forEach(function(p) {
              asyncQueue.push(GMaps.geocode(p, filters))
            })

            $q.all(asyncQueue)
              .then(function(res) {
                res = res.filter(function(x) {
                  return typeof x !== 'undefined'
                })

                innerDeferred.resolve(res)

              }, function(e) {
                console.log(e)
              })

            return innerDeferred.promise
          }
        },
        function(e) {
          deferred.reject({error: e})
        })

      return deferred.promise
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getFilteredOffers
     * @param {object} opts Options.
     * @returns {*} undefined
     */
    function getFilteredOffers(opts) {

      console.log(opts)

      try {
        delete opts.offerFilter.location.address

        if (opts.offerFilter.location.withinDistance) {
          opts.offerFilter.location.maxDistance = opts.offerFilter.location.withinDistance * 1000
          delete opts.offerFilter.location.withinDistance
        }

      } catch(e) {
        console.log(e)
      }

      console.log(opts)

      var deferred = $q.defer()
      var filtered = Restangular.all('offers/search')

      filtered.post(opts)
        .then(function(res) {

          res = (res && res.data)
            ? Restangular.stripRestangular(res.data)
            : Restangular.stripRestangular(res)

          deferred.resolve(res)

        }, function(e) {
          deferred.reject(e)
        })

      return deferred.promise
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getFormedObj
     * @param {object} offer Offer.
     * @param {object} profile Profile.
     * @returns {*} undefined
     */
    function getFormedObj(offer, profile) {
      var coins = []
      var _id = offer._id
      var currentUser = UserService.getSession()

      if (offer.coins && offer.coins.length) {
        _.each(offer.coins, function(coinRef) {
          coins.push(coinRef)
        })

      } else {

        offer.coins
        && offer.coins.fresh
        && offer.coins.fresh.forEach(function(coinRef) {
            coins.push(coinRef)
          })

        offer.coins
        && offer.coins.erc20
        && offer.coins.erc20.forEach(function(coinRef) {
            coins.push(coinRef)
          })

      }

      var name
      var phone
      var email
      var location
      var _photos = []
      var city
      var state
      var postalCode
      var country

      try {
        location = profile.location
      } catch(e) {
        console.log('Could not set location: ', e)
      }

      try {
        _photos = [profile.photo[0]]
      } catch(e) {
        console.log('No image loaded: ', e)
      }

      name = { first: 'New', last: 'User' }

      try {
        name = {
          first: currentUser.profile.name.first,
          last: currentUser.profile.name.last
        }
        phone = currentUser.profile.phone
        email = currentUser.email

      } catch(e) {
        name = { first: 'Test', last: 'User' }
        phone = '1111111111'
        email = 'offer.test.' + uuid4.generate() + '@tgm.local'
      }

      if (_photos && _photos.length && _photos[0].image.error) {
        _photos = []
      }

      if (_id) {
        return {
          _id: _id,
          assets: offer.assets,
          coins: coins,
          profile: {
            contacts: [
              {
                name: name.first + ' ' + name.last,
                phone: phone,
                email: email
              }
            ],
            offerType: offer.offerType || '',
            location: {
              address: location ? location.address : '',
              city: location ? location.city.long_name : 'Toronto',
              state: location ? location.state.long_name : 'ON',
              postalCode: location ? (location.zipCode ? location.zipCode.long_name : 'M4M') : 'M4M',
              country: location ? location.country[0] : 'CA'
            },
            title: profile.title,
            subtitle: profile.subtitle,
            description: profile.description,
            photos: _photos
          },

          geometry: {
            loc: {
              coordinates: location ? location.geometry : [0, 0]
            }
          }
        }
      }

      try {
        city       = profile.location.city.long_name || 'Toronto'
        state      = profile.location.state.long_name || 'ON'
        postalCode = profile.location.zipCode
          ? (profile.location.zipCode.long_name
            ? profile.location.zipCode.long_name
            : profile.location.zipCode)
              : '00000'
        country    = profile.location.country
          ? (profile.location.country.long_name
            ? profile.location.country.long_name
            : profile.location.country)
              : 'CA'
      } catch(e) {
        console.log(e)
      }

      return {

        assets: offer.assets,

        coins: coins,

        profile: {
          contacts: [
            {
              name: name.first + ' ' + name.last,
              phone: phone,
              email: email
            }
          ],
          offerType: offer.offerType || '',
          location: {
            address    : profile.location.address || '',
            city       : city,
            state      : state,
            postalCode : postalCode,
            country    : country
          },
          title: profile.title,
          subtitle: profile.subtitle,
          description: profile.description,
          photos: _photos
        },

        geometry: {
          loc: {
            coordinates: profile.location.geometry || [0, 0]
          }
        }

      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name createOffer
     * @param {object} offerData TBD
     * @returns {*} undefined
     */
    function createOffer(offerData) {

      var deferred = $q.defer()
      var coins = []

      _.each(offerData.coins, function(coinRefSet) {
        _.each(coinRefSet.selection, function(coinRef) {
          coinRef.coinType = (coinRef.payType !== '' ? 'erc721' : 'erc20')
          coinRef.bathType = (coinRef.coinType === 'collectibles' ? 'erc721' : 'erc20')
          // delete coinRef.payType
          delete coinRef.localId
          coins.push(coinRef)
        })

      });

      offerData.coins = coins;

      offers
        .post(offerData)
        .then(function(res) {
          console.log(res)
          if (res.error) {
            deferred.reject({'error': res.error.message})
          } else {
            deferred.resolve(res.data.offer)
          }
        }, function(e) {
          deferred.reject(e.data)
        })
      return deferred.promise
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getOfferById
     * @param {number} id ID of Offer.
     * @returns {object:Promise} Promise object.
     */
    function getOfferById(id) {

      var offer = Restangular.one('offers', id)

      return offer.get()
        .then((res) => {

          if (res.error)
            return {'error': res.error.message}
          else
            return res.data.offer
        }, (e) => {
          return e.data
        })

    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getCoinsByOfferId
     * @param {object} id TBD
     * @returns {*} undefined
     */
    function getCoinsByOfferId(id) {

      var offer = Restangular.one('offers', id)

      return offer
        .one('coins')
        .get().then(function (res) {

          return res.data.coins

        }, function(e) {
          return e.data
        })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name updateOffer
     * @param {string} id Offer ID (e.g., offer._id).
     * @param {object} offerData `assets`, `coins` {array}, `profile`, etc.
     * @returns {object:Promise} Promise object.
     */
    function updateOffer(id, offerData) {
      var offers = Restangular.one('offers', id)
      if (
        offerData.profile.photos
        && offerData.profile.photos.length
      ) {
        var profileData = offerData.profile
        delete profileData.contacts
        return offers.customPUT(profileData)
          .then(function(res) {
            console.log(res)
            if (res.error) {
              return {'error': res.error.message}
            } else {
              return ApiService.offers.action(id)('addPhoto', { photos: _.map(offerData.profile.photos, function(photo) {
                return photo.image._id
              })})
                .then(function(res) {
                  console.log('Updated offer with a new photo: ', res)
                  return { 'offer': { 'status': 'updated' } }
                }, function(e) {
                  console.log('Failed to update offer with new photo: ', e)
                  return { error: e }
                })
            }
          }, function(e) {
            return e.data
          })
      }

      var profileData = offerData.profile
      delete profileData.contacts

      return offers.customPUT(profileData)
        .then(function(res) {

          console.log(res)

          if (res.error) {

            return {'error': res.error.message}

          } else {

            return { 'offer': { 'status': 'updated' } }

          }
        }, function(e) {
          return e.data
        })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name updateOffers
     * @param {object} offersData TBD
     * @returns {object:Promise} Promise object.
     * @description TBD
     */
    function updateOffers(offersData) {

      return offers.post(offersData)
        .then(function(res) {

          console.log(res)

          if (res.error) {

            return {
              'error': res.error.message
            }

          } else {

            return res.data.offers

          }
        }, function(e) {
          return e.data
        })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name deleteOffer
     * @param {object} offerData TBD
     * @returns {object:Promise} Promise object.
     */
    function deleteOffer(id) {

      var offers = Restangular.one('offers', id)

      return offers.remove()
        .then(function(res) {

          console.log(res)

          if (res.error) {
            return {'error': res.error.message}
          } else {
            return res.data.offer
          }
        }, function(e) {
          return e.data
        })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getDefaultFilterOpts
     * @returns {*} undefined
     */
    function getDefaultFilterOpts() {

      var saved = getSavedFilter()
      if (saved) {
        return saved
      }

      return {

        offer: {
          location: {},
          coinTypes: [
            {
              name: 'ERC20 + Scrypt',
              type: 'erc20-scrypt',
              selected: false
            },
            {
              name: 'ERC721 + Scrypt',
              type: 'erc721-scrypt',
              selected: false
            },
            {
              name: 'ERC20 + SHA250',
              type: 'erc20-sha256',
              selected: false
            },
            {
              name: 'ERC721 + SHA250',
              type: 'erc721-sha256',
              selected: false
            }
          ]
        },

        location: {
          geo: {},
          address: '',
          current: [],
          useCurrent: true,
          withinDistance: 10
        },

        calendar: {
          dates: {
            from: '',
            to: ''
          }
        }
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name serializeFilters
     * @param {string} type Type.
     * @param {object} obj Object.
     * @returns {*} undefined
     */
    function serializeFilters(type, obj) {

      switch (type) {

        case 'coins':
          var returnObj = obj && obj.filter(function(x) {
            return x.selected
          }).map(function(x) {
            return x.name
          })

          if (returnObj && returnObj.length) {
            return returnObj.join(', ')
          } else {
            return 'All coins'
          }
          break
        case 'location':
          if (obj && obj.useCurrent) {
            return 'current location'
          }

          if (
            obj
            && obj.address
            && obj.address.length
          ) {
            return obj.address
          } else {
            return 'N/A'
          }
          break
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name deserializeOffer
     * @param {string} type Type.
     * @param {object} obj Object.
     * @returns {object} Deserialized offer
     */
    function deserializeOffer(prop) {

      prop.coins._parentId = prop._id
      // @see ACC https://docs.openchain.org/en/latest/api/ledger.html#acc-record
      prop.profile.coins = prop.coins
      // @see DATA https://docs.openchain.org/en/latest/api/ledger.html#data-record
      prop.profile.assets = prop.assets
      prop.profile.offerType = {
        name: prop.profile.offerType
      }
      prop.profile._parentId = prop._id

      return prop.profile
    }

    var savedFilterd

    /**
     * @name setSavedAddress
     * @param location
     * @returns {undefined}
     */
    function setSavedAddress(location) {
      if (savedFilterd && location) {
        _.extend(savedFilterd, location)
      }

      if (savedFilterd) {
        return savedFilterd
      } else {
        return {
          error: 'no_saved_address'
        }
      }
    }

    /**
     * @function setSavedFilter
     * @inner
     * @returns {undefined}
     */
    function setSavedFilter(newSavedFilter, override) {
      var def = $q.defer()
      if (newSavedFilter) {
        console.log('Saved filter: ', newSavedFilter)
      }
      savedFilterd = newSavedFilter
      if (savedFilterd && override) {
        console.log('Extend filter')
        _.extend(savedFilterd, override)
      }
      if (!newSavedFilter)
        def.reject({
          error: 'no_filter_provided'
        })
      else
        def.resolve(savedFilterd)
      return def.promise
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getFilterObj
     * @param {object} opts Options.
     * @returns {*} undefined
     */
    var useCurrent, coords, geometry
    function getFilterObj(opts) {

      if (opts.coinFilter) {
        _.extend(opts, opts.coinFilter)
      }

      if (opts.offerFilter) {
        _.extend(opts, opts.offerFilter)
      }

      console.log('filter obj: ', opts)

      try {
        useCurrent = opts.location.useCurrent
      } catch(e) {
        console.log(e)
      }

      try {
        coords = opts.location.geo.coords
      } catch(e) {
        console.log(e)
      }

      try {
        geometry = opts.location.geo.geometry
      } catch(e) {
        console.log(e)
      }

      var lat, lng

      if (geometry && _.isFunction(geometry.location.lat)) {

        lat = geometry.location.lat()
        lng = geometry.location.lng()

      } else {

        lat = 0
        lng = 0
      }

      var formedObj = {
        offerFilter: {
          location: {
            address: opts && opts.location && opts.location.address,
            coordinates    : [0, 0],
            withinDistance : opts.location.withinDistance
          }
        },
        coinFilter: {}
      }

      if (
        formedObj
        && formedObj.offerFilter
        && formedObj.offerFilter.location
        && !formedObj.offerFilter.location.withinDistance
      ) {
        formedObj.offerFilter.location.withinDistance = 10
      }

      if (coords && useCurrent) {

        formedObj.offerFilter.location.coordinates = coords
        formedObj.offerFilter.location.useCurrent = true

      } else {

        formedObj.offerFilter.location.useCurrent = false

        if (geometry) {

          formedObj.offerFilter.location.coordinates = [
            lat,
            lng
          ]
        }

        if (coords) {

          formedObj.offerFilter.location.coordinates = [
            coords[0],
            coords[1]
          ]
        }
      }

      var dates = opts.calendar ? opts.calendar.dates : {
        from: undefined,
        to: undefined
      }

      if (
        typeof dates !== 'undefined' &&
        typeof dates['from'] !== 'undefined' &&
        typeof dates['to'] !== 'undefined'
      ) {
        formedObj.coinFilter.availability = opts.calendar.dates
      }

      var coinTypes

      try {
        // @see b-corps
        coinTypes = opts.offer.coinTypes.filter(function(typeRef) {
            return typeRef.selected
          })

        if ( typeof coinTypes !== 'undefined' && coinTypes.length > 0 ) {

          formedObj.coinFilter.coinType = coinTypes.map(function(typeRef) {
            return typeRef.type
          })
        }
      } catch(e) {
        console.log(e)
      }

      console.log('formed obj:', formedObj)

      setSavedFilter(formedObj)
        .then(function(res) {
          console.log(res)
        }, function(e) {
          console.log(e)
        })

      return formedObj
    }

    /**
     * @ngdoc method
     * @name getSavedFilter
     * @returns {undefined}
     */
    function getSavedFilter() {
      return savedFilterd ? savedFilterd : false
    }

  }
})()

