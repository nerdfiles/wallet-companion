/**
 * @fileOverview ./app/scripts/services/ApiService.js
 * @name tgm.services:WalletService
 * @description
 * A module for programmable wallet-centric 'service' words to detail a front end
 * implemention given to microservice grammar (commands) and restful endpoint
 * (segmentarities; 'field'-like things) compositions[0] for management of
 * stateless, cached server-side abstractions (models).
 * @description API Service provides methods for configuring REST interface
 * method accessibility and configuration.
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 * @note Emojis should be used in logging statements inside services, and
 * emojives (emoji-based) directives should be preferred.
 * ---
 * [0]: http://microservices.io/patterns/data/cqrs.html
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('ApiService', ApiService);

  ApiService.$inject = [
    '$window',
    '$http',
    'API_ENDPOINT',
    'Restangular',
    '$q'
  ];

  /**
   * ApiService
   *
   * @param $window
   * @param $http
   * @param API_ENDPOINT
   * @param Restangular
   * @param $q
   * @returns {undefined}
   */
  function ApiService($window, $http, API_ENDPOINT, Restangular, $q) {

    var _api = API_ENDPOINT;

    var endpoint = _api.port ? (_api.host + ':' + _api.port + _api.path) : (_api.host + _api.path);

    var service = {

      users: {
        action            : userAction,
        create            : createUser,
        list              : getUserList,
        get               : getUser,
        getPaymentMethods : getUserPaymentMethods,
        update            : updateUser,
        delete            : deleteUser
      },

      offers: {
        action : offerAction,
        create : createOffer,
        delete : deleteOffer,
        get    : getOffer,
        list   : getOfferList,
        update : updateOffer
      },

      hotels: {
        list   : getHotels,
        action : hotelAction
      },

      // @alias contracts
      contracts: {
        create : createContract,
        list   : getContractList,
        get    : getContract,
        update : updateContract,
        delete : deleteContract,
        action : function(id) { return dispatchAction('contracts')(id); }
      },

      tickets: {
        list          : getTicketList,
        create        : createTicket,
        getTickets    : searchTickets,
        searchTickets : searchTickets
      },

      coins: {

        list : getOfferCoinsList,
        get  : getOfferCoin
      },

      assets: {
        list   : getAssetList,
        get    : getAsset,
        create : createAsset,
        delete : deleteAsset,
        action : function(id) { return dispatchAction('assets')(id); }
      },

      constants: {
        getCountries       : getCountries,
        getProvinceOrState : getProvinceOrState
      },

      getEndpoint: function() { return endpoint; },
      errors: [],
    };

    Restangular.setErrorInterceptor(_setErrorInterceptor);

    // activate for basic auth
    if (_api.needsAuth)
      $http.defaults.headers.common.Authorization = [
          'Basic ',
          $window.btoa([
            _api.username,
            _api.password
          ].join(':'))
        ].join('');

    return service;

    ////////////

    /**
     * @function _setErrorInterceptor
     * @param {object} response
     * @param {Promise} deferred
     * @param {function} responseHandler
     * @param responseHandler
     * @returns {boolean}
     */
    function _setErrorInterceptor(response, deferred, responseHandler) {
      if (response.status === 403) {
        dispatchAction('')(null).then(function() {
          $http(response.config).then(responseHandler, deferred.reject);
        });

        return false;
      }

      return true;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name dispatchAction
     * @param {string} route TBD
     * @returns {*} undefined
     * @example
     *
     * ApiService.users.action(userID)('addPaymentMethod', { token: stripeToken })
     *
     * @description HTTP POST action to representation.
     */
    function dispatchAction(route) {
      var deferred = $q.defer();

      // Sends an arbitrary post request with a restangular object and http bdy
      var sendRequest = function(req, body) {
        return req.post(body);
      };

      // Build the restangular URL for the entity to perform action for
      return function(id) {
        var restangularAction = Restangular.one(route, id).all('action');

        // function that takes (key, params) for the action
        return function(actionKey, actionParams) {
          var requestBody = {
            action: {
              key: actionKey,
              params: actionParams || {}
            }
          };

          // and send the request, with our default handlers
          deferred.resolve(sendRequest(restangularAction, requestBody));
          return deferred.promise;
        };
      };
    }

    /**
     * @name hotelAction
     * @param id
     * @returns {object:Promise} A Promised hotel event source.
     */
    function hotelAction(id) { return dispatchAction('hotels')(id); }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getHotels
     * @param {string} query TBD
     * @returns {*} A Promised list of hotels represented in JSON.
     */
    function getHotels(query, page) {
      var deferred = $q.defer();

      try {
        if (query && query.error) {
          delete query.error;
        }
      } catch(e) {
        console.log(e);
      }

      if (!query) {
        deferred.resolve({
          error: '404'
        });
        return deferred.promise;
      }

      try {
        delete query.error;
      } catch(e) {
        console.log(e);
      }

      var availability = query && query.coinFilter
        && query.coinFilter.availability
          ? query.coinFilter.availability : query;

      var _query = _.extend({}, availability);

      try {
          _.extend(_query, {
            limit  : query.limit,
            offset : query.offset
          });
      } catch(e) {
        service.errors.push({ error: e });
      }

      try {
        if (query.offerFilter.location.coordinates.length) {
          _.extend(_query, {
            lat : query.offerFilter.location.coordinates[0],
            lon : query.offerFilter.location.coordinates[1]
          });
        }
      } catch(e) {

        service.errors.push({ error: e });

        console.log(e);

        if (query.lat && query.lon) {
          _.extend(_query, {
            lat    : query.lat,
            lon    : query.lon,
            limit  : query.limit,
            offset : query.offset
          });
        }
      }

      if (!_query.lat && !_query.lon) {
        deferred.reject({ error: 'No lat/lon provided.' });
        return deferred.promise;
      }

      if (!_query.from) {
        _query.from  = moment().format('YYYY-MM-DD').toString();
      }

      if (!_query.to) {
        _query.to  = (moment(_query.from).add(1, 'days')
          || moment().add(1, 'days')).format('YYYY-MM-DD').toString();
      }

      // Only drop the first five results from last page and load next 50
      // Keep doing it this way until we have a means to append data to the $scope
      if (!page)
        page = 0
      _query.offset = (20 * page);
      _query.limit = (20 * page) + 20;

      if ( _query.limit > 400 ) {
        console.log('query.limit exeeds maximum permitted value');
        _query.limit = 400;
      }

      delete _query.coinFilter;
      delete _query.offerFilter;

      console.log(_query);

      return Restangular
        .all('hotels')
        .getList(_query);
    }

    /**
     * User API
     */

    /**
     * @ngdoc method
     * @name userAction
     * @param {String} id ID for the given user.
     * @returns {object:Promise}
     */
    function userAction(id) { return dispatchAction('users')(id); }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name createUser
     * @param {string} userData TBD
     * @returns {object:Promise} A Promised user from a creation event.
     */
    function createUser(userData) {
      return Restangular
        .all('users')
        .post(userData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getUserList
     * @returns {object:Promise} A Promised object for a user list.
     */
    function getUserList() {
      return Restangular
        .all('users')
        .getList();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getUser
     * @param {string} userID TBD
     * @returns {*} undefined
     */
    function getUser(userID) {
      return Restangular
        .one('users', userID)
        .get();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getUserPaymentMethods
     * @param {string} userID TBD
     * @param {object} userData TBD
     * @returns {object:Promise} A Promised HTTP object for a payment method use event.
     */
    function getUserPaymentMethods(userID, userData) {
      return Restangular
        .one('users', userID)
        .post('payment-methods', userData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name updateUser
     * @param {string} userID TBD
     * @param {object} userData TBD
     * @returns {*} undefined
     */
    function updateUser(userID, userData) {
      var preparedData = userData.profile;
      preparedData.avatar = preparedData.avatar._id;
      delete preparedData.photo;
      return Restangular
        .one('users', userID)
        .customPUT(preparedData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name deleteUser
     * @param {string} userID TBD
     * @returns {*} undefined
     */
    function deleteUser(userID) {
      return Restangular
        .one('users', userID)
        .delete();
    }

    /**
     * Offer API
     */

    /**
     * @ngdoc method
     * @name offerAction
     * @param {String} id ID for a given offer.
     * @returns {object:Promise}
     * @description
     * Offer actions conformant to ODRL Information Model[0] vocabulary and expressions.
     *
     * ---
     * [0]: https://www.w3.org/TR/odrl-vocab/#term-Offer
     */
    function offerAction(id) { return dispatchAction('offers')(id); }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name createOffer
     * @param {object} offerData
     * @param {object} offerData.profile
     * @param {object} offerData.profile.title [require]
     * @param {object} offerData.profile.offerType [required]
     * @param {object} offerData.asset
     * @param {object} offerData.contacts
     * @param {object} offerData.coins
     * @returns {*} A Promised object in response to an offer creation event.
     */
    function createOffer(offerData) {
      return Restangular
        .all('offers')
        .post(offerData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getOfferList
     * @returns {*} A Promised HTTP object containing a list of offers in JSON.
     */
    function getOfferList() {
      return Restangular
        .all('offers')
        .getList();
    }

    /**
     * getOfferCoinsList
     *
     * @param offerID
     * @returns {undefined}
     */
    function getOfferCoinsList(offerID) {
      return Restangular
        .one('offers', offerID)
        .getList('coins');
    }

    /**
     * getOfferCoinsList
     *
     * @param offerID
     * @param coinID
     * @returns {undefined}
     */
    function getOfferCoin(offerID, coinID) {
      return Restangular
        .one('offers', offerID)
        .one('coins', coinID)
        .get();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getOffer
     * @param {string} offerID TBD
     * @returns {*} A Promised HTTP object representing an offer in JSON.
     */
    function getOffer(offerID) {
      return Restangular
        .one('offers', offerID)
        .get();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name updateOffer
     * @param {string} offerID TBD
     * @param {object} offerData TBD
     * @returns {object:Promise} A Promised HTTP object for an update request event to a given offer.
     */
    function updateOffer(offerID, offerData) {
      return Restangular
        .one('offers', offerID)
        .put(offerData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name deleteOffer
     * @param {string} offerID TBD
     * @returns {*} undefined
     */
    function deleteOffer(offerID) {
      return Restangular
        .one('offers', offerID)
        .delete();
    }

    /**
     * Contract API
     */

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name createContract
     * @param {object} contractData TBD
     * @returns {*} undefined
     */
    function createContract(contractData) {
      return Restangular
        .all('contracts')
        .post(contractData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getContractList
     * @returns {*} undefined
     */
    function getContractList() {
      return Restangular
        .all('contracts')
        .getList();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getContract
     * @param {string} contractID TBD
     * @returns {*} undefined
     */
    function getContract(contractID) {
      return Restangular
        .one('contracts', contractID)
        .get();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name updateContract
     * @param {string} contractID TBD
     * @param {object} contractData TBD
     * @returns {*} undefined
     */
    function updateContract(contractID, contractData) {
      return Restangular
        .one('contracts', contractID)
        .put(contractData);
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name deleteContract
     * @param {string} contractID TBD
     * @returns {*} undefined
     */
    function deleteContract(contractID) {
      return Restangular
        .one('contracts', contractID)
        .delete();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getAssetList
     * @returns {Promise} Return a Promised status object for the given endpoint.
     */
    function getAssetList() {
      return Restangular
        .all('assets')
        .getList();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getAsset
     * @param {string} assetID ID for an AWS hosted asset.
     * @returns {Promise} Return a Promised status object for the given endpoint.
     */
    function getAsset(assetID) {
      return Restangular
        .one('assets', assetID)
        .get();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name createAsset
     * @param {object} assetData AWS asset to be hosted with a provided URI.
     * @returns {Promise} Return a Promised status object for the given
     * endpoint.
     */
    function createAsset(assetData) {

      var deferred = $q.defer();
      var fileList = assetData.files.files ? assetData.files.files : assetData.files;
      var fileItem = _.last(fileList);
      var formData = new FormData();

      try {
        formData.append('resized', assetData.image);
      } catch(e) {
        console.log(e);
      }

      fileList.type = assetData.type;
      formData.append('photo', fileItem);
      formData.append('type', fileList.type);

      return Restangular
        .all('assets')
        .withHttpConfig({
          transformRequest: angular.identity
        })
        .customPOST(formData, '', undefined, {
          'Content-Type': undefined
        });
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name deleteAsset
     * @param {string} assetID TBD
     * @returns {Promise} Return a Promised status object for the given endpoint.
     */
    function deleteAsset(assetID) {
      return Restangular
        .one('assets', assetID)
        .delete();
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.ApiService
     * @name getCountries
     * @returns {Promise} Return a Promised status object for the given endpoint.
     */
    function getCountries() {

      var def = $q.defer();

      $http({
        method: 'GET',
        url: endpoint + '../../../app/constants/countries'
      }).then(function(result) {

        if (result.data.countries)
          def.resolve(result.data.countries);
        else
          def.reject();

      }, function(e) {
        def.reject(e);
      });

      return def.promise;

    }

    /**
     * @name getProvinceOrState
     * @param {object} country A country object provided by reference data
     * services.
     * @returns {undefined}
     */
    function getProvinceOrState(country) {

      var c;
      var id;
      var co;

      try {
        c = JSON.parse(country);
        id = c.id;
      } catch(e) {
        if (country) {
          co = country;
        } else {
          co = e;
        }
      } finally {
        if (!id && co) {
          id = co;
        }
      }

      var def = $q.defer();
      var glue = '';

      $http({
        method: 'GET',
        url: [
          endpoint,
          '../../../app/constants/countries/',
          id,
          '/provinces'
        ].join(glue)
      }).then(function(result) {

        if (result.data.states)
          def.resolve(result.data.states);
        else
          def.reject();

      }, function(e) {
        def.reject(e);
      });

      return def.promise;
    }

    /**
     * getTicketList
     *
     * @returns {undefined}
     */
    function getTicketList() {
      return Restangular
        .all('tickets')
        .getList();
    }

    /**
     * searchTickets
     *
     * @returns {undefined}
     */
    function searchTickets(_query) {
      return Restangular
        .all('tickets')
        .getList(_query);

    }

    /**
     * getTickets
     *
     * @returns {undefined}
     */
    function getTickets(_query) {
      return Restangular
        .all('tickets')
        .getList(_query);

    }

    /**
     * createTicket
     *
     * @returns {undefined}
     */
    function createTicket(ticketData) {
      return Restangular
        .all('tickets')
        .post(ticketData);
    }

  }
})();
