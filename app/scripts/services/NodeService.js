/**
 * @fileOverview ./app/scripts/services/NodeService.js
 * @name tgm.services:NodeService
 * @description
 * A module for programmable node-centric "service" words to detail a front end
 * implementation given to microservice grammar (commands) and restful endpoint
 * compositions for management of stateless, cached server-side abstractions.
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .factory('NodeService', [
      '$q',
      '$timeout',
      '$http',
      NodeService
    ])

  function NodeService($q, $timeout, $http) {

    var serviceInterface = {
      ajaxRequest : ajaxRequest,
      request     : ajaxRequest
    }

    return serviceInterface

    function ajaxRequest(request, force) {

      var me = this
      // Stash the original success function for use later
      var successFn = request.success
      // Define the success/callback/failure functions
      var fn = {
        // Handle processing successfull responses
        success: function(res){
          var o = res
          // Handle trying to decode the response text
          if (res.responseText){
            try {
              var o = Ext.decode(res.responseText)
            } catch(e){
              o = false
            }
          }

          // If we detect a successfull response, hand to the success function for further processing
          if(o||force)
            successFn(o)
        },
        failure: request.failure,
        callback: request.callback
      }

      // Send request to server
      $http(_.merge(request, {
        timeout             : 60000,       // timeout after 60 seconds of waiting
        useDefaultXhrHeader : false,       // Set to false to make CORS requests (cross-domain)
        success             : fn.success,  // Success function called when we receive a success response
        callback            : fn.callback, // Callback function called on any response
        failure             : fn.failure   // Failure function called when the request fails
      }))
    }

  }
}())

