
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .service('PasscodeService', [
      '$timeout',
      'localStorageService',
      'FW',
      '$ionicPopup',
      'AppService',
      'Crypto',
      PasscodeService
    ])

  function PasscodeService($timeout, localStorageService, FW, $ionicPopup, AppService, Crypto) {

    var serviceInterface = this

    var implementInterface = {

      showPasscodeView: function(cfg) {

        //this.showView('passcodeView','FW.view.Passcode', cfg);
        var _cfg = {
          template: cfg.viewName,
          title: cfg.title,
          buttons: [
            { text: 'Cancel' },
            { text: 'OK',
              onTap: function(e) {
                if (_.isFunction(cfg.cb))
                  cfg.cb()
                else
                  console.log('...')
              }
            }
          ]
        }

        $ionicPopup.show(cfg)
      },

      /**
       * @description
       * Handle setting the user passcode and saving it to disk so we can
       * validate it later
       */
      setPasscode: function(code) {
        var self = this
        if (code) {
          FW.WALLET_PASSCODE = code;
          var enc = CryptoJS.AES.encrypt(String(FW.WALLET_PASSCODE), String(FW.WALLET_PASSCODE)).toString()
          localStorageService.set('passcode', enc)
        }
      },

      // Handle verifying if a given passcode is correct
      isValidPasscode: function(code) {

        var self = this
        var p  = localStorageService.get('passcode')
        var dev
        if (code) {
          dec = ''
          // Try to decrypt the encryptd passcode using the given passcode... catch any errors and treat as failures
          try {
            dec = CryptoJS.AES.decrypt(p, String(code)).toString(CryptoJS.enc.Utf8)
          } catch(e){
            console.log(e)
          }

          if (dec === code)
            return true
        }

        return false
      },

      enablePasscode: function() {
        var self  = this;
        self.showPasscodeView({
          title: 'Please enter desired passcode',
          cb: function(val){
            var AppService = AppService.getCmp('settingsPanel');
            if(val){
              // Confirm desired passcode
              // Defer msg slightly to fix known issue in sencha touch library
              $timeout(function(){
                self.showPasscodeView({
                  title: 'Please confirm desired passcode',
                  cb: function(val2){
                    if (val2 && val == val2 ){
                      AppService.toggleField(AppService.passcode, 1,'Passcode enabled');
                      self.setPasscode(val);
                      self.encryptWallet();
                      self.resetTouchId();
                    } else {
                      AppService.toggleField(AppService.passcode, 0,'Passcode do not match');
                    }
                  }
                });
              },10);
            } else {
              AppService.toggleField(AppService.passcode, 0,'No passcode entered');
            }
          }
        });
      },

      // Handle disabling the passcode service by requiring/validating passcode
      disablePasscode: function() {
        var self  = this
        self.showPasscodeView({
          title: 'Please enter your passcode',
          cb: function(val) {
            var AppService = AppService.getAppService('settingsPanel');
            if (val) {
              if (self.isValidPasscode(val)) {
                AppService.toggleField(AppService.passcode, 0,'Passcode disabled')
                self.resetPasscode();
              } else {
                AppService.toggleField(AppService.passcode, 1,'Invalid passcode');
              }
            } else {
              AppService.toggleField(AppService.passcode, 1,'No passcode entered');
            }
          }
        })
      },

      /**
       * @name resetPasscode
       * @description Handle resetting the wallet to use default passcode, and re-encrypt wallet
       */
      resetPasscode: function(){
        var self  = this
        var AppService = AppService.getAppService('settingsPanel')
        AppService.toggleField(AppService.passcode, 0)
        localStorageService.remove('passcode')
        FW.WALLET_PASSCODE = parseInt('0000', 10)
        self.encryptWallet()
      },

      /**
       * @name authenticatePasscode
       * @description
       */
      authenticatePasscode: function(successFn, errorFn, text, force){
        var self = this
        var text = (text) ? text: 'Please enter your passcode'

        self.showPasscodeView({
          title: text,
          cb: function(val){
            if (self.isValidPasscode(val)) {
              if (_.isFunction(successFn))
                successFn(val);
            } else {
              // Handle alerting user to invalid passcode, then re-authenticate
              // Defer showing the message a bit to prevent a known issue in sencha-touch with showing messageboxes too fast
              if (force) {
                $timeout(function(){
                  $ionicPopup.alert({
                    title: 'Invalid passcode',
                    template: 'Invalid passcode',
                  }).then(function() {
                    self.authenticatePasscode(successFn, errorFn, text, force)
                  })
                }, 10)
              } else if (_.isFunction(errorFn)) {
                errorFn()
              }
            }
          }
        })
      }
    }

    _.extend(serviceInterface, implementInterface)
  }

}())

/*
function showView(id, xclass, cfg){
    var view = false,
        vp   = Ext.Viewport;
    // console.log('showView xclass,cfg=',xclass,cfg);
    if(id)
        view = Ext.getCmp(id);
    // If we found existing view, update data and use it
    if(view){
        if(cfg)
            view.updateView(cfg);
    } else {
        view = vp.add(Ext.apply({ xclass: xclass }, cfg));
    }
    // Show view using correct method
    if(view.isInnerItem()){
        vp.setActiveItem(view);
    } else {
        view.show();
    }
}
*/

