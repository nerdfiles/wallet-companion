/**
 * @fileOverview ./app/scripts/services/TransactionService.js
 * @description
 * Transaction service.
 * @BEGIN
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('TransactionService', [
      '$timeout',
      'localStorageService',
      'FW',
      '$ionicPopup',
      'Crypto',
      'web3',
      'numeral',
      'contractManager',
      '$q',
      'EthTx',
      'Buffer',
      TransactionService
    ]);

  /**
   * TransactionService
   *
   * @param $timeout
   * @param localStorageService
   * @param FW
   * @param $ionicPopup
   * @param Crypto
   * @param web3
   * @param numeral
   * @returns {undefined}
   */
  function TransactionService($timeout, localStorageService, FW, $ionicPopup, Crypto, web3, numeral, contractManager, $q, EthTx, Buffer) {

    var implementInterface = {

      Approval       : Approval,
      rawTransaction : rawTransaction,
      send           : _send,
      name           : __name__
      // refresh     : refresh,
      // restore     : restore,

    };

    /*
     * @EXPORTS
     **/

    return implementInterface;

    /**
     * @name name
     * @param config
     * @returns {undefined}
     */
    function __name__(config) {

      var _config = {
        address : config.address || contractManager._address,
        name    : config.name
      };

      var contract = web3.eth.Contract(_config.address, contractManager.ethereum_contract);
      return contract.name(_config.name);
    }

    /**
     * @name Message
     * @param config
     * @returns {undefined}
     */
    function Message(config) {

      var _config = {
        address : config.address || contractManager._address
      };

      var contract = web3.eth.Contract(_config.address, contractManager.ethereum_contract);
      return contract.Message();
    }


    /**
     * SetMessage
     *
     * @param config
     * @returns {undefined}
     */
    function SetMessage(config) {

      var _config = {
        address : config.address || contractManager._address,
        message : config.message
      };

      var contract = web3.eth.Contract(_config.address, contractManager.ethereum_contract);
      return contract.SetMessage(_config.message);
    }

    /**
     * refresh
     *
     * @param config
     * @returns {undefined}
     */
    function refresh(config) {

      var _config = {
        address: config.address || contractManager._address
      };

      var debitcoinContract = web3.eth.Contract(contractManager.ethereum_contract);
      var contract = debitcoinContract.at(_config.address);

      return contract.Message();
    }

    /**
     * restore
     *
     * @param config
     * @returns {undefined}
     */
    function restore(config) {

      var _config = {
        address: config.address || contractManager._address
      };

      var contractData = web3.eth.storageAt(_config.address);
      var contractStringified = JSON.stringify(contractData);
      var restored = web3.toAscii(contractData['0x']);

      return restored;
    }

    /**
     * approve
     *
     * @param config
     * @returns {undefined}
     */
    function approve(config) {

      var _config = {
        address: config.address || contractManager._address
      };
      var contract = web3.eth.Contract(_config.address, contractManager.ethereum_contract);

      return contract.approve(_config);

    }

    /**
     * totalSupply
     *
     * @param config
     * @returns {undefined}
     */
    function totalSupply(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.totalSupply(_config);
    }

    /**
     * MiningRewardPerETHBlock
     *
     * @param config
     * @returns {undefined}
     */
    function MiningRewardPerETHBlock(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.MiningRewardPerETHBlock(_config);
    }

    /**
     * disableTransfers
     *
     * @param config
     * @returns {undefined}
     */
    function disableTransfers(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.disableTransfers(_config);
    }

    /**
     * transferFrom
     *
     * @param config
     * @returns {undefined}
     */
    function transferFrom(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.transferFrom(_config);
    }

    /**
     * transfer
     *
     * @param config
     * @returns {undefined}
     */
    function transfer(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.transfer(_config);
    }

    /**
     * transfersEnabled
     *
     * @param config
     * @returns {undefined}
     */
    function transfersEnabled(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.transfersEnabled(_config.enabled);
    }

    /**
     * TransferMinersReward
     *
     * @param config
     * @returns {undefined}
     */
    function TransferMinersReward(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.TransferMinersReward(_config);
    }

    /**
     * transferBuyership
     *
     * @param config
     * @returns {undefined}
     */
    function transferBuyership(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.transferBuyership(_config);
    }

    /**
     * Transfer
     *
     * @param config
     * @returns {undefined}
     */
    function Transfer(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.Transfer(_config);
    }

    /**
     * decimals
     *
     * @param config
     * @returns {undefined}
     */
    function decimals(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.decimals(_config);
    }

    /**
     * ChangeMiningReward
     *
     * @param config
     * @returns {undefined}
     */
    function ChangeMiningReward(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.ChangeMiningReward(_config);
    }

    /**
     * version
     *
     * @param config
     * @returns {undefined}
     */
    function version(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.version(_config);
    }

    /**
     * standard
     *
     * @param config
     * @returns {undefined}
     */
    function standard(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.standard(_config);
    }

    /**
     * _amount
     *
     * @param config
     * @returns {undefined}
     */
    function _amount(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract._amount(_config);
    }

    /**
     * withdrawTokens
     *
     * @param config
     * @returns {undefined}
     */
    function withdrawTokens(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.withdrawTokens(_config);
    }

    /**
     * balanceOf
     *
     * @param config
     * @returns {undefined}
     */
    function balanceOf(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.balanceOf(_config);
    }

    /**
     * acceptBuyership
     *
     * @param config
     * @returns {undefined}
     */
    function acceptBuyership(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.acceptBuyership(_config);
    }

    /**
     * issue
     *
     * @param config
     * @returns {undefined}
     */
    function issue(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.issue(_config);
    }

    /**
     * buyer
     *
     * @param config
     * @returns {undefined}
     */
    function buyer(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.buyer(_config);
    }

    /**
     * symbol
     *
     * @param config
     * @returns {undefined}
     */
    function symbol(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.symbol(_config);
    }

    /**
     * destroy
     *
     * @param config
     * @returns {undefined}
     */
    function destroy(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.destroy(_config);
    }

    /**
     * newBuyer
     *
     * @param config
     * @returns {undefined}
     */
    function newBuyer(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.newBuyer(_config);
    }

    /**
     * allowance
     *
     * @param config
     * @returns {undefined}
     */
    function allowance(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.allowance(_config);
    }

    /**
     * lastBlockRewarded
     *
     * @param config
     * @returns {undefined}
     */
    function lastBlockRewarded(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.lastBlockRewarded(_config);
    }

    /**
     * uint8
     *
     * @param config
     * @returns {undefined}
     */
    function uint8(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.unit8(_config);
    }

    /**
     * DebitCoinTokenGenesis
     *
     * @param config
     * @returns {undefined}
     */
    function DebitCoinTokenGenesis(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.DebitCoinTokenGenesis(_config);
    }

    /**
     * Issuance
     *
     * @param config
     * @returns {undefined}
     */
    function Issuance(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.Issuance(_config);
    }

    /**
     * Destruction
     *
     * @param config
     * @returns {undefined}
     */
    function Destruction(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.Destruction(_config);
    }

    /**
     * MiningRewardChanged
     *
     * @param config
     * @returns {undefined}
     */
    function MiningRewardChanged(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.MiningRewardChanged(_config);
    }

    /**
     * MiningRewardSent
     *
     * @param config
     * @returns {undefined}
     */
    function MiningRewardSent(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.MiningRewardSent(_config);
    }

    /**
     * BuyerUpate
     *
     * @param config
     * @returns {undefined}
     */
    function BuyerUpdate(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.BuyerUpdate(_config);
    }

    /**
     * Approval
     *
     * @param config
     * @returns {undefined}
     */
    function Approval(config) {

      var _config = config;
      var contractAddress = contractManager._address;
      var contract = web3.eth.Contract(contractAddress, contractManager.ethereum_contract);

      return contract.Approval(_config);
    }

    /**
     * @name sign
     * @param message
     * @returns {undefined}
     */
    function sign(message) {

      var address = FW.WALLET_KEYS.address;
      var password = String(FW.WALLET_HEX).fromHex();

      var msg = new Buffer(message);
      var signedTransaction = web3.eth.sign(address, [
        '0x',
        msg.toString('hex')
      ].join(''));

      return signedTransaction;
    }

    /**
     * @name verify
     * @param message
     * @param signature
     * @returns {undefined}
     */
    function verify(message, signature) {

      var prefix = new Buffer("\x19Ethereum Signed Message:\n");
      var v = web3.toDecimal([
        '0x',
        signature.slice(130, 132),
      ].join(''));
      var r = signature.slice(0, 66);
      var s = signature.slice(66, 130);

      var msg = new Buffer(message);
      var preparedMessage = web3.sha3(Buffer.concat([
        prefix,
        new Buffer(String(msg.length)),
        msg
      ]).toString('utf8'));

      var verifiedAddress = contractManager.contractInstance
        .verify.call( preparedMessage, v, r, [ '0x', s ].join('') );

      return verifiedAddress;
    }

    /**
     * Call a smart contract function from any keyset in which the caller has the
     *     private and public keys.
     * @param {string} senderPublicKey Public key in key pair.
     * @param {string} senderPrivateKey Private key in key pair.
     * @param {string} contractAddress Address of Solidity contract.
     * @param {string} data Data from the function's `getData` in web3.js.
     * @param {number} value Number of Ethereum wei sent in the transaction.
     * @return {Promise}
     */
    function rawTransaction(config) {

      var contractAddress = contractManager._address;
      var contractLoaded = contractManager.ethereum_contract;
      var currency = config.currency || 'ether';
      var senderPublicKey = ('0x' + FW.WALLET_ENTITY.address) || config.sender || contractAddress || web3.eth.coinbase;
      var senderPrivateKey = FW.WALLET_KEYS.privateKey;
      var data = config.message;
      var value = config.amount;
      var defer = $q.defer();

      fire();

      return defer.promise;

      /**
       * @name fire
       * @returns {undefined}
       */
      function fire() {

        var key = new Buffer(senderPrivateKey.data, 'hex').toString('hex');
				var _key = new Buffer(key, 'hex');
        var nonce = web3.toHex(web3.eth.getTransactionCount(senderPublicKey));

        var gasPrice = web3.eth.gasPrice;
        var gasPriceHex = web3.toHex(web3.eth.estimateGas({
          from: contractAddress
        }));

        // var gasLimitHex = web3.toHex(5500000);
        var gasLimitHex = web3.toHex(contractManager._ethereum_contract_fixed);

        var rawTx = {
          nonce    : nonce,
          gasPrice : gasPriceHex,
          gasLimit : gasLimitHex,
          data     : data,
          to       : contractAddress,
          value    : web3.toHex(value)
        };

        var tx = new EthTx.Tx(rawTx);
        tx.sign(_key);

        var stx = ['0x', tx.serialize().toString('hex')].join('');

        web3.eth.sendRawTransaction(stx, (err, hash) => {
          if (err) {
            defer.reject(err);
          } else {
            defer.resolve(hash);
          }
        });

      }
    }

    /**
     * @name _send
     * @param config
     * @returns {undefined}
     */
    function _send(config) {

      var defer = $q.defer();
      var contractAddress = contractManager._address;
      var contractLoaded = contractManager.ethereum_contract;
      var amount = config.amount;
      var message = config.message || '';
      var currency = config.current || 'ether';
      var recipient = config.recipient;
      var sender = config.sender || contractAddress || web3.eth.coinbase

      var state = web3.personal.unlockAccount(FW.WALLET_ENTITY.address, FW.WALLET_HEX, 100);

      var test = "603d80600c6000396000f3007c01000000000000000000000000000000000000000000000000000000006000350463c6888fa18114602d57005b6007600435028060005260206000f3";
      var trx = web3.eth.sendTransaction({
        data: test
        // from    : sender,
        // address : recipient,
        // value   : web3.toWei(amount, currency || 'ether')
      }, function(error, transactionRef) {
        if (!error) {

          defer.resolve({ item: trx });
        } else {
          defer.reject({ error: 'failed' });

        }

      });

      return defer.promise;

    }

  }
})();

