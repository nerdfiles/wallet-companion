/**
 * @fileOverview ./app/scripts/services/WalletService.js
 * @name tgm.services:WalletService
 * @description
 * A module for programmable wallet-centric 'service' words to detail a front end
 * implemention given to microservice grammar (commands) and restful endpoint
 * (segmentarities; 'field'-like things) compositions[0] for management of
 * stateless, cached server-side abstractions (models).
 * #ACID #POJOLOO #HATEOAS #EAAORKG #ROCA #HPWA
 * @note Emojis should be used in logging statements inside services, and
 * emojives (emoji-based) directives should be preferred.
 * ---
 * [0]: http://microservices.io/patterns/data/cqrs.html
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .factory('WalletService', [
      'AppService',
      '$q',
      '$ionicPopup',
      'TalkService',
      'FW',
      '$timeout',
      'web3',
      'localStorageService',
      'Crypto',
      'LogService',
      '_',
      '$window',
      'keythereum',
      'Buffer',
      'bitcore',
      WalletService
    ]);

  /**
   * WalletService
   *
   * @param AppService
   * @param $q
   * @param $ionicPopup
   * @param TalkService
   * @param FW
   * @param $timeout
   * @param web3
   * @param localStorageService
   * @param Crypto
   * @param LogService
   * @param _
   * @param $window
   * @param keythereum
   * @param Buffer
   * @param bitcore
   * @returns {undefined}
   */
  function WalletService(AppService, $q, $ionicPopup, TalkService, FW, $timeout, web3, localStorageService, Crypto, LogService, _, $window, keythereum, Buffer, bitcore) {

    var serviceInterface = {
      encryptWallet          : encryptWallet,
      decryptWallet          : decryptWallet,
      authorize              : authorize,
      getBalance             : getBalance,
      witness                : witness,
      collate                : collate,
      clearAllSubscriptions  : clearAllSubscriptions,
      clearSubscription      : clearSubscription
    };

    var subs = [];

    return serviceInterface;

    /////////////////

    /**
     * @name clearAllSubscriptions
     * @returns {undefined}
     */
    function clearAllSubscriptions() {

      if (!_.size(subs)) {
        return console.log('🤔 No subscriptions found.')
      }

      subs.forEach(function(sub) {
        if (sub) {
          sub.sub.unsubscribe(function(error, success) {
            if (success) {
              console.log('ℹ️ Unsubscribed from ', sub.type);
            }
          });
        }
      });
    }

    /**
     * @name clearSubscription
     * @param type
     * @returns {undefined}
     */
    function clearSubscription(type) {

      if (!_.size(subs)) {
        return console.log('🤔 No subscriptions found.')
      }

      subs.forEach(function(sub) {
        if (sub && sub.type && sub.type.includes(type)) {
          sub.sub.unsubscribe(function(error, success) {
            if (success) {
              console.log('ℹ️ Unsubscribed from ', sub.type);
            }
          });
        }
      });
    }

    /**
     * @name collate
     * @returns {undefined}
     * @description
     * @params {String} type Subscription type. e.g. logs
     * @usage
     * WalletService
     *   .collate('syncing', function(res) {
     *     console.log(res);
     *   })
     *   .then(angular.noop, angular.noop)
     *   .catch(angular.noop);
     */
    function collate(type, options, callback) {

      // Some cryptocurrency workflow for setting up trust I suppose?
      var btc_privateKey = new bitcore.HDPrivateKey();
      var btc_exported = btc_privateKey.toWIF();
      var retrieved = new HDPrivateKey(btc_privateKey);
      var derived = btc_privateKey.derive("m/0");

      if (!type || !callback) {
        console.log('💩 Your function is fucked!');
      }

      var defer = $q.defer();

      LogService.register(callback);

      authorize().then(function(res) {

        var config = {
          address: res.address
        };

        _.extend(config, options);

        var sub = web3.eth.subscribe(type, config, function(error, res) {
          if (!error) {
            defer.resolve({ item: res });
          } else {
            defer.reject({ error: error });
          }
        })
          .on('data', function(data) {
            LogService.log('data', data);
          })
          .on('changed', function(data) {
            LogService.log('changed', data);
          });

        subs.push({ type: type, sub: sub });

      }, function(rej) {
        defer.reject({ error: rej });
      })
        .catch(function(err) {
          defer.reject({ error: err });
        });

      return defer.promise;
    }

    /**
     * @name witness
     * @returns {undefined}
     * @description
     * @params {String} type Subscription type. e.g. pendingTransactions|newBlockHeaders|syncing
     * @usage
     * WalletService
     *   .witness('pendingTransactions', function(res) {
     *     console.log(res);
     *   })
     *   .then(angular.noop, angular.noop)
     *   .catch(angular.noop);
     */
    function witness(type, callback) {

      if (!type || !callback) {
        console.log('💩 Your function is fucked!');
      }

      var defer = $q.defer();

      LogService.register(callback); authorize().then(function(res) { warrant().
      then(function(w) { var sub = web3.eth.subscribe([ type, w ].join('/'),
      function(error, res) { if (!error) { defer.resolve({ item: res }); }
      else { defer.reject({ error: error }); } }).on('data', function(data) {
      LogService.log('data', data); }); subs.push({ type: type, sub: sub });
      }, function(e) { console.error({ error: e }); }); }, function(rej) { defer.
      reject({ error: rej }); }) .catch(function(err) { defer.reject({ error: err
      }); }); return defer.promise;
    }

    /**
     * @name warrant
     * @returns {undefined}
     * @description https://www.w3.org/TR/odrl-vocab/#actionConcepts extension called "Warrant".
     */
    function warrant(ticket) {

      var defer = $q.defer();
      var resolve = defer.resolve;
      var reject = defer.reject;

      if (!ticket) {
        reject({ error: '404' });
      }

      // Some cryptocurrency workflow for setting up trust I suppose?
      var btc_privateKey = new bitcore.HDPrivateKey();
      var retrieved = new bitcore.HDPrivateKey(btc_privateKey);
      var derived = btc_privateKey.derive(ticket);

      resolve(derived);

      return defer.promise;
    }

    /**
     * authorize
     *
     * @returns {undefined}
     */
    function authorize() {
      var defer = $q.defer();
      var enabled = true;
      var privkey = localStorageService.get('privkey');
      var defaultPrivKey = '0x95408930d6323ac7aa69e6c2cbfe58774d565fa8';
      var key = FW.WALLET_KEYS;
      defer.resolve(key);

      // decryptWallet().then(function() {
      //   if (enabled === true) {
      //     defer.resolve({
      //       '$status'     : 'enabled',
      //       'WALLET_HEX'  : FW.WALLET_HEX,
      //       'WALLET_KEYS' : FW.WALLET_KEYS
      //     });
      //   } else {
      //     defer.reject({ '$status': 'disabled' });
      //   }
      //   delete FW.PASSCODE;
      // }, function() {
      //   delete FW.PASSCODE;
      // })
      // .catch(function() {
      //   delete FW.PASSCODE;
      // });

      return defer.promise;
    }

    /**
     * getBalance
     *
     * @param address
     * @returns {undefined}
     */
    function getBalance(address) {
      var defer = $q.defer();
      try {
        defer.resolve({
          '$balance': web3.eth.getBalance(address)
        });
      } catch(err) {
        defer.reject({ error: err });
      }
      return defer.promise;
    }

    /**
     * @name encryptWallet
     * @description Handle encrypting wallet information using passcode and saving
     */
    function encryptWallet() {

      var defer = $q.defer();
      var sm  = localStorageService;
      var enc;
      var __error__;

      ethWallet();
      //genericWallet()
      // @TOOD moneroWallet(), zcashWallet(), ...

      /**
       * moneroWallet
       *
       * @returns {undefined}
       */
      function moneroWallet() {
        return new Error('Stub');
      }

      /**
       * ethWallet
       *
       * @returns {undefined}
       */
      function ethWallet() {

        var hex, _hex, keys;
        var _hex_buffer = Buffer(FW.WALLET_HEX, 'hex');
        console.log(_hex_buffer);
        //var _keys = Buffer(FW.WALLET_KEYS, 'hex');
        //var h = String(FW.WALLET_HEX);

        //var _h = FW.WALLET_HEX.toHDPrivateKey(FW.PASSCODE);
        //var _hex = String(_h.xprivkey);
        // _hex = String(FW.WALLET_HEX).toHex();
        _hex = FW.WALLET_HEX;

        var _privkey = FW.WALLET_KEYS;
        //var __privkey__ = JSON.stringify(privkey);
        //var x = String(__privkey__);

        try {

          // Must use passcode to decrypt the hex, then use hex to decrypt the wallet
          //hex = web3.eth.accounts.encrypt(_hex.toHex(), String(FW.PASSCODE));
          hex = Crypto.AES
            .encrypt(_hex.phrase, String(FW.PASSCODE))
            .toString();
          sm.set('wallet', hex);

          keys = Crypto.AES
            .encrypt(JSON.stringify(_privkey), String(FW.PASSCODE))
            .toString();
          sm.set('privkey', keys);

        } catch(e) {
          __error__ = { error: e };
          console.log(e);
        }

        if (!__error__) {
          defer.resolve({
            '$keys': keys,
            '$hex': hex
          });
        } else {
          defer.reject(__error__);
        }
      }

      /**
       * genericWallet
       *
       * @returns {undefined}
       */
      function genericWallet() {

        var hex, keys;

        try {
          // Encrypt the wallet seed
          hex = Crypto.AES
            .encrypt(FW.WALLET_HEX, String(FW.PASSCODE))
            .toString();
          sm.set('wallet', hex);
        } catch(e) {
          __error__ = { error: e };
        }

        try {
          // Encrypt any imported private keys
          keys = Crypto.AES
            .encrypt(JSON.stringify(FW.WALLET_KEYS), String(FW.PASSCODE))
            .toString();
          sm.set('privkey', keys);
        } catch(e) {
          __error__ = { error: e };
        }

        if (!__error__) {
          defer.resolve({
            '$keys': keys,
            '$hex': hex
          });
        } else {
          defer.reject(__error__);
        }
      }

      return defer.promise;
    }

    /**
     * @name decryptWallet
     * @description Handle decrypting stored wallet seed using passcode
     */
    function decryptWallet() {

      var defer = $q.defer();
      var sm = localStorageService;
      var w  = sm.get('wallet');
      var p  = sm.get('privkey');
      var dec;

      // @TODO moneroWallet, etc.
      ethWallet();
      //genericWallet()

      /**
       * moneroWallet
       *
       * @returns {undefined}
       */
      function moneroWallet() {
        return new Error('Stub');
      }

      /**
       * ethWallet
       *
       * @returns {undefined}
       */
      function ethWallet() {

        var hex, keys, entityRef;
        var options = {
          kdf: "pbkdf2",
          cipher: "aes-128-cbc",
          kdfparams: {
            c: 262144,
            dklen: 32,
            prf: "hmac-sha256"
          }
        };
        try {

          if (w) {

            dec = Crypto.AES
              .decrypt(w, String(FW.PASSCODE))
              .toString(Crypto.enc.Utf8);

            FW.WALLET_HEX = hex = dec;
          }

          if (w && p) {

            var params = {
              keyBytes: 32,
              ivBytes: 16,
              pbkdf2: {
                c: 2000,
                dklen: 32,
                hash: "sha256"
              }
            };

            keythereum.create(params, function(dk) {

              var b;

              try {
                // take the iv and block from the newly created key but
                // override with the SHA256 encrypted key; this may seem
                // excessive and causes extended wait time at the login.
                dec = Crypto.AES
                  .decrypt(p, String(FW.PASSCODE))
                  .toString(Crypto.enc.Utf8);

                FW.WALLET_KEYS = keys = JSON.parse(dec);

                dk.privateKey = FW.WALLET_KEYS.privateKey;

                b = new Buffer(dk.privateKey.data);

              } catch(e) {
                console.log({ error: e });
                defer.reject({
                  error: e
                });

              }

              try {

                entityRef = keythereum.dump(FW.WALLET_HEX, b, dk.salt, dk.iv, options);

              } catch(e) {
                console.log({ error: e });
                defer.reject({
                  error: e
                });
              }

              FW.WALLET_ENTITY = entityRef;

              defer.resolve({
                '$hex'    : hex,
                '$keys'   : keys,
                '$entity' : entityRef
              });

            });

          } else {
            defer.reject({ error: 'key_not_decrypted' });
          }

        } catch(e) {

          defer.reject({ error: e});

          console.log(e);
        }

      }

      /**
       * genericWallet
       *
       * @returns {undefined}
       */
      function genericWallet() {
        var hex;
        var keys;

        try {

          // Decrypt wallet
          if (w) {
            dec = Crypto.AES.decrypt(w, String(FW.PASSCODE)).toString(Crypto.enc.Utf8);
            FW.WALLET_HEX = hex = dec;
          }

          // Decrypt any saved/imported private keys
          if (p) {
            dec = Crypto.AES.decrypt(p, String(FW.PASSCODE)).toString(Crypto.enc.Utf8);
            FW.WALLET_KEYS = keys = JSON.parse(dec);
          }

          defer.resolve({ '$hex': hex, '$keys': keys });

        } catch(e) {
          console.log('SHOULD fail is FW.PASSCODE is null/undefined');
          defer.reject({ error: e })
        }

      }

      return defer.promise;
    }
  }
})();

