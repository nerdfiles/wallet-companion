/**
 * @ngdoc service
 *
 * @returns {undefined}
 */
(function() {
  'use strict';

  angular.module('WalletCompanion')
    .factory('GeoService', GeoService)

  GeoService.$inject = ['$q', '$window'];

  function GeoService($q, $window) {

    var serviceInterface = {};

    serviceInterface.isLocationAvailable = isLocationAvailable;

    return serviceInterface;

    /**
     * isLocationAvailable
     *
     * @returns {object:Promise} Location availability.
     */
    function isLocationAvailable() {

      var def = $q.defer();

      if (!$window.cordova) {

        def.reject({ error: 'no_location' });
      } else {

        try {

          $window.cordova.plugins.diagnostic.isLocationAvailable(function(available) {

            def.resolve(available);
          }, function(error) {
            def.reject({ error: error });
          });
        } catch(e) {

          def.reject({ error: e });
        }
      }

      return def.promise;
    }
  }
})();

