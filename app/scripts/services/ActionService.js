/**
 * @ngdoc service
 * @name WalletCompanion.service:ActionService
 * @description Action Service.
 */
(function(){
  'use strict';
  angular.module('WalletCompanion')
  .factory('ActionService', ActionService);

  ActionService.$inject = [
    'Restangular'
  ];

  function ActionService(Restangular) {

    var service = {
      perform  : perform,
      act      : act
    };

    return service;

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.service:ActionService
     * @name act
     *
     * @param {string} type Type.
     * @param {string} key Key.
     * @returns {*} undefined
     */
    function act(type, key) {
      return function(entity, data) {
        ActionService.perform(entity._id, type, key, data)
          .then(function(res) {
            console.log(res);
          });
      };
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.service:ActionService
     * @name perform
     * @param {string|number} id ID of the entity.
     * @param {string} type Model type.
     * @param {string} action Action name from ActionTable.
     * @param {object} data Data about the action.
     * @returns {object:Promise} Promise object.
     * @example
     * ActionService.perform(123, 'reservations', 'addBed', { prop: val });
     * @description Convenience method for working with API Actions.
     * @deprecated Use ApiService.
     */
    function perform (id, type, action, data) {

      var entity = Restangular.one(type, id);
      var _params = {};
      var actionTable = {
        key    : action,
        params : data
      };

      return entity.customPOST({ action: actionTable }, 'action', _params, {})
        .then(performCompleted, performFailed);

        /**
         * @function performCompleted
         * @param {object} res TBD
         * @returns {*} undefined
         */
        function performCompleted(res) {
          console.log(res);
          if (res.error) {
            return {'error': res.error.message};
          } else {
            return Restangular.stripRestangular(res.data);
          }
        }

        /**
         * @function performFailed
         * @param {object} e TBD
         * @returns {*} undefined
         */
        function performFailed(e) {
          return e.data;
        }
    }
  }
})();
