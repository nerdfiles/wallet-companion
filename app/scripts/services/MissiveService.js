
/**
 * @ngdoc service
 * @name WalletCompanion.MessageService
 * @description MessageService
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .factory('MissiveService', MissiveService)

  MissiveService.$inject = [
    '$state',
    '$rootScope',
    '$q',
    '$localStorage',
    'uuid4'
  ]

  function MissiveService($state, $rootScope, $q, $localStorage, uuid4) {
    var flows = {
      createUser: {
        route: 'app.createUser',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 0, title: 'Create Account' },
              { id: 1, title: 'Create Account' }
            ]
          }
        ]
      },

      createMissive: {
        route: 'app.createMissive',
        types: [
          {
            shortName: 'default',
            steps: [
              { id: 0, title: 'ad Missive', checklist: true, required: ['title_type'] },
              { id: 1, title: 'Locale', checklist: true, required: ['address'] },
              { id: 2, title: 'Photo, Image, or Symbol' },
              { id: 3, title: 'Describe Your Offer', checklist: true, required: ['description', 'availability'] },
              { id: 4, title: 'Manage Coin Features', checklist: true, required: ['coin_inventory'] },
              { id: 5, title: 'Edit Message' },
              { id: 6, title: 'Preview Message' }
            ],
            coinTypes: [
              { shortName: 'collectibles', name: 'Labor Token', disabled: false },
              { shortName: 'computer-equipment', name: 'Labor Token', disabled: false },
              { shortName: 'eth', name: 'Equity Token', disabled: false },
              { shortName: 'btc', name: 'Security Token', disabled: false },
              { shortName: 'xmr', name: 'Utility Token', disabled: false },
              { shortName: 'cot', name: 'Commodity Token', disabled: false }
            ]
          }
        ]
      },

      userProfile: {
        route: 'app.profile',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 0, title: 'Profile' },
              { id: 1, title: 'Settings' }
            ]
          }
        ]
      },

      createPaymentMethodForMessage: {
        route: 'app.createMessagePaymentSelect',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 0, title: 'Billing' },
              { id: 1, title: 'CC Info' }
            ]
          }
        ]
      },

      createPaymentMethod: {
        route: 'app.paymentMethods',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 0, title: 'Billing' },
              { id: 1, title: 'CC Info' }
            ]
          }
        ]
      },

      addPaymentMethod: {
        route: 'app.paymentMethods',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 0, title: 'Billing' },
              { id: 1, title: 'CC Info' }
            ]
          }
        ]
      },

      createMessage: {
        route: 'app.createMessage',
        types: [
          {
            shortName: 'buyer',
            steps: [
              { id: 1, title: 'Pre Order Coin'},
              { id: 2, title: 'Summary'},
              { id: 3, title: 'Confirm'}
            ]
          }
        ]
      }
    }

    var active = {
      route: '',
      flow: '',
      type: '',
      title: '',
      currentStep: 0,
      totalSteps: 0
    }
    var tempObj = {}
    var signup = false

    // @description
    // States as Reconfigurable DescribeBy-Describes metaallocation relations.
    // @see https://docs.openchain.org/en/latest/ledger-rules/general.html#asset-definition-record-asdef
    var service = {
      getTitle               : getTitle,
      getActive              : getActive,
      setActive              : setActive,
      getTempObj             : getTempObj,
      setTempObj             : setTempObj,
      // @see https://www.w3.org/TR/html5/links.html#link-type-next
      next                   : next,
      // @see https://www.w3.org/TR/html5/links.html#link-type-prev
      previous               : previous,
      // @see https://www.w3.org/TR/html5/links.html#element-statedef-link-next
      pause                  : pause,
      // @see https://www.w3.org/TR/html5/links.html#element-statedef-link-noreferrer
      resume                 : resume,
      initSignup             : initSignup,
      isSigningUp            : isSigningUp,
      validateChecklistItems : validateChecklistItems,
      resetActiveObj         : resetActiveObj,
      removeActiveObj        : removeActiveObj
    }

    return service

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name initSignup
     * @param {object} start TBD
     * @returns {*} undefined
     */
    function initSignup(start) {
      signup = start
    }

    /**
     * removeActiveObj
     *
     * @returns {undefined}
     */
    function removeActiveObj(objType) {
      if (tempObj[objType]) {
        tempObj[objType] = {}
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name isSigningUp
     * @returns {object} @TODO What is this for?
     */
    function isSigningUp() {
      return signup
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name resetActiveObj
     * @returns {*} undefined
     */
    function resetActiveObj(objType) {
      active = {
        route: '',
        flow: '',
        type: '',
        title: '',
        currentStep: 0,
        totalSteps: 0
      }
      if (tempObj[objType]) {
        tempObj[objType] = {}
      }
      return active
    }

    resetActiveObj()

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getTitle
     * @returns {string} Title string for flow step.
     */
    function getTitle() {
      return active.title
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name validateChecklistItems
     * @param {object} checklist Object.
     * @returns {object:Promise} Promise object.
     */
    function validateChecklistItems(checklist) {
      var deferred = $q.defer()
      var steps = selectSteps()
      var current = steps[active.currentStep]
      if (current.checklist === true) {
        current.required.forEach(function(x) {
          checklist.forEach(function(y) {
            if (x === y.shortName) { y.complete = true }
          })
        })
        deferred.resolve(checklist)
      }
      else { deferred.resolve({}) }
      return deferred.promise
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getActive
     * @returns {object} Object.
     */
    function getActive() {
      return active
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name setActive
     * @param {object} isNew TBD
     * @param {object} flow TBD
     * @param {object} type TBD
     * @param {object} step TBD
     * @returns {*} undefined
     */
    function setActive(isNew, flow, type, step) {

      var flowSelection = flows[flow]

      if (!flowSelection) {
        return console.log('No flow selected')
      }

      var stepSelection

      try {
        stepSelection = flowSelection.types
          .filter(function(x) {
            return x.shortName === type
          })[0].steps
      } catch(e) {
        console.log(e)
      }

      if (isNew) {
        active.flow = flow
        active.route = flowSelection.route
        active.type = type
        active.totalSteps = stepSelection.length
        active.title = stepSelection[step].title
      }

      active.currentStep = step

      return active
    }

    /**
     * @name selectSteps
     * @returns {*} undefined
     */
    function selectSteps() {
      var steps
      try {
        steps = flows[active.flow]
          .types.filter(function(x) {
            return x.shortName === active.type
          })[0].steps
      } catch(e) {
        console.log(e)
      }
      return steps
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name getTempObj
     * @returns {*} undefined
     */
    function getTempObj(objType) {
      return tempObj[objType]
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name setTempObj
     * @param {object} obj TBD
     * @returns {*} undefined
     */
    function setTempObj(obj, objType) {
      tempObj[objType] = tempObj[objType] || {}
      Object.keys(obj).forEach(function(x) {
        tempObj[objType][x] = obj[x]
      })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name next
     * @param {object} skip TBD
     * @returns {*} undefined
     */
    function next(skip, staleMessage) {

      if (skip) {
        $state.go('app.coins')
        return
      }

      if (staleMessage && staleMessage.flow !== '') {
        active = staleMessage
      }

      active.currentStep++

      if (active.currentStep >= active.totalSteps) {

        resetActiveObj()

        $state.go('app.coins')
      } else {

        var stepSelection = selectSteps()

        active.title = stepSelection[active.currentStep].title

        try {

          $state.transitionTo($state.current, {
            type: active.flow
          }, { reload: true })

          $rootScope.$broadcast('updateBody', active.flow)

        } catch (e) {
          console.log(e)
        }
      }

      $rootScope.$broadcast('fs:next')
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name previous
     * @returns {*} undefined
     */
    function previous(userType) {
      if (active.currentStep > 0) {
        active.currentStep--
        var stepSelection = selectSteps()
        active.title = stepSelection[active.currentStep].title

        try {

          $rootScope.$broadcast('updateBody', active.flow)

          $state.transitionTo($state.current, {
            type: active.flow
          }, {
            reload: true
          })

        } catch (e) {
          console.log(e)
        }
      }
      else {

        if (userType) {
          $state.transitionTo($state.current, {
            type: active.flow,
            userType: userType
          }, {
            reload: true
          })
          return
        }

        $rootScope.$broadcast('goBack')
      }

      $rootScope.$broadcast('fs:previous')
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name pause
     * @returns {string} uuid
     */
    function pause(flow) {
      var _id = uuid4.generate()
      flow.id = _id
      $localStorage.pausedMessages = ( typeof $localStorage.pausedMessages == 'undefined' ? [] : $localStorage.pausedMessages )
      $localStorage.pausedMessages.push(flow)
      return _id
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.MessageService
     * @name resume
     * @returns {object} flow
     */
    function resume(_id) {
      var index
      var resumedMessage = ( $localStorage.pausedMessages.filter(function(x, i) {
        var isMatch = x.id === _id
        if (isMatch) { index = i }
        return isMatch
      }) || {'error': 'flow_not_found'} )
      $localStorage.pausedMessages = $localStorage.pausedMessages.splice(index, 1)
      return resumedMessage
    }

  }

})()

