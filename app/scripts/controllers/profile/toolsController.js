
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ToolsController
 * @description User Settings controller.
 */

(function() {

  'use strict';

   angular
    .module('WalletCompanion')
    .controller('ToolsController', ToolsController);

   ToolsController.$inject = [
     '$scope',
     '$state',
     '$ionicLoading',
     'UserService',
     'MissiveService',
     '$ionicHistory',
     'ErrorService',
     '$window'
   ];

   function ToolsController($scope, $state, $ionicLoading, UserService, MissiveService, $ionicHistory, ErrorService, $window) {

   var vm = this;

   vm.logout = logout;
   vm.previous = previous;

   $scope.$on('$ionicView.beforeEnter', function() {

     $scope.$emit('toggleNavbar', true);
     UserService.getCurrentUser(true)
       .then(function(user) {
         vm.user = user;
       });
   });

   ////////////

   /**
    * @ngdoc method
    * @methodOf WalletCompanion.controller:userSettingsController
    * @name previous
    * @returns {*} undefined
    */
   function previous() {
     $state.go('app.balances');
   }

   /**
    * @ngdoc method
    * @methodOf WalletCompanion.controller:userSettingsController
    * @name logout
    * @returns {*} undefined
    */
   function logout() {

     UserService.removeCurrentUser().then(function() {
       UserService.deleteLocalSession().then(function() {
         console.log('Deleted local session');
         //$window.localStorage.clear();
       }, function() {
         console.log('Failed to delete local session');
       });
     });

     $ionicHistory.clearCache().then(function() {
       $state.go('app.login', {}, { reload: true, notify: true });
     }, function(e) {
       ErrorService.notify(e, 'info');
     });
   }

  }

})();

