
/**
 * @ngdoc function
 * @name WalletCompanion.controller:tosController
 * @description Terms of Service controller.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('TosController', TosController);

  TosController.$inject = [
    '$scope',
    '$state',
    'UserService',
    '$stateParams',
    '$ionicViewSwitcher',
    'MissiveService'
  ];

  function TosController($scope, $state, UserService, $stateParams, $ionicViewSwitcher, MissiveService) {

    var vm = this;
    vm.acceptTerms = acceptTerms;
    vm.previous = previous;
    vm.next = next;
    vm.user = {};
    vm.intro = $stateParams.intro || false;

    UserService.deleteLocalSession().then(function() {
      console.log('Clear under previous login conditions');
      UserService.getCurrentUser(true).then(function(user) {
        vm.user = user;
      });
    });

    /**
     * @name next
     * @returns {*} undefined
     */
    function next() {
      $ionicViewSwitcher.nextDirection('forward');
      UserService.getCurrentUser(true).then(function(user) {
        if (vm.user && vm.user.verification && vm.user.verification.hasAcceptedTerms) {
          MissiveService.setActive(true, 'createUser', 'buyer', 0)
          $state.go('app.createUser', {
            type: 'buyer'
          });
        } else {
          $state.go('app.login', { initial: 'reset' });
        }
      }, function() {
        $state.go('app.login', { initial: 'reset' });
      });
    }

    /**
     * @name acceptTerms
     * @param {boolean} acceptance User acceptance of TOS.
     * @returns {*} undefined
     */
    function acceptTerms() {

      $ionicViewSwitcher.nextDirection('forward');

      if (vm.user && vm.user.verification && vm.user.verification.hasAcceptedTerms) {

        UserService
          .updateCurrentUser({
            role: 'buyer'
          })
          .then(function(user) {
              //$state.go('app.login');

              MissiveService.setActive(true, 'createUser', 'buyer', 0)
              $state.go('app.createUser', {
                type: 'buyer'
              });
          }, function(user) {
            $state.go('app.login');
          });

      } else {
        $state.go('app.login', { initial: 'reset' });

      }

    }

    /**
     * @name previous
     * @returns {*} undefined
     */
    function previous() {
      $ionicViewSwitcher.nextDirection('forward');
      UserService.getCurrentUser(true)
        .then(function(user) {
          if (user.id) {
            $state.go('app.about');
          } else {
            $state.go('app.login');
          }
        }, function() {
          $state.go('app.login', { initial: 'reset' });
        });
    }
  }

})();

