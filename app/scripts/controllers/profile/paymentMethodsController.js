
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:paymentMethodsController
 * @description Payment Methods controller.
 */
(function() {
  'use strict';
  angular.module('WalletCompanion')
    .controller('PaymentMethodsController', paymentMethodsController);

  paymentMethodsController.$inject = [
    '$scope',
    '$state',
    'UserService',
    'ApiService',
    '$rootScope',
    '$ionicLoading',
    'cuid',
    'MissiveService',
    '$stateParams',
    '$timeout'
  ];

  function paymentMethodsController($scope, $state, UserService, ApiService, $rootScope, $ionicLoading, cuid, MissiveService, $stateParams, $timeout) {

    var vm = this;
    var promiseCurrentUser = true;
    vm.actionLabel = 'Continue';
    vm.submit = submit;
    vm.previous = previous;
    vm.next = next;
    $scope.showingForm = undefined;

    vm.paymentMissive = {};

    vm.paymentMissive = MissiveService.getActive();

    // if (vm.paymentMissive.currentStep === 1) {
    //   $rootScope.$emit('swap');
    // }

    $rootScope.$on('resetMissive', function() {
      console.log('reset createUser');
    });

    vm.sectionTitle = MissiveService.getTitle();
    vm.skip = skip;

    UserService.getCurrentUser(true).then(function(user) {
      vm.user = user;
      $scope.$broadcast('user-loaded', user);
    });

    $rootScope.$on('hideList', function() {
      $scope.showingForm = true;
      $scope.showForm = true;
    });

    $scope.$on('$destroy', function() {
      delete $scope._expiryDate;
      delete $scope.currentMonth;
      delete $scope.currentYear;
      $scope.selectedMonth = undefined;
      $scope.selectedYear = undefined;

      UserService.removeCcInfo();
    });

    $rootScope.$on('showList', function() {
      $scope.showingForm = false;
      $scope.showForm = false;
    });

    $rootScope.$on('resetForm', function($event, data) {

      delete $scope._expiryDate;
      delete $scope.currentMonth;
      delete $scope.currentYear;

      $scope.selectedMonth = undefined;
      $scope.selectedYear = undefined;
      $scope.showingForm = true;
      $scope.showingList = true;

      // UserService.removeCcInfo();
    });

    $rootScope.$on('completeForm', function($event, data) {

      delete $scope._expiryDate;
      delete $scope.currentMonth;
      delete $scope.currentYear;

      $scope.selectedMonth = undefined;
      $scope.selectedYear = undefined;
      $scope.showingForm = undefined;
      $scope.showingList = undefined;

      $scope.completeForm = true;

      $rootScope.$broadcast('showBillingForm');
      $scope.$broadcast('hidePaymentForm');

      // UserService.removeCcInfo();
    });

    $scope.$on('toggleList', function($event, data) {
      console.log('toggleList');
    });

    ////////////

    /**
     * @name next
     * @returns {*} undefined
     */
    function next() {
      if (vm.user && vm.user.billing) {
        UserService.addBillingInfo(vm.user);
      }

      $scope.showingList = false;

      $rootScope.$broadcast('initForm', vm.user);

      try {
        vm.createLocationForm.showBillingForm = false;
      } catch(e) {
      }
      try {
        vm.createPaymentForm.showBillingForm = false;
      } catch(e) {
      }
      $rootScope.$broadcast('swap', vm.createLocationForm);

      MissiveService.next(null, vm.paymentMissive);
    }

    /**
     * @name skip
     * @returns {*} undefined
     */
    function skip() {
      $scope.showingForm = false;
      vm.paymentMissive = MissiveService.resetActiveObj('user');
      UserService.removeCcInfo();

      if (vm.createLocationForm) {
        vm.createLocationForm.$setPristine();
      }

      $state.go('app.currencies');
    };

    /**
     * @name getCurrentUserCompleted
     * @param {object} user TBD
     * @returns {*} undefined
     */
    function getCurrentUserCompleted(user) {
      console.log('Loaded user: ', user);

      // Load current user from API and update user for its payment methods.
      // vm.user = user;

      if (!user._id) {
        return console.log('User not loaded');
      }

      ApiService.users
        .getPaymentMethods(user._id, user)
        .then(completed, function(e) {
          console.log(e);
        });

      /**
       * @name completed
       * @param {object} data Card data.
       * @returns {*} undefined
       */
      function completed(user) {
        var data = user.data.methods.data;

        vm.paymentMethods = vm.cards = _.map(data, function(method) {
          return method;
        });
      }
    }

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:paymentMethodsController
     * @name submit
     * @description Pass a controller method to our Directive.
     */
    function submit() {

      var paymentInfo = {
        $user: vm.user,
        ccInfo: vm.user.ccInfo,  // On user is in current controller form.
        billingInfo: vm.user.billing
      };

      $scope.$broadcast('addPaymentMethod:loaded', paymentInfo);

      $scope.$emit('userRole', vm.user.role);

      vm.paymentMissive = MissiveService.resetActiveObj('user')

      delete $scope._expiryDate;
      delete $scope.currentMonth;
      delete $scope.currentYear;
    }

    $scope.$on('base', function($event, data) {
      $scope.base = data;
    });

    $scope.$watch('vm.paymentMissive.currentStep', function(newVal, oldVal) {
      if  (newVal && vm.user && vm.user.billing) {
        UserService.addBillingInfo(vm.user);
      }

      try {
        UserService.getCurrentUser(true).then(function(user) {
          vm.user = user
        }, function() {
        });
      } catch(e) {}
    });

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:paymentMethodsController
     * @name previous
     * @description Return to app settings.
     */
    function previous() {

      if (vm.paymentMissive.currentStep === 0) {
        $rootScope.$emit('swap', { reset: true });
      } else {
        $rootScope.$emit('swap', { prev: true });
      }

      if (vm.paymentMissive.currentStep === 0) {
        UserService.removeCcInfo();
        $scope.$broadcast('resetBillingForm');
      }

      if (vm.paymentMissive.flow === 'createPaymentMethodForReservation') {
        MissiveService.previous();
      }

      vm.isValid = true;

      if (vm.paymentMissive.currentStep === 1) {
        $scope.showingForm = undefined;
        $scope.showBillingForm = true;
        $rootScope.$broadcast('showBillingForm');
        $rootScope.$broadcast('completeForm');
        MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0);
        return;
      }

      var vBtn = angular.element(document.querySelector('.virtual'));
      vBtn.remove();

      if ($scope.showingForm) {
        $rootScope.$emit('showList');
        $scope.showingForm = undefined;
        $rootScope.$broadcast('hideBillingForm');
      } else {
        if (
          vm.paymentMissive.flow === 'createUser'
          && vm.paymentMissive.currentStep === 0
        ) {
          MissiveService.setActive(true, 'createUser', 'buyer', 0);
          UserService.logout();
          $state.go('app.createUser');
        } else {
          $state.go('app.settings');
        }
      }

      $timeout(function() {
        $scope.showBillingForm = undefined;
        $scope.showForm = undefined;
        $scope.showingForm = undefined;
        $scope.showingList = undefined;
      }, 0);
    }

    $scope.$on('$ionicView.beforeEnter', function() {
      UserService.getCurrentUser(promiseCurrentUser)
        .then(getCurrentUserCompleted, function(e) {
        });
    });

  }
})();
