
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:AboutController
 * @description User Settings controller.
 */

(function() {

  'use strict';

   angular
    .module('WalletCompanion')
    .controller('AboutController', AboutController);

   AboutController.$inject = [
     '$scope',
     '$state',
     '$ionicLoading',
     'UserService',
     'MissiveService',
     '$ionicHistory',
     'ErrorService',
     '$window'
   ];

   function AboutController($scope, $state, $ionicLoading, UserService, MissiveService, $ionicHistory, ErrorService, $window) {

   var vm = this;

   vm.logout = logout;
   vm.previous = previous;
   vm.viewPaymentMethods = viewPaymentMethods;

   $scope.$on('$ionicView.beforeEnter', function() {

     $scope.$emit('toggleNavbar');
     UserService.getCurrentUser(true)
       .then(function(user) {
         vm.user = user;
       });
   });

   ////////////

   /**
    * @ngdoc method
    * @name viewPaymentMethods
    * @returns {undefined}
    */
   function viewPaymentMethods() {
     MissiveService.setActive(true, 'addPaymentMethod', 'buyer', 0);
     $state.go('app.paymentMethods', {}, { reload: true, notify: true });
   }

   /**
    * @ngdoc method
    * @methodOf WalletCompanion.controller:userSettingsController
    * @name previous
    * @returns {*} undefined
    */
   function previous() {
     $state.go('app.profile');
   }

   /**
    * @ngdoc method
    * @methodOf WalletCompanion.controller:userSettingsController
    * @name logout
    * @returns {*} undefined
    */
   function logout() {

     UserService.removeCurrentUser().then(function() {
       UserService.deleteLocalSession().then(function() {
         console.log('Deleted local session');
         //$window.localStorage.clear();
       }, function() {
         console.log('Failed to delete local session');
       });
     });

     $ionicHistory.clearCache().then(function() {
       $state.go('app.login', {}, { reload: true, notify: true });
     }, function(e) {
       ErrorService.notify(e, 'info');
     });
   }

  }

})();

