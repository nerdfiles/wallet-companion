/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ProfileController
 * @description Profile controller.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ProfileController', ProfileController);

  ProfileController.$inject = [
    '$scope',
    '$state',
    '$ionicLoading',
    'UserService',
    'ApiService',
    '$timeout',
    '$ionicPopup',
    '$analytics',
    '$q',
    'WalletService',
    '$ionicHistory',
    'FW'
  ];

  /**
   * # ProfileController
   *
   * @param $scope
   * @param $state
   * @param $ionicLoading
   * @param UserService
   * @param ApiService
   * @param $timeout
   * @param $ionicPopup
   * @param $analytics
   * @param $q
   * @returns {undefined}
   */
  function ProfileController($scope, $state, $ionicLoading, UserService, ApiService, $timeout, $ionicPopup, $analytics, $q, WalletService, $ionicHistory, FW) {

    // ## Controller Init

    var vm = this

    // ## Controller Hypermedia

    vm.sectionTitle = 'Profile'

    // ## Controller Data

    vm.images = []

    // ## Controller Capabilities

    vm.logout = logout;
    vm.save = save;
    vm.uploadPhoto = uploadPhoto;
    vm.toggleProfileEditMode = toggleProfileEditMode;

    // ## Controller Interaction Model

    vm.editMode = false
    $scope.$on('$destroy', function() {
      vm.editMode = false
      vm.images = []
    })

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', true)
    })

    WalletService
      .authorize()
      .then(function(res) {
        console.log(res);
        // vm.ethAddress = res.address;
        var address = '0x' + FW.WALLET_ENTITY.address;
        vm.ethAddress = address;
      }, function(rej) {
        console.log(rej);
      })
      .catch(function(err) {
        console.log(err);
      });


    /**
     * @function event::httpErrorMessage
     * @param $event
     * @param data
     * @returns {undefined}
     */
    $scope.$on('httpErrorMessage', function($event, data) {
      $ionicLoading.hide()
      $ionicPopup.show({
        title: 'HTTP Error',
        subTitle: 'There was an error in submitting your request.',
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
            }
          }
        ]
      })
    })

    // ## Controller Defaults

    UserService
      .getCurrentUser(true)
      .then(function(user) {

        $ionicLoading.hide();

        $timeout(function() {
          vm.user = user

          if (
            user.profile &&
            user.profile.avatar &&
            !user.profile.avatar.url
          ) {
            ApiService
              .assets
              .get(vm.user.profile.avatar)
              .then(function(res) {
                vm.user.profile.avatar = res.data.asset;
              }, function(e) {
                console.log({
                  error: e
                });
              });
          }

          $scope.$apply();
          console.log('Loaded profile for user: ', vm.user);
        }, 4);

      });


    /**
     * @function event::uploadPhoto
     * @param $event
     * @param data
     * @returns {undefined}
     */
    $scope.$on('uploadPhoto', function($event, data) {
      var photo = {
        type: 'userImage',
        files: data
      }

      ApiService.assets
        .create(photo)
        .then(function(res) {

          console.log('Uploaded ' + res.data.asset.type + ': ', vm.user.profile.photo)
          vm.user.profile.avatar = res.data.asset
        }, function(e) {
          console.log(e)
        })
    })

    ////////////

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:userSettingsController
     * @name logout
     * @returns {*} undefined
     */
    function logout() {
       UserService.removeCurrentUser().then(function() {
        UserService.deleteLocalSession().then(function() {
           console.log('Deleted local session');
          //$window.localStorage.clear();
        }, function() {
          console.log('Failed to delete local session');
        });
      });

      $ionicHistory.clearCache().then(function() {
        $state.go('app.login', {}, { reload: true, notify: true });
      }, function(e) {
        ErrorService.notify(e, 'info');
      });
    }


    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ProfileController
     * @name uploadPhoto
     * @returns {undefined}
     */
    function uploadPhoto() {
      return angular.noop()
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ProfileController
     * @name save
     * @param {object} userData User data from profile view
     * modifications.
     * @returns {*} undefined
     */
    function save(userData) {

      var _id
      var imageRef
      var def = $q.defer()

      /**
       * @name __init__
       * @returns {undefined}
       * @description A "pre" command thing.
       */
      function __init__() {
        $timeout(function() {
          if (userData.profile.phone) {
            ApiService.users.update(userData._id, userData)
              .then(function(res) {

                UserService.getCurrentUser(true)
                  .then(function(user) {
                    $timeout(function() {
                      try {
                        _.extend(vm.user, user)
                        def.resolve(vm.user)
                      } catch(e) {
                        console.log(e)
                        def.reject({ error: e })
                      }
                      $scope.$apply()
                    }, 4)
                  }, angular.noop)

                $scope.$broadcast('configMessage', {
                  title: 'Profile Updated',
                  subTitle: 'You have updated your profile.'
                })
              }, angular.noop)
          }
        }, 4)
        return def.promise
      }

      __init__().then(function(res) {

        _.extend(userData, res)

        $timeout(function() {
          if (userData.profile && userData.profile.avatar) {
            try {
              _id = vm.user.profile.avatar
            } catch(e) {
              console.log('Updated copy: ', e)
            }

            try {
              // old val
              _id = userData.profile.avatar
            } catch(e) {
              console.log('Old copy: ', e)
            }

            imageRef = {}

            ApiService.assets
              .get(_id).then(function(res) {

                var asset = res.data.asset
                imageRef.image = asset._id

                ApiService.users
                  .action(vm.user._id)('setProfilePhoto', imageRef)
                  .then(function(res) {

                    vm.editMode = false
                    var avatar = res.data.profile.avatar

                    ApiService.assets
                      .get(avatar).then(function(res) {
                        vm.user.profile.avatar = res.data.asset
                      }, function(e) {
                        console.log(e)
                      })

                    // UserService.getCurrentUser(true)
                    //   .then(function(user) {
                    //     try {
                    //       _.extend(vm.user, user)
                    //     } catch(e) {
                    //       console.log(e)
                    //     }
                    //     if (userData.profile.phone) {
                    //       $scope.$broadcast('configMessage', {
                    //         title: 'Profile Update',
                    //         subTitle: 'You have updated your profile photo.'
                    //       })
                    //     }
                    //   })
                  })
              }, function(e) {
                console.log(e)
              })
          }
        }, 104)
      }, function(e) {
        console.log(e)
      })
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ProfileController
     * @name toggleProfileEditMode
     * @returns {*} undefined
     */
    function toggleProfileEditMode () {
      vm.editMode = !vm.editMode
      if ($state.is('app.profileEdit')) {
        $state.go('app.profile', null, { notify: false })
      } else {
        $state.go('app.profileEdit', null, { notify: false })
      }
    }
  }

})()

