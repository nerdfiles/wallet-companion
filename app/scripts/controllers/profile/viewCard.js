(function(cuid) {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ViewCardController', ViewCardController);

  ViewCardController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    '$ionicLoading',
    '$ionicPopup',
    'UserService',
    'ApiService',
    '$timeout'
  ];

  function ViewCardController($scope, $state, $stateParams, $ionicLoading, $ionicPopup, UserService, ApiService, $timeout) {
    var vm = this;
    var promiseCurrentUser = true;
    var paymentMethod = {
      paymentSource: $stateParams.id
    };

    if ($stateParams.id) {
      vm.cardId = $stateParams.id;
    } else {
      $state.go('app.paymentMethods');
    }

    vm.previous = previous;
    vm.deleteCard = deleteCard;

    $scope.$on('$ionicView.beforeEnter', function() {
      $ionicLoading.show();
      UserService.getCurrentUser(promiseCurrentUser)
        .then(currentUserServiceGetCurrentUserCompleted, function(e) {
          console.log(e);
        });

      $ionicLoading.hide();
    });

    ////////////

    /**
     * @name currentUserServiceGetCurrentcurrentUserCompleted
     * @param {object} currentUser TBD
     * @returns {*} undefined
     */
    function currentUserServiceGetCurrentUserCompleted(user) {

      // Load current user from API and update user for its payment methods.
      vm.user = user;

      ApiService.users.getPaymentMethods(vm.user._id, vm.user)
        .then(apiServiceCurrentUsersGetPaymentMethods, function(e) {
          console.log(e);
        });

        /**
         * @name apiServiceCurrentUsersGetPaymentMethods
        * @param {object} currentUserData TBD
          * @returns {*} undefined
        */
        function apiServiceCurrentUsersGetPaymentMethods(currentUserData) {
          vm.paymentMethods = _.map(currentUserData.data.methods.data, function(method) {
            return method;
          });
          vm.card = _.last(_.filter(vm.paymentMethods, function(item) {
            if (item.id === vm.cardId) {
              return true;
            }
          }));
        }
    }

    /**
     * deleteCard
     *
     * @param id
     * @returns {undefined}
     */
    function deleteCard(id) {
      $ionicPopup.show({
        title: 'Delete Card',
        subTitle: 'Are you sure you want to delete this credit card?',
        buttons: [
          {
            text: 'No',
            onTap: function(e) {
              console.log(e);
            }
          },
          {
            text: 'Yes',
            onTap: function(e) {
              UserService.getCurrentUser(true).then(function(user) {
                ApiService.users.action(user.id)('removePaymentMethod', paymentMethod)
                  .then(function() {
                    $state.go('app.paymentMethods');
                  }, function(e) {
                    console.log(e);
                  });
              }, function(e) {
                console.log(e);
              });
            }
          }

        ]
      });
    }

    /**
     * @name previous
     * @description Return to app settings.
     */
    function previous() {
      $state.go('app.paymentMethods');
    }
  }
})(window.cuid);
