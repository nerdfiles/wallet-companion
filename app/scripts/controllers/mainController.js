/**
 * @ngdoc controller
 * @name WalletCompanion.controller:MainController
 * @description
 * The Main Controller is a bit of a beast, since it essentially is a platform
 * for modal windows.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('MainController', MainController);

  MainController.$inject = [
    '$scope',
    '$state',
    '$ionicHistory',
    '$timeout',
    'UserService',
    'ScriptLoader',
    '$ionicLoading',
    'ModelService',
    '$ionicPlatform',
    '$ionicPopup'
  ];

  function MainController($scope, $state, $ionicHistory, $timeout, UserService, ScriptLoader, $ionicLoading, ModelService, $ionicPlatform, $ionicPopup) {

    // ## Controller Init
    var vm = this;

    // ## Controller Hypermedia Controls
    // ## Controller Defaults

    vm.showNav = false;

    // ## Controller Capabilities

    vm.showBtn = showBtn;

    // ## Controller Interaction Model

    $scope.$on('$ionicView.enter', function() {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    });

    $scope.$on('loading:stop', function() {
      $ionicLoading.hide();
    });

    $scope.$on('goBack', goBack);
    $scope.$on('$ionicView.beforeEnter', ionicViewBeforeEnter);
    $scope.$on('userRole', userRole);
    $scope.$on('toggleNavbar', toggleNavbar);

    // ## Controller Data

    UserService.getCurrentUser(true)
      .then(function(user) {
        vm.user = user;
        $scope.role = user.role;
      });

    ////////////

    // ## Controller Utilities

    /**
     * @ngdoc methhod
     * @methodOf WalletCompanion.controller:MainController
     * @name showBtn
     * @param type
     * @returns {undefined}
     */
    function showBtn(type) {
      return true;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:MainController
     * @name goBack
     * @returns {*} undefined
     */
    function goBack() {
      $ionicHistory.goBack();
    }

    /**
     * ionicViewBeforeEnter
     *
     * @returns {*} undefined
     */
    function ionicViewBeforeEnter() {
      // Redirect to login state if user is not logged-in on controller load
      if (!ScriptLoader.isLoaded()) {
        $ionicPlatform.ready(function() {
          ScriptLoader.initialize
            .then(function() {
              $scope.$broadcast('scriptsLoaded');
            });
        });
      }
    }

    /**
     * @name toggleNavbar
     * @param {object} e TBD
     * @param {object} val TBD
     * @returns {*} undefined
     */
    function toggleNavbar(e, val) {
      vm.showNav = val;
      $timeout(angular.noop, 4);
    }

    /**
     * @name userRole
     * @param {object} e TBD
     * @param {object} role TBD
     * @returns {*} undefined
     */
    function userRole(e, role) {
      $scope.role = role;
      $timeout(function() {
        $scope.$apply();
      }, 4);
    }
  }
})()

