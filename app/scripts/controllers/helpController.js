/**
 * @ngdoc controller
 * @name WalletCompanion.controller:HelpController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('HelpController', HelpController);

  HelpController.$inject = [
    '$scope'
  ];

  /**
   * HelpController
   *
   * @param $scope
   * @returns {undefined}
   */
  function HelpController($scope) {
    // ## Controller Init
    var vm = this;
    // ## Controller Hypermedia Controls
    // ## Controller Data
    // ## Controller Defaults
    // ## Controller Capabilities
    // ## Controller Interaction Model
    ////////////
    // ## Controller Utilities
  }

})();

