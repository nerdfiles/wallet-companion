/**
 * @ngdoc function
 * @name WalletCompanion.controller:ContractListController
 */
(function() {

  'use strict';

   angular.module('WalletCompanion')
     .controller('ContractListController', ContractListController);

   ContractListController.$inject = [
     '$scope',
     'ContractService',
     'UserService',
     '$ionicPopup',
     '$state',
     'web3',
     'keythereum',
     'contractManager',
     'FW',
     '$http'
   ];

   function ContractListController($scope, ContractService, UserService, $ionicPopup, $state, web3, keythereum, contractManager, FW, $http) {

     var vm = this;

     //activate();

     function activate() {

       var walletPrivateKey = FW.WALLET_KEYS.privateKey;
       var walletAddress = keythereum.privateKeyToAddress(walletPrivateKey);

       var localWallet = {
         wallet: {
           address: walletAddress,
           balance: {
             eth: +(+web3.eth.getBalance(walletAddress) / 1000000000000000000).toFixed(8),
             ethusd: 0,
             dbc: (+contractManager.ethereum_contract.balanceOf(walletAddress) / contractManager._ethereum_contract_fixed).toFixed(2)
           }
         }
       };

       $http('https://api.coinmarketcap.com/v1/ticker/ethereum/')
         .then(function(res) {
           try {
             console.log(res)
           } catch (err) {
             console.error({ error: err })
           }
         });
     }

     $scope.$on('$ionicView.beforeEnter', function() {
       $scope.$emit('toggleNavbar', true);
     });

     /**
      * viewContract
      *
      * @returns {undefined}
      */
     vm.viewContract = function(contracts) {
       $state.go('app.contract', { id: contracts._id });
     };

     ContractService.getContracts()
       .then(getContractsCompleted, getContractsFailed);

     /**
      * @function getContractsCompleted
      * @param {object} contractsListData Contracts List data.
      * @returns {*} undefined
      * @description Contracts List data organized by Status recasted from
      * temporal period.
      */
     function getContractsCompleted(contractsListData) {
       vm.contracts = _.groupBy(contractsListData, 'status');
       vm.ActiveGroup = contractsByPeriod('Active', 'M');
       vm.PendingGroup = contractsByPeriod('Pending', 'M');
       vm.CancelledGroup = contractsByPeriod('Cancelled', 'M');
       vm.CompletedGroup = contractsByPeriod('Completed', 'M');
       vm.ApprovedGroup = contractsByPeriod('Approved', 'M');
       console.log(vm.PendingGroup);
       console.log(vm.CompletedGroup);
     }

    /**
     * getContractsFailed
     *
     * @param {object} e Error object.
     * @returns {object} Status object data.
     */
     function getContractsFailed(e) {
       console.log(e);
     }

     /**
      * Contracts
      *
      * @param {string} type Informed by types in schema for contracts.
      * @param {string} period Day, Month, Year, etc.
      * @returns {array} An array of contracts with keys by temporal
      * period.
      */
     function contractsByPeriod (type, period) {
       var groups = [];

       /**
        * contractsReduce
        *
        * @param {object} iteration TBD
        * @param {object} contract TBD
        * @param {object} key TBD
        * @returns {*} undefined
        */
       function contractsReduceDate (iteration, contract) {
         (iteration[contract.dates.from] || (iteration[contract.dates.from] = []))
           .push(contract);
         return iteration;
       }

       var contracts = _.reduce(vm.contracts[type], contractsReduceDate, {});

       _.each(contracts, function (contracts, key) {
         groups.push({
           name         : key,
           contracts : contracts
         });
       });

       return groups;
     }

   }
 })();
