/**
 * @ngdoc controller
 * @name WalletCompanion.controller:reservationCreationController
 * @description Reservations POST implemenation.
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ContractCreationController', ContractCreationController);

  ContractCreationController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$ionicModal',
    '$ionicLoading',
    'MissiveService',
    'ReservationService',
    '$ionicPopup',
    '$stateParams',
    'MessageService',
    'ErrorService',
    'UserService',
    'ApiService',
    '$timeout'
  ];

  /**
   * ContractCreationController
   *
   * @param $scope
   * @param $rootScope
   * @param $state
   * @param $ionicModal
   * @param $ionicLoading
   * @param MissiveService
   * @param ReservationService
   * @param $ionicPopup
   * @param $stateParams
   * @param MessageService
   * @param ErrorService
   * @param UserService
   * @param ApiService
   * @param $timeout
   * @returns {undefined}
   */
  function ContractCreationController($scope, $rootScope, $state, $ionicModal, $ionicLoading, MissiveService, ReservationService, $ionicPopup, $stateParams, MessageService, ErrorService, UserService, ApiService, $timeout) {

    var vm = this;
    vm.actionLabel = 'Continue';
    vm.detail = {};
    vm.reservation = {};
    vm.coinCount = 0;
    vm.reservation = {
      listingId   : $stateParams.id || null,
      coins        : [],
      dates       : {},
      buyer   : null,
      _hst        : 0.13,
      _nightCount : 0
    };

    vm.createReservationFlow = {};
    vm.createReservationFlow = MissiveService.getActive();
    console.log('res', vm.reservation);

    if (vm.reservation.invoice !== undefined || vm.createReservationFlow.flow === '' || vm.createReservationFlow.currentStep === 4) {
      vm.createReservationFlow = MissiveService.setActive(true, 'createMessage', 'buyer', 0);
    }

    vm.sectionTitle = MissiveService.getTitle();
    vm.getInvoiceCallback = function(invoice) {
      vm.reservation.invoice = invoice;
    };
    vm.submitAction = submitAction;
    vm.submit = submit;
    vm.viewCoins = viewCoins;

    vm.inc = inc;
    vm.dec = dec;
    vm.next = next;
    vm.previous = previous;
    vm.goToCreatePaymentMethodForReservation = goToCreatePaymentMethodForReservation;
    vm.viewReservations = viewReservations;
    vm.cancelMessage = cancelMessage;
    vm.steps = vm.createReservationFlow.totalSteps;
    vm.expandCalendar = expandCalendar;

    $scope.lastCheckIn = null;

    $ionicModal.fromTemplateUrl('templates/modals/calendar.html', { scope: $scope, animation: 'slide-in-up' })
      .then( function(modal) {

        ReservationService.getCurrentMessage()
          .then(function(res) {
            if (!res || !res.selectedOffer) {
              return console.log('No offer selected');
            }

            vm.selectedCoinDateRange = null;

            var scryptSelectedCoin = _.filter(res.selectedOffer.coins.scrypt, function(coin) {
              return coin.selected;
            });

            var sha256SelectedCoin = _.filter(res.selectedOffer.coins.sha256, function(coin) {
              return coin.selected;
            });

            if (_.size(scryptSelectedCoin)) {
              vm.selectedCoinDateRange = _.last(scryptSelectedCoin);
            } else if (_.size(sha256SelectedCoin)) {
              vm.selectedCoinDateRange = _.last(sha256SelectedCoin);
            }

            vm.availableFrom = moment(vm.selectedCoinDateRange.availability.from);
            vm.availableTo = moment(vm.selectedCoinDateRange.availability.to);
            if (vm.availableFrom.add(1, 'days').isSame(vm.availableTo)) {
              vm.sameness = true;
            }

            vm.calendarModal = modal;
            vm.calendarControl = {
              selectedDate: null,
              method: null,
              eventSource: ReservationService.getCalendarEvents(),
              isDateDisabled: vm.sameness ? undefined : function(date) {
                var a = new Date(vm.availableTo)
                var b = new Date(vm.availableFrom)
                return date > a || date < b
              },
              from: vm.availableFrom,
              to: vm.availableTo,
              onSelection: function(method) {
                $rootScope.$broadcast('calendarSelection', vm.calendarControl.selectedDate);

                vm.reservation._nightCount = moment(vm.reservation.dates.to).diff(moment(vm.reservation.dates.from), 'days');
                ReservationService.updateCurrentMessage(vm.reservation);
              },
              onViewTitleChanged: function(title) {
                vm.viewTitle = title;
              }
            };
          }, function(e) {
            console.log(e);
          })
      });

    UserService.getCurrentUser(true)
      .then(function(userData) {
        vm.user = userData;
        vm.user.paymentMethods = { list: [] };
        if (!vm.user._id) {
          return console.log('User not load', vm.user);
        }
        UserService.getUserPaymentMethods(vm.user._id, vm.user)
          .then(function(paymentData) {
            if (!_.size(vm.user.paymentMethods.list)) {
              vm.user.paymentMethods.list = paymentData;
            }
            vm.cards = vm.user.paymentMethods.list;
          });
      }, function(e) {
        vm.user.paymentMethods = { list: [] };
        if (!vm.user._id) {
          return console.log('User not load', vm.user);
        }
        UserService.getUserPaymentMethods(vm.user._id, vm.user)
          .then(function(paymentData) {
            if (!_.size(vm.user.paymentMethods.list)) {
              vm.user.paymentMethods.list = paymentData;
            }
            vm.cards = vm.user.paymentMethods.list;
          });
      });

    $scope.$on('$destroy', function() {
      try {
        $rootScope.$off('httpErrorMessage');
      } catch(e) {
        console.log(e);
      }
    });

    $scope.$on('httpErrorMessage', function($event, data) {
      if (data) {
        console.log(data);
      }

      $ionicLoading.hide();

      $ionicPopup.show({
        title: 'HTTP Error',
        subTitle: 'There was an error in your request. Please try again.',
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
            }
          }
        ]
      });
    });


    $scope.$on('$ionicView.beforeEnter', ionicViewBeforeEnter);
    $scope.$on('configError', configError);

    $scope.$on('calendarSelection', calendarSelection);
    $rootScope.$on('addPaymentMethod:init', function ($event, res) {
      vm._links = [];
      vm._links.push({
        '$container': false
      });
    });

    ////////////

    /**
     * @name next
     * @returns {undefined}
     */
    function next() {
      var flow = MissiveService.getActive();
      console.log(flow);
      // @TODO email enquiry
      if (
        vm.reservation.selectedOffer.isPlaceholder === true
        || vm.reservation.selectedCoin.description.indexOf('reservation enquiry') !== -1
      ) {
        // Special import list workflow
        var offer = {}
        var _offer = {
          dates    : vm.reservation.dates,
          quantity : vm.reservation.configuredCoin.quantity,
          coinType : vm.reservation.selectedCoin.coinType || 'any'
        }

        _.extend(offer, _offer)

        ApiService.offers
          .action(vm.reservation.selectedOffer._id)('requestInformation', offer)
          .then(function(res) {

            console.log('Listing ID: ', vm.reservation.listingId);
            vm.createReservationFlow = MissiveService.setActive(false, 'createMessage', 'buyer', 4);
            ReservationService.removeCurrentMessage();

            $ionicLoading.hide();

            $rootScope.$emit('swap', { reset: true });
            UserService.removeCcInfo();
            vm.user = {};
          }, function(e) {
            $scope.$broadcast('configError', {
              title: 'We could not make your reservation at this time',
              subTitle: 'Try again later'
            });
            ReservationService.removeCurrentMessage();
            $ionicLoading.hide();
            vm.user = {};
          });
      } else {
        MissiveService.next();
      }
    }

    /**
     * previous
     *
     * @returns {undefined}
     */
    function previous() {
      MissiveService.previous();
    }

    /**
     * loadData
     *
     * @returns {undefined}
     */
    function loadData() {

      ReservationService
        .getCurrentMessage()
        .then(function(res) {

          if (!res || !res.selectedOffer) {
            return console.log('No offer selected');
          }

          vm.selectedCoinDateRange = null;

          var scryptSelectedCoin = _.filter(res.selectedOffer.coins.scrypt, function(coin) {
            return coin.selected;
          });
          var sha256SelectedCoin = _.filter(res.selectedOffer.coins.sha256, function(coin) {
            return coin.selected;
          });
          if (_.size(scryptSelectedCoin)) {
            vm.selectedCoinDateRange = _.last(scryptSelectedCoin);
          } else if (_.size(sha256SelectedCoin)) {
            vm.selectedCoinDateRange = _.last(sha256SelectedCoin);
          }

          vm.availableFrom = moment(vm.selectedCoinDateRange.availability.from);
          vm.availableTo = moment(vm.selectedCoinDateRange.availability.to);

          if (res.configuredCoin) {
            vm.coinCount = res.configuredCoin.quantity;
          } else {
            vm.inc();
          }

          _.extend(vm.reservation, res);
          vm.card = res.paymentMethod;

          if (res.selectedOffer.name) {
            _.extend(vm.detail, res.selectedOffer);
            $scope.$emit('coinsData-loaded', res.selectedOffer.coins);
            return;
          }

          MessageService.getOfferById(vm.reservation.listingId)
            .then(ErrorService.notify(getOfferByIdLoaded))
            .catch(getOfferByIdError)
            .finally(() => {

              MessageService.getCoinsByOfferId(vm.reservation.listingId)
                .then(ErrorService.notify(getCoinsByOfferIdLoaded))
                .catch(getCoinsByOfferIdError)
            });

        }, function(e) {
          vm.coinCount = 1;
          console.log('No currencies selected!');
        });
    }

    /**
     * inc
     *
     * @returns {undefined}
     */
    function inc() {
      vm.coinCount++;
      $scope.$emit('coinCount', vm.coinCount);
    };

    /**
     * dec
     *
     * @returns {undefined}
     */
    function dec() {
      vm.coinCount--;
      if (vm.coinCount < 0) {
        vm.coinCount = 0;
      }
      $scope.$emit('coinCount', vm.coinCount);
    };

    /**
     * expandCalendar
     *
     * @param method
     * @returns {undefined}
     */
    function expandCalendar(method) {
      vm.calendarControl.method = method;
      vm.calendarModal.show();
    };

    /**
     * @function calendarSelection
     * @param {object} e TBD
     * @param {object} val TBD
     * @returns {*} undefined
     */
    function calendarSelection(e, val) {

      val = moment(val);
      var method = vm.calendarControl.method;
      // @UPDATE reservation object
      vm.reservation.dates[method] = val.format('MM/DD/YYYY');
      vm.reservation.dates.from = moment(vm.reservation.dates.from).format('MM/DD/YYYY');
      ReservationService.updateCurrentMessage(vm.reservation);

      var now = moment();

      if (method === 'from') {
        if(
          !moment(val.format('MM/DD/YYYY')).isSame(moment().format('MM/DD/YYYY')) &&
          moment(val.format('MM/DD/YYYY')).isBefore(moment().format('MM/DD/YYYY'))
        ) {
          $scope.$broadcast('configError', {
            title: 'Checkin date before current date',
            subTitle: 'Please select a later date'
          });
          vm.reservation.error = {error: 'Checkin date error'};
        } else {
          vm.reservation.error = undefined;
          vm.reservation.dates.to = moment(val).add(1, 'days').format('MM/DD/YYYY');
        }
      }

      if (method === 'to') {
        if(
          moment(val.format('MM/DD/YYYY')).isBefore(moment().format('MM/DD/YYYY'))
        ) {
          $scope.$broadcast('configError', {
            title: 'Checkout date before current date',
            subTitle: 'Please select a later date'
          });
          vm.reservation.error = {error: 'Checkout date error'};
        } else if(
          moment(val.format('MM/DD/YYYY')).isBefore(vm.reservation.dates.from)
        ) {
          $scope.$broadcast('configError', {
            title: 'Checkout date before selected checkin date',
            subTitle: 'Please select a later date'
          });
          vm.reservation.error = {error: 'Checkout date error'};
        } else {
          vm.reservation.error = undefined;
        }
      }

      $scope.lastCheckIn = vm.reservation.dates.from;

      if (vm.reservation.error === undefined) {
        vm.calendarModal.hide();
      }

      $timeout(function() {
        $scope.$apply();
      }, 4);
    }

    /**
     * configError
     *
     * @param $event
     * @param config
     * @returns {undefined}
     */
    function configError($event, config) {
      $ionicPopup.show({
        title: config.title,
        subTitle: config.subTitle,
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
              vm.reservation.error = {error: config.title};
            }
          }
        ]
      });
    }

    /**
     * cancelReservationProceedings
     * @returns {*} undefined
     */
    function cancelMessage() {
      $ionicPopup.show({
        title: '<i class="icon ionicons ion-ios-help-outline"></i>',
        subTitle: 'Do you want to cancel this current reservation? Cancelling will return you to the last coin views.',
        buttons: [
          {
            text: 'Cancel',
            onTap: function(e) {}
          },
          {
            text: 'Confirm',
            onTap: function(e) {
              ReservationService.cancelMessage();
              // $state.go('app.coins', { id: vm.reservation.listingId });
              $state.go('app.tools');
            }
          }
        ]
      });
    }

    $scope.$on('coinsData-loaded', function($event, coinsData) {

      $scope.$on('coinCount', function($event, coinCountValue) {

        ReservationService
          .getCurrentMessage()
          .then(reservationServiceGetCurrentReservationCompleted);

        /**
         * @name reservationServiceGetCurrentReservationCompleted
         * @param res
         * @returns {undefined}
         */
        function reservationServiceGetCurrentReservationCompleted(res) {

          if (!res.selectedOffer) {
            $scope.$broadcast('configError', {
              title: 'No currencies selected',
              subTitle: 'Please select a coin'
            });
            return;
          }

          var scryptSelectedCoin = _.filter(res.selectedOffer.coins.scrypt, function(coin) {
            return coin.selected;
          });

          var sha256SelectedCoin = _.filter(res.selectedOffer.coins.sha256, function(coin) {
            return coin.selected;
          });

          var scryptSelected = _.size(scryptSelectedCoin);
          var sha256Selected = _.size(sha256SelectedCoin);
          var _id;

          if (!vm.detail.name) {
            if (sha256Selected) {
              _id = _.last(_.filter(vm.detail.coins, function(coin) {
                if (coin.coinType === 'sha256') {
                  return coin;
                }
              }))._id;
              vm.reservation.coins.push({
                coin: _id,
                quantity: coinCountValue
              });
              vm.reservation.selectedCoin = _.last(sha256SelectedCoin);
            } else if (scryptSelected) {
              _id = _.last(_.filter(vm.detail.coins, function(coin) {
                if (coin.coinType === 'scrypt') {
                  return coin;
                }
              }))._id;
              vm.reservation.coins.push({
                coin: _id,
                quantity: coinCountValue
              });
              vm.reservation.selectedCoin = _.last(scryptSelectedCoin);
            }
          } else {
            var hotelCoinList = vm.detail.coins.sha256.concat(vm.detail.coins.scrypt);
            _.each(hotelCoinList, function(coinRef) {
              if (coinRef.selected) {
                vm.reservation.coins.push({
                  coin: vm.detail.id,
                  quantity: coinCountValue
                });
              }
            });
            if (sha256SelectedCoin.length) {
              vm.reservation.selectedCoin = _.last(sha256SelectedCoin);
            } else {
              vm.reservation.selectedCoin = _.last(scryptSelectedCoin);
            }
          }

          vm.reservation.configuredCoin = _.last(vm.reservation.coins);
          if (vm.reservation.configuredCoin.quantity > vm.reservation.selectedCoin.quantity) {
            $scope.$broadcast('configError', {
              title: 'Not enough coins',
              subTitle: 'Not enough coins are available'
            });
          } else if (vm.reservation.configuredCoin.quantity === 0) {
            $scope.$broadcast('configError', {
              title: 'No coins selected',
              subTitle: 'Please select a coin'
            });
          } else {
            vm.reservation.error = undefined;
            ReservationService.updateCurrentMessage(vm.reservation);
          }
        }
      });

      $scope.$emit('coinCount', vm.coinCount);
    });

    /**
     * @name ionicViewBeforeEnter
     * @returns {*} undefined
     */
    function ionicViewBeforeEnter() {
      $timeout(function() {
        loadData();
      }, 4);
      $scope.$emit('toggleNavbar', false);
    }

    /**
     * @name getCoinsByOfferIdLoaded
     * @param {object} coinsData TBD
     * @returns {*} undefined
     */
    function getCoinsByOfferIdLoaded(coinsData) {
      vm.detail.coins = coinsData;

      ReservationService.getCurrentMessage()
        .then(function(res) {

          for (var i = 0; i < res.selectedOffer.coins.scrypt.length; i++) {
            if (res.selectedOffer.coins.scrypt[i].selected) {
              vm.detail.coin = res.selectedOffer.coins.scrypt[i];
              return;
            }
          }

          for (var i = 0; i < res.selectedOffer.coins.sha256.length; i++) {
            if (res.selectedOffer.coins.sha256[i].selected) {
              vm.detail.coin = res.selectedOffer.coins.sha256[i];
              return;
            }
          }

      });

      $scope.$emit('coinsData-loaded', coinsData);

      return coinsData;
    }

    /**
     * @name getCoinsByOfferIdError
     * @param {object} e TBD
     * @returns {*} undefined
     */
    function getCoinsByOfferIdError(e) {
      // console.log(e)
      return e;
    }

    /**
     * @name getOfferByIdError
     * @param {object} e TBD
     * @returns {*} undefined
     * @description If there is an error passed to the client-side by
     * the server then we can treat it like any other protocol error
     * that would typically be handled by a catch. We can also check
     * at the error service if there is any data and treat that in a
     * standard way everywhere. So we can use `catch` for empty data
     * as well as explicit error conditions.
     */
    function getOfferByIdError(e) {
      vm.reservation.error = e;
      return e;
    }

    /**
     * @name getOfferByIdLoaded
     * @param {object} offerData TBD
     * @returns {*} undefined
     */
    function getOfferByIdLoaded(offerData) {
      vm.detail = offerData;
      $scope.$emit('offerData-loaded');
      return offerData;
    }

    ////////////

    /**
     * @name submitAction
     * @returns {*} undefined
     */
    function submitAction() {

      $ionicLoading.show();

      // Give user the last coin entry selected
      vm.reservation.coins = [_.last(vm.reservation.coins)];
      if (!vm.reservation.coins && !vm.reservation.coins.length) {
        return console.log('No coins selected');
      }

      if ( vm.reservation.selectedOffer.isPlaceholder === true ) {

        console.log(vm);

        ApiService.offers.action(vm.reservation.selectedOffer._id)('requestInformation', {
          dates: {
            from: vm.reservation.dates.from,
            to: vm.reservation.dates.to
          },
          quantity: vm.reservation.coins.length,
          coinType: 'any' //change this to reflect 'scrypt' or 'sha256'
        });
      }

      ReservationService.createMessage(vm.reservation).then(function(_res) {
        ReservationService.getMessages().then(function(res) {
          var latestReservations = _.last(res);
          vm.reservation.response = latestReservations;
          ApiService.reservations.action(latestReservations._id)('getInvoice', {}).then(function(invoice) {
            ApiService.reservations.action(latestReservations._id)('submitPayment', {}).then(function(submittedPayment) {
              vm.reservation.submittedPayment = submittedPayment;
              ApiService.reservations.action(latestReservations._id)('getInvoice', {}).then(function(invoice) {

                console.log('Listing ID: ', vm.reservation.listingId);
                vm.createReservationFlow = MissiveService.setActive(false, 'createMessage', 'buyer', 4);
                ReservationService.removeCurrentMessage();

                vm.reservation.invoice = invoice;

                console.log('Completed invoice', invoice);
                $ionicLoading.hide();

                $rootScope.$emit('swap', { reset: true });
                UserService.removeCcInfo();
                vm.user = {};

              }, function() {
                $state.go('app.coins');
                $ionicLoading.hide();
              });
            }, function(e) {
              console.log('Failed to submit payment');
              $state.go('app.coins');
              $ionicLoading.hide();
            });

          }, function(e) {
            console.log('Could not get invoice');
            $state.go('app.coins');
            $ionicLoading.hide();
          });
        }, function(e) {
          console.log('Could not get invoice');
          $state.go('app.coins');
          $ionicLoading.hide();
        });
      }, function(e) {

        console.log('Create reservation failed');
        $ionicLoading.hide();
        $rootScope.$emit('swap', { reset: true });
        UserService.removeCcInfo();
        vm.user = {};
      });
    }

    /**
     * @name submit
     * @returns {*} undefined
     */
    function submit() {
      var paymentInfo = {
        ccInfo: vm.user.ccInfo,
        billingInfo: $scope.user.billing
      };
      $scope.$broadcast('addPaymentMethod:loaded', paymentInfo);
      $scope.$emit('userRole', vm.user.role);

    }

    /**
     * goToCreatePaymentMethodForReservation
     *
     * @returns {undefined}
     */
    function goToCreatePaymentMethodForReservation() {
      $state.go('app.createMessagePaymentSelect', {
        id: vm.reservation.listingId
      }, {
        reload: true,
        notify: true
      });
    }

    /**
     * viewReservations
     *
     * @returns {undefined}
     */
    function viewReservations() {
      MissiveService.resetActiveObj('reservation');
      $state.go('app.history');
    }

    /**
     * viewCoins
     *
     * @returns {undefined}
     */
    function viewCoins() {
      MissiveService.resetActiveObj('reservation');
      $state.go('app.currencies');
    }
  }
})();

