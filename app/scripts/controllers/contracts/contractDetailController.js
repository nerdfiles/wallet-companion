// ./app/scripts/controllers/ContractDetailController.js
/**
 * @ngdoc controller
 * @name ContractDetailController
 * @description Contract Detail Controller.
 * @see
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ContractDetailController', ContractDetailController);

  ContractDetailController.$inject = [
    '$scope',
    '$rootScope',
    '$stateParams',
    'UserService',
    '$ionicHistory',
    'ActionService',
    'MessageService',
    '$state',
    'ErrorService',
    'ApiService',
    '$log',
    '$ionicLoading'
  ];

  function ContractDetailController ($scope, $rootScope, $stateParams, UserService, $ionicHistory, ActionService, MessageService, $state, ErrorService, ApiService, $log, $ionicLoading) {

    $scope.$log = $log;
    const vm = this;
    var id = $stateParams.id || null;
    vm.detail = {};
    vm.detail.invoice = {};

    // $ionicLoading.show();

    /**
     * cancelContract
     *
     * @returns {undefined}
     */
    vm.cancelContract = function() {
      ApiService.contracts.action(vm.detail._id)('cancel', {})
        .then(function(res) {
          $state.go('app.contracts');
          console.log(res);
        }, function(e) {
          ErrorService.notify(e, 'info');
        })
    };

    vm.user = {};
    vm.callback = function(invoice) {
      vm.detail.invoice = invoice.data;

      UserService.getCurrentUser(true).then(function(userData) {
        var id = userData.id || userData._id;
        vm.user = userData;
        console.log(vm.detail);
        UserService.getUserPaymentMethods(id, userData).then(function(paymentMethods) {
          vm.detail.invoice.card = _.filter(paymentMethods, function(paymentMethod) {
            return paymentMethod.id === vm.detail.invoice.paymentSource;
          });
          console.log(vm.detail);
        });
      });
    };

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', false);
      if (!$stateParams.id) {
        $state.go('app.contracts');
      }
    });

    MessageService.getContractById(id)
      .then(function(res) {
        if (!res) {
          $scope.error = {error: res};
          return;
        }

        _.extend(vm.detail, res);

        vm.detail.tax = {
          sales: 0.13
        };

        var id = vm.detail.coins[0].coin.offer;

        // $ionicLoading.show();
        MessageService.getOfferById(id).then(function(res) {
          vm.detail.offer = _.extend({}, res);
          vm.detail.offer.profile.photos.reverse();
          $scope.$broadcast('contract-loaded', vm.detail);
          $ionicLoading.hide();
        }, function(e) {
          console.log(e);
        });

      }, function(e) {
        $ionicLoading.hide();
        console.log(e);
      });
  }
})();
