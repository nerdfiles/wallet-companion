/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ContractTimelineController
 */
(function() {
  'use strict';

  String.prototype.getState = function() {
    var self = this || 'Pending';
    var state = {
      'Approved': 0
    };
    return state[self];
  };

  angular
    .module('WalletCompanion')
    .controller('ContractTimelineController', ContractTimelineController);

  ContractTimelineController.$inject = [
    '$scope',
    'ContractService',
    'UserService',
    '$ionicPopup',
    '$state'
  ];

  function ContractTimelineController($scope, ContractService, UserService, $ionicPopup, $state) {

    var vm = this;

    ContractService.getContracts().then(function(contracts) {

      vm.annotationSeries = _.map(contracts, function (contract, index) {
        var o = {
          x : new Date(moment(contract.createdAt).format('M-D-Y')).getTime(),
          y : contract.status.getState(),
        };
        return o;
      });

      vm.graphData = _.map(contracts, function (contract, index) {
        var o = {
          x : index,
          y : contract.status.getState(),
        };
        return o;
      });

      vm.timestamps = _.map(contracts, function (contract) {
        return {
          createdAt : moment(contract.createdAt).format('M-D-Y'),
          id        : contract._id,
          status    : contract.status
        };
      });
    });
  }
})();

