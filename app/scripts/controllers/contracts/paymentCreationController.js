/**
 * @ngdoc controller
 * @name WalletCompanion.controller:PaymentCreationController
 * @description Reservations POST implemenation.
 */

(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('PaymentCreationController', PaymentCreationController);

  PaymentCreationController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$ionicModal',
    '$ionicLoading',
    'MissiveService',
    'ReservationService',
    '$ionicPopup',
    '$stateParams',
    'MessageService',
    'ErrorService',
    'UserService',
    'ApiService',
    '$timeout'
  ];

  function PaymentCreationController($scope, $rootScope, $state, $ionicModal, $ionicLoading, MissiveService, ReservationService, $ionicPopup, $stateParams, MessageService, ErrorService, UserService, ApiService, $timeout) {

    var vm = this;
    vm.actionLabel = 'Continue';
    vm.inheritsFrom = $stateParams.name;
    vm.user = $stateParams.user || {};
    $scope.showingForm = undefined;

    vm.paymentMissive = {};
    vm.paymentMissive = MissiveService.getActive();

    if (vm.paymentMissive.flow === '' ||

      vm.paymentMissive.flow === 'createMessage'

    ) {

      vm.paymentMissive = MissiveService.setActive(true, 'createPaymentMethodForMessage', 'buyer', 0);
    }

    vm.sectionTitle = MissiveService.getTitle();
    vm.getInvoiceCallback = getInvoiceCallback;
    vm.submit = submit;
    vm.cancelReservation = cancelReservation;
    vm.attachCardToCheckout = attachCardToCheckout;
    vm.next = next;
    $scope.showingList = undefined;

    $rootScope.$on('resetForm', function($event, data) {
      $scope.showingForm = true;
      $scope.showingList = true;
    });

    $rootScope.$on('completeForm', function($event, data) {
      $scope.showingForm = undefined;
      $scope.showingList = undefined;
      $scope.completeForm = true;
      // $rootScope.$broadcast('hideBillingForm');
      $scope.$broadcast('hidePaymentForm');
    });

    $rootScope.$on('hideList', function($event, data) {
      $scope.showingList = false;
    });

    /**
     * @name next
     * @returns {*} undefined
     */
    function next() {
      if (vm.user && vm.user.billing) {
        UserService.addBillingInfo(vm.user);
      }
      $rootScope.$broadcast('hideBillingForm');
      MissiveService.next();
    }

    vm.previous = function() {
      if (vm.paymentMissive.currentStep === 0) {
        // $rootScope.$emit('swap', { reset: true });
        // UserService.removeCcInfo();
        // vm.user = {};
        UserService.removeCcInfo();
        $scope.$broadcast('resetBillingForm');
        MissiveService.setActive(true, 'createMessage', 'buyer', 2);
        $state.go('app.createMessage');
      } else {
        $rootScope.$emit('swap', { prev: true });
      }

    };

    $scope.$on('$ionicView.beforeEnter', ionicViewBeforeEnter);

    try {
      vm.steps = vm.paymentMissive.totalSteps;
    } catch(e) {
      console.log(e);
    }

    ////////////

    /**
     * @name getInvoiceCallback
     * @param {object} invoice Invoice object.
     * @returns {*} undefined
     */
    function getInvoiceCallback(invoice) {
      vm.reservation.invoice = invoice;
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:reservationCreationController
     * @name attachCardToCheckout
     * @param {object} card TBD
     * @returns {*} undefined
     */
    function attachCardToCheckout(card) {
      var selectedCard = {
        paymentMethod: card
      };
      $timeout(function() {
        ReservationService.updateCurrentMessage(selectedCard);
      }, 0);
      ReservationService.getCurrentMessage()
        .then(function(res) {
          MissiveService.setActive(true, 'createMessage', 'buyer', 2);
          $state.go('app.createMessage', { id: res.listingId }, { reload: true });
        }, function(e) {
          console.log(e);
        });

    }

    /**
     * cancelReservationProceedings
     * @returns {*} undefined
     */
    function cancelReservation() {
      $ionicPopup.show({
        title: '<i class="icon ionicons ion-ios-help-outline"></i>',
        subTitle: 'Do you want to cancel this current reservation? Cancelling will return you to the last coin views.',
        buttons: [
          {
            text: 'Cancel',
            onTap: function(e) {
            }
          },
          {
            text: 'Confirm',
            onTap: function(e) {
              ReservationService.cancelMessage();
              $state.go('app.currencies');
            }
          }
        ]
      });
    }

    /**
     * @name ionicViewBeforeEnter
     * @returns {*} undefined
     */
    function ionicViewBeforeEnter() {
      $scope.$emit('toggleNavbar', false);

      UserService.getCurrentUserForPayments(true).then(function(user) {
        if (vm.paymentMissive.currentStep === 1) {
          return;
        }

        _.extend(vm.user, user);

        if (!vm.user.paymentMethods || !vm.user.paymentMethods.list.length) {
          vm.user.paymentMethods = { list: [] };
        }

        var id = user.id || user._id;

        if (!id) {
          return console.log('User not loaded');
        }

        UserService.getUserPaymentMethods(id, user)
          .then(function(paymentData) {
            vm.user.paymentMethods.list = paymentData;
            vm.cards = vm.user.paymentMethods.list;
            console.log('vm: ', vm);
            $scope.$broadcast('user-loaded', user);
          }, function(e) {
            console.log(e);
          });
      }, function(e) {
        console.log(e);
      });

    }

    /**
     * @name submit
     * @returns {*} undefined
     */
    function submit() {
      // Consolidate or rename "session".
      var currentUser = UserService.getSession();
      var paymentInfo = {
        ccInfo: vm.user.ccInfo,
        billingInfo: currentUser.billing
      };
      $scope.$broadcast('addPaymentMethod:loaded', paymentInfo);
      $scope.$emit('userRole', vm.user.role);

      $rootScope.$emit('swap', { reset: true });
      UserService.removeCcInfo();
    }

  }
})();

