/**
 * @ngdoc controller
 * @name WalletCompanion.controller:SendController
 * @description # SendController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('SendController', SendController);

  SendController.$inject = [
    '$scope',
    'TransactionService',
    '$timeout',
    '$state',
    'FW',
    '$ionicPopup'
  ];

  /**
   * SendController
   *
   * @param $scope
   * @returns {undefined}
   */
  function SendController($scope, TransactionService, $timeout, $state, FW, $ionicPopup) {

    // ## Controller Init
    var vm = this;

    vm.message = {};

    // ## Controller Hypermedia Controls
    // ## Controller Defaults
    // ## Controller Capabilities
    vm.send = send;
    vm.previous = previous;

    // ## Controller Interaction Model
    $scope.$emit('toggleNavbar', false);

    // ## Controller Data

    $timeout(function() {
      try {
        vm.message.sender = '0x' + FW.WALLET_ENTITY.address;
      } catch(e) {
        console.log(e);
      } finally {
        $scope.$apply();
      }
    }, 4);

    ////////////

    // ## Controller Utilities

    /**
     * @name previous
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.balances');
    }

    /**
     * @name name
     * @returns {undefined}
     */
    function send($event, form, formData) {

      $event.preventDefault();

      var _config = {};

      _.extend(_config, formData);

      console.log( _config  );

      TransactionService
        .rawTransaction(_config)
        .then(sendCompleted, sendRejected)
        .catch(sendError);

      /**
       * @name sendCompleted
       * @returns {undefined}
       */
      function sendCompleted(res) {
        console.log(res);

      }

      /**
       * @name sendRejected
       * @returns {undefined}
       */
      function sendRejected(rej) {

        var _rej = rej.message.split('. ');
        var rejectionTitle = _rej[0];
        var rejectionCondition = _rej[1];
        var rejectionSuggestion = _rej[2];

        $ionicPopup.show({
          title: rejectionTitle,
          subTitle: [
            '<p>', rejectionCondition, '</p>',
            '<p>', rejectionSuggestion, '</p>'
          ].join(''),
          buttons: [{ text: 'Close' }]
        });
      }

      /**
       * @name sendError
       * @returns {undefined}
       */
      function sendError(err) {

        $ionicPopup.show({
          title: 'Error',
          subTitle: [
            '<p>', JSON.stringify(err), '</p>'
          ].join(''),
          buttons: [{ text: 'Close' }]
        });

      }
    }
  }
})();

