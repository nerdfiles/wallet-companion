
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:AddressController
 * @description # AddressController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('AddressController', AddressController);

  AddressController.$inject = [
    '$scope',
    'WalletService',
    '$state',
    'FW',
		'Buffer'
  ];

  function AddressController($scope, WalletService, $state, FW, Buffer) {

    // ## Controller Init
    var vm = this;

    // ## Controller Hypermedia Controls

    // ## Controller Defaults

    // ## Controller Capabilities

    // ## Controller Interaction Model
    vm.load = load;
    vm.viewBalances = viewBalances;

    // ## Controller Data

    ////////////

    // ## Controller Utilities

    function viewBalances() {

      $state.go('app.balances');

    }


    /**
     * @name load
     * @returns {undefined}
     */
    function load() {

      WalletService
        .authorize()
        .then(function(accountRef) {
          console.log('Account Ref: ', accountRef);
          vm.address = FW.WALLET_ENTITY.address;
          // vm.address = accountRef.address;
        }, function(rej) {
          console.log(rej);
        })
        .catch(function(err) {
          console.log(err);
        });

    }
  }
})();

