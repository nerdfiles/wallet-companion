/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ApprovalController
 * @description # ApprovalController
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('ApprovalController', ApprovalController)

  ApprovalController.$inject = ['$scope']

  function ApprovalController($scope) {
    // ## Controller Init
    var vm = this

    vm.init = init

    function init() {

      $scope.$on('loading:completed', function($event, $data) {

        console.log([
          'loading:completed',
          $data
        ].join(''))
      })

      vm.loading = true

      return function() {
        $scope.$broadcast('loading:completed')

      }
    }

    // ## Controller Hypermedia Controls
    // ## Controller Defaults
    // ## Controller Capabilities
    // ## Controller Interaction Model
    // ## Controller Data
    ////////////
    // ## Controller Utilities
  }
})()

