
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ImportKeyController
 * @description Part of the Onboarding flow, users are tagged with a given role
 * offer, which we will make use of throughout the app's features
 * presentation, behavior effects and interaction models.
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('ImportKeyController', ImportKeyController)

  ImportKeyController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    '$ionicPlatform',
    'MissiveService',
    'UserService',
    '$ionicHistory',
    '$ionicViewSwitcher'
  ]

  function ImportKeyController($scope, $state, $stateParams, $ionicPlatform, MissiveService, UserService, $ionicHistory, $ionicViewSwitcher) {

    // ## Controller Init

    var vm = this

    // ## Controller Hypermedia Controls

    vm.userType = $stateParams.userType

    // ## Controller SEO

    vm.sectionTitle = 'Import Key'

    // ## Controller Data

    // ## Controller Defaults

    // ## Controller Interaction Model

    $scope.$on('$stateChangeSuccess', function($event, state) {
      if ($event) {
        vm.user = {}
      }
    })

    // ## Controller Capabilities

    vm.submit = submit
    vm.skip = skip
    vm.previous = previous

    ////////////

    // ## Controller Utilities

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ImportKeyController
     * @name previous
     * @param userType
     * @param currentStep
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.login')
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ImportKeyController
     * @name submit
     * @returns {*} undefined
     */
    function submit() {
      $scope.$emit('userRole', vm.user.role)
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:ImportKeyController
     * @name skip
     * @returns {undefined}
     */
    function skip() {
      $state.go('app.balances')
    }
  }
})()

