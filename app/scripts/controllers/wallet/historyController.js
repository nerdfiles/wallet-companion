
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:HistoryController
 * @description # HistoryController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('HistoryController', HistoryController);

  HistoryController.$inject = [
    '$scope',
    'WalletService'
  ];

  function HistoryController($scope, WalletService) {

    // ## Controller Init
    var vm = this;

    // ## Controller Hypermedia Controls

    // ## Controller Defaults

    // ## Controller Capabilities

    // ## Controller Interaction Model
    vm.load = load;

    // ## Controller Data

    ////////////

    // ## Controller Utilities

    /**
     * @name load
     * @returns {undefined}
     */
    function load() {

      WalletService
        .authorize()
        .then(function(res) {
          console.log(res);

        }, function(rej) {
          console.log(rej);
        })
        .catch(function(err) {
          console.log(err);
        });

    }
  }
})();

