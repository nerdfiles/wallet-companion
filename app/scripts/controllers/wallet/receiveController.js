
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ReceiveController
 * @description # ReceiveController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ReceiveController', ReceiveController);

  ReceiveController.$inject = [
    '$scope',
    '$state',
    'cuid',
    'WalletService',
    '$ionicPopup'
  ];

  /**
   * ReceiveController
   *
   * @param $scope
   * @param $state
   * @returns {undefined}
   */
  function ReceiveController($scope, $state, cuid, WalletService, $ionicPopup) {

    // ## Controller Init

    var vm = this;
    vm.receiptsList = [];

    // ## Controller Hypermedia Controls

    // @see http://amundsen.com/hypermedia/hfactor/
    _.extend(vm, {

      // Support for adding semantic meaning to link elements using link
      // relations (e.g. HTML rel attribute).
      relations : {
        '/first-post': 'edit',
        '/previous-post': 'previous'
      },

      // Support for modifying control data for read requests (e.g. HTTP
      // Accept-* headers).
      reads     : {
        '/feed-action': {
          'accept': 'application-rss'
        }
      },

      // Support for modifying control data for update requests.
      updates   : {
        '/first-action': {
          'enctypes': [
            'application/json'
          ]
        }
      },

      // Support for indiciating the interface method for requests (e.g. HTTP
      // GET,POST,PUT,DELETE methods).
      methods   : {
        '/first-action': {
          'method': 'post'
        }
      }
    });

    _.extend(vm, {
      init      : init,
      contract  : contract,
      sync      : synchronize,
      rec       : recognize
    });

    // ## Controller Defaults

    // ## Controller Capabilities

    vm.previous = previous;

    // ## Controller Interaction Model

    $scope.$emit('toggleNavbar', false);

    // ## Controller Data

    vm.message = {
      address: undefined,
      name: undefined,
      baseCurrency: 'USD',
      refQuantity: 'BTC'
    };

    vm.configQrCodeDisplay = {
      'content' : undefined,
      'version' : '4',
      'level'   : 'Q',
      'size'    : '280'
    };

    ////////////

    // ## Controller Utilities

    /**
     * @see https://stackoverflow.com/questions/22429757/how-to-go-back-on-two-view-states-something-like-window-history-go-2#22441809
     */
    function previous() {

      $state.go('app.balances');

    }

    /**
     * @function init
     */
    function init(schemaObject) {

      WalletService.authorize()
        .then(function(res) {
          var address = res.address;
          vm.configQrCodeDisplay.content = address;
        }, function(rej) {
          console.log(rej);
        })
        .catch(function(err) {
          console.log(err);
        })
    }

    /**
     * @funtion contract
     */
    function contract(adjudicatedObject) {

    }

    /**
     * @funtion synchronize
     */
    function synchronize(temporalObject) {

    }

    /**
     * @funtion recognize
     */
    function recognize(packagedObject) {

    }
  }
})()

