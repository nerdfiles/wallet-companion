/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ExchangeController
 * @description # ExchangeController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('ExchangeController', ExchangeController);

  ExchangeController.$inject = ['$scope'];

  function ExchangeController($scope) {
    // ## Controller Init
    var vm = this;
    // ## Controller Hypermedia Controls
    // ## Controller Defaults
    // ## Controller Capabilities
    // ## Controller Interaction Model
    // ## Controller Data
    ////////////
    // ## Controller Utilities
  }
})();

