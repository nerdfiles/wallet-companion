/**
 * @ngdoc controller
 * @name WalletCompanion.controller:BalancesController
 * @description
 * Balances for Labor Value Prices (LVP) template which are collated and
 * correlated to Availability Confidence Intervals, which likely make sense
 * within the framework of ODRL Information Models[0].
 *
 * We likely can develop the idea of ODRL-ERC bindings which outlie a basic
 * epikernal operationing system which can encode various R-modules, prime ideals,
 * and other mathematical objects as basic information nomenclature features
 * of the formal input specification language[1].
 *
 * ## Error Handling
 *
 * Controllers SHOULD implement notifications, analytics, etc. specified for
 * the Releases Management schema loaded per environment. Keys should be
 * "inverted" as on-demand business objects within notifications, analytics
 * directives, etc.
 *
 * ---
 * [0]: https://www.w3.org/TR/odrl-model/
 * [1]:
 * @wireframe ./design.ignore/TelegrafMoneyBalances--Wireframe.png
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('BalancesController', BalancesController);

  BalancesController.$inject = [
    '$scope',
    'LedgerService',
    'TalkService',
    'WalletService',
    '$q',
    'web3',
    'cuid',
    '$timeout',
    'FW'
  ];

  function BalancesController($scope, LedgerService, TalkService, WalletService, $q, web3, cuid, $timeout, FW) {

    // ## Controller Init

    var vm = this;
    vm.balanceList = [];

    // ## Controller Hypermedia Controls
    // ## Controller Defaults (View Configurations)
    // ## Controller Capabilities (Operation Configurations)
    // ## Controller Interaction Model (Event Configurations)
    $scope.$emit('toggleNavbar', true);

    // ## Controller Data Model (Model Configurations)

    vm.hosts = [];

    //logging();
    activated();

    ////////////

    // ## Controller Utilities

    /**
     * @name logging
     * @returns {undefined}
     */
    function logging() {

      WalletService
        .witness('syncing', function(res) {
          console.log(res);
        })
        .then(function(res) {
          console.log(res);
        }, function(rej) {
          console.log(rej);
        })
        .catch(function(err) {
          console.log(err);
        });

    }

    /**
     * @ngdoc method
     * @memberOf
     * @name activated
     * @description
     * Authorization of basic data services for the controller.
     */
    function activated() {

      WalletService
        .authorize()
        .then(function(res) {

          console.log('Authorized: ', res);

          TalkService
            .getProvider()
            .then(getProviderCompleted, getProviderRejected)
            .catch(getProviderError);

          return res;
        })
        .catch(function(e) { console.log(e) })
        .then(function(authorizationRef) {

          var address = '0x' + FW.WALLET_ENTITY.address;

          WalletService
            .getBalance(address)
            .then(function(balanceRef) {

              _.extend(authorizationRef, balanceRef);

              var bal = authorizationRef.$balance;

              console.log('Balance: ', bal);

              $timeout(function() {
                vm.balanceList.push({
                  '$id'        : cuid(),
                  listAmount   : bal,
                  listCurrency : 'ether',
                  listPrice    : '', // @TODO all lists correspond to LVP (labor value prices).
                  urlAsset     : '',
                  altText      : ''
                });
                $scope.$apply();
              }, 4);

            }, function(err) {
              console.log({ error: err });
            });
        });

      // ## Default Behavior

      // @TODO cycle through all accounts and load balances per token
      // LedgerService.listAccounts()
      //   .then(function(res) {

          // var balanceList = [];

          // _.each(res.collection, function(accountRef) {
          //   balanceList.push(function() {

          //     var defer = $q.defer();
          //     var bal = web3.eth.getBalance(accountRef);

          //     if (bal) {
          //       defer.resolve(bal);
          //     } else {
          //       defer.reject({ error: '404' });
          //     }

          //     return defer.promise;
          //   })
          // });

          /*
          $q.all(balanceList).then(function(res) {
            console.log('Balance List: ', res);
            _.each(res, function(item) {
              vm.balanceList.concat(item);
            })
          });
          */

          /*
          LedgerService.firstAccountBalance()
            .then(function(res) {
              console.log(res)
            });
          */
        //});

      //////////

      /**
       * @function getProviderCompleted
       */
      function getProviderCompleted(res) {
        vm.hosts.push(res.item);
      }

      /**
       * @function getProviderRejected
       */
      function getProviderRejected(rej) {
        console.log(rej);
      }

      /**
       * @function getProviderError
       */
      function getProviderError(err) {
        console.log(err);

      }
    }

    // ## Model Details
    // ## Operation Details
    // ## View Details
    // ## Event Details
  }

})()

