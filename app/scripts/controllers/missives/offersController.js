/**
 * @ngdoc function
 * @name WalletCompanion.controller:MyOffersController
 * @description
 * # MyOffersController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('MyOffersController', MyOffersController);

  MyOffersController.$inject = [
    '$scope',
    '$ionicLoading',
    'UserService',
    'MessageService'
  ];

  /**
   * MyOffersController
   *
   * @param $scope
   * @param $ionicLoading
   * @param UserService
   * @param MessageService
   * @returns {undefined}
   */
  function MyOffersController($scope, $ionicLoading, UserService, MessageService) {

    var vm = this

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', true)
    });
  }

})();

