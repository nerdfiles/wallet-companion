
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:NewOfferCreationController
 * @description
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('NewOfferCreationController', NewOfferCreationController);

  NewOfferCreationController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$timeout',
    '$ionicModal',
    'ErrorService',
    'ReservationService',
    'MessageService',
    '$ionicPopup',
    'NgMap',
    'ApiService',
    'UserService',
    'resizeService',
    '$log',
    '$compile',
    '$ionicLoading',
    '$analytics',
    '$ionicHistory',
    'R',
    'moment',
		'$q'
  ];

  /**
   * NewOfferCreationController
   *
   * @param $scope
   * @param $rootScope
   * @param $state
   * @param $timeout
   * @param $ionicModal
   * @param ErrorService
   * @param ReservationService
   * @param MessageService
   * @param $ionicPopup
   * @param NgMap
   * @param ApiService
   * @param UserService
   * @param resizeService
   * @param $log
   * @param $compile
   * @param $ionicLoading
   * @param $analytics
   * @param $ionicHistory
   * @param R
   * @param moment
	 * @param $q
   * @returns {undefined}
   */
  function NewOfferCreationController($scope, $rootScope, $state, $timeout, $ionicModal, ErrorService, ReservationService, MessageService, $ionicPopup, NgMap, ApiService, UserService, resizeService, $log, $compile, $ionicLoading, $analytics, $ionicHistory, R, moment, $q) {

    var vm = this;

    vm.missive = {};
    vm.missive.profile = {};

    // try {
    //   var r = R.compose({});
    //   console.log(r);
    // } catch(e) {
    //   console.log(e);
    // }

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', false);
    });

    // ## Controller Capabilities

    vm.createMissive = function() {
      UserService.getCurrentUser(true).then((res) => {
        return createMissive(res);
      });
    };
    vm.previous = previous;
    vm.sectionTitle = 'New Offer';

    /////////////////

    /**
     * @name previous
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.myOffers');
    }

    /**
     * createMissive
     *
     * @returns {undefined}
     */
    function createMissive(user) {

			var defer = $q.defer();
      var obj = vm.missive;

      var _obj = {};

      _obj.profile = {
        title: obj.profile.title,
        description: obj.profile.description,
        subtitle: obj.profile.subtitle,
        offerType: obj.profile.offerType
      };

      _obj.profile.contacts = [
        {
          address   : '0x722931844f99378e500e0d57ba7915a3fb925a6b',
          name      : 'newbot',
          email     : 'newbot@example.com',
          phone     : '888 888 8888',
          isPrimary : true
        }
      ]

      _obj.assets = []

      _obj.geometry = {
        loc: {
          coordinates: [
            51.476852,
            -0.000500
          ]
        }
      }

      _obj.profile.location = {
        address    : '124 Street',
        country    : 'CA',
        state      : 'Ontario',
        city       : 'Toronto',
        postalCode : 'M5U R71'
      }

      _obj.coins = []

      var defaultStartDate = moment().subtract(7, 'd');
      var defaultEndDate = moment(defaultStartDate).add(14, 'd');

      var defaultCoin = {
        price: 50,
        quantity: 100,
        tokenType: 'erc20',
        coinType: 'sha256',
        payType: 'network',
        description: 'Test coin',
        availability: {
          from: defaultStartDate.format('MM-DD-YYYY'),
          to: defaultEndDate.format('MM-DD-YYYY'),
          checkIn: '15:00',
          checkOut: '11:00'
        }
      };

      _obj.coins.push(defaultCoin);

      ApiService
        .offers
        .create(_obj)
        .then(function(res) {

          console.log(res);
					defer.resolve({ item: res });

          $ionicPopup.show({
            title: 'Auction opens ' + defaultStartDate.format('MM-DD-YYYY'),
            subTitle: 'Check your balance or history for updates.',
            // subTitle: JSON.stringify(res),
            buttons: [
              {
                text: 'Close'
              }
            ]
          });

        }, function(err) {

          console.log(err);
					defer.reject({ error: err });
        })
        .catch(function(err) {

          console.log(err);
					defer.reject({ error: err });
        });

			return defer.promise;
    }
  }

})();

