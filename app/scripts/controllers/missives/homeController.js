/**
 * @ngdoc controller
 * @name WalletCompanion.controller:BaseController
 * @description Generally loads a primary data type list for the given role.
 * @see
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('HomeController', HomeController);

  HomeController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$cordovaGeolocation',
    '$ionicScrollDelegate',
    '$ionicModal',
    'ScriptLoader',
    'ReservationService',
    'MissiveService',
    'MessageService',
    '$stateParams',
    '$q',
    '$location',
    '_',
    '$timeout',
    '$ionicPopup',
    '$ionicPopover',
    'UserService',
    'ApiService',
    '$cordovaInAppBrowser',
    '$window',
    'NgMap',
    '$analytics',
    '$ionicLoading',
    'LedgerService',
    'FW',
    'web3',
    'bitcore'
  ];

  /**
   * HomeController
   *
   * @param $scope
   * @param $rootScope
   * @param $state
   * @param $cordovaGeolocation
   * @param $ionicScrollDelegate
   * @param $ionicModal
   * @param ScriptLoader
   * @param ReservationService
   * @param MissiveService
   * @param MessageService
   * @param $stateParams
   * @param $q
   * @param $location
   * @param _
   * @param $timeout
   * @param $ionicPopup
   * @param UserService
   * @param ApiService
   * @param $cordovaInAppBrowser
   * @param $window
   * @param NgMap
   * @param $analytics
   * @param $ionicLoading
   * @param LedgerService
   * @param FW
   * @param web3
   * @returns {undefined}
   */
  function HomeController($scope, $rootScope, $state, $cordovaGeolocation, $ionicScrollDelegate, $ionicModal, ScriptLoader, ReservationService, MissiveService, MessageService, $stateParams, $q, $location, _, $timeout, $ionicPopup, $ionicPopover, UserService, ApiService, $cordovaInAppBrowser, $window, NgMap, $analytics, $ionicLoading, LedgerService, FW, web3, bitcore) {

    var task;

    // ## Controller Init

    var now = moment();
    var vm = this;

    vm.qrcode = {
      'content' : '',
      'version' : '4',
      'level'   : 'Q',
      'size'    : '200'
    };

    // ## Controller Hypermedia

    vm.currentFilters = MessageService.getDefaultFilterOpts();
    vm.savedFilters = MessageService.getSavedFilter();
    vm.serializeFilters = MessageService.serializeFilters;
    vm.coinTypeSelected = false;
    vm.filterOpts = vm.currentFilters;

    // ## Controller Capabilities

    // $analytics.pageTrack('/currencies');
    $scope._ = _;
    vm.viewOffer = viewOffer;
    vm.selectScryptCoin = selectScryptCoin;
    vm.selectSha256Coin = selectSha256Coin;
    vm.openAffiliatePage = openAffiliatePage;
    vm.viewNewOffer = viewNewOffer;
    vm.takeSeat = takeSeat;

    // ## Controller Data

    vm.offers = [];
    vm.hotels = [];

    // ## Controller Defaults

    $scope.loading = true;
    $scope.data = {
    };

    $scope.options = {
      loop   : false,
      effect : 'fade',
      speed  : 500
    };

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data) {
      $scope.slider = data.slider
    });

    $scope.map = {
      markers : {}
    };

    // ## Controller Defaults

    var inAppBrowserRef;

    ApiService
      .offers
      .list()
      .then(function(res) {

        console.log(res.data);

        // ApiService.tickets.list().then(function(res) {
        //   console.log(res.data);
        // }, function(err) {
        //   console.log({ error: err });
        // }).catch(angular.noop);

        vm.offers = res.data;

        vm.promisedList = [];

        vm.offers.forEach(function(offerRef, index) {

          console.log('Offer: ', offerRef);

          var deferred = $q.defer();

          /**
           * offerLoaded
           *
           * @returns {undefined}
           */
          function offerLoaded() {

            ApiService
              .coins
              .list(offerRef._id)
              .then(function(res) {

                $timeout(function() {
                  offerRef.coins = res.data;
                  $scope.$apply();
                }, 4);
                console.log('OfferLoaded: ', res.data);
                deferred.resolve({
                  coins: res.data
                });
              });
          }

          offerLoaded();

          vm.promisedList.push(deferred.promise);
        });

        $q.all(vm.promisedList)
          .then(function(res) {
            console.log(res);
          });

    }, function(err) {

      console.log({ error: err });

    }).catch(angular.noop);

    // Capture user data on init of controller
    UserService
      .getCurrentUser(true)
      .then(function(user) {

        vm.user = user;

        vm.actionLabel = 'Create Offer';

        if (vm.currentOffer && vm.currentOffer._id) {
          vm.actionLabel = 'Update Offer';
        }

        if (vm.user && vm.user.role === 'buyer') {
          if (
            vm.currentOffer &&
            vm.currentOffer.isPlaceholder
          ) {
            vm.actionLabel = 'Request Seat';
          } else {
            vm.actionLabel = 'Book Now';
          }
        } else {
          vm.actionLabel = 'Book Now';
        }

      }, function(e) {
        console.log({ error: e });
      })

      // Catch 50x errors
      .catch(function(e) {
        console.log({ error: e });
      });

    // ## Controller Interaction Model

    $scope.$on('httpErrorMessage', function($event, data) {
      $scope.loading = false
      $ionicLoading.hide()
    });

    $scope.$on('$ionicView.beforeEnter', function() {

      // Some cryptocurrency workflow for setting up trust I suppose?
      var btc_privateKey = new bitcore.HDPrivateKey();
      // var btc_exported = btc_privateKey.toWIF();

      var retrieved = new bitcore.HDPrivateKey(btc_privateKey);
      var derived = btc_privateKey.derive("m/0");

      $scope.$emit('toggleNavbar', true)
      // $scope.$broadcast('loadOffers', { refresh: true });
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $scope.$broadcast('offerModalClosed')
      vm.viewOfferModal.hide()
    });

    $scope.$on('ready:failed', function($event, data) {
      // console.log('data failed: ', data)
      console.log('error_tag: ', data.tag);
      $scope.loading = false;
      $ionicLoading.hide();
    });

    $scope.$on('ready:data', function($event, data) {
      console.log('data loaded: ', data);
      $ionicLoading.hide();
      $scope.loading = false;
    });

    // $scope.$on('ready:dataHotels', function($event, data) {
    //   console.log('data loaded: ', data);
    //   $timeout(function() {
    //     vm.hotels = data.data;
    //     $scope.$apply();
    //   }, 4)
    //   $ionicLoading.hide();
    //   $scope.loading = false;
    // });

    $scope.$on('loading:start', function($event) {
      console.log('start loading');
      $scope.loading = true;
      $ionicLoading.show({
        templateUrl: 'templates/components/loading.html',
        duration: 60000
      });
    });

    // Load needful modals for use
    $ionicModal.fromTemplateUrl('templates/modals/view-offer.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
        // $analytics.eventTrack('offer:loaded', {
        //   category: 'offer',
        //   label: 'loaded'
        // });
        vm.viewOfferModal = modal;
      });

    $ionicModal.fromTemplateUrl('templates/modals/filter-offers.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
         vm.filterOffersModal = modal;
      });

    $ionicModal.fromTemplateUrl('templates/modals/calendar.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
        vm.calendarModal = modal;
        vm.calendarControl = {
          selectedDate: null,
          method: null,
          eventSource: ReservationService.getCalendarEvents(),
          onSelection: function() {
            var method = vm.calendarControl.method
            var val = vm.calendarControl.selectedDate

            if (
              vm.selectedFrom &&
              (
                method === 'from' &&
                moment(vm.selectedTo).isBefore(val)
              )
            ) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            if (
              vm.selectedTo &&
              (
                method === 'to' &&
                moment(vm.selectedFrom).isAfter(val)
              )
            ) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            if (moment(val).isBefore(now)) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            $rootScope.$broadcast('calendarSelection', val)
          },
          onViewTitleChanged: function(title) {
            vm.viewTitle = title
          }
        }
      });

    $scope.$on('$stateChangeSuccess', function() {
      var l = $location.$$path.split('/')
      if (l && l.length === 4) {
        $scope.$on('scriptsLoaded', function() {
          loadOfferView()
        })

      }
    })

    $rootScope.$on('calendarSelection', calendarSelection)

    $scope.$on('configError', configMessage);

    $scope.$on('displayTicket', function($event, data) {
      console.log(data);
      configDisplay($event);
    });

    /**
     * @function checkFormattedAddress
     * @param f
     * @returns {undefined}
     */
    function checkFormattedAddress(f) {
      if (typeof f !== 'string') {
        return false
      }
      var _f = f.split(' ')
      if (_f.length > 3) {
        return true
      } else {
        return false
      }
    }


    /**
     * @name showHelp
     * @param url
     * @returns {undefined}
     */
    function showHelp(url) {

      var options = [
        'location=yes',
        'toolbar=yes',
        'enableViewportScale=yes',
        'hidden=no'
      ].join(',')
      var ref
      var target
      target = "_blank"

      if (!$cordovaInAppBrowser) {
        ref = $window
        inAppBrowserRef = ref.open(url, target, options)
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack)
        inAppBrowserRef.addEventListener('loadstop', loadStopCallBack)
        inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack)
        inAppBrowserRef.addEventListener("exit", exitCallback)
      } else {
        ref = $cordovaInAppBrowser
        options = {
          location: 'yes',
          hardwareback: 'yes',
          toolbar: 'yes'
        }

        inAppBrowserRef = ref.open(url, target, options)
          .then(function(res) {
            console.log(res)
          }, function(e) {
            console.log(res)
          })
      }
    }

    /**
     * @name exitCallback
     * @returns {undefined}
     */
    function exitCallback() {
      inAppBrowserRef.close()

      inAppBrowserRef = undefined
    }

    /**
     * @name loadStartCallBack
     * @returns {undefined}
     */
    function loadStartCallBack() {
    }

    /**
     * @name loadStopCallBack
     * @returns {undefined}
     */
    function loadStopCallBack() {

      if (inAppBrowserRef !== undefined) {
        // inAppBrowserRef.insertCSS({ code: "body { font-size: 16px }" })
        inAppBrowserRef.show()
      }
    }

    /**
     * @name loadErrorCallBack
     * @param params
     * @returns {undefined}
     */
    function loadErrorCallBack(params) {
      var scriptErrorMesssage = "console.log('" + params.message + "')";
      inAppBrowserRef.executeScript({
        code: scriptErrorMesssage
      }, executeScriptCallBack)
      inAppBrowserRef.close()
      inAppBrowserRef = undefined
    }

    /**
     * @name executeScriptCallBack
     * @param params
     * @returns {undefined}
     */
    function executeScriptCallBack(params) {
      if (params[0] === null) {
        console.log(params)
      }
    }

    /**
     * @name openAffiliatePage
     * @returns {undefined}
     */
    function openAffiliatePage(offer) {
      console.log('Open Affiliate page: ', offer)
      var coins = offer.coins;
      var _coins = coins.scrypt.concat(coins.sha256);
      var selectedCoinRef = _.filter(_coins, function(coinRef) {
        return coinRef.selected;
      });

      showHelp(_.last(selectedCoinRef).fullReservationURL);
    }

    /**
     * @name submitAction
     * @returns {undefined}
     */
    vm.submitAction = function() {

      vm.viewOfferModal.hide();

      var offer = {
        selectedOffer: vm.currentOffer
      };

      ReservationService.updateCurrentReservation(offer);

      $state.go('app.createReservation', {
        id: vm.currentOffer._id
      }, { notify: true });
    };

    /**
     * @name hideOfferModal
     * @returns {undefined}
     */
    vm.hideOfferModal = function() {
      vm.viewOfferModal.hide();
      $scope.$broadcast('offerModalClosed');
      $state.go('app.currencies', { id: null }, { notify: false });
    };

    $scope.$on('$destroy', function() {
      try {
        if ($scope.displayRef) {
          $scope.displayRef.remove();
        }
      } catch(err) {
        console.log({ error: err });
      }
    });

    ////////////

    /**
     * takeSeat
     *
     * @returns {undefined}
     */
    function takeSeat() {
      $scope.displayRef.hide();
    }

    /**
     * configDisplay
     *
     * @param $event
     * @returns {undefined}
     */
    function configDisplay($event) {

      $ionicPopover.fromTemplateUrl('templates/popovers/display.html', {
        scope: $scope
      }).then(function(res) {
        $scope.displayRef = res;
      });

      $timeout(function() {
        $scope.displayRef.show($event);
      }, 4);

    }

    /**
     * @function configMessage
     * @param $event
     * @param config
     * @returns {undefined}
     */
    function configMessage($event, config) {
      $ionicPopup.show({
        title    : config.title,
        subTitle : config.subTitle,
        buttons  : [
          {
            text  : 'OK',
            onTap : function(e) {
            }
          }
        ]
      })
    }

    /**
     * @name calendarSelection
     * @param e
     * @param val
     * @returns {undefined}
     */
    function calendarSelection(e, val) {

      if (!vm.selectedFrom && !vm.selectedFrom) {
        vm.currentFilters.calendar.dates['from'] = moment().format('MMM D, YYYY').toString()
        vm.currentFilters.calendar.dates['to'] = moment()
          .add(1, 'days')
          .format('MMM D, YYYY').toString()
      } else {
        if (!vm.selectedFrom) {
          vm.currentFilters.calendar.dates['from'] = moment()
            .format('MMM D, YYYY').toString()
        }

        if (!vm.selectedTo) {
          vm.currentFilters.calendar.dates['to'] = moment(val)
            .add(1, 'days')
            .format('MMM D, YYYY').toString()
        }
      }

      val = moment(val).format('MMM D, YYYY')
      vm.currentFilters.calendar.dates[vm.calendarControl.method] = val.toString()
      vm.calendarModal.hide()

      vm.selectedFrom = moment(vm.currentFilters.calendar.dates.from).toString() || undefined
      vm.selectedTo = moment(vm.currentFilters.calendar.dates.to).add(1, 'days').toString() || undefined
    }

    /**
     * @name loadOfferView
     * @returns {undefined}
     */
    function loadOfferView() {

      var _id = $stateParams.id || null

      if (_id &&
        $state.current.name.indexOf('app.currencies') !== -1
      ) {

        MessageService.getOfferById(_id)
          .then(function(offer) {
            viewOffer(offer)
          }, function(e) {
            console.log(e)
          })
      }
    }

    /**
     * viewNewOffer
     *
     * @param offer
     * @returns {undefined}
     */
    function viewNewOffer($event, offer) {

      console.log(offer);

      var contractCoin = _.last(offer.coins);
      var _contractCoin = {
        coin: contractCoin._id,
        quantity: 1
      };
      var contract = {
        coins: [_contractCoin],
        dates: {
          from: moment().add(1, 'days').format('MM-DD-YYYY'),
          to: moment().add(2, 'days').format('MM-DD-YYYY')
        }
      };
      // var offerID = offer._id;

      ApiService
        .contracts
        .create(contract)
          .then(function(res) {
            console.log(res);

            vm.qrcode.content = res.data.cipher;

            $scope.$broadcast('displayTicket', {
              title: res.data.cipher,
              subtitle: 'this'
            });

          }, function(err) {
            console.log(err);

          }).catch(function(err) {
            console.log(err);

          });

    }

    /**
     * @name viewOffer
     * @param {object} offer A selected Offer.
     * @returns {*} undefined
     */
    function viewOffer(offer) {

      vm.coinTypeSelected = false

      vm.viewOfferModal.show()

      if (!offer) {
        return console.log('No offer found');
      }

      if (!offer.profile) {
        offer.profile = {
          photos: []
        };
      }

      // Coins are hotel ontology
      if (offer.coins) {
        offer.coins = _.extend({}, offer.coins);
      }

      vm.currentOffer = offer;

      var title = offer.title || undefined;

      var _profile = {};

      if (!title) {
        _profile.title = offer.name;
        _profile.subtitle = offer.name;
      }

      vm.currentProfile = title ? offer.profile : _profile;

      vm.mainPhoto = vm.currentProfile.photos;

      try {
        vm.currentProfile.photo = vm.mainPhoto.length ? vm.mainPhoto[0] : null;
      } catch(e) {
        console.log(e);
      }

      console.log('current offer: ', vm.currentOffer);
      console.log('current profile: ', vm.currentProfile);

      try {
        vm.offers = vm.currentOffer.coins
          ? vm.currentOffer.coins.scrypt.concat(vm.currentOffer.coins.sha256) : [];
        vm.offersForDateRangeDisplay = _.size(vm.offers) ? [_.last(vm.offers)] : [];
      } catch(e) {
        console.log(e);
      }

      vm.cachedYear = undefined;
      vm.cachedInitYear = undefined;

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {
          var initYear = moment(coin.availability.from).format('YYYY').toString();
          var year = moment(coin.availability.to).format('YYYY').toString();
          vm.cachedYear = (!vm.cachedYear || vm.cachedYear > parseInt(year, 10))
            ? parseInt(year, 10)
            : vm.cachedYear;
          vm.cachedInitYear = (!vm.cachedInitYear || vm.cachedInitYear < parseInt(initYear, 10))
            ? parseInt(initYear, 10)
            : vm.cachedInitYear;
        });

      vm.cachedDate = {};

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {

          var to = moment(coin.availability.to).format('MMM DD').toString();
          var from = moment(coin.availability.from).format('MMM DD').toString();

          vm.cachedDate.first = !vm.cachedDate.first
            || moment(coin.availability.from).isBefore(vm.cachedDate.first)
              ? from
              : vm.cachedDate.first;
          vm.cachedDate.last = !vm.cachedDate.last
            || moment(coin.availability.to).isBefore(vm.cachedDate.last)
              ? to
              : vm.cachedDate.last;
        });

      $ionicScrollDelegate.$getByHandle('modalContent').scrollTop(true);

      $scope.$broadcast('offerModalOpened', vm.currentOffer);

      $state.go('app.coins', { id: offer._id }, { notify: false });

      MissiveService.resetActiveObj('offer');

      ReservationService.removeCurrentReservation();

      var figure;
      var body;

      task = window.setInterval(function() {

        body = document.querySelector('body');

        angular.element(body).css({
          'pointer-events': 'unset'
        });

        figure = document.querySelectorAll('figure');

        figure = Array.prototype.slice.call(figure);

        if (_.isArray(figure) && figure && figure.length > 1) {

          console.log(figure);

          window.setTimeout(function() {

            var fig = angular.element(figure.pop()).find('img');

            if (fig && fig.length) {

              angular.element(fig).css({
                'object-fit' : 'scale-down',
                'transform'  : 'translate3d(-25%, 0px, 0px)'
              });

              angular.element(figure).remove();
            }
          }, 4);
        }

      }, 4);

      try {

        window.setTimeout(function() {

          var $element = document.querySelectorAll('.intense');

          $element = _.last(Array.prototype.slice.call($element));

          try {
            window.Intense($element, {
              invertInteractionDirection: true
            });
          } catch(e) {
            console.log(e);
          }

          angular.element(body).on('click', function() {
            angular.element(body).css({
              'pointer-events': 'none'
            });
          });
        }, 4);
      } catch(e) {
        console.log(e);
      }

      $timeout(function() {
        var $element = document.querySelector('.intense');
        $scope.$broadcast('tgm:intenseOffer', $element);
      }, 4);
    }

    /**
     * @name selectSha256Coin
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectSha256Coin(i) {

      $analytics.eventTrack('coins:loaded')

      _.each(vm.currentOffer.coins.sha256, function(coin) {
        coin.selected = false
      });

      vm.currentOffer.coins.sha256[i].selected = !vm.currentOffer.coins.sha256[i].selected;

      try {

        vm.currentOffer.coins.sha256[i].selected = vm.currentOffer.coins.sha256[i].selected;

      } catch(e) {
        console.log({ error: e });
      }

      vm.coinTypeSelected = true;
    }

    /**
     * @name selectScryptCoin
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectScryptCoin(i) {

      $analytics.eventTrack('coins:loaded')

      _.each(vm.currentOffer.coins.scrypt, function(coin) {
        coin.selected = false
      })

      vm.currentOffer.coins.scrypt[i].selected = !vm.currentOffer.coins.scrypt[i].selected

      try {

        vm.currentOffer.coins.scrypt[i].selected = vm.currentOffer.coins.scrypt[i].selected

      } catch(e) {
        console.log({ error: e });
      }

      vm.coinTypeSelected = true;
    }
  }
})()

