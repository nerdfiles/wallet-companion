/**
 * @ngdoc controller
 * @name WalletCompanion.controller:ApprovalController
 * @description # ApprovalController
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('AccommodationsController', AccommodationsController)

  AccommodationsController.$inject = ['$scope']

  function AccommodationsController($scope) {
    // ## Controller Init
    var vm = this
    // ## Controller Hypermedia Controls
    // ## Controller Defaults
    // ## Controller Capabilities
    // ## Controller Interaction Model
    // ## Controller Data
    ////////////
    // ## Controller Utilities
  }
})()

