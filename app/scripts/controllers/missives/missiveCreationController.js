
/**
 * @ngdoc function
 * @name WalletCompanion.controller:MissiveCreationController
 * @description
 * # MissiveCreationController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('MissiveCreationController', MissiveCreationController);

  MissiveCreationController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$timeout',
    '$ionicModal',
    'ErrorService',
    'MissiveService',
    'ReservationService',
    'MessageService',
    '$ionicPopup',
    'NgMap',
    'ApiService',
    'UserService',
    'resizeService',
    '$log',
    '$compile',
    '$ionicLoading',
    '$analytics'
  ];

  function MissiveCreationController($scope, $rootScope, $state, $timeout, $ionicModal, ErrorService, MissiveService, ReservationService, MessageService, $ionicPopup, NgMap, ApiService, UserService, resizeService, $log, $compile, $ionicLoading, $analytics) {

     var vm = this;

     var loaded = false;

     vm.listingMissive = {};
     vm.listingMissive = MissiveService.getActive();

     vm.uploadPhoto = uploadPhoto;

     $scope.$on('$destroy', function() {

       vm.assetsModal.remove();
       vm.coinManagementModal.remove();
       vm.viewOfferModal.remove();
       vm.calendarModal.remove();

       try {
         $rootScope.$off('httpErrorMissive');
       } catch(e) {
         console.log(e);
       }
     });

     $scope.$on('coins:loaded', function($event, coinsRef) {
       console.log('Coins:loaded: ', coinsRef);
       vm.coinsValid = coinsRef;
     });

     $scope.$on('httpErrorMissive', function($event, data) {

       var _data = data ? data : null;

       if (_data) {
         console.log(data);
       }

       $ionicLoading.hide();

       try {
         $analytics.eventTrack('api/v1/offers', {
           category: 'error',
           label: 'POST or customPUT [' + JSON.stringify(data) + ']'
         });
       } catch(e) {
         console.log(e);
       }

       $ionicPopup.show({
         title: 'HTTP Error',
         subTitle: 'There was an error in your request. Please try again.',
         buttons: [
           {
              text: 'OK',
              onTap: function(e) {
                $rootScope.$off('httpErrorMissive');
              }
            }
          ]
       });
     });

     $scope.getHelp = function() {

       // @TODO use hxnormalize and setup templateUrl prop instead.
       $ionicPopup.show({
         title: [
           '<span style="padding-top: 25px;" class="help--header">',
           '<i class="icon ionicons ion-help" style="color: #0092BC;"></i>',
           '<span class="contet">What\'s the offer for the taking?</span>',
           '</span>'
         ].join(''),
         template: [
           '<dl><dt>Title</dt><dd>Name of Open Offer</dd>',
           '<dt>Subtitle</dt><dd>Any constraints?</dd>',
           '<dt>Description</dt><dd>What are we looking at here?</dd></dl>',
           '<hr class="sep" />',
           '<style>.help--header { text-align: center; position: relative; display: block; padding-top: 50px; } .help--header .icon { display: block; width: 100%; text-align: center; left: 0; top: 15px; position: absolute; } .help--header .icon:after { content: " "; display: block; width: 50px; height: 50px; border-radius: 100px; border: 1px #0092BC solid; left: 50%; position: absolute; margin-left: -25px; margin-top: -37px; } .popup-head { padding-top: 25px; } .popup .ion-help:before { line-height: 0; font-size: 1.325rem; position: relative; top: -2px; color: #0092BC; text-align: center; } .popup .sep { width: calc(100% + 30px); padding-top: 10px; margin-left: -15px; border: none; border-bottom: 1px #eee solid; } .popup dl { margin-bottom: 0; } .popup dd { font-weight: 500; margin-bottom: 10px; color: #aaa; } .popup dt { font-variant: small-caps; text-transform: lowercase; font-size: 1.325rem; line-height: 1; font-weight: 500;} .popup-body { padding-bottom: 0; }</style>'
         ].join(''),
         buttons: [
           {
              text: 'Close',
              onTap: function(e) {
              }
            }
          ]
       });
     };


     /**
      * @name uploadPhoto
      * @returns {*} undefined
      */
     function uploadPhoto() {
       if (loaded === true) return;
       consolel.log('Photo upload unloaded');
     }

     $scope.$on('uploadPhoto', function($event, res) {

       var data = res.files;
       var image = res.image;
       var errorMissive;
       loaded = true;
       if (data && data.error) {

         if (data.error === 'file_type') {
           errorMissive = 'Try using a JPG instead.';
         } else {
           errorMissive = 'Your file may be too large.';
         }

         $ionicPopup.show({
           title: 'Photo Upload Error',
           subTitle: errorMissive,
           buttons: [
             {
                text: 'OK',
                onTap: function(e) {
                }
              }
            ]
         });
       }

       var photo = {
         type: 'offerImage',
         files: data,
         image: image
       };

       $ionicLoading.show();

       ApiService.assets.create(photo)
         .then(function(res) {
           $ionicLoading.hide();
           MessageService.addPhoto([res.data.asset]);
           vm.newOffer = MissiveService.getTempObj('offer');
           vm.newOffer.photos = [res.data.asset];
           // MissiveService.setTempObj(vm.newOffer, 'offer');
           $ionicLoading.hide();
           $ionicPopup.show({
             title: 'Photo Successfully Upload',
             subTitle: 'Resize and position, then drag and drop to finalize.',
             buttons: [
               {
                  text: 'OK',
                  onTap: function(e) {
                    $scope.$broadcast('photo-uploaded', vm.newOffer.photos);
                    $scope.$broadcast('photo-changed', vm.newOffer.photos);
                  }
                }
              ]
           });
         }, function(e) {
           $ionicLoading.hide();
         });
     });

     /**
      * resizeWidth
      *
      * @returns {undefined}
      */
     vm.resizeWidth = function() {
       var lastPhoto = _.last(vm.newOffer.photos);

       resizeService
         .resizeImage(lastPhoto.image.url, {
            width: vm.photoSubmissionForm.resizeWidth,
            sizeScale: 'ko'
         })
         .then(function(image) {
            var imageResized = document.querySelector('.upload-photo-staple');
            imageResized.src = image;
            $compile(imageResized)($scope);
         })
         .catch($log.error);
      }

      /**
       * resizeHeight
       *
       * @returns {undefined}
       */
      vm.resizeHeight = function() {
       var lastPhoto = _.last(vm.newOffer.photos);

       resizeService
         .resizeImage(lastPhoto.image.url, {
            height: vm.photoSubmissionForm.resizeHeight,
            sizeScale: 'ko'
         })
         .then(function(image) {
            var imageResized = document.querySelector('.upload-photo-staple');
            imageResized.src = image;
            $compile(imageResized)($scope);
         })
         .catch($log.error);

     }

     vm.hideOfferModal = function() {
       vm.viewOfferModal.hide();
       $scope.$broadcast('offerModalClosed');
       $scope.$broadcast('goBack');
     };

     vm.previous = function() {
       if (vm.listingMissive.currentStep === 0) {
         $state.go('app.myOffers');
       } else {
         $scope.$broadcast('goBack');
       }
     };

     vm.actionLabel = 'Create Offer';
     $scope.$on('offer:loaded', function($event, offer) {
       vm.offerDescription = offer.description;
       if (offer && offer._parentId) {
         vm.actionLabel = 'Update Offer';
       }
     });
     vm.currentMapLatLng = undefined;

     $scope.$emit('toggleNavbar', false);

      $scope.map = {
        defaults: {
          tileLayer: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png',
          maxZoom: 14,
          minZoom: 10,
          reuseTiles: true,
          unloadInvisibleTiles: true,
          zoomControl: false,
          zoomControlPosition: 'bottomleft'
        },
        markers : [],
        styles: [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels",
                "stylers": [
                    {
                        "hue": "#ffe500"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.attraction",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.business",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.place_of_worship",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "poi.school",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "transit.station.airport",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#9bdffb"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ],
        events: {
          map: {
            enable: ['context'],
            logic: 'emit'
          }
        },
        controls: {
          custom: [ L.control.zoom({
            position: 'topright'
          }) ]
        }
      };


     $scope.$on('location-loaded', function($event, data) {

       vm.currentMapLatLng = _.map({
         lat: data.geometry.location.lat(),
         lng: data.geometry.location.lng()
       }, function(item, key) {
         return item;
       });

       NgMap.getMap().then(function(map) {

         var styledMapType = new google.maps.StyledMapType($scope.map.styles, {
           name: 'Styled'
         });
         map.mapTypes.set('Styled', styledMapType);

         NgMap.getGeoLocation(data.formatted_address)
           .then(function (latlng) {

             map.setCenter(latlng);

             if (latlng && _.isFunction(latlng.lat)) {
               vm.currentMapLatLng = latlng.lat() + ',' + latlng.lng();
             }
             google.maps.event.trigger(map, "resize");
           });
       });
     });

     $ionicModal
       .fromTemplateUrl('templates/modals/assets.html', {
         scope: $scope,
         animation: 'slide-in-up'
       })
       .then(function(modal) {
          vm.assetsModal = modal;
       });

     $ionicModal
       .fromTemplateUrl('templates/modals/coin-management.html', {
         scope: $scope,
         animation: 'slide-in-up'
       })
       .then(function(modal) {
          vm.coinManagementModal = modal;
       });

     $ionicModal
       .fromTemplateUrl('templates/modals/view-offer.html', {
         scope: $scope,
         animation: 'slide-in-up'
       })
       .then(function(modal) {
          vm.viewOfferModal = modal;
       });

     /**
      * calendarModalView
      *
      * @param modal
      * @returns {undefined}
      */
     function calendarModalView(modal) {
       vm.calendarModal = modal;

       vm.calendarControl = {
         selectedDate: null,
         method: null,
         eventSource: ReservationService.getCalendarEvents(),
         onSelection: function(method) {
           $rootScope.$broadcast('calendarSelection', vm.calendarControl.selectedDate);
         },
         onViewTitleChanged: function(title) {
           vm.viewTitle = title;
         }
       };
     }

     $ionicModal
       .fromTemplateUrl('templates/modals/calendar.html', {
         scope: $scope,
         animation: 'slide-in-up'
       })
       .then(calendarModalView);

      /**
       * selectErc20Coin
       *
       * @param i
       * @returns {undefined}
       */
      vm.selectErc20Coin = function(i) {
        vm.currentOffer.coins.erc20[i].selected = !vm.currentOffer.coins.erc20[i].selected;
      };

      /**
       * selectErc721Coin
       *
       * @param i
       * @returns {undefined}
       */
      vm.selectErc721Coin = function(i) {
        vm.currentOffer.coins.erc721[i].selected = !vm.currentOffer.coins.erc721[i].selected;
      };

      /**
       * submitAction
       *
       * @param offer
       * @param profile
       * @returns {undefined}
       */
      vm.submitAction = function(offer, profile) {

        $ionicLoading.show();

        UserService.getCurrentUser(true).then(function(user) {

          var photo = MessageService.getPhoto();
          if (_.keys(photo).length) {
            profile.photo = [{ image: photo }];
          }

          profile.contacts
            && profile.contacts[0]
            && _.extend(profile.contacts[0], user.profile);

          if (typeof offer !== 'object' || typeof profile !== 'object') {
            return;
          }


          var formedObj = MessageService.getFormedObj(offer, profile);

          if (formedObj._id) {

            return MessageService.updateOffer(formedObj._id, formedObj)
              .then(function(res) {

                $ionicLoading.hide();
                MessageService.resetPhoto();

                if (res.error) {

                  $ionicPopup.show({
                    title: 'Offer Update',
                    subTitle: 'An error occurred while updating this offer.',
                    buttons: [
                      {
                        text: 'OK',
                        onTap: function(e) {
                          vm.viewOfferModal.hide();
                          $ionicLoading.hide();
                        }
                      }
                    ]
                  });

                  console.log('error: ', res);

                } else {

                  $ionicPopup.show({
                    title: 'Offer Update',
                    subTitle: 'Offer updated successfully!',
                    buttons: [
                      {
                        text: 'OK',
                        onTap: function(e) {
                          vm.viewOfferModal.hide();
                          $ionicLoading.hide();
                        }
                      }
                    ]
                  });

                  console.log('update offer result: ', res);
                }
              }, function(e) {

                MessageService.resetPhoto();
                $ionicLoading.hide();
                console.log(e);

                $scope.$broadcast('httpErrorMissive');
                $ionicPopup.show({
                  title: 'An error occurred',
                  subTitle: JSON.stringify(e),
                  buttons: [
                    {
                      text: 'Close'
                    },
                    {
                      text: 'View Offer Contracts',
                      onTap: function() { $state.go('app.myOffers'); }
                    }
                  ]
                });


              }).catch(function(e) {
                MessageService.resetPhoto();
                $ionicLoading.hide();
                ErrorService.error(e, 'info').then(function(res) {
                  console.log(res);

                  $scope.$broadcast('httpErrorMissive');
                  $ionicPopup.show({
                    title: 'An error occurred',
                    subTitle: JSON.stringify(res),
                    buttons: [
                      {
                        text: 'Close'
                      },
                      {
                        text: 'View Offer Contracts',
                        onTap: function() { $state.go('app.myOffers'); }
                      }
                    ]
                  });

                });
              });
              // }).finally(function() {
              //   MessageService.resetPhoto();
              //   MissiveService.resetActiveObj();
              //   $state.go('app.myOffers');
              // });
          }

          MessageService.createOffer(formedObj)
            .then(function(res) {

              MessageService.resetPhoto();
              $ionicLoading.hide();

              if (res.error) {

                $scope.$broadcast('httpErrorMissive');
                $ionicPopup.show({
                  title: 'Offer Creation',
                  subTitle: 'An error occurred while creating this offer.',
                  buttons: [
                    {
                      text: 'OK',
                      onTap: function(e) {
                        vm.viewOfferModal.hide();
                        $ionicLoading.hide();
                      }
                    }
                  ]
                });

                console.log('error: ', res);

              } else {

                $scope.$broadcast('httpErrorMissive');
                $ionicPopup.show({
                  title: 'Offer Creation',
                  subTitle: 'Offer created successfully!',
                  buttons: [
                    {
                      text: 'OK',
                      onTap: function(e) {
                        vm.viewOfferModal.hide();
                        $ionicLoading.hide();
                      }
                    }
                  ]
                });

                console.log('create offer result: ', res);
              }

            }, function(e) {
              $ionicLoading.hide();
              MessageService.resetPhoto();
              ErrorService.error(e, 'info').then(function(res) {
                console.log(res);

                $scope.$broadcast('httpErrorMissive');
                $ionicPopup.show({
                  title: 'An error occurred',
                  subTitle: JSON.stringify(res),
                  buttons: [
                    {
                      text: 'Close'
                    },
                    {
                      text: 'View Offer Contracts',
                      onTap: function() { $state.go('app.myOffers'); }
                    }
                  ]
                });

              });
            }).catch(function(e) {
              $ionicLoading.hide();
              MessageService.resetPhoto();
              ErrorService.error(e, 'info').then(function(res) {
                console.log(res);
                $scope.$broadcast('httpErrorMissive');

                $ionicPopup.show({
                  title: 'An error occurred',
                  subTitle: JSON.stringify(res),
                  buttons: [
                    {
                      text: 'Close'
                    },
                    {
                      text: 'View Offer Contracts',
                      onTap: function() { $state.go('app.myOffers'); }
                    }
                  ]
                });

              });
            });
        }, function() {
          $ionicLoading.hide();
          MessageService.resetPhoto();
          ErrorService.error(e, 'info').then(function(res) {
            console.log(res);
            $scope.$broadcast('httpErrorMissive');

            $ionicPopup.show({
              title: 'An error occurred',
              subTitle: JSON.stringify(res),
              buttons: [
                {
                  text: 'Close'
                },
                {
                  text: 'View Offer Contracts',
                  onTap: function() { $state.go('app.myOffers'); }
                }
              ]
            });
          });
        });
      };

      /**
       * showAssetsModal
       *
       * @returns {undefined}
       */
      vm.showAssetsModal = function() {
        var loadedAssets = vm.newOffer;
        $scope.$broadcast('deserializeAssets', { item: vm.newOffer });
      };

   }
})();
