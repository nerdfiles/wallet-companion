/**
 * @ngdoc controller
 * @name WalletCompanion.controller:CurrencyController
 * @description Generally loads a primary data type list for the given role.
 * @see
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('CurrencyController', CurrencyController)

  CurrencyController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$cordovaGeolocation',
    '$ionicScrollDelegate',
    '$ionicModal',
    'ScriptLoader',
    'ReservationService',
    'MissiveService',
    'MessageService',
    '$stateParams',
    '$q',
    '$location',
    '_',
    '$timeout',
    '$ionicPopup',
    'UserService',
    'ApiService',
    '$cordovaInAppBrowser',
    '$window',
    'NgMap',
    '$analytics',
    '$ionicLoading'
  ]

  function CurrencyController($scope, $rootScope, $state, $cordovaGeolocation, $ionicScrollDelegate, $ionicModal, ScriptLoader, ReservationService, MissiveService, MessageService, $stateParams, $q, $location, _, $timeout, $ionicPopup, UserService, ApiService, $cordovaInAppBrowser, $window, NgMap, $analytics, $ionicLoading) {

    var task

    // ## Controller Init

    var now = moment()
    var vm = this

    // ## Controller Hypermedia

    vm.currentFilters = MessageService.getDefaultFilterOpts()
    vm.savedFilters = MessageService.getSavedFilter()
    vm.serializeFilters = MessageService.serializeFilters
    vm.coinTypeSelected = false
    vm.filterOpts = vm.currentFilters

    // ## Controller Capabilities

    $analytics.pageTrack('/coins')
    $scope._ = _
    vm.viewOffer = viewOffer
    vm.selectErc20Coin = selectErc20Coin
    vm.selectErc721Coin = selectErc721Coin
    vm.selectFreshCoin = selectFreshCoin
    vm.openHotelPage = openHotelPage

    // ## Controller Data

    vm.offers = []
    vm.hotels = []

    // ## Controller Defaults

    $scope.loading = true
    $scope.data = {
    }

    $scope.options = {
      loop   : false,
      effect : 'fade',
      speed  : 500
    }

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data) {
      $scope.slider = data.slider
    })

    $scope.map = {
      markers : {}
    }

    // ## Controller Defaults

    var inAppBrowserRef

    UserService.getCurrentUser(true).then(function(user) {
      vm.user = user

      vm.actionLabel = 'Create Offer'

      if (vm.currentOffer && vm.currentOffer._id) {
        vm.actionLabel = 'Update Offer'
      }

      if (vm.user && vm.user.role === 'buyer') {
        if (vm.currentOffer && vm.currentOffer.isPlaceholder) {
          vm.actionLabel = 'Reserve Coin'
        } else {
          vm.actionLabel = 'Buy Now'
        }
      } else {
        // vm.actionLabel = vm.currentOffer && vm.currentOffer._id ? 'Update Offer' : 'Buy Now'
      }

    })

    // ## Controller Interaction Model

    $scope.$on('httpErrorMessage', function($event, data) {
      $scope.loading = false
      $ionicLoading.hide()
    })

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', true)
    })

    $scope.$on('$ionicView.beforeLeave', function() {
      $scope.$broadcast('offerModalClosed')
      vm.viewOfferModal.hide()
    })

    $scope.$on('ready:failed', function($event, data) {
      // console.log('data failed: ', data)
      console.log('error_tag: ', data.tag)
      $scope.loading = false
      $ionicLoading.hide()
    })

    $scope.$on('ready:data', function($event, data) {
      console.log('data loaded: ', data)
      $ionicLoading.hide()
      $scope.loading = false
    })

    $scope.$on('ready:dataHotels', function($event, data) {
      console.log('data loaded: ', data)
      $timeout(function() {
        vm.hotels = data.data
        $scope.$apply()
      }, 4)
      $ionicLoading.hide()
      $scope.loading = false
    })

    $scope.$on('loading:start', function($event) {
      console.log('start loading')
      $scope.loading = true
      $ionicLoading.show({
        templateUrl: 'templates/components/loading.html',
        duration: 60000
      })
    })

    // Load needful modals for use
    $ionicModal.fromTemplateUrl('templates/modals/view-offer.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
        $analytics.eventTrack('offer:loaded', {
          category: 'offer',
          label: 'loaded'
        })
        vm.viewOfferModal = modal
      })

    $ionicModal.fromTemplateUrl('templates/modals/filter-offers.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
         vm.filterOffersModal = modal
      })

    $ionicModal.fromTemplateUrl('templates/modals/calendar.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(function(modal) {
        vm.calendarModal = modal
        vm.calendarControl = {
          selectedDate: null,
          method: null,
          eventSource: ReservationService.getCalendarEvents(),
          onSelection: function() {
            var method = vm.calendarControl.method
            var val = vm.calendarControl.selectedDate

            if (
              vm.selectedFrom
              && (
                method === 'from'
                && moment(vm.selectedTo).isBefore(val)
              )
            ) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            if (
              vm.selectedTo
              && (
                method === 'to'
                && moment(vm.selectedFrom).isAfter(val)
              )
            ) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            if (moment(val).isBefore(now)) {
              return $scope.$broadcast('configError', {
                title: 'Invalid Date',
                subTitle: 'Invalid Date Selected'
              })
            }

            $rootScope.$broadcast('calendarSelection', val)
          },
          onViewTitleChanged: function(title) {
            vm.viewTitle = title
          }
        }
      })

    $scope.$on('$stateChangeSuccess', function() {
      var l = $location.$$path.split('/')
      if (l && l.length === 4) {
        $scope.$on('scriptsLoaded', function() {
          loadOfferView()
        })

      }
    })

    $rootScope.$on('calendarSelection', calendarSelection)

    $scope.$on('configError', configMessage)

    /**
     * @function checkFormattedAddress
     * @param f
     * @returns {undefined}
     */
    function checkFormattedAddress(f) {
      if (typeof f !== 'string') {
        return false
      }
      var _f = f.split(' ')
      if (_f.length > 3) {
        return true
      } else {
        return false
      }
    }


    /**
     * @name showHelp
     * @param url
     * @returns {undefined}
     */
    function showHelp(url) {

      var options = [
        'location=yes',
        'toolbar=yes',
        'enableViewportScale=yes',
        'hidden=no'
      ].join(',')
      var ref
      var target
      target = "_blank"

      if (!$cordovaInAppBrowser) {
        ref = $window
        inAppBrowserRef = ref.open(url, target, options)
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack)
        inAppBrowserRef.addEventListener('loadstop', loadStopCallBack)
        inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack)
        inAppBrowserRef.addEventListener("exit", exitCallback)
      } else {
        ref = $cordovaInAppBrowser
        options = {
          location: 'yes',
          hardwareback: 'yes',
          toolbar: 'yes'
        }

        inAppBrowserRef = ref.open(url, target, options)
          .then(function(res) {
            console.log(res)
          }, function(e) {
            console.log(res)
          })
      }
    }

    /**
     * @name exitCallback
     * @returns {undefined}
     */
    function exitCallback() {
      inAppBrowserRef.close()

      inAppBrowserRef = undefined
    }

    /**
     * @name loadStartCallBack
     * @returns {undefined}
     */
    function loadStartCallBack() {
    }

    /**
     * @name loadStopCallBack
     * @returns {undefined}
     */
    function loadStopCallBack() {

      if (inAppBrowserRef !== undefined) {
        // inAppBrowserRef.insertCSS({ code: "body { font-size: 16px }" })
        inAppBrowserRef.show()
      }
    }

    /**
     * @name loadErrorCallBack
     * @param params
     * @returns {undefined}
     */
    function loadErrorCallBack(params) {
      var scriptErrorMesssage = "console.log('" + params.message + "')"
      inAppBrowserRef.executeScript({
        code: scriptErrorMesssage
      }, executeScriptCallBack)
      inAppBrowserRef.close()
      inAppBrowserRef = undefined
    }

    /**
     * @name executeScriptCallBack
     * @param params
     * @returns {undefined}
     */
    function executeScriptCallBack(params) {
      console.log(params)
      if (params[0] === null) {
      }
    }

    /**
     * @name openHotelPage
     * @returns {undefined}
     */
    function openHotelPage(offer) {
      console.log('Open Hotel page: ', offer)
      var coins = offer.coins
      var _coins = coins.fresh.concat(coins.erc20)
      var selectedRoomRef = _.filter(_coins, function(roomRef) {
        return roomRef.selected
      })

      showHelp(_.last(selectedRoomRef).fullBookingURL)
    }

    /**
     * @name submitAction
     * @returns {undefined}
     */
    vm.submitAction = function() {

      vm.viewOfferModal.hide()

      var offer = {
        selectedOffer: vm.currentOffer
      }

      ReservationService.updateCurrentReservation(offer)

      $state.go('app.createReservation', {
        id: vm.currentOffer._id
      }, { notify: true })
    }

    /**
     * @name hideOfferModal
     * @returns {undefined}
     */
    vm.hideOfferModal = function() {
      vm.viewOfferModal.hide()
      $scope.$broadcast('offerModalClosed')
      $state.go('app.currencies', { id: null }, { notify: false })
    }

    ////////////

    /**
     * @function configMessage
     * @param $event
     * @param config
     * @returns {undefined}
     */
    function configMessage($event, config) {
      $ionicPopup.show({
        title    : config.title,
        subTitle : config.subTitle,
        buttons  : [
          {
            text  : 'OK',
            onTap : function(e) {
            }
          }
        ]
      })
    }

    /**
     * @name calendarSelection
     * @param e
     * @param val
     * @returns {undefined}
     */
    function calendarSelection(e, val) {

      if (!vm.selectedFrom && !vm.selectedFrom) {
        vm.currentFilters.calendar.dates['from'] = moment().format('MMM D, YYYY').toString()
        vm.currentFilters.calendar.dates['to'] = moment()
          .add(1, 'days')
          .format('MMM D, YYYY').toString()
      } else {
        if (!vm.selectedFrom) {
          vm.currentFilters.calendar.dates['from'] = moment()
            .format('MMM D, YYYY').toString()
        }

        if (!vm.selectedTo) {
          vm.currentFilters.calendar.dates['to'] = moment(val)
            .add(1, 'days')
            .format('MMM D, YYYY').toString()
        }
      }

      val = moment(val).format('MMM D, YYYY')
      vm.currentFilters.calendar.dates[vm.calendarControl.method] = val.toString()
      vm.calendarModal.hide()

      vm.selectedFrom = moment(vm.currentFilters.calendar.dates.from).toString() || undefined
      vm.selectedTo = moment(vm.currentFilters.calendar.dates.to).add(1, 'days').toString() || undefined
    }

    /**
     * @name loadOfferView
     * @returns {undefined}
     */
    function loadOfferView() {

      var _id = $stateParams.id || null

      if (_id &&
        $state.current.name.indexOf('app.currencies') !== -1
      ) {

        MessageService.getOfferById(_id)
          .then(function(offer) {
            viewOffer(offer)
          }, function(e) {
            console.log(e)
          })
      }
    }

    /**
     * @name viewOffer
     * @param {object} offer A selected Offer.
     * @returns {*} undefined
     */
    function viewOffer(offer) {

      vm.coinTypeSelected = false

      vm.viewOfferModal.show()

      if (!offer) {
        return
      }

      if (!offer.profile) {
        offer.profile = {
          photos: []
        }
      }

      // Rooms are hotel ontology
      if (offer.coins) {
        offer.coins = _.extend({}, offer.coins)
      }

      vm.currentOffer = offer

      var title = offer.title || undefined

      var _profile = {}

      if (!title) {
        _profile.title = offer.name
        _profile.subtitle = offer.name
      }

      vm.currentProfile = title ? offer.profile : _profile

      vm.mainPhoto = vm.currentProfile.photos

      try {
        vm.currentProfile.photo = vm.mainPhoto.length ? vm.mainPhoto[0] : null
      } catch(e) {
        console.log(e)
      }

      console.log('current offer: ', vm.currentOffer)
      console.log('current profile: ', vm.currentProfile)

      try {
        vm.offers = vm.currentOffer.coins
          ? vm.currentOffer.coins.erc20.concat(vm.currentOffer.coins.fresh) : []
        vm.offersForDateRangeDisplay = _.size(vm.offers) ? [_.last(vm.offers)] : []
      } catch(e) {
        console.log(e)
      }

      vm.cachedYear = undefined
      vm.cachedInitYear = undefined

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {
          var initYear = moment(coin.availability.from).format('YYYY').toString()
          var year = moment(coin.availability.to).format('YYYY').toString()
          vm.cachedYear = (!vm.cachedYear || vm.cachedYear > parseInt(year, 10))
            ? parseInt(year, 10)
            : vm.cachedYear
          vm.cachedInitYear = (!vm.cachedInitYear || vm.cachedInitYear < parseInt(initYear, 10))
            ? parseInt(initYear, 10)
            : vm.cachedInitYear
        })

      vm.cachedDate = {}

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {

          var to = moment(coin.availability.to).format('MMM DD').toString()
          var from = moment(coin.availability.from).format('MMM DD').toString()

          vm.cachedDate.first = !vm.cachedDate.first
            || moment(coin.availability.from).isBefore(vm.cachedDate.first)
              ? from
              : vm.cachedDate.first
          vm.cachedDate.last = !vm.cachedDate.last
            || moment(coin.availability.to).isBefore(vm.cachedDate.last)
              ? to
              : vm.cachedDate.last
        })

      $ionicScrollDelegate.$getByHandle('modalContent').scrollTop(true)

      $scope.$broadcast('offerModalOpened', vm.currentOffer)

      $state.go('app.currencies', { id: offer._id }, { notify: false }) 

      MissiveService.resetActiveObj('offer')

      ReservationService.removeCurrentReservation()

      var figure
      var body

      task = window.setInterval(function() {

        body = document.querySelector('body')

        angular.element(body).css({
          'pointer-events': 'unset'
        })

        figure = document.querySelectorAll('figure')

        figure = Array.prototype.slice.call(figure)

        if (_.isArray(figure) && figure && figure.length > 1) {

          console.log(figure)

          window.setTimeout(function() {

            var fig = angular.element(figure.pop()).find('img')

            if (fig && fig.length) {

              angular.element(fig).css({
                'object-fit': 'scale-down',
                'transform': 'translate3d(-25%, 0px, 0px)'
              })

              angular.element(figure).remove()
            }
          }, 4)
        }

      }, 4)

      try {
        window.setTimeout(function() {

          var $element = document.querySelectorAll('.intense')

          $element = _.last(Array.prototype.slice.call($element))

          try {
            Intense($element, { invertInteractionDirection: true })
          } catch(e) {
          }

          angular.element(body).on('click', function() {
            angular.element(body).css({
              'pointer-events': 'none'
            })
          })
        }, 4)
      } catch(e) {
        console.log(e)
      }

      var $element = document.querySelector('.intense')
      $scope.$broadcast('tgm:intenseOffer', $element)
    }

    /**
     * resetCoins
     *
     * @inner
     * @returns {undefined}
     */
    function resetCoins() {

      var coins

      try {
        coins = vm.currentOffer
            .coins.erc20.concat(vm.currentOffer.coins.fresh)
        _.each(coins, function(coin) {
          coin.selected = false
        })
      } catch(e) { 
        console.log(e)
      }
      try {
        coins = vm.currentOffer
            .coins.erc20.concat(vm.currentOffer.coins.fresh)
        _.each(coins, function(coin) {
          coin.selected = false
        })
      } catch(e) {
        console.log(e)
      }
    }

    /**
     * selectFreshCoin
     *
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectFreshCoin(i) {

      $analytics.eventTrack('coins:loaded')
      resetCoins()

      if (vm.currentOffer.coins && vm.currentOffer.coins.fresh) {
        _.each(vm.currentOffer.coins.fresh, function(coin) {
          coin.selected = false
        })
      }

      _.each(vm.currentOffer.coins.fresh, function(coin) {
        coin.selected = false
      })

      vm.currentOffer.coins.fresh[i].selected = !vm.currentOffer.coins.fresh[i].selected

      try {
        vm.currentOffer.coins.fresh[i].selected = vm.currentOffer.coins.fresh[i].selected
      } catch(e) {}
      vm.coinTypeSelected = true
    }

    /**
     * selectErc721Coin
     *
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectErc721Coin(i) {

      $analytics.eventTrack('coins:loaded')
      resetCoins()

      if (vm.currentOffer.coins && vm.currentOffer.coins.erc721) {
        _.each(vm.currentOffer.coins.erc721, function(coin) {
          coin.selected = false
        })
      }
      _.each(vm.currentOffer.coins.erc721, function(coin) {
        coin.selected = false
      })
      vm.currentOffer.coins.erc721[i].selected = !vm.currentOffer.coins.erc721[i].selected
      try {
        vm.currentOffer.coins.erc721[i].selected = vm.currentOffer.coins.erc721[i].selected
      } catch(e) {}
      vm.coinTypeSelected = true
    }

    /**
     * selectErc20Coin
     *
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectErc20Coin(i) {

      $analytics.eventTrack('coins:loaded')
      resetCoins()

      if (vm.currentOffer.coins && vm.currentOffer.coins.erc20) {
        _.each(vm.currentOffer.coins.erc20, function(coin) {
          coin.selected = false
        })
      }
      _.each(vm.currentOffer.coins.erc20, function(coin) {
        coin.selected = false
      })
      vm.currentOffer.coins.erc20[i].selected = !vm.currentOffer.coins.erc20[i].selected
      try {
        vm.currentOffer.coins.erc20[i].selected = vm.currentOffer.coins.erc20[i].selected
      } catch(e) {}
      vm.coinTypeSelected = true
    }
  }
})()

