/**
 * @ngdoc function
 * @name WalletCompanion.controller:LoginController
 * @description
 * # LoginController
 */
(function() {

  'use strict';

  angular
    .module('WalletCompanion')
    .controller('LoginController', LoginController);

  LoginController.$inject = [
    '$scope',
    '$state',
    '$window',
    '$ionicModal',
    '$cordovaInAppBrowser',
    'UserService',
    '$ionicPopup',
    '$ionicLoading',
    '$stateParams',
    '$timeout',
    '$ionicViewSwitcher',
    '$ionicHistory',
    'Random'
  ];

  /**
   * LoginController
   *
   * @param $scope
   * @param $state
   * @param $window
   * @param $ionicModal
   * @param $cordovaInAppBrowser
   * @param UserService
   * @param $ionicPopup
   * @param $ionicLoading
   * @param $stateParams
   * @param $timeout
   * @param $ionicViewSwitcher
   * @param $ionicHistory
   * @returns {undefined}
   */
  function LoginController($scope, $state, $window, $ionicModal, $cordovaInAppBrowser, UserService, $ionicPopup, $ionicLoading, $stateParams, $timeout, $ionicViewSwitcher, $ionicHistory, Random) {

    // ## Controller Init

    var vm = this;

    // ## Controller Hypermedia

    vm.initial = $stateParams.initial;

    // ## Controller Data

    vm.user = UserService.getSession();

    // ## Controller Defaults

    var loaded;
    vm.status = '';
    $scope.res = {};
    $scope.errors = [];

    $ionicModal
      .fromTemplateUrl('templates/modals/login.html', {
        scope: $scope,
        animation: 'slide-in-up'
      })
      .then( function(modal) {
        vm.loginModal = modal;
        $scope.$broadcast('login-loaded');
      });

    // ## Controller Interaction Model

    $scope.$on("$ionicView.enter", function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
      UserService.deleteLocalSession()
        .then(function() {
          console.log('Clearing tos setting');
        }, function() {
          console.log('Clearing tos setting');
        });
    });

    $scope.$emit('toggleNavbar', false);

    $scope.$on('httpErrorMessage', function($event, data) {
      $scope.errors.push(data);
    });

    $scope.$on('login-loaded', loginLoaded);

    vm.handler = {

      tos: function() {
        $ionicViewSwitcher.nextDirection('forward')
        vm.status = 'create'
        vm.loginModal.hide()
        $state.go('app.tos', { intro: true }, {
          notify: true,
          reload: true,
          inherit: true
        })
      },

      importKey: function() {
        $ionicViewSwitcher.nextDirection('forward')
        vm.status = 'import'
        vm.loginModal.hide()
        $state.go('app.importKey', { intro: false }, {
          notify: true,
          reload: true,
          inherit: true
        })
      },

      createAccount: function() {

        $ionicViewSwitcher.nextDirection('forward')

        $timeout(function() {
          vm.status = 'create'
          vm.loginModal.hide()

        }, 400);


        $state.go('app.tos', { intro: false }, {
          notify: true,
          reload: true,
          inherit: true
        });
      },

      logIn: function() {
        vm.status = 'login';
        vm.loginModal.hide();
      },

      skip: function() {
        $state.go('app.coins', {}, {
          notify: true,
          reload: true,
          inherit: true
        });

        vm.loginModal.hide();
      },

      status: function(status) {
        return (vm.status === status)
      },

      list: function() {
        $state.go('app.history', {}, {
          notify: true,
          reload: true,
          inherit: true
        });
        vm.loginModal.hide();
      }
    };

    ////////////

    /**
     * @function loginLoaded
     * @param $event
     * @returns {undefined}
     */
    function loginLoaded($event) {

      if (loaded) {
        vm.status = 'login';
        $ionicLoading.hide();
        vm.loginModal.hide();
        return;
      }

      if (vm.initial && vm.initial.previous) {
        vm.status = 'login'
        $ionicLoading.hide();
        vm.loginModal.hide();
        // vm.loginModal.show();
        return;
      }

      UserService
        .getCurrentUser(true)
        .then(function(userData) {

          if (userData &&
            !_.isUndefined(userData.verification) &&
            userData.verification.hasAcceptedTerms
          ) {

            // vm.status = 'create'
            // $ionicLoading.hide()
            // vm.loginModal.hide()

            vm.status = 'login'
            $ionicLoading.hide()
            vm.loginModal.show()

          } else {

            vm.status = 'login'
            $ionicLoading.hide()
            vm.loginModal.show()
          }

          loaded = true

        }, function(e) {

          vm.status = 'login'
          $ionicLoading.hide()
          vm.loginModal.show()
          loaded = true
        })
    }
  }

})();

