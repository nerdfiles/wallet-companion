
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:AccountCreationController
 * @description Part of the Onboarding flow, users are tagged with a given role
 * offer, which we will make use of throughout the app's features
 * presentation, behavior effects and interaction models.
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('AccountCreationController', AccountCreationController)

  AccountCreationController.$inject = [
    '$scope',
    '$state',
    '$stateParams',
    '$ionicPlatform',
    'MissiveService',
    'UserService',
    '$ionicHistory',
    '$ionicViewSwitcher'
  ]

  function AccountCreationController($scope, $state, $stateParams, $ionicPlatform, MissiveService, UserService, $ionicHistory, $ionicViewSwitcher) {

    // ## Controller Init

    var vm = this

    // ## Controller Hypermedia Controls

    vm.userType = $stateParams.userType

    // ## Controller SEO

    vm.sectionTitle = MissiveService.getTitle()

    // ## Controller Data

    // ## Controller Defaults

    vm.accountFlow = {}
    vm.accountFlow = MissiveService.getActive()

    if (vm.accountFlow.flow === '' && vm.userType) {
      vm.accountFlow = MissiveService.setActive(true, 'createUser', vm.userType, 0)
    }

    // ## Controller Interaction Model

    $scope.$on('$stateChangeSuccess', function($event, state) {
      if ($event) {
        vm.user = {}
      }
    })

    // ## Controller Capabilities

    vm.submit = submit
    vm.skip = skip
    vm.previous = previous

    ////////////

    // ## Controller Utilities

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:AccountCreationController
     * @name previous
     * @param userType
     * @param currentStep
     * @returns {undefined}
     */
    function previous(userType, currentStep) {
      if (currentStep === 0) {
        $ionicViewSwitcher.nextDirection('back');
        vm.user = {};
        $state.go('app.login', { initial: 'reset' });
      } else if (currentStep === 1) {
        vm.accountFlow = MissiveService.setActive(true, 'createUser', userType, 0);
      }
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:AccountCreationController
     * @name submit
     * @returns {*} undefined
     */
    function submit() {
      var paymentInfo = {
        ccInfo: vm.user.ccInfo,
        billingInfo: vm.user.billing
      }
      $scope.$broadcast('addPaymentMethod:loaded', paymentInfo)
      $scope.$emit('userRole', vm.user.role)
    }

    /**
     * @ngdoc method
     * @methodOf WalletCompanion.controller:AccountCreationController
     * @name skip
     * @returns {undefined}
     */
    function skip() {
      MissiveService.resetActiveObj('user')
      $state.go('app.coins')
    }
  }
})()

