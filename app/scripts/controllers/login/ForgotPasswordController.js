/**
 * @ngdoc function
 * @name WalletCompanion.controller:ForgotPasswordController
 * @description
 * # ForgotPasswordController
 */
(function() {
  'use strict';
  angular.module('WalletCompanion')
    .controller('ForgotPasswordController', ForgotPasswordController);

  ForgotPasswordController.$inject = [
    '$scope',
    '$state',
    '$window',
    '$ionicModal',
    '$cordovaInAppBrowser',
    'UserService',
    '$ionicPopup',
    '$ionicLoading',
    '$ionicHistory',
    'ErrorService',
    '$rootScope',
    '$timeout'
  ];

  function ForgotPasswordController($scope, $state, $window, $ionicModal, $cordovaInAppBrowser, UserService, $ionicPopup, $ionicLoading, $ionicHistory, ErrorService, $rootScope, $timeout) {

    var vm = this;
    vm.status = '';
    vm.user = UserService.getSession();
    vm.submit = submit
    vm.previous = previous;

    vm.res = {};
    vm.errors = [];
    vm.sectionTitle = 'Recovery';

    $scope.$emit('toggleNavbar', false);

    $scope.$on('configMessage', configMessage);

    function configMessage($event, config) {
      $ionicPopup.show({
        title: config.title,
        subTitle: config.subTitle,
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
            }
          }
        ]
      });
    }

    $rootScope.$on('httpErrorMessage', function($event, data) {
      vm.errors.loaded = true;
      vm.errors.push(data);
      $timeout(function() { vm.errors.length = 0; }, 3000);
      var title;
      var message;
      title = 'Forgot Password';
      message = 'Error in sending your message';
      $scope.$broadcast('configMessage', {
        title: title,
        subTitle: message
      });
    });

    $scope.$on('login-loaded', function($event) {

      $window.localStorage.clear();

      UserService.getCurrentUser(true).then(function(userData) {
        if (userData &&
          !_.isUndefined(userData.verification) &&
          userData.verification.hasAcceptedTerms
        ) {
          vm.status = 'create';
          $ionicLoading.hide();
        } else {
          vm.status = 'login';
          $ionicLoading.hide();
        }
      }, function(e) {
        vm.status = 'login';
        $ionicLoading.hide();
      });
    });

    /**
     * submit
     *
     * @returns {undefined}
     */
    function submit() {

      var user = vm.user;

      UserService.forgot(user).then(function(res) {
        var res = {};
        var outcome;
        var title;
        title = 'Forgot Password';
        var message;
        res.loaded = true;
        try {
          outcome = res.data;
          message = 'Sent';
        } catch(e) {
          outcome = res || e;
          message = 'Failed';
        } finally {
          vm.res = outcome;
          $scope.$broadcast('configMessage', {
            title: title,
            subTitle: message
          });
        }
      }, angular.noop);
    }

    /**
     * previous
     *
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.login', { initial: { previous: true } });
    }
  }
})();
