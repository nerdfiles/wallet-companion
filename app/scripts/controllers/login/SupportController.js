/**
 * @ngdoc function
 * @name WalletCompanion.controller:SupportController
 * @description
 * # SupportController
 */
(function() {
  'use strict';
  angular.module('WalletCompanion')
    .controller('SupportController', SupportController);

  SupportController.$inject = [
    '$scope',
    '$state',
    '$window',
    '$ionicModal',
    '$cordovaInAppBrowser',
    'UserService',
    '$ionicPopup',
    '$ionicLoading',
    '$ionicHistory',
    'ErrorService',
    '$rootScope',
    '$timeout',
    'cuid'
  ];

  function SupportController($scope, $state, $window, $ionicModal, $cordovaInAppBrowser, UserService, $ionicPopup, $ionicLoading, $ionicHistory, ErrorService, $rootScope, $timeout, cuid) {

    var vm = this;
    vm.status = '';
    vm.user = UserService.getSession();
    vm.submit = submit
    vm.previous = previous;

    vm.res = {};
    vm.errors = [];
    vm.sectionTitle = 'Support';

    $scope.$emit('toggleNavbar', false);

    $rootScope.$on('httpErrorMessage', function($event, data) {
      vm.errors.loaded = true;
      vm.errors.push(data);
      $timeout(function() { vm.errors.length = 0; }, 3000);
    });

    $scope.$on('login-loaded', function($event) {

      $window.localStorage.clear();

      UserService.getCurrentUser(true).then(function(userData) {
        if (userData &&
          !_.isUndefined(userData.verification) &&
          userData.verification.hasAcceptedTerms
        ) {
          vm.status = 'create';
          $ionicLoading.hide();
        } else {
          vm.status = 'login';
          $ionicLoading.hide();
        }
      }, function(e) {
        vm.status = 'login';
        $ionicLoading.hide();
      });
    });

    $scope.$on('configMessage', configMessage);

    /**
     * configMessage
     *
     * @param $event
     * @param config
     * @returns {undefined}
     */
    function configMessage($event, config) {
      $ionicPopup.show({
        title: config.title,
        subTitle: config.subTitle,
        buttons: [
          {
            text: 'OK',
            onTap: function(e) {
            }
          }
        ]
      });
    }

    /**
     * submit
     *
     * @returns {undefined}
     */
    function submit() {

      var feedback = vm.feedback;
      feedback.subject = [
        'Support Request',
        '[' + cuid() + ']'
      ].join(' ')

      function mf() {
        $scope.$broadcast('configMessage', {
          title: 'Message failed to send',
          subTitle: 'Try again later!'
        });
      }

      function ms() {
        $scope.$broadcast('configMessage', {
          title: 'Support Request Sent',
          subTitle: 'Thanks!'
        });
      }

      UserService
        .support(feedback)
        .then(function(res) {
          var outcome = {};
          try {
            outcome.loaded = true;
            outcome = res.data;
            ms();
          } catch(e) {
            outcome.loaded = false;
            outcome = res || e;
            mf();
          } finally {
            vm.res = outcome;
            vm.supportForm.$setPristine(true);
          }
        }, function(e) {
          vm.errors.loaded = true;
          vm.errors.push({
            message: 'There was an error in sending your message. Please try again later.'
          });
        });

    }

    /**
     * previous
     *
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.settings', { initial: { previous: true } });
    }
  }
})();
