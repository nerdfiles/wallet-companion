/* jshint asi:true, */
/* global google:true */
/**
 * @ngdoc controller
 * @name WalletCompanion.controller:MapViewController
 * @description BPC on maps.
 */
(function() {

  'use strict'

  angular
    .module('WalletCompanion')
    .controller('MapViewController', MapViewController)

  MapViewController.$inject = [
    '$scope',
    '$rootScope',
    '$state',
    '$cordovaGeolocation',
    '$ionicScrollDelegate',
    '$ionicModal',
    'ScriptLoader',
    'ReservationService',
    'MissiveService',
    'MessageService',
    '$stateParams',
    '$q',
    '$location',
    '_',
    '$timeout',
    '$ionicPopup',
    'UserService',
    'ApiService',
    '$cordovaInAppBrowser',
    '$window',
    '$ionicHistory',
    'NgMap',
    'mapStyles',
    '$log',
    '$ionicLoading',
    'uuid4'
  ]

  function MapViewController($scope, $rootScope, $state, $cordovaGeolocation, $ionicScrollDelegate, $ionicModal, ScriptLoader, ReservationService, MissiveService, MessageService, $stateParams, $q, $location, _, $timeout, $ionicPopup, UserService, ApiService, $cordovaInAppBrowser, $window, $ionicHistory, NgMap, mapStyles, $log, $ionicLoading, uuid4) {

    // ## Controller Init

    var vm = this
    var now = moment()

    // ## Controller Hypermedia Controls

    var inAppBrowserRef

    // Load needful modals for use

    $ionicModal.fromTemplateUrl('templates/modals/view-offer.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then( function(modal) {
        vm.viewOfferModal = modal
      })

    $ionicModal.fromTemplateUrl('templates/modals/filter-offers.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then( function(modal) {
         vm.filterOffersModal = modal
      })

    $ionicModal.fromTemplateUrl('templates/modals/calendar.html', {
      scope: $scope,
      animation: 'slide-in-up'
    })
      .then(calendarModalInit)

    // ## Controller Defaults

    vm.currentFilters = MessageService.getDefaultFilterOpts()
    vm.serializeFilters = MessageService.serializeFilters
    vm.coinTypeSelected = false
    vm.filterOpts = vm.currentFilters

    // ## Controller Capabilities

    $scope._ = _
    $scope.$log = $log
    vm.previous = previous
    vm.viewOffer = viewOffer
    vm.selectScryptCoin = selectScryptCoin
    vm.selectSha256Coin = selectSha256Coin;
    vm.submitAction = submitAction;
    vm.hideOfferModal = hideOfferModal;

    // ## Controller Interaction Model

    $scope.$on('httpErrorMessage', function($event, data) {
      $scope.loading = false;
      $ionicLoading.hide();

      $ionicPopup.show({
        title: 'HTTP Error',
        subTitle: 'There was an error in your request. Please try again.',
        buttons: [
          {
             text: 'OK',
             onTap: function(e) {
             }
           }
         ]
      });
    });

    $scope.$on('$ionicView.beforeEnter', function() {
      $scope.$emit('toggleNavbar', true)
    });

    $scope.$on('$ionicView.beforeLeave', function() {
      $scope.$broadcast('offerModalClosed')
      vm.viewOfferModal.hide()
      $ionicLoading.hide()
    });

    $scope.$on('ready:failed', function($event, data) {
      console.log('data failed: ', data)
      $scope.loading = false
      $ionicLoading.hide()
    });

    $scope.$on('ready:data', function($event, data) {
      console.log('data loaded: ', data)
      $ionicLoading.hide()
      $scope.loading = false
    });

    $scope.$on('loading:start', function($event) {
      console.log('start loading')
      $scope.loading = true
      $ionicLoading.show({
        templateUrl: 'templates/components/loading.html'
      });
    });


    $scope.$on('$stateChangeSuccess', function() {
      var l = $location.$$path.split('/')
      if (l && l.length === 4) {
        $scope.$on('scriptsLoaded', function() {
          loadOfferView()
        })

      }
    })

    $rootScope.$on('calendarSelection', calendarSelection)

    $scope.$on('configError', configMessage)

    // ## Controller Data

    vm.offers = []

    // Much of this is Leaflet configuration, and should be ignored.
    $scope.map = {
      defaults: {
        tileLayer: 'https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_nolabels/{z}/{x}/{y}.png',
        maxZoom: 14,
        minZoom: 10,
        reuseTiles: true,
        unloadInvisibleTiles: true,
        zoomControl: false,
        zoomControlPosition: 'bottomleft'
      },
      markers : [],
      styles: mapStyles,
      events: {
        map: {
          enable: ['context'],
          logic: 'emit'
        }
      },
      controls: {
        custom: []
      }
    }

    angular.extend($scope.map, {
      center: {
        lat  : 39.5,
        lng  : -98.35,
      },
      zoom : 3
    })

    // Initialize geolocation metadata
    $cordovaGeolocation.getCurrentPosition()
      .then(function(pos) {
        angular.extend($scope.map, {
          center: {
            lat  : pos.coords.latitude,
            lng  : pos.coords.longtitude,
            zoom : 3
          }
        })
      }, function(e) {
        console.log(e)

        angular.extend($scope.map, {
          center: {
            lat  : 39.5,
            lng  : -98.35,
            zoom : 3
          }
        })
      })
      .catch(function(e) {

        angular.extend($scope.map, {
          center: {
            lat  : 39.5,
            lng  : -98.35,
            zoom : 3
          }
        })
      })

    // Initialize map styles
    NgMap
      .getMap("mapViewContainer")
      .then(function(map) {
        var styledMapType = new google.maps.StyledMapType($scope.map.styles, {
          name: 'Styled'
        });
        map.mapTypes.set('Styled', styledMapType)
      });

    // Initialize user settings and metadata
    UserService.getCurrentUser(true).then(function(user) {
      vm.user = user
      vm.actionLabel = 'Create Offer'
      if (vm.currentOffer && vm.currentOffer._id) {
        vm.actionLabel = 'Update Offer'
      }
      if (vm.user && vm.user.role === 'buyer') {
        vm.actionLabel = 'Order Now'
      }
    }, function(e) {
      console.log({ error: e });
    });


    ApiService
      .offers
      .list()
      .then(function(res) {

        console.log(res.data);

        // ApiService.tickets.list().then(function(res) {
        //   console.log(res.data);
        // }, function(err) {
        //   console.log({ error: err });
        // }).catch(angular.noop);

        vm.offers = res.data;

        vm.promisedList = [];

        vm.offers.forEach(function(offerRef, index) {

          console.log('Offer: ', offerRef);

          var deferred = $q.defer();

          /**
           * offerLoaded
           *
           * @returns {undefined}
           */
          function offerLoaded() {

            ApiService
              .coins
              .list(offerRef._id)
              .then(function(res) {
                $timeout(function() {
                  offerRef.coins = res.data;
                  $scope.$apply();
                }, 4);
                console.log('OfferLoaded: ', res.data);
                deferred.resolve({
                  coins: res.data
                });
              });
          }

          offerLoaded();

          vm.promisedList.push(deferred.promise);
        });

        $q.all(vm.promisedList)
          .then(function(res) {
            console.log(res);
          });

    }, function(err) {

      console.log({ error: err });

    }).catch(angular.noop);

    // Watch for map versus home/coins controller call on filterOffersModal
    // which will invariably change these list containers.
    $scope.$watchGroup([
      'vm.offers'
    ], watchedMarkers)

    var __markers = []

    /**
     * @name watchedMarkers
     * @param newVal
     * @returns {undefined}
     */
    function watchedMarkers(newVal) {

      var markers = []
      var _markers = []

      $scope.map.markers.length = 0

      if (newVal && newVal[0] && newVal[0].length) {

        $scope.$emit('ready:data', { data: newVal[0] })

        NgMap
          .getMap("mapViewContainer")
          .then(mapViewContainerRender)

        /**
         * @function mapViewContainerRender
         * @param {Object} map Map object from Google Maps to be loaded with
         * initialized hotels and offer data list views.
         * @returns {undefined}
         * @inner
         */
        function mapViewContainerRender(map) {

          var listComplex = newVal[0]
          var group

          angular.forEach(__markers, function(marker) {
            try {
              marker.setMap(null)
            } catch(e) {
              console.log(e)
            }
          })

          listComplex = _.filter(listComplex, function(markerRef) {
            return (markerRef.lat !== 0 && markerRef.lon !== 0)
          });

          _.each(listComplex, function(offerRef, index) {

            var __name__

            try {
              __name__ = ('' + offerRef._id ? offerRef._id : offerRef.id)
            } catch(e) {
              __name__ = uuid4.generate()
            }

            if (!offerRef) {
              return
            }

            __markers[index] = new google.maps.Marker({
              title    : __name__,
              icon: {
                url        : './images/icons/iconPin@3x.png',
                scaledSize : new google.maps.Size(21, 31),
                origin     : new google.maps.Point(0, 0),
                anchor     : new google.maps.Point(10, 21)
              }
            })

            google.maps.event.addListener(__markers[index], 'click', function() {
              vm.viewOffer(offerRef)
            })

            var marker;
            try {
              marker = {
                lat : offerRef.geometry
                  ? offerRef.geometry.position.latitude
                  : offerRef.location.lat,
                lng : offerRef.geometry
                  ? offerRef.geometry.position.longitude
                  : offerRef.location.lon,
              }
            } catch (e) {
              console.log(e)
            }

            var latlng = new google.maps.LatLng(marker.lat, marker.lng)
            __markers[index].setPosition(latlng)
            __markers[index].setMap(map)

            $scope.map.markers.push(marker)
          })

          if (_.size($scope.map.markers)) {

            console.log('Centering on', $scope.map.markers)
            $scope.map.markers = _.filter($scope.map.markers, function(markerRef) {
              return (markerRef.lat !== 0 && markerRef.lon !== 0)
            });

            try {
              group = new google.maps.LatLngBounds()
              _.each($scope.map.markers, function(m) {
                var latlng = new google.maps.LatLng(m.lat, m.lng)
                group.extend(latlng)
              })
            } catch(e) {
              console.log(e)
            }

            try {
              map.setCenter(group.getCenter())
              map.fitBounds(group)
            } catch(e) {
              console.log(e)
            }
          }
        }
      }
    }

    //////////////

    // ## Controller Utilities

    /**
     * @function calendarModalInit
     * @param modal
     * @returns {undefined}
     * @inner
     */
    function calendarModalInit(modal) {
      vm.calendarModal = modal
      vm.calendarControl = {
        selectedDate : null,
        method       : null,
        eventSource  : ReservationService.getCalendarEvents(),
        onSelection  : onSelection,
        onViewTitleChanged: function(title) {
          vm.viewTitle = title
        }
      }

      /**
       * @function onSelection
       * @returns {undefined}
       * @inner
       */
      function onSelection() {
        var method = vm.calendarControl.method
        var val = vm.calendarControl.selectedDate

        if (
          vm.selectedFrom
          && (method === 'from' && moment(vm.selectedTo).isBefore(val))
        ) {
          return $scope.$broadcast('configError', {
            title: 'Invalid Date',
            subTitle: 'Invalid Date Selected'
          })
        }

        if (
          vm.selectedTo
          && (method === 'to' && moment(vm.selectedFrom).isAfter(val))
        ) {
          return $scope.$broadcast('configError', {
            title: 'Invalid Date',
            subTitle: 'Invalid Date Selected'
          })
        }

        if (moment(val).isBefore(now)) {
          return $scope.$broadcast('configError', {
            title: 'Invalid Date',
            subTitle: 'Invalid Date Selected'
          })
        }

        $rootScope.$broadcast('calendarSelection', val)
      }
    }
    /**
     * @ngdoc method
     * @name previous
     * @returns {undefined}
     */
    function previous() {
      $state.go('app.coins')
    }

    /**
     * @name showHelp
     * @param url
     * @returns {undefined}
     */
    function showHelp(url) {

      var ref
      var target
      var options = [
        'enableViewportScale=yes',
        'hidden=no',
        'location=yes',
        'toolbar=yes'
      ].join(',')

      target = "_blank"

      if (!$cordovaInAppBrowser) {

        ref = $window
        inAppBrowserRef = ref.open(url, target, options)
        inAppBrowserRef.addEventListener('loadstart', loadStartCallBack)
        inAppBrowserRef.addEventListener('loadstop', loadStopCallBack)
        inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack)
        inAppBrowserRef.addEventListener("exit", exitCallback)

      } else {

        ref = $cordovaInAppBrowser
        options = {
          hardwareback : 'yes',
          hidden       : 'no',
          location     : 'yes',
          toolbar      : 'yes'
        }
        inAppBrowserRef = ref.open(url, target, options).then(function(res) {
          console.log(res)
        }, function(e) {
          console.log(res)
        })
      }

      /**
       * exitCallback
       *
       * @returns {undefined}
       */
      function exitCallback() {
        inAppBrowserRef.close()

        inAppBrowserRef = undefined
      }

      /**
       * loadStartCallBack
       *
       * @returns {undefined}
       */
      function loadStartCallBack() {
      }

      /**
       * loadStopCallBack
       *
       * @returns {undefined}
       */
      function loadStopCallBack() {

        if (inAppBrowserRef !== undefined) {
          // inAppBrowserRef.insertCSS({ code: "body { font-size: 16px }" })
          inAppBrowserRef.show()
        }
      }

      /**
       * loadErrorCallBack
       *
       * @param params
       * @returns {undefined}
       */
      function loadErrorCallBack(params) {
        var scriptErrorMesssage = "console.log('" + params.message + "')"
        inAppBrowserRef.executeScript({
          code: scriptErrorMesssage
        }, executeScriptCallBack)
        inAppBrowserRef.close()
        inAppBrowserRef = undefined
      }

      /**
       * executeScriptCallBack
       *
       * @param params
       * @returns {undefined}
       */
      function executeScriptCallBack(params) {
        if (_.first(params) === null) {
          console.log(params)
        }
      }
    }

    /**
     * @name submitAction
     * @returns {undefined}
     */
    function submitAction() {

      vm.viewOfferModal.hide()

      var offer = {
        selectedOffer: vm.currentOffer
      }

      ReservationService.updateCurrentReservation(offer)

      $state.go('app.createReservation', {
        id: vm.currentOffer._id
      }, { notify: true })
    }

    /**
     * @name hideOfferModal
     * @returns {undefined}
     */
    function hideOfferModal() {
      vm.viewOfferModal.hide()
      $scope.$broadcast('offerModalClosed')
      $state.go('app.mapView', { id: null }, { notify: false })
    }

    /**
     * @function configMessage
     * @param $event
     * @param config
     * @returns {undefined}
     * @inner
     */
    function configMessage($event, config) {
      $ionicPopup.show({
        title    : config.title,
        subTitle : config.subTitle,
        buttons  : [
          {
            text  : 'OK',
            onTap : function(e) {
            }
          }
        ]
      })
    }

    /**
     * @ngdoc method
     * @name calendarSelection
     * @param e
     * @param val
     * @returns {undefined}
     */
    function calendarSelection(e, val) {

      if (!vm.selectedFrom && !vm.selectedFrom) {
        vm.currentFilters.calendar.dates['from'] = moment().format('MMM D, YYYY').toString();
        vm.currentFilters.calendar.dates['to'] = moment()
          .add(1, 'days')
          .format('MMM D, YYYY').toString();
      } else {
        if (!vm.selectedFrom) {
          vm.currentFilters.calendar.dates['from'] = moment().format('MMM D, YYYY').toString();
        }

        if (!vm.selectedTo) {
          vm.currentFilters.calendar.dates['to'] = moment(val).format('MMM D, YYYY').toString();
        }
      }

      val = moment(val).format('MMM D, YYYY');
      vm.currentFilters.calendar.dates[vm.calendarControl.method] = val.toString();
      vm.calendarModal.hide();

      vm.selectedFrom = moment(vm.currentFilters.calendar.dates.from).toString() || undefined;
      vm.selectedTo = moment(vm.currentFilters.calendar.dates.to).toString() || undefined;
    }

    /**
     * @ngdoc method
     * @name loadOfferView
     * @returns {undefined}
     */
    function loadOfferView() {

      var _id = $stateParams.id || null;

      if (
        _id &&
        $state.current.name.indexOf('app.coins') !== -1
      ) {

        MessageService
          .getOfferById(_id)
          .then(function(offer) {
            viewOffer(offer);
          }, function(e) {
            console.log(e);
          });
      }
    }

    /**
     * @ngdoc method
     * @name viewOffer
     * @param {object} offer A selected Offer.
     * @returns {*} undefined
     */
    function viewOffer(offer) {

      vm.coinTypeSelected = false;

      vm.viewOfferModal.show();

      if (!offer)
        return console.log({ error: '404' });

      if (!offer.profile) {
        offer.profile = {
          photos: []
        };
      }

      if (offer.coins)
        offer.coins = _.extend({}, offer.coins)

      vm.currentOffer = offer;

      var title = offer.title || undefined;
      var _profile = {};

      if (!title) {
        _profile.title = offer.name;
        _profile.subtitle = offer.name;
      }

      vm.currentProfile = title ? offer.profile : _profile;
      vm.mainPhoto = vm.currentProfile.photos;

      try {
        vm.currentProfile.photo = vm.mainPhoto.length ? vm.mainPhoto[0] : null;
      } catch(e) {
        console.log('Unable to load photo data: ', e);
      }

      try {

        vm.offers = vm.currentOffer.coins ?
          vm.currentOffer.coins.erc20.concat(vm.currentOffer.coins.fresh) :
          [];

        vm.offersForDateRangeDisplay = _.size(vm.offers) ? [
          _.last(vm.offers)
        ] : [];

      } catch(e) {
        console.log('Unable to load offers data: ', e);
      }

      console.log('current offer: ', vm.currentOffer);
      console.log('current profile: ', vm.currentProfile);

      vm.cachedYear = undefined;
      vm.cachedInitYear = undefined;

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {
          var initYear = moment(coin.availability.from).format('YYYY').toString();
          var year = moment(coin.availability.to).format('YYYY').toString();
          vm.cachedYear = (!vm.cachedYear || vm.cachedYear > parseInt(year, 10))
            ? parseInt(year, 10)
            : vm.cachedYear;
          vm.cachedInitYear = (!vm.cachedInitYear || vm.cachedInitYear < parseInt(initYear, 10))
            ? parseInt(initYear, 10)
            : vm.cachedInitYear;
        });

      vm.cachedDate = {};

      vm.offers
        && vm.offers.length
        && _.each(vm.offers, function(coin) {

          var to = moment(coin.availability.to).format('MMM DD').toString();
          var from = moment(coin.availability.from).format('MMM DD').toString();

          vm.cachedDate.first = !vm.cachedDate.first
            || moment(coin.availability.from).isBefore(vm.cachedDate.first)
              ? from
              : vm.cachedDate.first;
          vm.cachedDate.last = !vm.cachedDate.last
            || moment(coin.availability.to).isBefore(vm.cachedDate.last)
              ? to
              : vm.cachedDate.last;
        });

      $ionicScrollDelegate.$getByHandle('modalContent').scrollTop(true);

      $scope.$broadcast('offerModalOpened', vm.currentOffer);

      $state.go('app.coins', { id: offer._id }, { notify: false });

      MissiveService.resetActiveObj('offer');

      ReservationService.removeCurrentReservation();
    }

    /**
     * @ngdoc method
     * @name selectFreshCoin
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectSha256Coin(i) {

      if (vm.currentOffer.coins && vm.currentOffer.coins.sha256) {

        _.each(vm.currentOffer.coins.sha256, function(coin) {
          coin.selected = false;
        });

        vm.currentOffer.coins.sha256[i].selected = !vm.currentOffer.coins.sha256[i].selected;

        try {
          vm.currentOffer.coins.sha256[i].selected = vm.currentOffer.coins.sha256[i].selected;
        } catch(e) {
          console.log(e);
        }

        vm.coinTypeSelected = true;
      }
    }

    /**
     * @ngdoc method
     * @name selectErc20Coin
     * @param {number} i Position index.
     * @returns {*} undefined
     */
    function selectScryptCoin(i) {

      if (vm.currentOffer.coins && vm.currentOffer.coins.scrypt) {

        _.each(vm.currentOffer.coins.scrypt, function(coin) {
          coin.selected = false;
        });

        vm.currentOffer.coins.scrypt[i].selected = !vm.currentOffer.coins.scrypt[i].selected;

        try {
          vm.currentOffer.coins.scrypt[i].selected = vm.currentOffer.coins.scrypt[i].selected;
        } catch(err) {
          console.log({ error: err });
        }

        vm.coinTypeSelected = true;
      }
    }
  }
})();

