// ./app/unit/directives.js
'use strict';

describe('directive', function() {
  beforeEach(module('WalletCompanion'));
  it('should set a step', inject(function($compile, $rootScope) {
    var $scope = $rootScope.$new();
    var $element = angular.element("<tgm-progress-bar></tgm-progress-bar>");
    $compile($element)($scope);
    expect($scope.dm).toBeUndefined();
  }));
});
