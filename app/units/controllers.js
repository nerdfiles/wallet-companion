// ./app/unit/controllers.js
'use strict';

describe('controller', function() {
  beforeEach(module('WalletCompanion'));

  var userService = {
    currentUser: {},
    getCurrentUser: function() {
      return this.currentUser;
    }
  };

  it('should return user1', inject(function($rootScope, $controller) {
    var $scope = $rootScope.$new();
    var $ctrl = $controller("MainController", {
      $scope: $scope,
      userService: userService
    });
    expect($scope.userRole).toBeUndefined();
  }));
});
