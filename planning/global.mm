
<map version="0.9.0">
    <node ID="ID_841F3C53-4D82-423E-9FDB-5996F184A393" TEXT="BPC">
        <node ID="ID_4E77319B-158B-4C8E-B375-643A85125E29" TEXT="Splash">
            <node ID="ID_637088BB-34D2-45C0-9BA2-DF5BC45848A8" TEXT="Welcome">
                <node ID="ID_C1CB219C-A3BD-4D83-906A-731D319B7C11" TEXT="Create">
                    <node ID="ID_C5AA81B8-6B04-4A1D-A692-4C6AD78B7D77" TEXT="Create Bed Owner">
                        <node ID="ID_EB9FB162-2DF0-4AD0-A72E-D21698C65C59" TEXT="Register Bed Owner"/>
                    </node>
                    <node ID="ID_C8BA21A5-E9C7-4D53-8B68-9DAD92469FF6" TEXT="Create Traveller">
                        <node ID="ID_1FF018D1-C681-4A09-829A-1F551E314739" TEXT="Register Traveller">
                            <node ID="ID_A4E558F6-964C-4C41-BB3D-032FD9B810C2" TEXT="Add Payment Method">
                                <node ID="ID_12E9A219-247A-4015-8E47-B719968CD1A1" TEXT="Add Credit Card">
                                    <icon BUILTIN="desktop_new"/>
                                    <node ID="ID_722CA6A1-E765-4106-BE8A-9451B36E2ADC" TEXT="Billing Info">
                                        <node ID="ID_E3358972-ECD8-4EA3-B98E-098C776EC27B" TEXT="CC Info">
                                            <node ID="ID_CCBB7EFC-F152-4BC4-A4D4-77A9452AE49C" TEXT="Bookings"/>
                                        </node>
                                    </node>
                                </node>
                                <node ID="ID_6E1B78AF-B7DF-45AB-A4CE-9D8865D0D2CC" TEXT="PayPal"/>
                            </node>
                        </node>
                    </node>
                </node>
                <node ID="ID_2B6A5590-268D-43CB-98E5-DFDFC844B1E1" TEXT="Log In">
                    <node ID="ID_DA4B1776-9F87-4248-A20D-18823049B6A9" TEXT="Traveller — Beds">
                        <node ID="ID_18A2D044-3A6C-4F3F-82AD-EA5F937A5902" TEXT="Beds Filter">
                            <icon BUILTIN="xmag"/>
                            <node ID="ID_59443408-5049-4E1C-BB7D-503FC8063FE4" TEXT="Date Picker">
                                <icon BUILTIN="xmag"/>
                            </node>
                        </node>
                        <node ID="ID_01010DC2-5D27-43CF-922A-274584EF8991" TEXT="View Bed">
                            <node ID="ID_EBC3290B-98A2-4E31-93F9-6F36927D894F" TEXT="Book Bed">
                                <node ID="ID_0FEFA5BF-8A83-4B89-9352-A0F6AE3BD0DE" TEXT="Book Bed Summary">
                                    <node ID="ID_38C410E6-347D-45AB-BBA4-6B73DFCE3E8E" TEXT="Book Bed Confirmation">
                                        <node ID="ID_69590314-79AB-47A6-879F-020F089AEFEE" TEXT="Book Bed Cancel">
                                            <icon BUILTIN="clanbomber"/>
                                        </node>
                                        <node ID="ID_E4265CAB-38A5-4832-8041-10AB6DBEBE2A" TEXT="Book Bed Payment Select">
                                            <icon BUILTIN="attach"/>
                                        </node>
                                        <node ID="ID_ABD1825C-8A89-4D64-A8CB-C3D04FE7E97F" TEXT="Book Bed Confirmation Payment">
                                            <node ID="ID_F358E133-57A4-4558-800C-E995D13C803F" TEXT="Book Bed Complete">
                                                <node ID="ID_644FFA2B-9638-4A45-9DAD-914D4222146E" TEXT="Bookings Listing"/>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node ID="ID_6A3D1226-66D4-430E-B3C7-0E976AC80266" TEXT="Traveller — Bookings Listing">
                        <node ID="ID_E78183A3-34AB-4D01-9238-C71856E83B0D" TEXT="Booking — Detail View">
                            <node ID="ID_E92E9714-337F-4909-9951-CDC3F41EAA38" TEXT="Pending">
                                <icon BUILTIN="clock"/>
                            </node>
                            <node ID="ID_A00B49BE-7A1E-4E15-9219-73A5BA2C5B9D" TEXT="Completed">
                                <icon BUILTIN="button_ok"/>
                            </node>
                        </node>
                    </node>
                    <node ID="ID_33BA7E22-3C8E-42B9-A7E3-B9E0DDAF6479" TEXT="Bed Owner — Properties">
                        <node ID="ID_E39E9B20-C7D4-46E7-94EC-43FF32C8B457" TEXT="Listing Welcome">
                            <node ID="ID_96EA4857-A694-45E3-AED5-1C2899ABF8F0" TEXT="List Residence Type">
                                <node ID="ID_6782410F-54DB-427F-AC55-64953A42BB40" TEXT="Listing Location">
                                    <node ID="ID_CB1BFF41-1AE4-40E2-9227-6CA3B97AEF7E" TEXT="Location Map">
                                        <node ID="ID_48BF52C8-491D-4369-8A06-52CA662E6592" TEXT="Location Photos">
                                            <node ID="ID_B6CF7F6B-A045-4252-817B-FF34098F76E6" TEXT="Location Photos Confirm">
                                                <node ID="ID_CB5D0DDA-A3C0-4734-AEA5-2A4E5A8DE5AF" TEXT="Location Listing Help"/>
                                                <node ID="ID_01011B29-63F9-4D0B-A8E2-B5D6F3268121" TEXT="Location Listing Description">
                                                    <node ID="ID_985CE672-8E1D-4670-88A0-1CBB6D73C58E" TEXT="Location Amenities">
                                                        <node ID="ID_322D9709-03CF-413C-9CF9-D15300D8A126" TEXT="Bed Management (Delete)"/>
                                                        <node ID="ID_9E3E67EC-8FF0-4FD5-9DE4-30A90FECFB78" TEXT="Bed Management">
                                                            <node ID="ID_F06C4E80-094B-4F56-89EF-1B6E64500974" TEXT="Add Bed">
                                                                <node ID="ID_DBE059B5-79B0-4801-8E14-30913AC99DEB" TEXT="Bed Availability">
                                                                    <node ID="ID_F384D07F-5389-4AD8-9FD4-C5224D2D9855" TEXT="Listing Edit">
                                                                        <node ID="ID_C9F413C2-C5D5-46BB-94D3-67FC9619009C" TEXT="Listing Preview"/>
                                                                    </node>
                                                                </node>
                                                            </node>
                                                        </node>
                                                    </node>
                                                </node>
                                            </node>
                                        </node>
                                    </node>
                                </node>
                            </node>
                        </node>
                    </node>
                    <node ID="ID_88B44D1F-C125-4748-96C5-C6614C8E8712" TEXT="[Any User] Profile">
                        <node ID="ID_74BB5120-E1DF-4FD9-97AE-135E5AD49121" TEXT="Settings">
                            <icon BUILTIN="help"/>
                            <node ID="ID_8129F297-7B83-4D6B-B4C1-D0233F692BE3" TEXT="Payment Methods">
                                <icon BUILTIN="forward"/>
                                <node ID="ID_5A77D6AB-1006-4527-A652-AFEF77DBC7D3" TEXT="Payment Cards">
                                    <icon BUILTIN="folder"/>
                                    <node ID="ID_EDB70750-58B1-428F-96AE-8FDD46BC75F3" TEXT="View Card">
                                        <icon BUILTIN="password"/>
                                        <node ID="ID_680109FC-BD22-4750-B53F-DCF944185DEE" TEXT="Edit Card">
                                            <icon BUILTIN="edit"/>
                                        </node>
                                    </node>
                                </node>
                                <node ID="ID_C5DAE909-2C83-4CFC-9A11-CFBDAA45AA38" TEXT="Add Card">
                                    <icon BUILTIN="desktop_new"/>
                                    <node ID="ID_807B6F66-A69E-46B6-88AC-5CD18D3C9973" TEXT="Add Billing Info">
                                        <icon BUILTIN="password"/>
                                        <node ID="ID_4FC818A7-A22D-443D-8C7F-9A2585028B14" TEXT="CC Info">
                                            <icon BUILTIN="password"/>
                                        </node>
                                    </node>
                                </node>
                            </node>
                            <node ID="ID_B688EC09-84B0-4A4F-B18A-BB4B6410D95F" TEXT="Version">
                                <icon BUILTIN="info"/>
                            </node>
                            <node ID="ID_2634687C-8DFA-40EA-BBDF-198F3BAFE6FC" TEXT="Terms of Service">
                                <icon BUILTIN="info"/>
                            </node>
                            <node ID="ID_FB9407B8-7B26-432C-92CA-F651D57D4D9C" TEXT="Log Out"/>
                        </node>
                    </node>
                </node>
                <node ID="ID_15323123-60E1-4A46-823A-AA66CD0C727D" TEXT="Forgot Password">
                    <node ID="ID_A2B5B642-D68F-4270-8BBB-44CCE6C09D39" TEXT="Reset Successful"/>
                </node>
            </node>
        </node>
        <node ID="ID_E10EA0E5-5654-4EA7-9A7C-C585C4D081B0" TEXT="Dashboard">
            <node ID="ID_C2BBE525-AB5B-4190-84C3-0ECFAD2A9995" TEXT="Log In">
                <node ID="ID_517E7F96-45DA-48D7-AFEE-B29D67E90AA5" TEXT="Property Listing">
                    <node ID="ID_A2C162A7-4BD3-4656-8B2C-050929ACCBF4" TEXT="Delete Property Listing"/>
                </node>
                <node ID="ID_C4BAC08F-9FAB-47E4-9FE8-323A191C8AD8" TEXT="[No Active] Bookings List"/>
                <node ID="ID_0169E782-209D-48E7-BE76-3AE15F8162AC" TEXT="Bookings List">
                    <node ID="ID_8E91CF6D-F04B-4A70-834C-ABDB9B300B40" TEXT="Pending Booking">
                        <node ID="ID_39ABD576-DA4A-4D26-86B3-0576A083AFC5" TEXT="Cancel Booking"/>
                    </node>
                    <node ID="ID_9DDF13EE-7D77-4800-A0C8-2F3BD3884EE1" TEXT="Completed Booking"/>
                </node>
                <node ID="ID_9663189E-8BE6-4AD2-A58E-21AFB9D4D4A5" TEXT="Bookings Verification Pending"/>
                <node ID="ID_99B70C2F-1502-4E8C-B536-DBE7E98041CA" TEXT="Experiences"/>
                <node ID="ID_DBB80F46-5E5D-4E1F-B7FC-DFF1057E37C6" TEXT="Home"/>
                <node ID="ID_9F62DB59-B21F-45C1-A204-269A82D2304C" TEXT="Profile">
                    <node ID="ID_178306D7-DBD3-42E1-B4E1-8F51002CA8C2" TEXT="Settings">
                        <node ID="ID_75D9A52E-D115-4210-B361-838D5CFF1E03" TEXT="Support">
                            <node ID="ID_11981A06-5D34-4815-9F84-D91A9AF89B14" TEXT="Thanks"/>
                        </node>
                    </node>
                </node>
            </node>
        </node>
    </node>
</map>